<?php

//InAdhesives
$router->map('POST', '/getAllInAdhesives', function() {
    $main = new DataTablesModel_PO();
    echo $main->getAllInAdhesives();
});
$router->map('POST', '/getAllInAdhesivesLogs', function() {
    $main = new DataTablesModel_PO();
    echo $main->getAllInAdhesivesLogs();
});

$router->map('GET', '/loadInAdhesive', function() {
	$id=$_GET["id"];
	$details=prepareTable("SELECT
`name`,
`type`,
`minimum`,
`starting_qty`,
`price`
FROM in_adhesives 
WHERE `id`=?",array($id));

	echo json_encode(array($details));
});


$router->map('POST', '/saveInAdhesiveLogs', function() {
    $id = $_POST["id"];
    $uid = $_POST["uid"];
    $arr = $_POST["arr"];
    $type = $_POST["type"];

    $main = new MainModel();
    $field_array = array();
    $check_array = array();
    $field_array[] = array("table_name" => "in_adhesive_has_logs", "columns" => array("`id_adhesive`","`qty`","`initial_qty`","`remaining_qty`",
		array("col_name" => "`issued_by`", "values" => array($uid)),
		array("col_name" => "`type`", "values" => array($type))
	));
    echo $main->addData($arr, $field_array, "contact_types", $check_array);
});


//InPlastics
$router->map('POST', '/getAllInPlastics', function() {
    $main = new DataTablesModel_PO();
    echo $main->getAllInPlastics();
});
$router->map('POST', '/getAllInPlasticsLogs', function() {
    $main = new DataTablesModel_PO();
    echo $main->getAllInPlasticsLogs();
});

$router->map('GET', '/loadInPlastic', function() {
	$id=$_GET["id"];
	$details=prepareTable("SELECT
`name`,
`type`,
`minimum`,
`starting_qty`,
`price`
FROM in_plastics 
WHERE `id`=?",array($id));

	echo json_encode(array($details));
});


$router->map('POST', '/saveInPlasticLogs', function() {
    $id = $_POST["id"];
    $uid = $_POST["uid"];
    $arr = $_POST["arr"];
    $type = $_POST["type"];

    $main = new MainModel();
    $field_array = array();
    $check_array = array();
    $field_array[] = array("table_name" => "in_plastic_has_logs", "columns" => array("`id_plastic`","`qty`","`initial_qty`","`remaining_qty`",
		array("col_name" => "`issued_by`", "values" => array($uid)),
		array("col_name" => "`type`", "values" => array($type))
	));
    echo $main->addData($arr, $field_array, "contact_types", $check_array);
});


//InInks
$router->map('POST', '/getAllInInks', function() {
    $main = new DataTablesModel_PO();
    echo $main->getAllInInks();
});
$router->map('POST', '/getAllInInksLogs', function() {
    $main = new DataTablesModel_PO();
    echo $main->getAllInInksLogs();
});

$router->map('GET', '/loadInInk', function() {
	$id=$_GET["id"];
	$details=prepareTable("SELECT
`name`,
`type`,
`minimum`,
`starting_qty`,
`price`
FROM in_inks 
WHERE `id`=?",array($id));

	echo json_encode(array($details));
});


$router->map('POST', '/saveInInkLogs', function() {
    $id = $_POST["id"];
    $uid = $_POST["uid"];
    $arr = $_POST["arr"];
    $type = $_POST["type"];

    $main = new MainModel();
    $field_array = array();
    $check_array = array();
    $field_array[] = array("table_name" => "in_ink_has_logs", "columns" => array("`id_ink`","`qty`","`initial_qty`","`remaining_qty`",
		array("col_name" => "`issued_by`", "values" => array($uid)),
		array("col_name" => "`type`", "values" => array($type))
	));
    echo $main->addData($arr, $field_array, "contact_types", $check_array);
});


//InCylinders
$router->map('POST', '/getAllInCylinders', function() {
    $main = new DataTablesModel_PO();
    echo $main->getAllInCylinders();
});
$router->map('POST', '/getAllInCylindersLogs', function() {
    $main = new DataTablesModel_PO();
    echo $main->getAllInCylindersLogs();
});

$router->map('GET', '/loadInCylinder', function() {
	$id=$_GET["id"];
	$details=prepareTable("SELECT
`name`,
`type`,
`minimum`,
`starting_qty`,
`price`
FROM in_cylinders 
WHERE `id`=?",array($id));

	echo json_encode(array($details));
});


$router->map('POST', '/saveInCylinderLogs', function() {
    $id = $_POST["id"];
    $uid = $_POST["uid"];
    $arr = $_POST["arr"];
    $type = $_POST["type"];

    $main = new MainModel();
    $field_array = array();
    $check_array = array();
    $field_array[] = array("table_name" => "in_cylinder_has_logs", "columns" => array("`id_cylinder`","`qty`","`initial_qty`","`remaining_qty`",
		array("col_name" => "`issued_by`", "values" => array($uid)),
		array("col_name" => "`type`", "values" => array($type))
	));
    echo $main->addData($arr, $field_array, "contact_types", $check_array);
});

//InChemicals
$router->map('POST', '/getAllInChemicals', function() {
    $main = new DataTablesModel_PO();
    echo $main->getAllInChemicals();
});
$router->map('POST', '/getAllInChemicalsLogs', function() {
    $main = new DataTablesModel_PO();
    echo $main->getAllInChemicalsLogs();
});

$router->map('GET', '/loadInChemical', function() {
	$id=$_GET["id"];
	$details=prepareTable("SELECT
`name`,
`type`,
`minimum`,
`starting_qty`,
`price`
FROM in_chemicals 
WHERE `id`=?",array($id));

	echo json_encode(array($details));
});


$router->map('POST', '/saveInChemicalLogs', function() {
    $id = $_POST["id"];
    $uid = $_POST["uid"];
    $arr = $_POST["arr"];
    $type = $_POST["type"];

    $main = new MainModel();
    $field_array = array();
    $check_array = array();
    $field_array[] = array("table_name" => "in_chemical_has_logs", "columns" => array("`id_chemical`","`qty`","`initial_qty`","`remaining_qty`",
		array("col_name" => "`issued_by`", "values" => array($uid)),
		array("col_name" => "`type`", "values" => array($type))
	));
    echo $main->addData($arr, $field_array, "contact_types", $check_array);
});
