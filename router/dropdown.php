<?php

$router->map('GET', '/getDropdownDepartments', function() {
	$data = prepareTable("select `id`,`name` from `departments` ORDER BY `name`", array());
	echo json_encode($data);
});
$router->map('GET', '/getDropdownCustomers', function() {
	$data = prepareTable("select `id`,`name` from `customers` ORDER BY `name`", array());
	echo json_encode($data);
});
$router->map('GET', '/getDropdownCurrency', function() {
	$data = prepareTable("select `id`,concat(`name`,' (',`symbol`,')') 'name' from `currencys` ORDER BY `name`", array());
	echo json_encode($data);
});
$router->map('GET', '/getDropdownPlastic', function() {
	$data = prepareTable("select `id`,`specification` 'name' from `plastics` ORDER BY `name`", array());
	echo json_encode($data);
});
$router->map('GET', '/getDropdownPlasticsPO', function() {
	$data = prepareTable("select B.`id`,B.`specification` 'name',A.`qty` from `in_plastics` A LEFT JOIN `plastics` B ON A.`id_plastic`=B.`id` ORDER BY `name`", array());
	echo json_encode($data);
});
$router->map('GET', '/getDropdownInks', function() {
	$data = prepareTable("select `id`,`name` from `inks` ORDER BY `name`", array());
	echo json_encode($data);
});
$router->map('GET', '/getDropdownInksPO', function() {
	$data = prepareTable("select B.`id`,B.`name`,A.`qty` from `in_inks` A LEFT JOIN `inks` B ON A.`id_ink`=B.`id` ORDER BY `name`", array());
	echo json_encode($data);
});
$router->map('GET', '/getDropdownAdhesives', function() {
	$data = prepareTable("select `id`,`name` from `adhesives` ORDER BY `name`", array());
	echo json_encode($data);
});
$router->map('GET', '/getDropdownAdhesivesPO', function() {
	$data = prepareTable("select B.`id`,B.`name`,A.`qty` from `in_adhesives` A LEFT JOIN `adhesives` B ON A.`id_adhesive`=B.`id` ORDER BY `name`", array());
	echo json_encode($data);
});
$router->map('GET', '/getDropdownChemicals', function() {
	$data = prepareTable("select `id`,`name` from `chemicals` ORDER BY `name`", array());
	echo json_encode($data);
});
$router->map('GET', '/getDropdownChemicalsPO', function() {
	$data = prepareTable("select B.`id`,B.`name`,A.`qty` from `in_chemicals` A LEFT JOIN `chemicals` B ON A.`id_chemical`=B.`id` ORDER BY `name`", array());
	echo json_encode($data);
});
$router->map('GET', '/getDropdownCylinders', function() {
	$data = prepareTable("select `id`,CONCAT(`color`,' (',`size`,')')'name' from `cylinders` ORDER BY `name`", array());
	echo json_encode($data);
});
$router->map('GET', '/getDropdownCylindersPO', function() {
	$data = prepareTable("select B.`id`,B.`color` 'name',A.`qty` from `in_cylinders` A LEFT JOIN `cylinders` B ON A.`id_cylinder`=B.`id` ORDER BY `name`", array());
	echo json_encode($data);
});
$router->map('GET', '/getDropdownProducts', function() {
	$data = prepareTable("select `id`,`name` from `products` ORDER BY `name`", array());
	echo json_encode($data);
});
$router->map('GET', '/getDropdownProductsPerCustomer', function() {
	$cust = "";
	if($_GET['cust']!=0){
		$cust = "WHERE `id_customer`={$_GET['cust']}";
	}
	$data = prepareTable("select `id`,`name` from `products` {$cust} ORDER BY `name`", array());
	echo json_encode($data);
});


?>

