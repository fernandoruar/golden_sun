<?php

$router->map('POST', '/checkAccount', function() {
    $main = new MainModel();
    echo $main->checkAccount();
});
$router->map('GET', '/logout', function() {
    unset($_SESSION[getSessionName()]);
    header("Location:/login");
});
$router->map('POST', '/changeLanguage', function() {
    $id = $_POST["id"];
    setcookie("language", "", time() - 3600);
    setcookie("language", $id, time() + 31556926);
    echo json_encode(1);
});



$router->map('GET', '/', function() {
        include './lib/language.php';
        $module_name = $dashboard;
    $tablename = "blank";
    $head = "header2";
    include linkPage("templateload");
});


$router->map('GET', '/login', function() {
        include './lib/language.php';
        $module_name = $login;
    if(isset($_SESSION[getSessionName()])) {
        header('Location: /');
    } else {
        $tablename = "login";
        $head = "login_header";
        include linkPage("templateload");
    }
});

$router->map('GET', '/contact_types', function() {
        include './lib/language.php';
        $module_name = $contact_types;
        $tablename = "contact_types";
        $head = "header2";
        include linkPage("templateload");
});

$router->map('GET', '/departments', function() {
        include './lib/language.php';
        $module_name = $departments;
        $tablename = "departments";
        $head = "header2";
        include linkPage("templateload");
});
$router->map('GET', '/currency', function() {
        include './lib/language.php';
        $module_name = $currency;
        $tablename = "currency";
        $head = "header2";
        include linkPage("templateload");
});


$router->map('GET', '/employees', function() {
    include "./lib/language.php";
    $module_name = $employees;
    $tablename = "employees";
    $head = "header2";
    include linkPage("templateload");
});

$router->map('GET', '/customers', function() {
    include "./lib/language.php";
    $module_name = $customers;
    $tablename = "customers";
    $head = "header2";
    include linkPage("templateload");
});
$router->map('GET', '/suppliers', function() {
    include "./lib/language.php";
    $module_name = $suppliers;
    $tablename = "suppliers";
    $head = "header2";
    include linkPage("templateload");
});


///prods
$router->map('GET', '/plastics', function() {
    include "./lib/language.php";
    $module_name = $plastics;
    $tablename = "plastics";
    $head = "header2";
    include linkPage("templateload");
});

$router->map('GET', '/adhesives', function() {
    include "./lib/language.php";
    $module_name = $adhesives;
    $tablename = "adhesives";
    $head = "header2";
    include linkPage("templateload");
});

$router->map('GET', '/chemicals', function() {
    include "./lib/language.php";
    $module_name = $chemicals;
    $tablename = "chemicals";
    $head = "header2";
    include linkPage("templateload");
});

$router->map('GET', '/inks', function() {
    include "./lib/language.php";
    $module_name = $inks;
    $tablename = "inks";
    $head = "header2";
    include linkPage("templateload");
});

$router->map('GET', '/cylinders', function() {
    include "./lib/language.php";
    $module_name = $cylinders;
    $tablename = "cylinders";
    $head = "header2";
    include linkPage("templateload");
});

$router->map('GET', '/products', function() {
    include "./lib/language.php";
    $module_name = $products;
    $tablename = "products";
    $head = "header2";
    include linkPage("templateload");
});


///Job Order
$router->map('GET', '/customer_po', function() {
    include "./lib/language.php";
    $module_name = $customer_po;
    $tablename = "customer_po";
    $head = "header2";
    include linkPage("templateload");
});
$router->map('GET', '/job_orders', function() {
    include "./lib/language.php";
    $module_name = $job_orders;
    $tablename = "job_orders";
    $head = "header2";
    include linkPage("templateload");
});

$router->map('GET', '/create_job_order', function() {
    include "./lib/language.php";
        $module_name = $create_job_orders;
    $tablename = "create_job_order";
    $head = "header";
    include linkPage("templateload");
});



///Inventory
$router->map('GET', '/plastics_po', function() {
    include "./lib/language.php";
    $module_name = $plastics;
    $tablename = "plastics_po";
    $head = "header2";
    include linkPage("templateload");
});

$router->map('GET', '/adhesives_po', function() {
    include "./lib/language.php";
    $module_name = $adhesives;
    $tablename = "adhesives_po";
    $head = "header2";
    include linkPage("templateload");
});

$router->map('GET', '/chemicals_po', function() {
    include "./lib/language.php";
    $module_name = $chemicals;
    $tablename = "chemicals_po";
    $head = "header2";
    include linkPage("templateload");
});

$router->map('GET', '/inks_po', function() {
    include "./lib/language.php";
    $module_name = $inks;
    $tablename = "inks_po";
    $head = "header2";
    include linkPage("templateload");
});

$router->map('GET', '/cylinders_po', function() {
    include "./lib/language.php";
    $module_name = $cylinders;
    $tablename = "cylinders_po";
    $head = "header2";
    include linkPage("templateload");
});









/***Back End ***/
$router->map('GET', '/costing', function() {
    include "./lib/language.php";
        $module_name = "";
    $tablename = "costing";
    $head = "header";
    include linkPage("templateload");
});
$router->map('GET', '/print_job_order', function() {
    include "./lib/language.php";
        $module_name = $print_job_orders;
    $tablename = "print_job_order";
    $head = "print_header";
    include linkPage("templateload");
});
$router->map('GET', '/print_delivery_receipt', function() {
    include "./lib/language.php";
        $module_name = "";
    $tablename = "print_delivery_receipt";
    $head = "print_header";
    include linkPage("templateload");
});
$router->map('GET', '/print_costing', function() {
    include "./lib/language.php";
        $module_name = "";
    $tablename = "print_costing";
    $head = "print_header";
    include linkPage("templateload");
});













//DELETE
$router->map('POST', '/deleteData', function() {
    $mode = 0;
    $id = 0;
    
    if (isset($_POST["mode"])) {
        $mode = $_POST["mode"];
    }
    if (isset($_POST["id"])) {
        $id = $_POST["id"];
    }
    
    $table_arr = [
        array("table_name" => "contact_types", "has_sub"=>[["contact_type_has_logs","id_contact_type"]]),//0
        array("table_name" => "employees", "has_sub"=>[["employee_has_logs","id_employee"]]),//1
        array("table_name" => "departments", "has_sub"=>[["department_has_logs","id_department"]]),//2
        array("table_name" => "currencys", "has_sub"=>[["currency_has_logs","id_currency"]]),//3
        array("table_name" => "customers", "has_sub"=>[["customer_has_logs","id_customer"]]),//4
        array("table_name" => "suppliers", "has_sub"=>[["supplier_has_logs","id_supplier"]]),//5
        array("table_name" => "adhesives", "has_sub"=>[["adhesive_has_logs","id_adhesive"]]),//6
        array("table_name" => "chemicals", "has_sub"=>[["chemical_has_logs","id_chemical"]]),//7
        array("table_name" => "inks", "has_sub"=>[["ink_has_logs","id_ink"]]),//8
        array("table_name" => "plastics", "has_sub"=>[["plastic_has_logs","id_plastic"]]),//9
        array("table_name" => "cylinders", "has_sub"=>[["cylinder_has_logs","id_cylinder"]]),//10
        array("table_name" => "products", "has_sub"=>[["product_has_logs","id_product"]]),//11
        array("table_name" => "customerpos", "has_sub"=>[["customerpo_has_logs","id_customerpo"]]),//12
        array("table_name" => "joborders", "has_sub"=>[["joborder_has_logs","id_joborder"]]),//13
    ];
    $table_name = "";
        if (is_array($table_arr[$mode])) {
            $table_name = $table_arr[$mode]["table_name"];
        }
        
    $queries = array();
    $arrays = array();

    $check_arr = prepareTable("select * from `{$table_name}` where `id`=?", array($id));
    if (count($check_arr) > 0) {
        if ($check_arr[0]['conflicts'] == 0) {
			if(isset($table_arr[$mode]["has_sub"])){
				foreach($table_arr[$mode]["has_sub"] as $subs){
					$table_sub = $subs[0];
					$sub_id = $subs[1];
					//var_dump($subs);
					$queries[] = "delete from `{$table_sub}` where `{$sub_id}`=?";
					$arrays[] = array($id);
				}
			}
			
			
			$queries[] = "delete from `{$table_name}` where `id`=?";
			$arrays[] = array($id);

			echo json_encode(transactStatement($queries, $arrays));
		} else {
			echo json_encode(-1);
		}
    } else {
        echo json_encode(0);
    }
});
?>

