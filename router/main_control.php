<?php


//Contact Types 
$router->map('GET', '/getAllContact_types', function() {

    $data = new DataTablesModel();
    echo $data->getAllContact_types();
});
$router->map('GET', '/loadContact_type', function() {
    $id = $_GET["id"];
    $details = prepareTable("select 
		`name` 
		from `contact_types` where `id`=?", 
	array($id));
    echo json_encode(array($details));
});
$router->map('POST', '/saveContact_type', function() {
    $id = $_POST["id"];
    $uid = $_POST["uid"];
    $deleted_fields = array();
    if (isset($_POST["deleted_fields"])) {
        $deleted_fields = $_POST["deleted_fields"];
    }
    $arr = $_POST["arr"];
    $mode = $_POST["mode"];
    $main = new MainModel();
    $field_array = array();
    $check_array = array();
        if ($mode == 0) {
            $field_array[] = array("table_name" => "contact_types", "columns" => array("`name`", 
				array("col_name" => "`issued_by`", "values" => array($uid)),
			));
            $check_array[] = array("index" => 0, "col_index" => [0]);
            echo $main->addData($arr, $field_array, "contact_types", $check_array);
        } else if ($mode == 1) {
            $field_array[] = array("table_name" => "contact_types", "where" => array("fields" => array("`id`"), "values" => array($id)), "columns" => array("`name`", 
				  array("col_name" => "`modified_by`", "values" => array($uid)),
				  array("col_name" => "`modified_on`", "query" => 'NOW(6)')
			));
            $check_array[] = array("index" => 0, "col_index" => [0], "col_name" => "`id`");
            $tables = array();
            echo $main->editData($arr, $field_array, $deleted_fields, $tables, $check_array, $id);
        } else {
            echo json_encode(0);
        }
});
$router->map('POST', '/saveContact_typeMultiple', function() {
    $id = $_POST["id"];
    $uid = $_POST["uid"];
    $arr = $_POST["arr"];

    $main = new MainModel();
    $field_array = array();
    $check_array = array();
    $field_array[] = array("table_name" => "contact_types", "columns" => array("`name`", 
		array("col_name" => "`issued_by`", "values" => array($uid))
	));
    $check_array[] = array("index" => 0, "col_index" => [0]);
    echo $main->addData($arr, $field_array, "contact_types", $check_array);
});


//Departments 
$router->map('GET', '/getAllDepartments', function() {

    $data = new DataTablesModel();
    echo $data->getAllDepartments();
});
$router->map('GET', '/loadDepartment', function() {
    $id = $_GET["id"];
    $details = prepareTable("select 
		`name` 
		from `departments` where `id`=?", 
	array($id));
    echo json_encode(array($details));
});

$router->map('POST', '/saveDepartment', function() {
    $id = $_POST["id"];
    $uid = $_POST["uid"];
    $deleted_fields = array();
    if (isset($_POST["deleted_fields"])) {
        $deleted_fields = $_POST["deleted_fields"];
    }
    $arr = $_POST["arr"];
    $mode = $_POST["mode"];
    $main = new MainModel();
    $field_array = array();
    $check_array = array();
        if ($mode == 0) {
            $field_array[] = array("table_name" => "departments", "columns" => array("`name`", 
				array("col_name" => "`issued_by`", "values" => array($uid)),
			));
            $check_array[] = array("index" => 0, "col_index" => [0]);
            echo $main->addData($arr, $field_array, "departments", $check_array);
        } else if ($mode == 1) {
            $field_array[] = array("table_name" => "departments", "where" => array("fields" => array("`id`"), "values" => array($id)), "columns" => array("`name`", 
				  array("col_name" => "`modified_by`", "values" => array($uid)),
				  array("col_name" => "`modified_on`", "query" => 'NOW(6)')
			));
            $check_array[] = array("index" => 0, "col_index" => [0], "col_name" => "`id`");
            $tables = array();
            echo $main->editData($arr, $field_array, $deleted_fields, $tables, $check_array, $id);
        } else {
            echo json_encode(0);
        }
});
$router->map('POST', '/saveDepartmentMultiple', function() {
    $id = $_POST["id"];
    $uid = $_POST["uid"];
    $arr = $_POST["arr"];

    $main = new MainModel();
    $field_array = array();
    $check_array = array();
    $field_array[] = array("table_name" => "departments", "columns" => array("`name`", 
		array("col_name" => "`issued_by`", "values" => array($uid))
	));
    $check_array[] = array("index" => 0, "col_index" => [0]);
    echo $main->addData($arr, $field_array, "departments", $check_array);
});


//Currencys 
$router->map('GET', '/getAllCurrencys', function() {

    $data = new DataTablesModel();
    echo $data->getAllCurrencys();
});
$router->map('GET', '/loadCurrency', function() {
    $id = $_GET["id"];
    $details = prepareTable("select 
		`name`,
		`symbol` 
		from `currencys` where `id`=?", 
	array($id));
    echo json_encode(array($details));
});
$router->map('POST', '/saveCurrency', function() {
    $id = $_POST["id"];
    $uid = $_POST["uid"];
    $deleted_fields = array();
    if (isset($_POST["deleted_fields"])) {
        $deleted_fields = $_POST["deleted_fields"];
    }
    $arr = $_POST["arr"];
    $mode = $_POST["mode"];
    $main = new MainModel();
    $field_array = array();
    $check_array = array();
        if ($mode == 0) {
            $field_array[] = array("table_name" => "currencys", "columns" => array("`name`","`symbol`", 
				array("col_name" => "`issued_by`", "values" => array($uid)),
			));
            $check_array[] = array("index" => 0, "col_index" => [0]);
            echo $main->addData($arr, $field_array, "currencys", $check_array);
        } else if ($mode == 1) {
            $field_array[] = array("table_name" => "currencys", "where" => array("fields" => array("`id`"), "values" => array($id)), "columns" => array("`name`","`symbol`", 
				  array("col_name" => "`modified_by`", "values" => array($uid)),
				  array("col_name" => "`modified_on`", "query" => 'NOW(6)')
			));
            $check_array[] = array("index" => 0, "col_index" => [0], "col_name" => "`id`");
            $tables = array();
            echo $main->editData($arr, $field_array, $deleted_fields, $tables, $check_array, $id);
        } else {
            echo json_encode(0);
        }
});
$router->map('POST', '/saveCurrencyMultiple', function() {
    $id = $_POST["id"];
    $uid = $_POST["uid"];
    $arr = $_POST["arr"];

    $main = new MainModel();
    $field_array = array();
    $check_array = array();
    $field_array[] = array("table_name" => "currencys", "columns" => array("`name`","`symbol`", 
		array("col_name" => "`issued_by`", "values" => array($uid))
	));
    $check_array[] = array("index" => 0, "col_index" => [0]);
    echo $main->addData($arr, $field_array, "currencys", $check_array);
});


//Employees
$router->map('POST', '/getAllEmployees', function() {
    $main = new DataTablesModel();
    echo $main->getAllEmployees();
});

$router->map('GET', '/loadEmployee', function() {
	$id=$_GET["id"];
	$details=prepareTable("SELECT
`name`,
`username`,
`password`,
`id_department`
FROM employees 
WHERE `id`=?",array($id));
	echo json_encode(array($details));
});

$router->map('POST', '/saveEmployees', function() {
	$id=$_POST["id"];
	$uid=$_POST["uid"];
	$arr=$_POST["arr"];
	$mode=$_POST["mode"];
	$field_array=array();
	$check_array=array();
	$deleted_fields=array();
	if(isset($_POST["deleted_fields"])){
		$deleted_fields=$_POST["deleted_fields"];
	}
	$main=new MainModel();
	if($mode==0){
			$field_array[]=array("table_name"=>"employees","columns"=>array("`name`","`username`", "`password`", "`id_department`",
				  array("col_name" => "`issued_by`", "values" => array($uid)),
				  array("col_name" => "`modified_by`", "values" => array($uid)),
				  array("col_name" => "`modified_on`", "query" => 'now(6)')
			));
		$check_array[] = array("index" => 0, "col_index" => [0]);
		$check_array[] = array("index" => 0, "col_index" => [1]);
		echo $main->addData($arr,$field_array,"employees",$check_array);
	}else if($mode==1){
			$field_array[]=array("table_name"=>"employees","where"=>array("fields"=>array("`id`"),"values"=>array($id)),"columns"=>array("`name`","`username`", "`password`", "`id_department`", 
				  array("col_name" => "`modified_by`", "values" => array($uid)),
				  array("col_name" => "`modified_on`", "query" => 'NOW(6)')
			));
		$check_array[] = array("index" => 0, "col_index" => [0], "col_name" => "`id`");
		$check_array[] = array("index" => 0, "col_index" => [1], "col_name" => "`id`");
		$tables=array("employees");
		echo $main->editData($arr,$field_array,$deleted_fields,$tables,$check_array,$id);
	}
});

$router->map('POST', '/saveEmployeeMultiple', function() {
    $id = $_POST["id"];
    $uid = $_POST["uid"];
    $arr = $_POST["arr"];

    $main = new MainModel();
    $field_array = array();
    $check_array = array();
    $field_array[] = array("table_name" => "employees", "columns" => array("`name`","`username`", "`password`", "`id_department`",
		array("col_name" => "`issued_by`", "values" => array($uid))
	));
    $check_array[] = array("index" => 0, "col_index" => [0]);
    $check_array[] = array("index" => 0, "col_index" => [1]);
    echo $main->addData($arr, $field_array, "contact_types", $check_array);
});


//Customers
$router->map('POST', '/getAllCustomers', function() {
    $main = new DataTablesModel();
    echo $main->getAllCustomers();
});

$router->map('GET', '/loadCustomer', function() {
	$id=$_GET["id"];
	$details=prepareTable("SELECT
`name`,
`contact_person`,
`address`,
`balance`
FROM customers 
WHERE `id`=?",array($id));

	echo json_encode(array($details));
});

$router->map('POST', '/saveCustomers', function() {
	$id=$_POST["id"];
	$uid=$_POST["uid"];
	$arr=$_POST["arr"];
	$mode=$_POST["mode"];
	$field_array=array();
	$check_array=array();
	$deleted_fields=array();
	if(isset($_POST["deleted_fields"])){
		$deleted_fields=$_POST["deleted_fields"];
	}
	$main=new MainModel();
	if($mode==0){
			$field_array[]=array("table_name"=>"customers","columns"=>array("`name`","`contact_person`", "`address`", "`balance`",
				  array("col_name" => "`issued_by`", "values" => array($uid)),
				  array("col_name" => "`modified_by`", "values" => array($uid)),
				  array("col_name" => "`modified_on`", "query" => 'now(6)')
			));
		$check_array[] = array("index" => 0, "col_index" => [0]);
		echo $main->addData($arr,$field_array,"customers",$check_array);
	}else if($mode==1){
			$field_array[]=array("table_name"=>"customers","where"=>array("fields"=>array("`id`"),"values"=>array($id)),"columns"=>array("`name`","`contact_person`", "`address`", "`balance`", 
				  array("col_name" => "`modified_by`", "values" => array($uid)),
				  array("col_name" => "`modified_on`", "query" => 'NOW(6)')
			));
		$check_array[] = array("index" => 0, "col_index" => [0], "col_name" => "`id`");
		$tables=array("customers");
		echo $main->editData($arr,$field_array,$deleted_fields,$tables,$check_array,$id);
	}
});

$router->map('POST', '/saveCustomerMultiple', function() {
    $id = $_POST["id"];
    $uid = $_POST["uid"];
    $arr = $_POST["arr"];

    $main = new MainModel();
    $field_array = array();
    $check_array = array();
    $field_array[] = array("table_name" => "customers", "columns" => array("`name`","`contact_person`", "`address`", "`balance`",
		array("col_name" => "`issued_by`", "values" => array($uid))
	));
    $check_array[] = array("index" => 0, "col_index" => [0]);
    echo $main->addData($arr, $field_array, "contact_types", $check_array);
});

//Suppliers
$router->map('POST', '/getAllSuppliers', function() {
    $main = new DataTablesModel();
    echo $main->getAllSuppliers();
});

$router->map('GET', '/loadSupplier', function() {
	$id=$_GET["id"];
	$details=prepareTable("SELECT
`name`,
`contact_person`,
`address`,
`id_currency`,
`balance`
FROM suppliers 
WHERE `id`=?",array($id));
	echo json_encode(array($details));
});

$router->map('POST', '/saveSuppliers', function() {
	$id=$_POST["id"];
	$uid=$_POST["uid"];
	$arr=$_POST["arr"];
	$mode=$_POST["mode"];
	$field_array=array();
	$check_array=array();
	$deleted_fields=array();
	if(isset($_POST["deleted_fields"])){
		$deleted_fields=$_POST["deleted_fields"];
	}
	$main=new MainModel();
	if($mode==0){
			$field_array[]=array("table_name"=>"suppliers","columns"=>array("`name`","`contact_person`", "`address`","`id_currency`", "`balance`",
				  array("col_name" => "`issued_by`", "values" => array($uid)),
				  array("col_name" => "`modified_by`", "values" => array($uid)),
				  array("col_name" => "`modified_on`", "query" => 'now(6)')
			));
		$check_array[] = array("index" => 0, "col_index" => [0]);
		echo $main->addData($arr,$field_array,"suppliers",$check_array);
	}else if($mode==1){
			$field_array[]=array("table_name"=>"suppliers","where"=>array("fields"=>array("`id`"),"values"=>array($id)),"columns"=>array("`name`","`contact_person`", "`address`","`id_currency`", "`balance`",
				  array("col_name" => "`modified_by`", "values" => array($uid)),
				  array("col_name" => "`modified_on`", "query" => 'NOW(6)')
			));
		$check_array[] = array("index" => 0, "col_index" => [0], "col_name" => "`id`");
		$tables=array("suppliers");
		echo $main->editData($arr,$field_array,$deleted_fields,$tables,$check_array,$id);
	}
});

$router->map('POST', '/saveSupplierMultiple', function() {
    $id = $_POST["id"];
    $uid = $_POST["uid"];
    $arr = $_POST["arr"];

    $main = new MainModel();
    $field_array = array();
    $check_array = array();
    $field_array[] = array("table_name" => "suppliers", "columns" => array("`name`","`contact_person`", "`address`","`id_currency`", "`balance`",
		array("col_name" => "`issued_by`", "values" => array($uid))
	));
    $check_array[] = array("index" => 0, "col_index" => [0]);
    echo $main->addData($arr, $field_array, "contact_types", $check_array);
});


//Plastics
$router->map('POST', '/getAllPlastics', function() {
    $main = new DataTablesModel();
    echo $main->getAllPlastics();
});

$router->map('GET', '/loadPlastic', function() {
	$id=$_GET["id"];
	$details=prepareTable("SELECT
`specification`,
`minimum`,
`starting_qty`,
`price`
FROM plastics 
WHERE `id`=?",array($id));

	echo json_encode(array($details));
});

$router->map('POST', '/savePlastics', function() {
	$id=$_POST["id"];
	$uid=$_POST["uid"];
	$arr=$_POST["arr"];
	$mode=$_POST["mode"];
	$field_array=array();
	$check_array=array();
	$deleted_fields=array();
	if(isset($_POST["deleted_fields"])){
		$deleted_fields=$_POST["deleted_fields"];
	}
	$main=new MainModel();
	if($mode==0){
			$field_array[]=array("table_name"=>"plastics","columns"=>array("`specification`", "`minimum`", "`starting_qty`", "`price`",
				  array("col_name" => "`issued_by`", "values" => array($uid)),
				  array("col_name" => "`modified_by`", "values" => array($uid)),
				  array("col_name" => "`modified_on`", "query" => 'now(6)')
			));
		$check_array[] = array("index" => 0, "col_index" => [0]);
		echo $main->addData($arr,$field_array,"plastics",$check_array);
	}else if($mode==1){
			$field_array[]=array("table_name"=>"plastics","where"=>array("fields"=>array("`id`"),"values"=>array($id)),"columns"=>array("`specification`", "`minimum`", "`starting_qty`", "`price`",
				  array("col_name" => "`modified_by`", "values" => array($uid)),
				  array("col_name" => "`modified_on`", "query" => 'NOW(6)')
			));
		$check_array[] = array("index" => 0, "col_index" => [0], "col_name" => "`id`");
		$tables=array("plastics");
		echo $main->editData($arr,$field_array,$deleted_fields,$tables,$check_array,$id);
	}
});

$router->map('POST', '/savePlasticMultiple', function() {
    $id = $_POST["id"];
    $uid = $_POST["uid"];
    $arr = $_POST["arr"];

    $main = new MainModel();
    $field_array = array();
    $check_array = array();
    $field_array[] = array("table_name" => "plastics", "columns" => array("`specification`", "`minimum`", "`starting_qty`", "`price`",
		array("col_name" => "`issued_by`", "values" => array($uid))
	));
    $check_array[] = array("index" => 0, "col_index" => [0]);
    echo $main->addData($arr, $field_array, "contact_types", $check_array);
});
//Adhesives
$router->map('POST', '/getAllAdhesives', function() {
    $main = new DataTablesModel();
    echo $main->getAllAdhesives();
});

$router->map('GET', '/loadAdhesive', function() {
	$id=$_GET["id"];
	$details=prepareTable("SELECT
`name`,
`type`,
`minimum`,
`starting_qty`,
`price`
FROM adhesives 
WHERE `id`=?",array($id));

	echo json_encode(array($details));
});

$router->map('POST', '/saveAdhesives', function() {
	$id=$_POST["id"];
	$uid=$_POST["uid"];
	$arr=$_POST["arr"];
	$mode=$_POST["mode"];
	$field_array=array();
	$check_array=array();
	$deleted_fields=array();
	if(isset($_POST["deleted_fields"])){
		$deleted_fields=$_POST["deleted_fields"];
	}
	$main=new MainModel();
	if($mode==0){
			$field_array[]=array("table_name"=>"adhesives","columns"=>array("`name`","`type`", "`minimum`", "`starting_qty`", "`price`",
				  array("col_name" => "`issued_by`", "values" => array($uid)),
				  array("col_name" => "`modified_by`", "values" => array($uid)),
				  array("col_name" => "`modified_on`", "query" => 'now(6)')
			));
		$check_array[] = array("index" => 0, "col_index" => [0]);
		echo $main->addData($arr,$field_array,"adhesives",$check_array);
	}else if($mode==1){
			$field_array[]=array("table_name"=>"adhesives","where"=>array("fields"=>array("`id`"),"values"=>array($id)),"columns"=>array("`name`","`type`", "`minimum`", "`starting_qty`", "`price`",
				  array("col_name" => "`modified_by`", "values" => array($uid)),
				  array("col_name" => "`modified_on`", "query" => 'NOW(6)')
			));
		$check_array[] = array("index" => 0, "col_index" => [0], "col_name" => "`id`");
		$tables=array("adhesives");
		echo $main->editData($arr,$field_array,$deleted_fields,$tables,$check_array,$id);
	}
});

$router->map('POST', '/saveAdhesiveMultiple', function() {
    $id = $_POST["id"];
    $uid = $_POST["uid"];
    $arr = $_POST["arr"];

    $main = new MainModel();
    $field_array = array();
    $check_array = array();
    $field_array[] = array("table_name" => "adhesives", "columns" => array("`name`","`type`", "`minimum`", "`starting_qty`", "`price`",
		array("col_name" => "`issued_by`", "values" => array($uid))
	));
    $check_array[] = array("index" => 0, "col_index" => [0]);
    echo $main->addData($arr, $field_array, "contact_types", $check_array);
});

//Chemicals
$router->map('POST', '/getAllChemicals', function() {
    $main = new DataTablesModel();
    echo $main->getAllChemicals();
});

$router->map('GET', '/loadChemical', function() {
	$id=$_GET["id"];
	$details=prepareTable("SELECT
`name`,
`type`,
`minimum`,
`starting_qty`,
`price`
FROM chemicals 
WHERE `id`=?",array($id));

	echo json_encode(array($details));
});

$router->map('POST', '/saveChemicals', function() {
	$id=$_POST["id"];
	$uid=$_POST["uid"];
	$arr=$_POST["arr"];
	$mode=$_POST["mode"];
	$field_array=array();
	$check_array=array();
	$deleted_fields=array();
	if(isset($_POST["deleted_fields"])){
		$deleted_fields=$_POST["deleted_fields"];
	}
	$main=new MainModel();
	if($mode==0){
			$field_array[]=array("table_name"=>"chemicals","columns"=>array("`name`","`type`", "`minimum`", "`starting_qty`", "`price`",
				  array("col_name" => "`issued_by`", "values" => array($uid)),
				  array("col_name" => "`modified_by`", "values" => array($uid)),
				  array("col_name" => "`modified_on`", "query" => 'now(6)')
			));
		$check_array[] = array("index" => 0, "col_index" => [0]);
		echo $main->addData($arr,$field_array,"chemicals",$check_array);
	}else if($mode==1){
			$field_array[]=array("table_name"=>"chemicals","where"=>array("fields"=>array("`id`"),"values"=>array($id)),"columns"=>array("`name`","`type`", "`minimum`", "`starting_qty`", "`price`",
				  array("col_name" => "`modified_by`", "values" => array($uid)),
				  array("col_name" => "`modified_on`", "query" => 'NOW(6)')
			));
		$check_array[] = array("index" => 0, "col_index" => [0], "col_name" => "`id`");
		$tables=array("chemicals");
		echo $main->editData($arr,$field_array,$deleted_fields,$tables,$check_array,$id);
	}
});

$router->map('POST', '/saveChemicalMultiple', function() {
    $id = $_POST["id"];
    $uid = $_POST["uid"];
    $arr = $_POST["arr"];

    $main = new MainModel();
    $field_array = array();
    $check_array = array();
    $field_array[] = array("table_name" => "chemicals", "columns" => array("`name`","`type`", "`minimum`", "`starting_qty`", "`price`",
		array("col_name" => "`issued_by`", "values" => array($uid))
	));
    $check_array[] = array("index" => 0, "col_index" => [0]);
    echo $main->addData($arr, $field_array, "contact_types", $check_array);
});

//Inks
$router->map('POST', '/getAllInks', function() {
    $main = new DataTablesModel();
    echo $main->getAllInks();
});

$router->map('GET', '/loadInk', function() {
	$id=$_GET["id"];
	$details=prepareTable("SELECT
`name`,
`type`,
`minimum`,
`starting_qty`,
`price`
FROM inks 
WHERE `id`=?",array($id));

	echo json_encode(array($details));
});

$router->map('POST', '/saveInks', function() {
	$id=$_POST["id"];
	$uid=$_POST["uid"];
	$arr=$_POST["arr"];
	$mode=$_POST["mode"];
	$field_array=array();
	$check_array=array();
	$deleted_fields=array();
	if(isset($_POST["deleted_fields"])){
		$deleted_fields=$_POST["deleted_fields"];
	}
	$main=new MainModel();
	if($mode==0){
			$field_array[]=array("table_name"=>"inks","columns"=>array("`name`","`type`", "`minimum`", "`starting_qty`", "`price`",
				  array("col_name" => "`issued_by`", "values" => array($uid)),
				  array("col_name" => "`modified_by`", "values" => array($uid)),
				  array("col_name" => "`modified_on`", "query" => 'now(6)')
			));
		$check_array[] = array("index" => 0, "col_index" => [0]);
		echo $main->addData($arr,$field_array,"inks",$check_array);
	}else if($mode==1){
			$field_array[]=array("table_name"=>"inks","where"=>array("fields"=>array("`id`"),"values"=>array($id)),"columns"=>array("`name`","`type`", "`minimum`", "`starting_qty`", "`price`",
				  array("col_name" => "`modified_by`", "values" => array($uid)),
				  array("col_name" => "`modified_on`", "query" => 'NOW(6)')
			));
		$check_array[] = array("index" => 0, "col_index" => [0], "col_name" => "`id`");
		$tables=array("inks");
		echo $main->editData($arr,$field_array,$deleted_fields,$tables,$check_array,$id);
	}
});

$router->map('POST', '/saveInkMultiple', function() {
    $id = $_POST["id"];
    $uid = $_POST["uid"];
    $arr = $_POST["arr"];

    $main = new MainModel();
    $field_array = array();
    $check_array = array();
    $field_array[] = array("table_name" => "inks", "columns" => array("`name`","`type`", "`minimum`", "`starting_qty`", "`price`",
		array("col_name" => "`issued_by`", "values" => array($uid))
	));
    $check_array[] = array("index" => 0, "col_index" => [0]);
    echo $main->addData($arr, $field_array, "contact_types", $check_array);
});

//Cylinders
$router->map('POST', '/getAllCylinders', function() {
    $main = new DataTablesModel();
    echo $main->getAllCylinders();
});

$router->map('GET', '/loadCylinder', function() {
	$id=$_GET["id"];
	$details=prepareTable("SELECT
`color`,
`size`,
`minimum`,
`starting_qty`,
`price`
FROM cylinders 
WHERE `id`=?",array($id));

	echo json_encode(array($details));
});

$router->map('POST', '/saveCylinders', function() {
	$id=$_POST["id"];
	$uid=$_POST["uid"];
	$arr=$_POST["arr"];
	$mode=$_POST["mode"];
	$field_array=array();
	$check_array=array();
	$deleted_fields=array();
	if(isset($_POST["deleted_fields"])){
		$deleted_fields=$_POST["deleted_fields"];
	}
	$main=new MainModel();
	if($mode==0){
			$field_array[]=array("table_name"=>"cylinders","columns"=>array("`color`","`size`", "`minimum`", "`starting_qty`", "`price`",
				  array("col_name" => "`issued_by`", "values" => array($uid)),
				  array("col_name" => "`modified_by`", "values" => array($uid)),
				  array("col_name" => "`modified_on`", "query" => 'now(6)')
			));
		$check_array[] = array("index" => 0, "col_index" => [0]);
		echo $main->addData($arr,$field_array,"cylinders",$check_array);
	}else if($mode==1){
			$field_array[]=array("table_name"=>"cylinders","where"=>array("fields"=>array("`id`"),"values"=>array($id)),"columns"=>array("`color`","`size`", "`minimum`", "`starting_qty`", "`price`",
				  array("col_name" => "`modified_by`", "values" => array($uid)),
				  array("col_name" => "`modified_on`", "query" => 'NOW(6)')
			));
		$check_array[] = array("index" => 0, "col_index" => [0], "col_name" => "`id`");
		$tables=array("cylinders");
		echo $main->editData($arr,$field_array,$deleted_fields,$tables,$check_array,$id);
	}
});

$router->map('POST', '/saveCylinderMultiple', function() {
    $id = $_POST["id"];
    $uid = $_POST["uid"];
    $arr = $_POST["arr"];

    $main = new MainModel();
    $field_array = array();
    $check_array = array();
    $field_array[] = array("table_name" => "cylinders", "columns" => array("`color`","`size`", "`minimum`", "`starting_qty`", "`price`",
		array("col_name" => "`issued_by`", "values" => array($uid))
	));
    $check_array[] = array("index" => 0, "col_index" => [0]);
    echo $main->addData($arr, $field_array, "contact_types", $check_array);
});




//Products
$router->map('POST', '/getAllProducts', function() {
    $main = new DataTablesModel();
    echo $main->getAllProducts();
});

$router->map('GET', '/loadProduct', function() {
	$id=$_GET["id"];
	$details=prepareTable("SELECT
`date`,
`name`,
`id_customer`,
`id_ink`,
`id_adhesive`,
`id_chemical`,
`id_cylinder`
FROM products 
WHERE `id`=?",array($id));

    $arr = prepareTable("select
        `id`,
`id_plastic`,
`size`,
`thickness`,
'button' `button`
FROM `product_has_plastics` 
where `id_product`=?", array($id));



	echo json_encode(array($details,$arr));
});

$router->map('POST', '/saveProducts', function() {
	$id=$_POST["id"];
	$uid=$_POST["uid"];
	$arr=$_POST["arr"];
	$mode=$_POST["mode"];
	$field_array=array();
	$check_array=array();
	$deleted_fields=array();
	if(isset($_POST["deleted_fields"])){
		$deleted_fields=$_POST["deleted_fields"];
	}
	$main=new MainModel();
	if($mode==0){
			$field_array[]=array("table_name"=>"products","columns"=>array("`date`","`name`", "`id_customer`", "`id_ink`", "`id_adhesive`", "`id_chemical`", "`id_cylinder`",
				  array("col_name" => "`issued_by`", "values" => array($uid)),
				  array("col_name" => "`modified_by`", "values" => array($uid)),
				  array("col_name" => "`modified_on`", "query" => 'now(6)')
			));
			$field_array[] = array("table_name" => "product_has_plastics", "columns" => array("`id_product`", "`id_plastic`", "`size`", "`thickness`",
				  array("col_name" => "`issued_by`", "values" => array($uid)),
				  array("col_name" => "`modified_by`", "values" => array($uid)),
				  array("col_name" => "`modified_on`", "query" => 'now(6)')
			
			));
		echo $main->addData($arr,$field_array,"products",$check_array);
	}else if($mode==1){
			$field_array[]=array("table_name"=>"products","where"=>array("fields"=>array("`id`"),"values"=>array($id)),"columns"=>array("`date`","`name`", "`id_customer`", "`id_ink`", "`id_adhesive`", "`id_chemical`", "`id_cylinder`",
				  array("col_name" => "`modified_by`", "values" => array($uid)),
				  array("col_name" => "`modified_on`", "query" => 'NOW(6)')
			));
        $field_array[] = array("table_name" => "product_has_plastics", "columns" => array(array("`id_plastic`", "`size`", "`thickness`","`id_product`",
				  array("col_name" => "`issued_by`", "values" => array($uid)),
				  array("col_name" => "`modified_by`", "values" => array($uid)),
				  array("col_name" => "`modified_on`", "query" => 'now(6)')),
		array("`id_plastic`", "`size`", "`thickness`", "`id`",
				  array("col_name" => "`modified_by`", "values" => array($uid)),
				  array("col_name" => "`modified_on`", "query" => 'NOW(6)')
		)
		));
        $tables = array("product_has_plastics");
		echo $main->editData($arr,$field_array,$deleted_fields,$tables,$check_array,$id);
	}
});

$router->map('POST', '/saveProductMultiple', function() {
    $id = $_POST["id"];
    $uid = $_POST["uid"];
    $arr = $_POST["arr"];

    $main = new MainModel();
    $field_array = array();
    $check_array = array();
    $field_array[] = array("table_name" => "products", "columns" => array("`name`","`type`", "`minimum`", "`starting_qty`", "`price`",
		array("col_name" => "`issued_by`", "values" => array($uid))
	));
    $check_array[] = array("index" => 0, "col_index" => [0]);
    echo $main->addData($arr, $field_array, "contact_types", $check_array);
});

//CustomerPO
$router->map('POST', '/getAllCustomerPO', function() {
    $main = new DataTablesModel();
    echo $main->getAllCustomerPO();
});

$router->map('GET', '/getProductSpecs', function() {
	$id=$_GET["id"];
	$details=prepareTable("SELECT GROUP_CONCAT(DISTINCT CONCAT(`specification`,'(',`size`,'x',`thickness`,')') SEPARATOR ' | ') `plastic` FROM `product_has_plastics` PP  
LEFT JOIN `plastics` P ON P.`id`=PP.`id_plastic`
WHERE `id_product`=?",array($id));
	echo json_encode($details);
});
$router->map('GET', '/loadCustomerPO', function() {
	$id=$_GET["id"];
	$details=prepareTable("SELECT
`id_customer`,
`date`,
`po_number`
FROM `customerpos` 
WHERE `id`=?",array($id));

    $arr = prepareTable("select
CP.`id`,
P.`name` 'products',
CP.`id_product`,
'specification' `specification`,
CP.`qty` 'po_qty',
CP.`price`,
(`qty`*`price`) 'total',
'button' `button`
from `customerpo_has_products`CP
LEFT JOIN `products` P ON P.`id`=CP.`id_product`
where `id_customerpo`=?", array($id));
	echo json_encode(array($details,$arr));
});

$router->map('POST', '/saveCustomerPO', function() {
	$id=$_POST["id"];
	$uid=$_POST["uid"];
	$arr=$_POST["arr"];
	$mode=$_POST["mode"];
	$field_array=array();
	$check_array=array();
	$deleted_fields=array();
	if(isset($_POST["deleted_fields"])){
		$deleted_fields=$_POST["deleted_fields"];
	}
	$main=new MainModel();
	if($mode==0){
			$field_array[]=array("table_name"=>"customerpos","columns"=>array( "`id_customer`", "`date`", "`po_number`",
				  array("col_name" => "`issued_by`", "values" => array($uid)),
				  array("col_name" => "`modified_by`", "values" => array($uid)),
				  array("col_name" => "`modified_on`", "query" => 'now(6)')
			));
			$field_array[] = array("table_name" => "customerpo_has_products", "columns" => array("`id_customerpo`", "`id_product`", "`qty`", "`price`",
				  array("col_name" => "`issued_by`", "values" => array($uid)),
				  array("col_name" => "`modified_by`", "values" => array($uid)),
				  array("col_name" => "`modified_on`", "query" => 'now(6)')
			
			));
				$check_array[] = array("index" => 0, "col_index" => [2]);
		echo $main->addData($arr,$field_array,"customerpos",$check_array);
	}else if($mode==1){
			$field_array[]=array("table_name"=>"customerpos","where"=>array("fields"=>array("`id`"),"values"=>array($id)),"columns"=>array("`id_customer`", "`date`", "`po_number`",
				  array("col_name" => "`modified_by`", "values" => array($uid)),
				  array("col_name" => "`modified_on`", "query" => 'NOW(6)')
			));
        $field_array[] = array("table_name" => "customerpo_has_products", "columns" => array(array( "`id_product`", "`qty`", "`price`","`id_customerpo`",
				  array("col_name" => "`issued_by`", "values" => array($uid)),
				  array("col_name" => "`modified_by`", "values" => array($uid)),
				  array("col_name" => "`modified_on`", "query" => 'now(6)')),
		array("`id_product`", "`qty`", "`price`", "`id`",
				  array("col_name" => "`modified_by`", "values" => array($uid)),
				  array("col_name" => "`modified_on`", "query" => 'NOW(6)')
		)
		));
            $check_array[] = array("index" => 0, "col_index" => [2], "col_name" => "`id`");
        $tables = array("customerpo_has_products");
		echo $main->editData($arr,$field_array,$deleted_fields,$tables,$check_array,$id);
	}
});

//JobOrder
$router->map('POST', '/getAllJobOrder', function() {
    $main = new DataTablesModel();
    echo $main->getAllJobOrder();
});
$router->map('POST', '/getAllJobOrderProductions', function() {
    $main = new DataTablesModel();
    echo $main->getAllJobOrderProductions();
});

$router->map('GET', '/getProductSpecs', function() {
	$id=$_GET["id"];
	$details=prepareTable("SELECT GROUP_CONCAT(DISTINCT CONCAT(`specification`,'(',`size`,'x',`thickness`,')') SEPARATOR ' | ') `plastic` FROM `product_has_plastics` PP  
LEFT JOIN `plastics` P ON P.`id`=PP.`id_plastic`
WHERE `id_product`=?",array($id));
	echo json_encode($details);
});
$router->map('GET', '/loadJobOrder', function() {
	$id=$_GET["id"];
	$details=prepareTable("SELECT
`date`,
(CASE WHEN `jo_number` IS NULL THEN ''  ELSE `jo_number` END) 'jo_number',
`recieved_by`,
`noted_by`,
`thickness`,
' ' `a1`,
' ' `a2`,
' ' `a3`,
' ' `a4`,
' ' `a5`,
' ' `a6`,
`weight_roll`,
`barcode`,
`meter_roll`,
`prepared_by`,
`filled_up_by`,
`approved_by`
FROM `joborders`
WHERE `id`=?",array($id));
	$detailsF=prepareTable("SELECT
(SELECT GROUP_CONCAT(DISTINCT `specification` SEPARATOR '|') 'x' FROM `plastics` WHERE `id` IN (SELECT `id_plastic` FROM `product_has_plastics` WHERE `id_product`=P.`id` ORDER BY `id`)) 'material',
C.`name` 'customer',
CY.`color` 'cylinder',
P.`name` 'product',
`po_number`,
`po_qty`,
`winding_direction`
FROM `joborders` JO
LEFT JOIN `products` P ON P.`id`=JO.`id_product`
LEFT JOIN `customers` C ON C.`id`=JO.`id_customer`
LEFT JOIN `cylinders` CY ON CY.`id`=P.`id_cylinder`
WHERE  JO.`id`=?",array($id));
	$plastica=prepareTable("SELECT * 
FROM `joborder_has_plastics`
WHERE  `id_joborder`=?
ORDER BY `id`",array($id));
	echo json_encode(array($details,$detailsF,$plastica));
});

$router->map('GET', '/loadJobOrderProductions', function() {
	include './lib/language.php';
	$id=$_GET["id"];
	$details=prepareTable("SELECT
	concat('<input  type=\"text\" value=\"',JP.`id`,'\" class=\"deliveryID\">') 'id',
	JP.`date`,
	`pd_roll`,
	`pd_weight`,
	`ld_roll`,
	`ld_weight`,
	`sd_roll`,
	(CASE WHEN (JD.`status` IS NULL) THEN (concat(`sd_weight`,'<br><input data_rem=\"',(`sd_weight`),'\" type=\"radio\" name=\"deliverys0',JP.`id`,'\" data_val=\"1\" class=\"delivery\">{$deliver}')) ELSE (CASE WHEN JD.`status`=1 THEN (CASE WHEN JP.`sd_weight`=sum(JD.`qty`) THEN JP.`sd_weight` ELSE (concat(`sd_weight`,'<br>(',(`sd_weight`-sum(JD.`qty`)),' pending)','<br><input data_rem=\"',(`sd_weight`-sum(JD.`qty`)),'\" type=\"radio\" name=\"deliverys0',JP.`id`,'\" data_val=\"1\" class=\"delivery\">{$deliver}')) END) ELSE JP.`sd_weight`END)END)'sd_weight',
	`bf_roll`,
	(CASE WHEN (JD.`status` IS NULL) THEN (concat(`bf_weight`,'<br><input data_rem=\"',(`bf_weight`),'\" type=\"radio\" name=\"deliverys0',JP.`id`,'\" data_val=\"2\" class=\"delivery\">{$deliver}')) ELSE (CASE WHEN JD.`status`=2 THEN (CASE WHEN JP.`bf_weight`=sum(JD.`qty`) THEN JP.`bf_weight` ELSE (concat(`bf_weight`,'<br>(',(`bf_weight`-sum(JD.`qty`)),' pending)','<br><input data_rem=\"',(`bf_weight`-sum(JD.`qty`)),'\" type=\"radio\" name=\"deliverys0',JP.`id`,'\" data_val=\"2\" class=\"delivery\">{$deliver}')) END) ELSE JP.`bf_weight`END)END)'bf_weight',
	`remarks`,
	JP.`status` 'btn',
    ((CASE JD.`status` WHEN 1 THEN `sd_weight` WHEN 2 THEN `bf_weight` END)-sum(JD.`qty`)) `remaining`,
	sum(JD.`qty`) `qty`,
	JD.`status`
	
FROM `joborder_has_productions` JP
LEFT JOIN `joborder_production_has_deliverys` JD ON JD.`id_joborder_production`=JP.`id`
WHERE `id_joborder`=?  GROUP BY JP.`id` ORDER BY `date`",array($id));
	echo json_encode(array($details));
});
$router->map('GET', '/loadJobOrderProductionsList', function() {
	include './lib/language.php';
	$id=$_GET["id"];
	$details=prepareTable("SELECT
JP.`id`,
	JP.`date` '0',
	`pd_roll` '1',
	`pd_weight` '2',
	`ld_roll` '3',
	`ld_weight` '4',
	`sd_roll` '5',
	`sd_weight` '6',
	`bf_roll` '7',
	`bf_weight` '8',
	(SELECT group_concat(`dr_no` separator ',') FROM `joborder_production_has_deliverys` WHERE `id_joborder_production`=JP.`id`) '9',
	(SELECT concat(CAST(sum(`qty`) as CHAR),'<br>(',(CASE `status` WHEN 1 THEN '$slitting_dept' ELSE '$bagforming_dept' END),')') FROM `joborder_production_has_deliverys` WHERE `id_joborder_production`=JP.`id` GROUP BY `id_joborder_production`) '10',
	`remarks` '11'
	
FROM `joborder_has_productions` JP
LEFT JOIN `joborder_production_has_deliverys` JD ON JD.`id_joborder_production`=JP.`id`
WHERE `id_joborder`=?  GROUP BY JP.`id` ORDER BY JP.`date`",array($id));
	echo json_encode($details);
});

$router->map('POST', '/saveJobOrders', function() {
	$id=$_POST["id"];
	$uid=$_POST["uid"];
	$arr=$_POST["arr"];
	$mode=$_POST["mode"];
	$field_array=array();
	$check_array=array();
	$deleted_fields=array();
	if(isset($_POST["deleted_fields"])){
		$deleted_fields=$_POST["deleted_fields"];
	}
	$main=new MainModel();
	if($mode==0){
			$field_array[]=array("table_name"=>"joborders","columns"=>array( "`date`", "`jo_number`","`recieved_by`","`noted_by`","`thickness`","`weight_roll`","`barcode`","`meter_roll`","`prepared_by`","`filled_up_by`","`approved_by`","`winding_direction`",
				  array("col_name" => "`issued_by`", "values" => array($uid)),
				  array("col_name" => "`modified_by`", "values" => array($uid)),
				  array("col_name" => "`modified_on`", "query" => 'now(6)')
			));
			$field_array[] = array("table_name" => "joborder_has_plastics", "columns" => array("`id_joborder`","`type_size`", "`qty`",
				  array("col_name" => "`issued_by`", "values" => array($uid)),
				  array("col_name" => "`modified_by`", "values" => array($uid)),
				  array("col_name" => "`modified_on`", "query" => 'now(6)')
			
			));
				$check_array[] = array("index" => 0, "col_index" => [1]);
		echo $main->addData($arr,$field_array,"joborders",$check_array);
	}else if($mode==1){
			$field_array[]=array("table_name"=>"joborders","where"=>array("fields"=>array("`id`"),"values"=>array($id)),"columns"=>array( "`date`", "`jo_number`","`recieved_by`","`noted_by`","`thickness`","`weight_roll`","`barcode`","`meter_roll`","`prepared_by`","`filled_up_by`","`approved_by`","`winding_direction`",
				  array("col_name" => "`modified_by`", "values" => array($uid)),
				  array("col_name" => "`modified_on`", "query" => 'NOW(6)')
			));
        $field_array[] = array("table_name" => "joborder_has_plastics", "columns" => array(array("`type_size`", "`qty`","`id_joborder`",
				  array("col_name" => "`issued_by`", "values" => array($uid)),
				  array("col_name" => "`modified_by`", "values" => array($uid)),
				  array("col_name" => "`modified_on`", "query" => 'now(6)')),
		array("`type_size`", "`qty`", "`id`",
				  array("col_name" => "`modified_by`", "values" => array($uid)),
				  array("col_name" => "`modified_on`", "query" => 'NOW(6)'),
				  array("col_name" => "`status`", "query" => '1')
		)
		));
            $check_array[] = array("index" => 0, "col_index" => [1], "col_name" => "`id`");
        $tables = array("joborder_has_plastics");
		//echo '222';
		echo $main->editData($arr,$field_array,$deleted_fields,$tables,$check_array,$id);
	}
});

$router->map('POST', '/saveJobOrdersProductions', function() {
	$id=$_POST["id"];
	$uid=$_POST["uid"];
	$arr=$_POST["arr"];
	$mode=$_POST["mode"];
	$field_array=array();
	$check_array=array();
	$deleted_fields=array();
	if(isset($_POST["deleted_fields"])){
		$deleted_fields=$_POST["deleted_fields"];
	}
	$main=new MainModel();
	if($mode==0){
			$field_array[]=array("table_name"=>"joborder_has_productions","columns"=>array("`date`", "`pd_roll`","`pd_weight`","`ld_roll`","`ld_weight`","`sd_roll`","`sd_weight`","`bf_roll`","`bf_weight`","`remarks`","`id_joborder`","`status`",
				  array("col_name" => "`issued_by`", "values" => array($uid)),
				  array("col_name" => "`modified_by`", "values" => array($uid)),
				  array("col_name" => "`modified_on`", "query" => 'now(6)')
			));
		echo $main->addData($arr,$field_array,"joborder_has_productions",$check_array);
	}else if($mode==1){
			$field_array[]=array("table_name"=>"joborder_has_productions","where"=>array("fields"=>array("`id`"),"values"=>array($id)),"columns"=>array("`date`", "`pd_roll`","`pd_weight`","`ld_roll`","`ld_weight`","`sd_roll`","`sd_weight`","`bf_roll`","`bf_weight`","`remarks`","`id_joborder`","`status`",
				  array("col_name" => "`modified_by`", "values" => array($uid)),
				  array("col_name" => "`modified_on`", "query" => 'NOW(6)')
			));
        $tables = array("joborder_has_productions");
		echo $main->editData($arr,$field_array,$deleted_fields,$tables,$check_array,$id);
	}
});

$router->map('POST', '/saveJobOrdersProductionMultiple', function() {
    $id = $_POST["id"];
    $uid = $_POST["uid"];
    $arr = $_POST["arr"];

    $main = new MainModel();
    $field_array = array();
    $check_array = array();
    $field_array[] = array("table_name" => "joborder_has_productions", "columns" => array("`date`", "`pd_roll`","`pd_weight`","`ld_roll`","`ld_weight`","`sd_roll`","`sd_weight`","`bf_roll`","`bf_weight`","`remarks`","`id_joborder`",
				  array("col_name" => "`issued_by`", "values" => array($uid)),
				  array("col_name" => "`modified_by`", "values" => array($uid)),
				  array("col_name" => "`modified_on`", "query" => 'now(6)')
	));
    echo $main->addData($arr, $field_array, "joborder_has_productions", $check_array);
});

$router->map('POST', '/saveJobOrdersProductionDelivery', function() {
    $id = $_POST["id"];
    $uid = $_POST["uid"];
    $arr = $_POST["arr"];

    $main = new MainModel();
    $field_array = array();
    $check_array = array();
    $field_array[] = array("table_name" => "joborder_production_has_deliverys", "columns" => array("`dr_no`","`id_joborder_production`","`deliver_to`","`date`","`address`","`terms`","`qty`","`price`","`status`",
						  array("col_name" => "`issued_by`", "values" => array($uid)),
						  array("col_name" => "`modified_by`", "values" => array($uid)),
						  array("col_name" => "`modified_on`", "query" => 'now(6)')
	));
    $check_array[] = array("index" => 0, "col_index" => [0]);
    echo $main->addData($arr, $field_array, "joborder_production_has_deliverys", $check_array);
});