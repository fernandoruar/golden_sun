<?php

session_start();
$router = new AltoRouter();

function getServer_filters($arr, $settings) {

    $order_string = "";
    $search_string = "";
    $limit_string = "";
    $column_settings = $settings["columns"];
    //   $search_value = $settings["search"]["value"];
    $order_type = $settings["order"][0]["dir"];
    $order_col = $settings["order"][0]["column"];
    //searchable

    $col_search_arr = array();
    $search_counter = 0;
    $cancel_search = array();
    if (isset($settings["cancel_search"])) {
        $cancel_search = $settings["cancel_search"];
    }
    // if ($search_value != "") {

    foreach ($column_settings as $rows) {
        if ($rows["searchable"] == "true") {

            if (isset($arr[$search_counter])) {

                $search_column = $arr[$search_counter];
                if ($rows["search"]["value"] != "" && !in_array(str_replace("`", "", $search_column), $cancel_search)) {
                    $col_search_arr[] = $search_column . " like '%{$rows["search"]["value"]}%'";
                }
            }
        }
        $search_counter++;
    }

    if (isset($settings["extra_search"])) {
        $col_search_arr = array_merge($col_search_arr, $settings["extra_search"]);
    }
    // }


    if (count($col_search_arr) > 0) {
        $search_string = " where (" . implode(" or ", $col_search_arr) . ") ";
    }

    //orderable
    if ($column_settings[$order_col]["orderable"] == "true") {

        if (isset($settings["order_overide"]["$order_col"])) {
            $order_cols = $settings["order_overide"]["$order_col"];
            $order_cols = implode($order_type . ",", $order_cols) . " $order_type";
            $order_string = " order by $order_cols";
        } else {
            $order_string = " order by " . $arr[$order_col] . " " . $order_type;
        }
    }

    //limit
    $start = $settings["start"];
    $length = $settings["length"];
    if ($length != -1) {
        $limit_string = "limit $start,$length";
    }

    return array("search" => $search_string, "limit" => $limit_string, "order" => $order_string);
}
