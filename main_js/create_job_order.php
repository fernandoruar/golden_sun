<?php
$global_id = 0;
if(isset($_GET['id'])){
	$global_id = $_GET['id'];
}
?>

<script>
	var mode = 0;
	var global_id = <?=$global_id?>;
	var plastica = []
	//
	//<input type="text" class="w400 solid_bottom">
        //select2_ajax = ["/getDropdownCustomers","/getDropdownProducts"];
	
		
$(window).load(function(){
	if(global_id!=0){
			mode=1;
		$("#section").disable_fields(true);
		clearFields($('#section').find(".box-body"),{select_load:true});
        $.getJSON("/loadJobOrder", {id: global_id}, function (data) {
			var fixed = data[1][0];
			plastica = data[2];
				//console.log(plastica);
				$('#materials').text(fixed.material);
				$('#customer').text(fixed.customer);
				$('#cylinder_number').text(fixed.cylinder);
				$('#product').text(fixed.product);
				$('#po_number').text(fixed.po_number);
				$('#po_qty').text(fixed.po_qty);
				$('input[data_val='+fixed.winding_direction+']').attr('checked','checked')
            $("#section").loadDetails(data[0]);
			$("#section").disable_fields(false);
			$('.pps').attr('disabled',true);
			for(var x=0;x<plastica.length;x++){
				//console.log(plastica[x]);
				if(plastica[x].status==0){
					//alert(1);
					$('#PTS'+[x]).attr('disabled',false);
					$('#QTY'+[x]).attr('disabled',false);
				}
				$('#PTS'+[x]).val(plastica[x].type_size);
				$('#QTY'+[x]).val(plastica[x].qty);
			}
			
        });
		
        $.getJSON("/loadJobOrderProductionsList", {id: global_id}, function (data) {
			//console.log(data.length);
			for(var y=0;y<data.length;y++){
				var trNew =	document.createElement('tr');
				for(var x=0;x<12;x++){
					var tdHold = document.createElement('td');
					var labelHold = document.createElement('label');
						tdHold.className = "b_all"
					if(x==0){
						tdHold.style="width:115px"
					}else if(x==11){
						tdHold.style="width:154px"
					}else{
						tdHold.style="width:98px"
					}
					labelHold.innerHTML = data[y][x];
					tdHold.appendChild(labelHold);
					trNew.innerHTML += tdHold.outerHTML; 
				}
				$('#production_table').append(trNew);
			}
        });
	}else{
		mode=0;
	}
		
});
    $('#save').on("click", function () {
        if (checkTabs($('#section').find(".details-container"))) {
            return;
        }
		//console.log($('input[name=figs]:checked').attr('data_val'));
        var arr = getFields($('#section'), mode);
		if($('input[name=figs]:checked').attr('data_val')=='undefined'){
			//alert("select")
			return;
		}
		// console.log(arr);
		//arr[1]=arr[0].slice(5,10);
		arr[0].splice(5,10);
		if($('input[name=figs]:checked').attr('data_val')==undefined){
			arr[0].push(0);
		}else{
			arr[0].push($('input[name=figs]:checked').attr('data_val'));
		}
		var arrcon = []
		for(var x=0;x<plastica.length;x++){
			var arrcontainer = [];
			arrcontainer.push($('#PTS'+[x]).val());
			arrcontainer.push($('#QTY'+[x]).val());
			arrcontainer.push(plastica[x].id);
			//arrcontainer.push(plastica[x].id_joborder);
			//arrcontainer.push(0);
			arrcon.push(arrcontainer);
		}
		arr[1] = arrcon;
		//console.log(arr);
		//return;
        $('#section').disable_fields(true);
        $.post("/saveJobOrders", {deleted_fields: deleted_rows, id: global_id, uid: "1", arr: arr, mode: mode, from: 1}, function (data) {
            if (data === 1) {
				$($("#section").find(".alert")[0]).display_alert("success");
				// $($(".main-content").find(".alert")[0]).display_alert("success");
				// $('#dataModal').modal('hide');
				$('#section').disable_fields(false);
				// $('#table_job_order').DataTable().ajax.reload(null, false);
				document.location.href = '/job_orders';
            } else if (data === 0) {
                $($('#section').find(".alert")[0]).display_alert("failed");
                $('#section').disable_fields(false);
            } else if (data === -1) {
                $($('#section').find(".alert")[0]).display_alert({"message": "<i class='fa fa-exclamation-circle'></i>&nbsp;<?= $server_error ?>", "alert_class": "alert alert-danger"});
                $('#section').disable_fields(false);
            }
        }, 'json');
    });
	
/* 	$('#section').find(".select2-field.ajax").each(function () {
                if (parseInt($(this).closest(".details-container").length) > 0 || parseInt($(this).closest("[name='field_container']").length) > 0) {
                    var index = $('#section').find(".select2-field.ajax").index($(this));
                    $(this).ajaxSelect2(select2_ajax[index]);
                }
            });
 */
 
 
 
 
 
 
 
 /* 
                                                        <tr>
                                                            <td class="b_all" style="width:115px">
                                                                <!--input type="text" style="width:105px"-->
                                                                <label></label>
                                                            </td>
                                                            <td class="b_all" style="width:98px">
                                                                <!--input type="text" style="width:88px"-->
                                                                <label></label>
                                                            </td>
                                                            <td class="b_all" style="width:98px">
                                                                <!--input type="text" style="width:88px"-->
                                                                <label></label>
                                                            </td>
                                                            <td class="b_all" style="width:98px">
                                                                <!--input type="text" style="width:88px"-->
                                                                <label></label>
                                                            </td>
                                                            <td class="b_all" style="width:98px">
                                                                <!--input type="text" style="width:88px"-->
                                                                <label></label>
                                                            </td>
                                                            <td class="b_all" style="width:98px">
                                                                <!--input type="text" style="width:88px"-->
                                                                <label></label>
                                                            </td>
                                                            <td class="b_all" style="width:98px">
                                                                <!--input type="text" style="width:88px"-->
                                                                <label></label>
                                                            </td>
                                                            <td class="b_all" style="width:98px">
                                                                <!--input type="text" style="width:88px"-->
                                                                <label></label>
                                                            </td>
                                                            <td class="b_all" style="width:98px">
                                                                <!--input type="text" style="width:88px"-->
                                                                <label></label>
                                                            </td>
                                                            <td class="b_all" style="width:98px">
                                                                <!--input type="text" style="width:88px"-->
                                                                <label></label>
                                                            </td>
                                                            <td class="b_all" style="width:98px">
                                                                <!--input type="text" style="width:88px"-->
                                                                <label></label>
                                                            </td>
                                                            <td class="b_all" style="width:154px">
                                                                <!--input type="text" style="width:154px"-->
                                                                <label></label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="b_all">
                                                                <!--input type="text" style="width:105px"-->
                                                                <label></label>
                                                            </td>
                                                            <td class="b_all">
                                                                <!--input type="text" style="width:88px"-->
                                                                <label></label>
                                                            </td>
                                                            <td class="b_all">
                                                                <!--input type="text" style="width:88px"-->
                                                                <label></label>
                                                            </td>
                                                            <td class="b_all">
                                                                <!--input type="text" style="width:88px"-->
                                                                <label></label>
                                                            </td>
                                                            <td class="b_all">
                                                                <!--input type="text" style="width:88px"-->
                                                                <label></label>
                                                            </td>
                                                            <td class="b_all">
                                                                <!--input type="text" style="width:88px"-->
                                                                <label></label>
                                                            </td>
                                                            <td class="b_all">
                                                                <!--input type="text" style="width:88px"-->
                                                                <label></label>
                                                            </td>
                                                            <td class="b_all">
                                                                <!--input type="text" style="width:88px"-->
                                                                <label></label>
                                                            </td>
                                                            <td class="b_all">
                                                                <!--input type="text" style="width:88px"-->
                                                                <label></label>
                                                            </td>
                                                            <td class="b_all">
                                                                <!--input type="text" style="width:88px"-->
                                                                <label></label>
                                                            </td>
                                                            <td class="b_all">
                                                                <!--input type="text" style="width:88px"-->
                                                                <label></label>
                                                            </td>
                                                            <td class="b_all">
                                                                <!--input type="text" style="width:154px"-->
                                                                <label></label>
                                                            </td>
                                                        </tr>
 */
</script>