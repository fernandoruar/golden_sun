<script>
    $('#nav_customer_pos').addClass('active');
    var mode = -1;
    var global_id = 0;
	
    var clone_error = $.extend(true, {}, $.fn.display_alert.server_problem_defaults);
    var clone_success = $.extend(true, {}, $.fn.display_alert.success_defaults);
    $('#table_multiple').fieldTable({searching: false, ordering: false});
	
	
	var prodtable = {searching: false, ordering: false, 
		"columns": [
			{data: "id"},
			{data: "products"},
			{data: "id_product"},
			{data: "specification"},
			{data: "po_qty"},
			{data: "price"},
			{data: "total"},
			{data: "button"}
		]
	};
	
			prodtable.created_row_function = function (row,data){
				var total = parseFloat($(row).find("[type='number']:eq(0)").val())*parseFloat($(row).find("[type='number']:eq(1)").val());
				$(row).find("td:eq(6)").html(total);
				$(row).find("[type='number']").on('change',function(){
					var total = parseFloat($(row).find("[type='number']:eq(0)").val())*parseFloat($(row).find("[type='number']:eq(1)").val());
					$(row).find("td:eq(6)").html(total);
				});
				$.getJSON("/getProductSpecs", {id: $(row).find("[type='hidden']").val()}, function (data) {
					$(row).find("td:eq(3)").html(data[0].plastic);
				});
			}
			
    $('#product_table').fieldTable(prodtable);
	///CustomerPO 
		var cust = 0; 
        select2_ajax = ["/getDropdownCustomers","/getDropdownProductsPerCustomer?cust="+cust];
	
		$('#id_customer').on('change',function(){
			cust = $(this).val();
			select2_ajax = ["/getDropdownCustomers","/getDropdownProductsPerCustomer?cust="+cust];
			if(cust!=null){
				$('#product_table').fieldTable(prodtable);
				$('#dataModal').find(".select2-field.ajax:eq(1)").ajaxSelect2({queued:true,url:select2_ajax[1]});
			}
		});
	
	$('#table_customer_po').serverTable({
		method:"POST",url: "/getAllCustomerPO", 
		extra_data: {
			from: function () {
                return 1;
            }
		},
		columns: [
            {"data": "id"},
            {"data": "date", name: "<?= $date ?>"},
            {"data": "customer", name: "<?= $jo_number ?>"},
            {"data": "po_number", name: "<?= $po_number ?>"},
            {"data": "status", name: "<?= $status ?>"},
            {"data": "button"}
        ]
	});
	
    $('#abutton').on("click", function () {
		mode = 0;
		global_id = 0;
		// document.location.href="/create_customer_po?id="+global_id;
        $('#dataModal').disable_fields(false);
        clearFields($('#dataModal').find(".modal-body"));
        $('#dataModal').find("[name='title']").html("<?= $add ?>");
        $('#dataModal').find(".sample").attr("src","");
        $('#dataModal').modal("show");
    });
	
    $('#abutton_multiple').on("click", function () {
        mode = 0;
        $('#dataModal').disable_fields(false);
        clearFields($('#dataModal_Multiple').find(".modal-body"));
        $('#dataModal_Multiple').find("[name='title']").html("<?= $add ?>(<?= $multiple ?>)");
        $('#dataModal_Multiple').modal("show");
    });
    $('#add_multiple').on("click", function () {
        $('#table_multiple').addFieldRow($('#dataModal_Multiple').find("div[name='field_container']"));
    });
	
    $('#add_product').on("click", function () {
        $('#product_table').addFieldRow($('#job_details').find("div[name='field_container']"));
		scoreme();
    });
	
    $('#save').on("click", function () {
        if (checkTabs($('#dataModal').find(".tab-container")[0])) {
            return;
        }
		
        var arr = getFields($('#dataModal'), mode);
		
		
        $('#dataModal').disable_fields(true);
		
//$_SESSION[getSessionName()]["id"]<?  ?>
        $.post("/saveCustomerPO", {deleted_fields: deleted_rows, id: global_id, uid: "1", arr: arr, mode: mode, from: 1}, function (data) {
            if (data === 1) {
				$($(".main-content").find(".alert")[0]).display_alert("success");
				$('#dataModal').modal('hide');
				$('#dataModal').disable_fields(false);
				$('#table_customer_po').DataTable().ajax.reload(null, false);
            } else if (data === 0) {
                $($('#dataModal').find(".alert")[0]).display_alert("failed");
                $('#dataModal').disable_fields(false);
            } else if (data === -1) {
                $($('#dataModal').find(".alert")[0]).display_alert({"message": "<i class='fa fa-exclamation-circle'></i>&nbsp;<?= $server_error ?>", "alert_class": "alert alert-danger"});
                $('#dataModal').disable_fields(false);
            }
        }, 'json');
    });
	
	
    $("#save_multiple").on("click", function () {
        if (checkTable($('#dataModal_Multiple').find(".table-container"))) {
            return;
        }
        if (checkDuplicates([1], $('#dataModal_Multiple').find(".table-container").find("table")[0])) {
            $('#dataModal_Multiple').find(".table-container").find(".alert").display_alert({message: "<i class='fa fa-exclamation-circle'></i>&nbsp;<?= $duplicate_error ?>", alert_class: "alert alert-danger"});
            return;
        }
        var arr = getFields($('#dataModal_Multiple'));

        $('#dataModal_Multiple').disable_fields(true);
        $.post("/saveCustomerPOMultiple", {id: global_id, uid: "<?= $_SESSION[getSessionName()]["id"] ?>", arr: arr}, function (data) {
            if (data === 1) {
                $($(".main-content").find(".alert")[0]).display_alert("success");
                $('#dataModal_Multiple').modal('hide');
                $('#table_customer_po').DataTable().ajax.reload(null, false);
            } else if (data === 0) {
                $($('#dataModal_Multiple').find(".alert")[0]).display_alert("failed");
                $('#dataModal_Multiple').disable_fields(false);
            } else if (data === -1) {
                $($('#dataModal_Multiple').find(".alert")[0]).display_alert({"message": "<i class='fa fa-exclamation-circle'></i>&nbsp;<?= $customer_po_error2 ?>", "alert_class": "alert alert-danger"});
                $('#dataModal_Multiple').disable_fields(false);
            }
        }, 'json');
    });
	var edit_data = [];
    function editCustomerPO(item) {
        mode = 1;
        $('#dataModal').find("[name='title']").html("<?= $edit ?>");
        var row_details = $('#table_customer_po').DataTable().row($(item).parents("tr")).data();
        global_id = row_details.id;
		// document.location.href="/create_customer_po?id="+global_id;
        $("#dataModal").disable_fields(true);
        $(item).find(".loading").css("display", "");
        clearFields($('#dataModal').find(".modal-body"),{select_load:true});
        $.getJSON("/loadCustomerPO", {id: row_details.id}, function (data) {
			edit_data = data;
            $("#dataModal").loadDetails(data[0]);
			$("#dataModal").disable_fields(false);
            //$('#dataModal').modal("show");
			
			$('#dataModal').find(".select2-field.ajax").each(function () {
                if (parseInt($(this).closest(".details-container").length) > 0 || parseInt($(this).closest("[name='field_container']").length) > 0) {
                    var index = $('#dataModal').find(".select2-field.ajax").index($(this));
                    queued_ajaxs.push({element: this, options: {queued: true, url: select2_ajax[index]}});
                }
            });
			$('#dataModal').find(".select2-field.ajax:eq(0)").ajaxSelect2({queued:true,url:select2_ajax[0]});
        });
		
		
    }
	
	 end_queue_function = function () {
        if (parseInt(mode) === 1) {

            $('#dataModal').on("shown.bs.modal", function () {
				$('#product_table').fieldTable(prodtable);
                $('#product_table').DataTable().rows.add(edit_data[1]).draw(); 
                $('#dataModal').off("shown.bs.modal");
            });

            $('#dataModal').modal("show");
        }
    }
    function deleteCustomerPO(item) {
        clearFields($('#deleteModal'));
        var row_details = $('#table_customer_po').DataTable().row($(item).parents("tr")).data();
        global_id = row_details.id;
        $('#deleteModal').find("[name='message']").html("<?= $delete_message ?> <strong><?= $module_name ?>:&nbsp;" + row_details.customer + "</strong>?");
        $('#deleteModal').modal("show");
    }
	
    $('#delete_yes').on("click", function () {
        $('#deleteModal').disable_fields(true);
        $.post("/deleteData", {mode: 12, id: global_id}, function (data) {
            if (data === 1) {
                clone_success.message = "<?= $module_name . ' ' . $sdeleted ?>"
                $($(".main-content").find(".alert")[0]).display_alert(clone_success);
                $('#deleteModal').disable_fields(false);
                $('#deleteModal').modal('hide');
                $('#table_customer_po').DataTable().ajax.reload(null, false);
            } else if (data === 0) {
                $($("#deleteModal").find(".alert")[0]).display_alert("failed");
                $('#deleteModal').disable_fields(false);
            } else if (data === -1) {
                clone_error.message = "<?= $conflict_found ?>";
                $($("#deleteModal").find(".alert")[0]).display_alert(clone_error);
                $('#deleteModal').disable_fields(false);
            }

        }, 'json');
    });
	
</script>