<script>
    $('#nav_chemicals_po').addClass('active');
    var mode = -1;
    var global_id = 0;
	
    var clone_error = $.extend(true, {}, $.fn.display_alert.server_problem_defaults);
    var clone_success = $.extend(true, {}, $.fn.display_alert.success_defaults);
    $('#table_multiple').fieldTable({searching: false, ordering: false});
	
	
        select2_ajax = ["/getDropdownChemicalsPO"];
	
	
	///InChemicals
	
	
	$('#table_chemical_po').serverTable({
		method:"POST",url: "/getAllInChemicals", 
		extra_data: {
			from: function () {
                return 1;
            }
		},
		columns: [
            {"data": "id"},
            {"data": "name", name: "<?= $name ?>"},
            {"data": "qty", name: "<?= $qty?>"},
            {"data": "button"}
        ]
	});
	
	$('#table_chemical_logs').serverTable({
		method:"POST",url: "/getAllInChemicalsLogs", 
		extra_data: {
			from: function () {
                return 1;
            }
		},
		columns: [
            {"data": "id"},
            {"data": "issued_on", name: "<?= $date ?>"},
            {"data": "name", name: "<?= $name ?>"},
            {"data": "typed", name: "<?= $type ?>"},
            {"data": "initial_qty", name: "<?= $initial_qty?>"},
            {"data": "qty", name: "<?= $qty?>"},
            {"data": "remaining_qty", name: "<?= $final_qty?>"},
            {"data": "button"}
        ]
	});
	
	
    $('#abutton_multiple').on("click", function () {
        mode = 0;
        $('#dataModal_logs').disable_fields(false);
        clearFields($('#dataModal_logs').find(".modal-body"));
        $('#dataModal_logs').find("[name='title']").html("<?= $add ?>(<?= $multiple ?>)");
        $('#dataModal_logs').modal("show");
		$('input[name="inout"][data_val=0]').prop( "checked", true );
		//alert($('input[name="inout"]:checked').attr( "data_val"));
    });
    $('#add_multiple').on("click", function () {
		var json_holder = JSON.parse($('#items>option:selected').attr('json'));
        $('#table_multiple').addFieldRow($('#dataModal_logs').find("div[name='field_container']"));
		$('#table_multiple tr:last').find('input[type=number]').attr('data_qty',json_holder.qty);
		scoreme();

    });
	
	
    $('#save').on("click", function () {
        if (checkFields($('#dataModal').find(".details-container")[0]).status) {
            return;
        }
		
		
        var arr = getFields($('#dataModal'), mode);
        $('#dataModal').disable_fields(true);
		
//$_SESSION[getSessionName()]["id"]<?  ?>
        $.post("/saveInChemicals", {deleted_fields: deleted_rows, id: global_id, uid: "1", arr: arr, mode: mode, from: 1}, function (data) {
            if (data === 1) {
				$($("#section").find(".alert")[0]).display_alert("success");
				$('#dataModal').modal('hide');
				$('#dataModal').disable_fields(false);
				$('#table_chemical_po').DataTable().ajax.reload(null, false);
				$('#table_chemical_logs').DataTable().ajax.reload(null, false);
            } else if (data === 0) {
                $($('#dataModal').find(".alert")[0]).display_alert("failed");
                $('#dataModal').disable_fields(false);
            } else if (data === -1) {
                $($('#dataModal').find(".alert")[0]).display_alert({"message": "<i class='fa fa-exclamation-circle'></i>&nbsp;<?= $chemical_error ?>", "alert_class": "alert alert-danger"});
                $('#dataModal').disable_fields(false);
            }
        }, 'json');
    });
	
	
    $("#save_multiple").on("click", function () {
        if (checkTable($('#dataModal_logs').find(".table-container"))) {
            return;
        }
        /* if (checkDuplicates([1], $('#dataModal_logs').find(".table-container").find("table")[0])) {
            $('#dataModal_logs').find(".table-container").find(".alert").display_alert({message: "<i class='fa fa-exclamation-circle'></i>&nbsp;<?= $duplicate_error ?>", alert_class: "alert alert-danger"});
            return;
        } */
		
		var type = $('input[name="inout"]:checked').attr( "data_val");
        var arr = getFields($('#dataModal_logs'));
		var log_counter = 0;
		$('#table_multiple>tbody').find('tr').each(function(){
			// console.log($(this).find('input[type=number]').val());
			// console.log($(this).find('input[type=number]').attr('data_qty'));
			var log_qty =  parseInt($(this).find('input[type=number]').attr('data_qty'));
			var log_val = parseInt($(this).find('input[type=number]').val());
			
			arr[0][log_counter].push(log_qty);
			if(type==0){
				arr[0][log_counter].push(log_qty+log_val);
			}else if(type==1){
				arr[0][log_counter].push(log_qty-log_val);
			}
			log_counter++;
		});
		// return;
        $('#dataModal_logs').disable_fields(true);
        $.post("/saveInChemicalLogs", {id: global_id, type: type, uid: "<?= $_SESSION[getSessionName()]["id"] ?>", arr: arr}, function (data) {
            if (data === 1) {
                $($("#logs").find(".alert")[0]).display_alert("success");
                $('#dataModal_logs').modal('hide');
				$('#table_chemical_po').DataTable().ajax.reload(null, false);
				$('#table_chemical_logs').DataTable().ajax.reload(null, false);
            } else if (data === 0) {
                $($('#dataModal_logs').find(".alert")[0]).display_alert("failed");
                $('#dataModal_logs').disable_fields(false);
            } else if (data === -1) {
                $($('#dataModal_logs').find(".alert")[0]).display_alert({"message": "<i class='fa fa-exclamation-circle'></i>&nbsp;<?= $chemical_error2 ?>", "alert_class": "alert alert-danger"});
                $('#dataModal_logs').disable_fields(false);
            }
        }, 'json');
    });
	
    function editChemical(item) {
        mode = 1;
        $('#dataModal').find("[name='title']").html("<?= $edit ?>");
        var row_details = $('#table_chemical_logs').DataTable().row($(item).parents("tr")).data();
        global_id = row_details.id;
        $("#dataModal").disable_fields(true);
        $(item).find(".loading").css("display", "");
        clearFields($('#dataModal').find(".modal-body"),{select_load:true});
        $.getJSON("/loadChemical", {id: row_details.id}, function (data) {
            $("#dataModal").loadDetails(data[0]);
			$("#dataModal").disable_fields(false);
            $('#dataModal').modal("show");
        });
    }
	
    function deleteChemical(item) {
        clearFields($('#deleteModal'));
        var row_details = $('#table_chemical_logs').DataTable().row($(item).parents("tr")).data();
        global_id = row_details.id;
        $('#deleteModal').find("[name='message']").html("<?= $delete_message ?> <strong><?= $module_name ?>:&nbsp;" + row_details.name + "</strong>?");
        $('#deleteModal').modal("show");
    }
	
    $('#delete_yes').on("click", function () {
        $('#deleteModal').disable_fields(true);
        $.post("/deleteData", {mode: 6, id: global_id}, function (data) {
            if (data === 1) {
                clone_success.message = "<?= $module_name . ' ' . $sdeleted ?>"
                $($("#section").find(".alert")[0]).display_alert(clone_success);
                $('#deleteModal').disable_fields(false);
                $('#deleteModal').modal('hide');
                $('#table_chemical_logs').DataTable().ajax.reload(null, false);
            } else if (data === 0) {
                $($("#deleteModal").find(".alert")[0]).display_alert("failed");
                $('#deleteModal').disable_fields(false);
            } else if (data === -1) {
                clone_error.message = "<?= $conflict_found ?>";
                $($("#deleteModal").find(".alert")[0]).display_alert(clone_error);
                $('#deleteModal').disable_fields(false);
            }

        }, 'json');
    });
	
</script>