<script>
    $('#nav_cylinders').addClass('active');
    var mode = -1;
    var global_id = 0;
	
    var clone_error = $.extend(true, {}, $.fn.display_alert.server_problem_defaults);
    var clone_success = $.extend(true, {}, $.fn.display_alert.success_defaults);
    $('#table_multiple').fieldTable({searching: false, ordering: false});
	
	
	
	///Cylinders
	
	
	$('#table_cylinder').serverTable({
		method:"POST",url: "/getAllCylinders", 
		extra_data: {
			from: function () {
                return 1;
            }
		},
		columns: [
            {"data": "id"},
            {"data": "color", name: "<?= $cylinder.'_ID' ?>"},//$color
            {"data": "size", name: "<?= $description?>"},//$size
            {"data": "minimum", name: "<?= $minimum ?>"},
            {"data": "starting_qty", name: "<?= $initial_qty ?>"},
            {"data": "price", name: "<?= $price ?>"},
            {"data": "button"}
        ]
	});
	
    $('#abutton').on("click", function () {
		mode = 0;
		global_id = 0;
        $('#dataModal').disable_fields(false);
        clearFields($('#dataModal').find(".modal-body"));
        $('#dataModal').find("[name='title']").html("<?= $add ?>");
        $('#dataModal').find(".sample").attr("src","");
        $('#dataModal').modal("show");
    });
	
    $('#abutton_multiple').on("click", function () {
        mode = 0;
        $('#dataModal').disable_fields(false);
        clearFields($('#dataModal_Multiple').find(".modal-body"));
        $('#dataModal_Multiple').find("[name='title']").html("<?= $add ?>(<?= $multiple ?>)");
        $('#dataModal_Multiple').modal("show");
    });
    $('#add_multiple').on("click", function () {

        $('#table_multiple').addFieldRow($('#dataModal_Multiple').find("div[name='field_container']"));
		scoreme();

    });
	
	
    $('#save').on("click", function () {
        if (checkFields($('#dataModal').find(".details-container")[0]).status) {
            return;
        }
		
		
        var arr = getFields($('#dataModal'), mode);
        $('#dataModal').disable_fields(true);
		
//$_SESSION[getSessionName()]["id"]<?  ?>
        $.post("/saveCylinders", {deleted_fields: deleted_rows, id: global_id, uid: "1", arr: arr, mode: mode, from: 1}, function (data) {
            if (data === 1) {
				$($("#section").find(".alert")[0]).display_alert("success");
				$('#dataModal').modal('hide');
				$('#dataModal').disable_fields(false);
				$('#table_cylinder').DataTable().ajax.reload(null, false);
            } else if (data === 0) {
                $($('#dataModal').find(".alert")[0]).display_alert("failed");
                $('#dataModal').disable_fields(false);
            } else if (data === -1) {
                $($('#dataModal').find(".alert")[0]).display_alert({"message": "<i class='fa fa-exclamation-circle'></i>&nbsp;<?= $cylinder_error ?>", "alert_class": "alert alert-danger"});
                $('#dataModal').disable_fields(false);
            }
        }, 'json');
    });
	
	
    $("#save_multiple").on("click", function () {
        if (checkTable($('#dataModal_Multiple').find(".table-container"))) {
            return;
        }
        if (checkDuplicates([1], $('#dataModal_Multiple').find(".table-container").find("table")[0])) {
            $('#dataModal_Multiple').find(".table-container").find(".alert").display_alert({message: "<i class='fa fa-exclamation-circle'></i>&nbsp;<?= $duplicate_error ?>", alert_class: "alert alert-danger"});
            return;
        }
        var arr = getFields($('#dataModal_Multiple'));

        $('#dataModal_Multiple').disable_fields(true);
        $.post("/saveCylinderMultiple", {id: global_id, uid: "<?= $_SESSION[getSessionName()]["id"] ?>", arr: arr}, function (data) {
            if (data === 1) {
                $($("#section").find(".alert")[0]).display_alert("success");
                $('#dataModal_Multiple').modal('hide');
                $('#table_cylinder').DataTable().ajax.reload(null, false);
            } else if (data === 0) {
                $($('#dataModal_Multiple').find(".alert")[0]).display_alert("failed");
                $('#dataModal_Multiple').disable_fields(false);
            } else if (data === -1) {
                $($('#dataModal_Multiple').find(".alert")[0]).display_alert({"message": "<i class='fa fa-exclamation-circle'></i>&nbsp;<?= $cylinder_error2 ?>", "alert_class": "alert alert-danger"});
                $('#dataModal_Multiple').disable_fields(false);
            }
        }, 'json');
    });
	
    function editCylinder(item) {
        mode = 1;
        $('#dataModal').find("[name='title']").html("<?= $edit ?>");
        var row_details = $('#table_cylinder').DataTable().row($(item).parents("tr")).data();
        global_id = row_details.id;
        $("#dataModal").disable_fields(true);
        $(item).find(".loading").css("display", "");
        clearFields($('#dataModal').find(".modal-body"),{select_load:true});
        $.getJSON("/loadCylinder", {id: row_details.id}, function (data) {
            $("#dataModal").loadDetails(data[0]);
			$("#dataModal").disable_fields(false);
            $('#dataModal').modal("show");
        });
    }
	
    function deleteCylinder(item) {
        clearFields($('#deleteModal'));
        var row_details = $('#table_cylinder').DataTable().row($(item).parents("tr")).data();
        global_id = row_details.id;
        $('#deleteModal').find("[name='message']").html("<?= $delete_message ?> <strong><?= $module_name ?>:&nbsp;" + row_details.color + "</strong>?");
        $('#deleteModal').modal("show");
    }
	
    $('#delete_yes').on("click", function () {
        $('#deleteModal').disable_fields(true);
        $.post("/deleteData", {mode: 10, id: global_id}, function (data) {
            if (data === 1) {
                clone_success.message = "<?= $module_name . ' ' . $sdeleted ?>"
                $($("#section").find(".alert")[0]).display_alert(clone_success);
                $('#deleteModal').disable_fields(false);
                $('#deleteModal').modal('hide');
                $('#table_cylinder').DataTable().ajax.reload(null, false);
            } else if (data === 0) {
                $($("#deleteModal").find(".alert")[0]).display_alert("failed");
                $('#deleteModal').disable_fields(false);
            } else if (data === -1) {
                clone_error.message = "<?= $conflict_found ?>";
                $($("#deleteModal").find(".alert")[0]).display_alert(clone_error);
                $('#deleteModal').disable_fields(false);
            }

        }, 'json');
    });
	
</script>