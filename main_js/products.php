<script>
    $('#nav_products').addClass('active');
    var mode = -1;
    var global_id = 0;
	
    var clone_error = $.extend(true, {}, $.fn.display_alert.server_problem_defaults);
    var clone_success = $.extend(true, {}, $.fn.display_alert.success_defaults);
    $('#table_multiple').fieldTable({searching: false, ordering: false});
    $('#plastic_table').fieldTable({searching: false, ordering: false, "columns": [{data: "id"}, {data: "id_plastic"}, {data: "size"}, {data: "thickness"}, {data: "button"}]})
	
	
        select2_ajax = ["/getDropdownCustomers", "/getDropdownInks", "/getDropdownAdhesives", "/getDropdownChemicals", "/getDropdownCylinders", "/getDropdownPlastic"];
	
	///Products
	
	
	$('#table_product').serverTable({
		method:"POST",url: "/getAllProducts", 
		extra_data: {
			from: function () {
                return 1;
            }
		},
		columns: [
            {"data": "id"},
            {"data": "name", name: "<?= $name ?>"},
            {"data": "date", name: "<?= $date?>"},
            {"data": "customer", name: "<?= $customers ?>"},
            {"data": "plastic", name: "<?= $plastics ?>"},
            {"data": "button"}
        ]
	});
	
    $('#abutton').on("click", function () {
		mode = 0;
		global_id = 0;
        $('#dataModal').disable_fields(false);
        clearFields($('#dataModal').find(".modal-body"));
        $('#dataModal').find("[name='title']").html("<?= $add ?>");
        $('#dataModal').find(".sample").attr("src","");
        $('#dataModal').modal("show");
    });
	
    $('#abutton_multiple').on("click", function () {
        mode = 0;
        $('#dataModal').disable_fields(false);
        clearFields($('#dataModal_Multiple').find(".modal-body"));
        $('#dataModal_Multiple').find("[name='title']").html("<?= $add ?>(<?= $multiple ?>)");
        $('#dataModal_Multiple').modal("show");
    });
    $('#add_plastic').on("click", function () {
        $('#plastic_table').addFieldRow($('#plastic_details').find("div[name='field_container']"));
    });
    $('#add_multiple').on("click", function () {

        $('#table_multiple').addFieldRow($('#dataModal_Multiple').find("div[name='field_container']"));
		scoreme();

    });
	
	
    $('#save').on("click", function () {
        if (checkTabs($('#dataModal').find(".tab-container")[0])) {
            return;
        }
		
        var arr = getFields($('#dataModal'), mode);
		//console.log(arr);
		//return;
        $('#dataModal').disable_fields(true);
		
//$_SESSION[getSessionName()]["id"]<?  ?>
        $.post("/saveProducts", {deleted_fields: deleted_rows, id: global_id, uid: "1", arr: arr, mode: mode, from: 1}, function (data) {
            if (data === 1) {
				$($(".main-content").find(".alert")[0]).display_alert("success");
				$('#dataModal').modal('hide');
				$('#dataModal').disable_fields(false);
				$('#table_product').DataTable().ajax.reload(null, false);
            } else if (data === 0) {
                $($('#dataModal').find(".alert")[0]).display_alert("failed");
                $('#dataModal').disable_fields(false);
            } else if (data === -1) {
                $($('#dataModal').find(".alert")[0]).display_alert({"message": "<i class='fa fa-exclamation-circle'></i>&nbsp;<?= $server_error ?>", "alert_class": "alert alert-danger"});
                $('#dataModal').disable_fields(false);
            }
        }, 'json');
    });
	
	
    $("#save_multiple").on("click", function () {
        if (checkTable($('#dataModal_Multiple').find(".table-container"))) {
            return;
        }
        if (checkDuplicates([1], $('#dataModal_Multiple').find(".table-container").find("table")[0])) {
            $('#dataModal_Multiple').find(".table-container").find(".alert").display_alert({message: "<i class='fa fa-exclamation-circle'></i>&nbsp;<?= $duplicate_error ?>", alert_class: "alert alert-danger"});
            return;
        }
        var arr = getFields($('#dataModal_Multiple'));

        $('#dataModal_Multiple').disable_fields(true);
        $.post("/saveProductMultiple", {id: global_id, uid: "<?= $_SESSION[getSessionName()]["id"] ?>", arr: arr}, function (data) {
            if (data === 1) {
                $($(".main-content").find(".alert")[0]).display_alert("success");
                $('#dataModal_Multiple').modal('hide');
                $('#table_product').DataTable().ajax.reload(null, false);
            } else if (data === 0) {
                $($('#dataModal_Multiple').find(".alert")[0]).display_alert("failed");
                $('#dataModal_Multiple').disable_fields(false);
            } else if (data === -1) {
                $($('#dataModal_Multiple').find(".alert")[0]).display_alert({"message": "<i class='fa fa-exclamation-circle'></i>&nbsp;<?= $product_error2 ?>", "alert_class": "alert alert-danger"});
                $('#dataModal_Multiple').disable_fields(false);
            }
        }, 'json');
    });
	var edit_data = [];
    function editProduct(item) {
        mode = 1;
        $('#dataModal').find("[name='title']").html("<?= $edit ?>");
        var row_details = $('#table_product').DataTable().row($(item).parents("tr")).data();
        global_id = row_details.id;
        $("#dataModal").disable_fields(true);
        $(item).find(".loading").css("display", "");
        clearFields($('#dataModal').find(".modal-body"),{select_load:true});
        $.getJSON("/loadProduct", {id: row_details.id}, function (data) {
			edit_data = data;
            $("#dataModal").loadDetails(data[0]);
			$("#dataModal").disable_fields(false);
            //$('#dataModal').modal("show");
			
			$('#dataModal').find(".select2-field.ajax").each(function () {
                if (parseInt($(this).closest(".details-container").length) > 0 || parseInt($(this).closest("[name='field_container']").length) > 0) {
                    var index = $('#dataModal').find(".select2-field.ajax").index($(this));
                    queued_ajaxs.push({element: this, options: {queued: true, url: select2_ajax[index]}});
                }
            });
			$('#dataModal').find(".select2-field.ajax:eq(0)").ajaxSelect2({queued:true,url:select2_ajax[0]});
        });
		
		
    }
	
	 end_queue_function = function () {
        if (parseInt(mode) === 1) {

            $('#dataModal').on("shown.bs.modal", function () {
				console.log(edit_data[1]);
                $('#plastic_table').DataTable().rows.add(edit_data[1]).draw();
                $('#dataModal').off("shown.bs.modal");
            });

            $('#dataModal').modal("show");
        }
    }
    function deleteProduct(item) {
        clearFields($('#deleteModal'));
        var row_details = $('#table_product').DataTable().row($(item).parents("tr")).data();
        global_id = row_details.id;
        $('#deleteModal').find("[name='message']").html("<?= $delete_message ?> <strong><?= $module_name ?>:&nbsp;" + row_details.name + "</strong>?");
        $('#deleteModal').modal("show");
    }
	
    $('#delete_yes').on("click", function () {
        $('#deleteModal').disable_fields(true);
        $.post("/deleteData", {mode: 11, id: global_id}, function (data) {
            if (data === 1) {
                clone_success.message = "<?= $module_name . ' ' . $sdeleted ?>"
                $($(".main-content").find(".alert")[0]).display_alert(clone_success);
                $('#deleteModal').disable_fields(false);
                $('#deleteModal').modal('hide');
                $('#table_product').DataTable().ajax.reload(null, false);
            } else if (data === 0) {
                $($("#deleteModal").find(".alert")[0]).display_alert("failed");
                $('#deleteModal').disable_fields(false);
            } else if (data === -1) {
                clone_error.message = "<?= $conflict_found ?>";
                $($("#deleteModal").find(".alert")[0]).display_alert(clone_error);
                $('#deleteModal').disable_fields(false);
            }

        }, 'json');
    });
	
</script>