<script>

    $('body').addClass('loginbg');


		 $(document).keypress(function(event) {
			var key = window.event ? event.keyCode : event.which;
			if (key == 13) {
				$('#signin_btn').click();
			}
			else return true;
		});
		
    if (window.localStorage.getItem("etech_login_fields") !== null) {
        var etech_login_fields = JSON.parse(window.localStorage.getItem("etech_login_fields"));
        $('#username').val(etech_login_fields.username);
        $('#password').val(etech_login_fields.password);
        $('#remember_me').prop("checked", true);
    }

    $('#signin_btn').on('click', function (e) {
        e.preventDefault();
        if (checkFields($('#login_container')).status) {
            return;
        }
        var item = this;
        $(item).find("#loading").css("display", "");
        $('#login_container').disable_fields(true);
        $.post("/checkAccount", {username: $('#username').val(), password: $('#password').val()}, function (data) {
            if (data === 1) {
                if ($('#remember_me').prop("checked")) {
                    window.localStorage.setItem("etech_login_fields", JSON.stringify({"username": $('#username').val(), "password": $('#password').val()}));
                }
                document.location.reload();
            } else if (data === 0) {
                $.fn.display_alert.server_problem_defaults.message = "<i class='fa fa-exclamation-circle'></i>&nbsp;<?= $invalid_login ?>";
                $('#login_container').find(".alert").display_alert("failed");
                $(item).find("#loading").css("display", "none");
                $('#login_container').disable_fields(false);
            }
        }, 'json');
    });

</script>