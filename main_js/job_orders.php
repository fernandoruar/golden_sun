<script>
    $('#nav_job_orders').addClass('active');
    var mode = -1;
    var global_id = 0;
    var global_item = [];
	//Field Only
		$('#table_job_order_production').fieldTable({
			searching: false,
			ordering: false, 
			columns: [
				{"data": "id"},
				{"data": "date", name: "<?= $date ?>"},
				{"data": "pd_roll", name: "<?= $printing_dept.'-'.$roll ?>"},
				{"data": "pd_weight", name: "<?= $printing_dept.'-'.$weight ?>"},
				{"data": "ld_roll", name: "<?= $lamination_dept.'-'.$roll ?>"},
				{"data": "ld_weight", name: "<?= $lamination_dept.'-'.$weight ?>"},
				{"data": "sd_roll", name: "<?= $slitting_dept.'-'.$roll ?>"},
				{"data": "sd_weight", name: "<?= $slitting_dept.'-'.$weight ?>"},
				{"data": "bf_roll", name: "<?= $bagforming_dept.'-'.$roll ?>"},
				{"data": "bf_weight", name: "<?= $bagforming_dept.'-'.$weight ?>"},
				//{"data": "lod_dr_number", name: "<?= $logistic_dept.'-DR#' ?>"},
				//{"data": "lod_wieght", name: "<?= $logistic_dept.'-'.$weight ?>"},
				{"data": "remarks", name: "<?= $remarks ?>"},
				{"data": "btn"}
			],
			columnDefs: [
				{
				  targets: -2,
				  className: 'breakingline'
				}
			  ]
		});
		$('#table_job_order_delivery').fieldTable({
			searching: false,
			ordering: false, 
			columns: [
				{"data": "id"},
				{"data": "remaining", name: "<?= $total_remaining ?>"},
				{"data": "qty", name: "<?= $qty ?>"},
				{"data": "unit", name: "<?= $unit ?>"},
				{"data": "description", name: "<?= $description ?>"},
				{"data": "price", name: "<?= $price ?>"},
				{"data": "amount", name: "<?= $amount ?>"},
				//{"data": "btn"}
			],
			columnDefs: [
				{
				  targets: -2,
				  className: 'breakingline'
				}
			  ]
		});
	
	//Modal Listing
	var addRowArray = [{
					"id":"<input class='listing' type='text' value='0'>",
					"date":"<input type='text' class='datepicker form-control' placeholder='<?= $prod_date ?>' style='width: 150px;'>",
					"pd_roll":"<input  type='text' class='nimp form-control ' placeholder='<?= $roll ?>'>",
					"pd_weight":"<input  type='text' class='nimp form-control ' placeholder='<?= $weight ?>'>",
					"ld_roll":"<input  type='text' class='nimp form-control' placeholder='<?= $roll ?>'>",
					"ld_weight":"<input  type='text' class='nimp form-control' placeholder='<?= $weight ?>'>",
					"sd_roll":"<input  type='text' class='nimp form-control ' placeholder='<?= $roll ?>'>",
					"sd_weight":"<input  type='text' class='nimp form-control' placeholder='<?= $weight ?>'>",
					// +
								// "<input  type='radio' name='deliverys"+counter+"' data_val='1' class='delivery'><?= $deliver ?>",
					"bf_roll":"<input  type='text' class='nimp form-control ' placeholder='<?= $roll ?>'>",
					"bf_weight":"<input  type='text' class='nimp form-control' placeholder='<?= $weight ?>'>",
					// +
								// "<input  type='radio' name='deliverys"+counter+"' data_val='2' class='delivery'><?= $deliver ?>",
					"lod_dr_number":"<input  type='text' class='nimp form-control' placeholder='DR#'>",
					//"lod_wieght":"<input  type='text' class='nimp form-control' placeholder='<?= $weight ?>'>"+
								//"<input  type='radio' name='deliverys"+counter+"' data_val='3' class='delivery'><?= $deliver ?>",
					"remarks":"<textarea class='nimp form-control' placeholder='<?= $remarks ?>'></textarea>",
					"button":"<button type='button' class='deleteRow"+counter+" btn btn-danger btn-flat btn-sm pull-left'><i class='fa fa-trash'></i></button>"
				}];
				
	$(".datepicker").datepicker({ dateFormat: 'yy-mm-dd' });
    var clone_error = $.extend(true, {}, $.fn.display_alert.server_problem_defaults);
    var clone_success = $.extend(true, {}, $.fn.display_alert.success_defaults);
	
	///JobOrders
	
	
	$('#table_job_order').serverTable({
		method:"POST",
		url: "/getAllJobOrder", 
		extra_data: {
			from: function () {
                return 1;
            }
		},
		columns: [
            {"data": "id"},
            {"data": "date", name: "<?= $date ?>"},
            {"data": "customer", name: "<?= $customers ?>"},
            {"data": "jo_number", name: "<?= $jo_number ?>"},
            {"data": "po_number", name: "<?= $po_number ?>"},
            {"data": "po_qty", name: "<?= $po_qty?>"},
            {"data": "status", name: "<?= $status ?>"},
            {"data": "button"}
        ]
	});
	
	
    $('#abutton').on("click", function () {
		mode = 0;
		global_id = 0;
		document.location.href="/create_job_order?id="+global_id;
    });
	
	var edit_data = [];
    function editJobOrder(item) {
        mode = 1;
        $('#dataModal').find("[name='title']").html("<?= $edit ?>");
        var row_details = $('#table_job_order').DataTable().row($(item).parents("tr")).data();
        global_id = row_details.id;
		document.location.href="/create_job_order?id="+global_id;
    }
	
   function logProductions(item,details){
	   var row_details = 0;
	   if(item==0){
		   row_details = details;
	   }else{
			row_details = $('#table_job_order').DataTable().row($(item).parents("tr")).data();
	   }
		global_item = row_details;
        global_id = row_details.id;
        $('#logModal').disable_fields(false);
        clearFields($('#logModal').find(".modal-body"));
		
		$('#table_job_order_production').DataTable().clear();
		$.getJSON("/loadJobOrderProductions", {id: row_details.id}, function (data){
			var datahold = data[0];
			for(var x=0;x<datahold.length;x++){
				datahold[x].remarks = "<div class='breakingline'>"+datahold[x].remarks+"</div>";
				if(datahold[x].status==1 && datahold[x].remaining!=0){
					datahold[x].btn = "<span class='label label-warning'><?=$partially_delivered .' &nbsp;<br>&nbsp;('.$slitting_dept.')'?></span>";
				}else if(datahold[x].status==1 && datahold[x].remaining==0){
					datahold[x].btn = "<span class='label label-success'><?=$all_delivered .' &nbsp;<br>&nbsp;('.$slitting_dept.')'?></span>";
				}else if(datahold[x].status==2 && datahold[x].remaining!=0){
					datahold[x].btn = "<span class='label label-warning'><?=$partially_delivered .' &nbsp;<br>&nbsp;('.$bagforming_dept.')'?></span>";
				}else if(datahold[x].status==2 && datahold[x].remaining==0){
					datahold[x].btn = "<span class='label label-success'><?=$all_delivered .' &nbsp;<br>&nbsp;('.$bagforming_dept.')'?></span>";
				}else{
					datahold[x].btn = "<span class='label label-default'><?=$pending?></span>";
				}
			}
				$('#table_job_order_production').DataTable().rows.add(datahold).draw();
			/* if(data[0].length>0){
				alert(data[0].length);
			} */
        });
        $('#logModal').find("[name='title']").html("<?= $joborder_production ?>");
        $('#logModal').modal("show");
    }
	
	
	var counter = 0;
    $('#add_joblogs').on("click", function (){
		addRowArray = [{
					"id":"<input class='listing' type='text' value='0'>",
					"date":"<input type='text' class='datepicker form-control' placeholder='<?= $prod_date ?>' style='width: 150px;'>",
					"pd_roll":"<input  type='text' class='nimp form-control ' placeholder='<?= $roll ?>'>",
					"pd_weight":"<input  type='text' class='nimp form-control ' placeholder='<?= $weight ?>'>",
					"ld_roll":"<input  type='text' class='nimp form-control' placeholder='<?= $roll ?>'>",
					"ld_weight":"<input  type='text' class='nimp form-control' placeholder='<?= $weight ?>'>",
					"sd_roll":"<input  type='text' class='nimp form-control ' placeholder='<?= $roll ?>'>",
					"sd_weight":"<input  type='text' class='nimp form-control' placeholder='<?= $weight ?>'>",//+
								//"<input  type='radio' name='deliverys"+counter+"' data_val='1' class='delivery'><?= $deliver ?>",
					"bf_roll":"<input  type='text' class='nimp form-control ' placeholder='<?= $roll ?>'>",
					"bf_weight":"<input  type='text' class='nimp form-control' placeholder='<?= $weight ?>'>",//+
								//"<input  type='radio' name='deliverys"+counter+"' data_val='2' class='delivery'><?= $deliver ?>",
					"remarks":"<textarea class='nimp form-control' placeholder='<?= $remarks ?>'></textarea>",
					"btn":"<button type='button' data-val='"+counter+"' class='deleteRow"+counter+" btn btn-danger btn-flat btn-sm pull-left'><i class='fa fa-trash'></i></button>"
				}];
		
        $('#table_job_order_production').DataTable().rows.add(addRowArray).draw();
		$(".datepicker").datepicker({ dateFormat: 'yy-mm-dd' });
		$('.deleteRow'+counter).on("click", function () {
			//console.log($(this).parent().parent());
			$('#table_job_order_production').DataTable().row( $(this).parent().parent() ).remove().draw();
			var listcount = 0;
			$('.listing').each(function(){
				listcount++;
			});
			if(listcount==0){
				$('#save_multiple').hide();
			}
		});
		$('#save_multiple').show();
		counter++;
    });
	
	
    $("#save_multiple").on("click", function () {
		var flag = 0;
       /*  if (checkTable($('#logModal').find(".table-container"))) {
            return;
        } */
        if (checkDuplicates([1], $('#logModal').find(".table-container").find("table")[0])) {
            $('#logModal').find(".table-container").find(".alert").display_alert({message: "<i class='fa fa-exclamation-circle'></i>&nbsp;<?= $duplicate_error ?>", alert_class: "alert alert-danger"});
            return;
        }
		
		
		var arr = [[]];
		$('.listing').each(function(){
			var tr = $(this).parent().parent();
			var arr_in = []
			tr.find('.form-control').css('border-color','');
			tr.find('.form-control').each(function(){
				arr_in.push($(this).val());
				if($(this).val()==""&&!($(this).hasClass('nimp'))){
					$(this).css('border-color','red');
					$('#logModal').find(".table-container").find(".alert").display_alert({message: "<i class='fa fa-exclamation-circle'></i>&nbsp;<?= $error_blank ?>", alert_class: "alert alert-danger"});
					flag = 1;
					return false;
				}
			});
				arr_in.push(global_id);
			arr[0].push(arr_in);
		});
		
//console.log(arr);
		//return;
		if(flag==0){
			$('#logModal').disable_fields(true);
			$.post("/saveJobOrdersProductionMultiple", {id: global_id, uid: "<?= $_SESSION[getSessionName()]["id"] ?>", arr: arr}, function (data) {
				if (data === 1) {
					$($('#logModal').find(".alert")[0]).display_alert("success");
					//$('#logModal').modal('hide');
					logProductions(0,global_item);
					//$('#table_job_order_production').DataTable().ajax.reload(null, false);
					$('#table_job_order').DataTable().ajax.reload(null, false);
				} else if (data === 0) {
					$($('#logModal').find(".alert")[0]).display_alert("failed");
					$('#logModal').disable_fields(false);
				} else if (data === -1) {
					$($('#logModal').find(".alert")[0]).display_alert({"message": "<i class='fa fa-exclamation-circle'></i>&nbsp;<?= $job_order_error2 ?>", "alert_class": "alert alert-danger"});
					$('#logModal').disable_fields(false);
				}
			}, 'json');
		}
    });
	$('#deliveryModal').on('hidden.bs.modal',function(){
		logProductions(0,global_item);
        $('#logModal').modal("show");
	})
	
	
	///Delivery Modal Saving
	$('#save_delivery').on('click',function(){
		var flag2 = 0;
		var arr = [[]];
			if($('#dr_no').val()==""&&!($('#dr_no').hasClass('nimp'))){
					$('#dr_no').css('border-color','red');
					$('#deliveryModal').find(".table-container").find(".alert").display_alert({message: "<i class='fa fa-exclamation-circle'></i>&nbsp;<?= $error_blank ?>", alert_class: "alert alert-danger"});
					flag2 = 1;
					return false;
			}
			if($('#del_to').val()==""&&!($('#del_to').hasClass('nimp'))){
					$('#del_to').css('border-color','red');
					$('#deliveryModal').find(".table-container").find(".alert").display_alert({message: "<i class='fa fa-exclamation-circle'></i>&nbsp;<?= $error_blank ?>", alert_class: "alert alert-danger"});
					flag2 = 1;
					return false;
			}
			if($('#del_date').val()==""&&!($('#del_date').hasClass('nimp'))){
					$('#del_date').css('border-color','red');
					$('#deliveryModal').find(".table-container").find(".alert").display_alert({message: "<i class='fa fa-exclamation-circle'></i>&nbsp;<?= $error_blank ?>", alert_class: "alert alert-danger"});
					flag2 = 1;
					return false;
			}
			if($('#del_add').val()==""&&!($('#del_add').hasClass('nimp'))){
					$('#del_add').css('border-color','red');
					$('#deliveryModal').find(".table-container").find(".alert").display_alert({message: "<i class='fa fa-exclamation-circle'></i>&nbsp;<?= $error_blank ?>", alert_class: "alert alert-danger"});
					flag2 = 1;
					return false;
			}
			if($('#del_term').val()==""&&!($('#del_term').hasClass('nimp'))){
					$('#del_term').css('border-color','red');
					$('#deliveryModal').find(".table-container").find(".alert").display_alert({message: "<i class='fa fa-exclamation-circle'></i>&nbsp;<?= $error_blank ?>", alert_class: "alert alert-danger"});
					flag2 = 1;
					return false;
			}
			
		$('.listing2').each(function(){
			var tr = $(this).parent().parent();
			var arr_in = [];
				arr_in.push($('#dr_no').val());
				arr_in.push($(this).val());
				arr_in.push($('#del_to').val());
				arr_in.push($('#del_date').val());
				arr_in.push($('#del_add').val());
				arr_in.push($('#del_term').val());
				
			tr.find('.form-control').css('border-color','');
			tr.find('.form-control').each(function(){
				if($(this).val()==""&&!($(this).hasClass('nimp'))){
					$(this).css('border-color','red');
					$('#deliveryModal').find(".table-container").find(".alert").display_alert({message: "<i class='fa fa-exclamation-circle'></i>&nbsp;<?= $error_blank ?>", alert_class: "alert alert-danger"});
					flag2 = 1;
					return false;
				}
				arr_in.push($(this).val());
			});
				arr_in.push($(this).attr('data_status'));
			arr[0].push(arr_in);
		});
		//console.log(arr);
		
		
				//return;
		if(flag2==0){
			//alert(1);
			$('#deliveryModal').disable_fields(true);
			$.post("/saveJobOrdersProductionDelivery", {deleted_fields: deleted_rows, mode: 0,id: global_id, uid: "<?= $_SESSION[getSessionName()]["id"] ?>", arr: arr}, function (data) {
				if (data === 1) {
					$($('#logModal').find(".alert")[0]).display_alert("success");
					logProductions(0,global_item);
					//$('#table_job_order_production').DataTable().ajax.reload(null, false);
					$('#table_job_order').DataTable().ajax.reload(null, false);
					$('#logModal').modal("show");
					$('#deliveryModal').modal("hide");
					$('#deliveryModal').disable_fields(false);
				} else if (data === 0) {
					$($('#deliveryModal').find(".alert")[0]).display_alert("failed");
					$('#deliveryModal').disable_fields(false);
				} else if (data === -1) {
					$($('#deliveryModal').find(".alert")[0]).display_alert({"message": "<i class='fa fa-exclamation-circle'></i>&nbsp;<?= $dr_exists ?>", "alert_class": "alert alert-danger"});
					$('#deliveryModal').disable_fields(false);
				}
			}, 'json');
		}
	})
	
	
	$('#DeliverThis').on('click',function(){
		$('#deliveryModal').disable_fields(false);
		
		var row_details = global_item;
			var datalist = [];
			$('#table_job_order_delivery').DataTable().clear();
			$('.deliveryID').each(function(){
				var tr = $(this).parent().parent();
				var id_me = $(this).val();
				var arr_in = []
				//console.log($('input[name="deliverys0'+id_me+'"]:checked'));
					if($('input[name="deliverys0'+id_me+'"]:checked').val()!=undefined){
						arr_in.push($('input[name="deliverys0'+id_me+'"]:checked').attr('data_rem'));
						arr_in.push($('input[name="deliverys0'+id_me+'"]:checked').attr('data_val'));
						arr_in.push(id_me);
						datalist.push(arr_in);
					}
			});
			if(datalist.length==0){
				$($('#logModal').find(".alert")[0]).display_alert({"message": "<i class='fa fa-exclamation-circle'></i>&nbsp;<?= $select_deliver ?>", "alert_class": "alert alert-danger"});
				return;
			}
			//console.log(datalist)
			for(var x=0;x<datalist.length;x++){
					var total = datalist[x][0];
					var status = datalist[x][1];
					var del_id = datalist[x][2];
					if(datalist[x]==1){
						total = datalist[x][0];
					}else if(datalist[x]==2){
						total = datalist[x][0];
					}
					var addDeliverRowArray = [{
								"id":"<input class='listing2' type='text' data_status='"+status+"' value='"+del_id+"'>",
								"remaining":"<label>"+total+"</label>",
								"qty":"<input id='qty"+del_id+"' data-val='"+del_id+"' type='number' class='score form-control' max='"+total+"' min='0' value='"+total+"'>",
								"unit":"<label><?= $weight ?></label>",
								"description":"<label>"+row_details.product+"</label>",
								"price":"<input id='price"+del_id+"' data-val='"+del_id+"'  type='text' class='score form-control' value='0' placeholder='<?=$price?>'>",
								"amount":"<label id='total"+del_id+"'><?= $amount ?></label>"
							}];
					$('#table_job_order_delivery').DataTable().rows.add(addDeliverRowArray).draw();
					
					$("#qty"+del_id).on('change',function(){
						if(parseFloat($(this).val())>=$(this).attr('max')){
							$(this).val($(this).attr('max'));
						}
						var value = $(this).val()*$("#price"+$(this).attr('data-val')).val();
						$("#total"+$(this).attr('data-val')).html(value);
					});
					$("#price"+del_id).on('change',function(){
						$(this).attr('data-val')
						var value = $(this).val()*$("#qty"+$(this).attr('data-val')).val();
						$("#total"+$(this).attr('data-val')).html(value);
					});
					$("#price"+del_id).change();
			}
				$('#dr_no').val("");
				$('#del_to').val("");
				$('#del_date').val("");
				$('#del_add').val("");
				$('#del_term').val("");
			$('#logModal').modal('hide');
			$('#deliveryModal').modal('show');
	});
	
	
	function deliverJobOrders(){
		alert(1);
	}
	
	
    function deleteJobOrder(item) {
        clearFields($('#deleteModal'));
        var row_details = $('#table_job_order').DataTable().row($(item).parents("tr")).data();
        global_id = row_details.id;
        $('#deleteModal').find("[name='message']").html("<?= $delete_message ?> <strong><?= $module_name ?>:&nbsp;" + row_details.name + "</strong>?");
        $('#deleteModal').modal("show");
    }
	
    $('#delete_yes').on("click", function () {
        $('#deleteModal').disable_fields(true);
        $.post("/deleteData", {mode: 13, id: global_id}, function (data) {
            if (data === 1) {
                clone_success.message = "<?= $module_name . ' ' . $sdeleted ?>"
                $($(".main-content").find(".alert")[0]).display_alert(clone_success);
                $('#deleteModal').disable_fields(false);
                $('#deleteModal').modal('hide');
				$('#table_job_order_production').DataTable().ajax.reload(null, false);
				$('#table_job_order').DataTable().ajax.reload(null, false);
            } else if (data === 0) {
                $($("#deleteModal").find(".alert")[0]).display_alert("failed");
                $('#deleteModal').disable_fields(false);
            } else if (data === -1) {
                clone_error.message = "<?= $conflict_found ?>";
                $($("#deleteModal").find(".alert")[0]).display_alert(clone_error);
                $('#deleteModal').disable_fields(false);
            }

        }, 'json');
    });
	
	/* $('#dataModal').on('hidden.bs.modal',function(){
        $('#logModal').modal("show");
	})
	
    $('#save').on("click", function () {
        if (checkFields($('#dataModal').find(".details-container")[0]).status) {
            return;
        }
		
        var arr = getFields($('#dataModal'), mode);
		arr[0].splice(13,2);
		arr[0].splice(9,2);
		arr[0].splice(5,2);
		arr[0].push(global_id);
		if($('input[name=deliverys]:checked').attr('data_val')==undefined){
			arr[0].push(0);
		}else{
			arr[0].push($('input[name=deliverys]:checked').attr('data_val'));
		}
			
		//console.log(arr);
		//return;
        $('#dataModal').disable_fields(true);
		
        $.post("/saveJobOrdersProductions", {deleted_fields: deleted_rows, id: global_id, uid: "1", arr: arr, mode: mode, from: 1}, function (data) {
            if (data === 1) {
				$($("logModal").find(".alert")[0]).display_alert("success");
				$('#dataModal').modal('hide');
				$('#dataModal').disable_fields(false);
				$('#table_job_order_production').DataTable().ajax.reload(null, false);
				$('#table_job_order').DataTable().ajax.reload(null, false);
            } else if (data === 0) {
                $($('#dataModal').find(".alert")[0]).display_alert("failed");
                $('#dataModal').disable_fields(false);
            } else if (data === -1) {
                $($('#dataModal').find(".alert")[0]).display_alert({"message": "<i class='fa fa-exclamation-circle'></i>&nbsp;<?= $server_error ?>", "alert_class": "alert alert-danger"});
                $('#dataModal').disable_fields(false);
            }
        }, 'json');
    }); */
	
	
	/* ///Solo adding
	$('#add_joblogs').on("click", function () {
        mode = 0;
        $('#dataModal').disable_fields(false);
        clearFields($('#dataModal').find(".modal-body"));
        $('#dataModal').find("[name='title']").html("<?= $add ?>");
		$('input[name=deliverys]:checked').attr('checked',false);
		$('input[name=deliverys]').trigger('change');
		//alert($('input[name=deliverys]:checked').attr('data_val'));
        $('#logModal').modal("hide");
        $('#dataModal').modal("show");
    }); */
	
	
	/* //Modal Listing
		$('#table_job_order_production').serverTable({
			method:"POST",
			url: "/getAllJobOrderProductions", 
			extra_data: {
				from: function () {
					return global_id;
				}
			},
			columns: [
				{"data": "id"},
				{"data": "date", name: "<?= $date ?>"},
				{"data": "pd_roll", name: "<?= $printing_dept.'-'.$roll ?>"},
				{"data": "pd_weight", name: "<?= $printing_dept.'-'.$weight ?>"},
				{"data": "ld_roll", name: "<?= $lamination_dept.'-'.$roll ?>"},
				{"data": "ld_weight", name: "<?= $lamination_dept.'-'.$weight ?>"},
				{"data": "sd_roll", name: "<?= $slitting_dept.'-'.$roll ?>"},
				{"data": "sd_weight", name: "<?= $slitting_dept.'-'.$weight ?>"},
				{"data": "bf_roll", name: "<?= $bagforming_dept.'-'.$roll ?>"},
				{"data": "bf_weight", name: "<?= $bagforming_dept.'-'.$weight ?>"},
				{"data": "lod_dr_number", name: "<?= $logistic_dept.'-DR#' ?>"},
				{"data": "lod_wieght", name: "<?= $logistic_dept.'-'.$weight ?>"},
				{"data": "remarks", name: "<?= $remarks ?>"},
				//{"data": "button"}
			],
			columnDefs: [
				{
				  targets: -1,
				  className: 'breakingline'
				}
			  ]
		});
	//Modal Listing */
</script>