<?php
$global_id = 0;
if(isset($_GET['id'])){
	$global_id = $_GET['id'];
}
?>

<script>
	var global_id = <?=$global_id?>;
	//
	//<input type="text" class="w400 solid_bottom">
        select2_ajax = ["/getDropdownCustomers","/getDropdownProducts"];
	
		
$(window).load(function(){
	if(global_id!=0){
        $.getJSON("/loadCustomerPO", {id: global_id}, function (data) {
			edit_data = data;
            $("#dataModal").loadDetails(data[0]);
			$("#dataModal").disable_fields(false);
            //$('#dataModal').modal("show");
			
        });
	}
		
});
    $('#save').on("click", function () {
        if (checkTabs($('#dataModal').find(".tab-container")[0])) {
            return;
        }
		
        var arr = getFields($('#dataModal'), mode);
		
		
        $('#dataModal').disable_fields(true);
		
        $.post("/saveCustomerPO", {deleted_fields: deleted_rows, id: global_id, uid: "1", arr: arr, mode: mode, from: 1}, function (data) {
            if (data === 1) {
				$($(".main-content").find(".alert")[0]).display_alert("success");
				$('#dataModal').modal('hide');
				$('#dataModal').disable_fields(false);
				$('#table_customer_po').DataTable().ajax.reload(null, false);
            } else if (data === 0) {
                $($('#dataModal').find(".alert")[0]).display_alert("failed");
                $('#dataModal').disable_fields(false);
            } else if (data === -1) {
                $($('#dataModal').find(".alert")[0]).display_alert({"message": "<i class='fa fa-exclamation-circle'></i>&nbsp;<?= $server_error ?>", "alert_class": "alert alert-danger"});
                $('#dataModal').disable_fields(false);
            }
        }, 'json');
    });
	
	$('#section').find(".select2-field.ajax").each(function () {
                if (parseInt($(this).closest(".details-container").length) > 0 || parseInt($(this).closest("[name='field_container']").length) > 0) {
                    var index = $('#section').find(".select2-field.ajax").index($(this));
                    $(this).ajaxSelect2(select2_ajax[index]);
                }
            });

</script>