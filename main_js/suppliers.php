<script>
    $('#nav_suppliers').addClass('active');
    var mode = -1;
    var global_id = 0;
	
    var clone_error = $.extend(true, {}, $.fn.display_alert.server_problem_defaults);
    var clone_success = $.extend(true, {}, $.fn.display_alert.success_defaults);
    $('#table_multiple').fieldTable({searching: false, ordering: false});
	
	
	
	
	///Suppliers
	
        select2_ajax = ["/getDropdownCurrency", "/getDropdownCurrency"];
	
	$('#table_supplier').serverTable({
		method:"POST",url: "/getAllSuppliers", 
		extra_data: {
			from: function () {
                return 1;
            }
		},
		columns: [
            {"data": "id"},
            {"data": "name", name: "<?= $name ?>"},
            {"data": "contact_person", name: "<?= $contact_person?>"},
            {"data": "address", name: "<?= $address ?>"},
            {"data": "currency", name: "<?= $currencys?>"},
            {"data": "balance", name: "<?= $starting_balance ?>"},
            {"data": "button"}
        ]
	});
	
    $('#abutton').on("click", function () {
		mode = 0;
		global_id = 0;
        $('#dataModal').disable_fields(false);
        clearFields($('#dataModal').find(".modal-body"));
        $('#dataModal').find("[name='title']").html("<?= $add ?>");
        $('#dataModal').find(".sample").attr("src","");
        $('#dataModal').modal("show");
    });
	
    $('#abutton_multiple').on("click", function () {
        mode = 0;
        $('#dataModal').disable_fields(false);
        clearFields($('#dataModal_Multiple').find(".modal-body"));
        $('#dataModal_Multiple').find("[name='title']").html("<?= $add ?>(<?= $multiple ?>)");
        $('#dataModal_Multiple').modal("show");
    });
    $('#add_multiple').on("click", function () {

        $('#table_multiple').addFieldRow($('#dataModal_Multiple').find("div[name='field_container']"));
		scoreme();
    });
	
	
    $('#save').on("click", function () {
        if (checkFields($('#dataModal').find(".details-container")[0]).status) {
            return;
        }
		
		
        var arr = getFields($('#dataModal'), mode);
        $('#dataModal').disable_fields(true);
		
//$_SESSION[getSessionName()]["id"]<?  ?>
        $.post("/saveSuppliers", {deleted_fields: deleted_rows, id: global_id, uid: "1", arr: arr, mode: mode, from: 1}, function (data) {
            if (data === 1) {
				$($("#section").find(".alert")[0]).display_alert("success");
				$('#dataModal').modal('hide');
				$('#dataModal').disable_fields(false);
				$('#table_supplier').DataTable().ajax.reload(null, false);
            } else if (data === 0) {
                $($('#dataModal').find(".alert")[0]).display_alert("failed");
                $('#dataModal').disable_fields(false);
            } else if (data === -1) {
                $($('#dataModal').find(".alert")[0]).display_alert({"message": "<i class='fa fa-exclamation-circle'></i>&nbsp;<?= $supplier_error ?>", "alert_class": "alert alert-danger"});
                $('#dataModal').disable_fields(false);
            }
        }, 'json');
    });
	
	
    $("#save_multiple").on("click", function () {
        if (checkTable($('#dataModal_Multiple').find(".table-container"))) {
            return;
        }
        if (checkDuplicates([1], $('#dataModal_Multiple').find(".table-container").find("table")[0])) {
            $('#dataModal_Multiple').find(".table-container").find(".alert").display_alert({message: "<i class='fa fa-exclamation-circle'></i>&nbsp;<?= $duplicate_error ?>", alert_class: "alert alert-danger"});
            return;
        }
        var arr = getFields($('#dataModal_Multiple'));

        $('#dataModal_Multiple').disable_fields(true);
        $.post("/saveSupplierMultiple", {id: global_id, uid: "<?= $_SESSION[getSessionName()]["id"] ?>", arr: arr}, function (data) {
            if (data === 1) {
                $($("#section").find(".alert")[0]).display_alert("success");
                $('#dataModal_Multiple').modal('hide');
                $('#table_supplier').DataTable().ajax.reload(null, false);
            } else if (data === 0) {
                $($('#dataModal_Multiple').find(".alert")[0]).display_alert("failed");
                $('#dataModal_Multiple').disable_fields(false);
            } else if (data === -1) {
                $($('#dataModal_Multiple').find(".alert")[0]).display_alert({"message": "<i class='fa fa-exclamation-circle'></i>&nbsp;<?= $supplier_error2 ?>", "alert_class": "alert alert-danger"});
                $('#dataModal_Multiple').disable_fields(false);
            }
        }, 'json');
    });
	
    function editSupplier(item) {
        mode = 1;
        $('#dataModal').find("[name='title']").html("<?= $edit ?>");
        var row_details = $('#table_supplier').DataTable().row($(item).parents("tr")).data();
        global_id = row_details.id;
        $("#dataModal").disable_fields(true);
        $(item).find(".loading").css("display", "");
        clearFields($('#dataModal').find(".modal-body"),{select_load:true});
        $.getJSON("/loadSupplier", {id: row_details.id}, function (data) {
            $("#dataModal").loadDetails(data[0]);
			$("#dataModal").disable_fields(false);
     
			
			$('#dataModal').find(".select2-field.ajax").each(function () {
                if (parseInt($(this).closest(".details-container").length) > 0 || parseInt($(this).closest("[name='field_container']").length) > 0) {
                    var index = $('#dataModal').find(".select2-field.ajax").index($(this));
                    queued_ajaxs.push({element: this, options: {queued: true, url: select2_ajax[index]}});
                }
            });
			$('#dataModal').find(".select2-field.ajax:eq(0)").ajaxSelect2({queued:true,url:select2_ajax[0]});
        });
    }
	
	 end_queue_function = function () {
        if (parseInt(mode) === 1) {

            $('#dataModal').on("shown.bs.modal", function () {
                //$('#contact_table').DataTable().rows.add(edit_data[1]).draw();
                $('#dataModal').off("shown.bs.modal");
            });

            $('#dataModal').modal("show");
        }
    }
	
    function deleteSupplier(item) {
        clearFields($('#deleteModal'));
        var row_details = $('#table_supplier').DataTable().row($(item).parents("tr")).data();
        global_id = row_details.id;
        $('#deleteModal').find("[name='message']").html("<?= $delete_message ?> <strong><?= $module_name ?>:&nbsp;" + row_details.name + "</strong>?");
        $('#deleteModal').modal("show");
    }
	
    $('#delete_yes').on("click", function () {
        $('#deleteModal').disable_fields(true);
        $.post("/deleteData", {mode: 5, id: global_id}, function (data) {
            if (data === 1) {
                clone_success.message = "<?= $module_name . ' ' . $sdeleted ?>"
                $($("#section").find(".alert")[0]).display_alert(clone_success);
                $('#deleteModal').disable_fields(false);
                $('#deleteModal').modal('hide');
                $('#table_supplier').DataTable().ajax.reload(null, false);
            } else if (data === 0) {
                $($("#deleteModal").find(".alert")[0]).display_alert("failed");
                $('#deleteModal').disable_fields(false);
            } else if (data === -1) {
                clone_error.message = "<?= $conflict_found ?>";
                $($("#deleteModal").find(".alert")[0]).display_alert(clone_error);
                $('#deleteModal').disable_fields(false);
            }

        }, 'json');
    });
	
</script>