<script>
    var select2_ajax = [];
    var queued_ajaxs = [];
    var end_queue_function;
    var field_row_initialize;
    function computeCenter(settings, container, item) {
        var default_settings = {};
        var container_height = parseFloat($(container).height());

        var item_height = parseFloat($(item).outerHeight());
        var offset_container_body = parseFloat($(container).offset().top);
        var container_height = container_height - item_height;
        var top = parseFloat(parseFloat(container_height / 2).toFixed(2)) + offset_container_body;

        var container_width = parseFloat($(container).width());
        var item_width = parseFloat($(item).width());
        var offset_container = parseFloat($(container).offset().left);
        var div_length = container_width - item_width;
        div_length = parseFloat(parseFloat(div_length / 2).toFixed(2));
        var left = div_length + offset_container;
        return {top: top, left: left};
    }

    var datatable_language_options = {
        "lengthMenu": "<?= $length_message ?>",
        "emptyTable": "<?= $empty_message ?>",
        "zeroRecords": "<?= $zero_message ?>",
        "lengthMenuCustom": "<?= $length_message_custom ?>",
        "info": "<?= $info_message ?>",
        "thousands": ",",
        "infoEmpty": "<?= $info_empty_message ?>",
        "infoFiltered": "<?= $info_filtered_message ?>",
        "processing": "<?= $processing ?>",
        "search": "<?= $search ?>:&nbsp;",
        "paginate": {
            "first": "<?= $first ?>",
            "last": "<?= $last ?>",
            "next": "<?= $next ?>",
            "previous": "<?= $previous ?>"
        }
    }
    /*Reference Arrays*/
    var image_extensions = ["jpg", "jpeg", "gif", "png"];
    var input_types = ["text", "number", "password"]; //for blank checking
    var detail_input_types = ["text", "number", "password", "hidden", "radio", "checkbox"]; //for retrieving field data
    var load_inputs = ["text", "number", "password", "hidden", "checkbox"]; //for loading details
    /*Reference Arrays end*/
    $(".datepicker").each(function () {
        if (typeof $(this).attr("format") === "undefined") {
            $(this).attr("format", JSON.stringify({"delimiter": "-", "date_format": "yyyy-mm-dd"}));
        }
    })
    $(".select2-field").select2({width: "100%", allowClear: true});

    function matchIndex(all_arr, arr, strict_stat) {


        for (var x = 0; x < all_arr.length; x++) {
            var arr_index = all_arr[x];
            var same_fields = 0;
            for (var y = 0; y < arr.length; y++) {
                if (arr_index[y] !== "") {
                    if (arr_index[y] === arr[y]) {
                        same_fields++;
                    }
                }

            }

            if (strict_stat) {
                if (parseInt(same_fields) === parseInt(arr.length)) {
                    return true;
                }
            } else {

                if (parseInt(same_fields) > 0) {
                    return true;
                }
            }


        }
        return false;
    }
    function checkDuplicates(indexes, table, strict) {
        var all_arr = [];
        var flag = false;
        var strict_stat;
        if (typeof strict === "undefined") {
            strict_stat = true;
        } else {
            strict_stat = strict;
        }
        $(table).DataTable().$("tr").each(function () {
            var tr = this;
            var arr = [];
            for (var x = 0; x < indexes.length; x++) {
                if (parseInt($(tr).find("td:eq(" + indexes[x] + ")").find("input[type!='search'],select,textarea").length) > 0) {
                    arr.push($(tr).find("td:eq(" + indexes[x] + ")").find("input[type!='search'],select,textarea").val());
                }
            }
            if (parseInt(arr.length) > 0) {

                if (!matchIndex(all_arr, arr, strict_stat)) {
                    all_arr.push(arr);
                } else {
                    flag = true;
                    return false;
                }
            }

        });
        return flag;
    }
    var load_init;
    $.fn.loadDetails = function (details) {

        $(this).find("div.details-container").each(function (index) {
            if (!$(this).hasClass("nload")) {
                var object_arr = details[index];
                var keys = Object.keys(object_arr);

                var index2 = 0;

                $(this).find("input,textarea,select").each(function () {
                    if (!$(this).hasClass("nload")) {
                        if ($(this).is("input") && parseInt($.inArray($(this).attr("type"), load_inputs)) !== -1) {
                            if ($(this).hasClass("has-tags")) {

                                $("#" + $(this).attr("id")).tagsinput();
                                var arr = JSON.parse(object_arr[keys[index2]]);
                                for (var y = 0; y < arr.length; y++) {
                                    $("#" + $(this).attr("id")).tagsinput('add', arr[y]);
                                }

                            } else if ($(this).attr("type") === "checkbox") {

                                var prop_check = (parseInt(object_arr[keys[index2]]) === 1) ? true : false;
                                $(this).prop("checked", prop_check);
                                $(this).trigger("change");
                            } else {
                                $(this).val(object_arr[keys[index2]]);

                            }
                            index2++;
                        } else if ($(this).is("select")) {
                            if ($(this).hasClass("select2-field")) {
                                if ($(this).hasClass("ajax")) {
                                    $(this).attr("data-val", object_arr[keys[index2]]);


                                    index2++;
                                } else {

                                    $(this).select2("val", object_arr[keys[index2]]);
                                    index2++;
                                }
                            }
                        } else if ($(this).is("textarea")) {
                            $(this).val(object_arr[keys[index2]].toString().replace(/\\n/g, "\n"));
                            index2++;
                        }
                    }
                });
            }
        });
        if (typeof load_init === "function") {
            load_init();
        }

    }
    $.fn.disable_fields = function (bool) {
        $(this).find("input[type!='search'],select,textarea,button,a").prop("disabled", bool);
    }


    var modal_open = false;
    var global_modal;
    var modal_show_function;

    $(".modal").on('shown.bs.modal', function () {
        modal_open = true;
        global_modal = this;
        $(this).find("[auto_focus]").focus();
        $(".loading").css("display", "none");
        if (!$(this).hasClass("no-disable")) {
            $("html,body").disable_fields(false);
            $(this).removeClass("no-disable");
        }
        $('div[role="tooltip"]').remove();
        //   $('[data-toggle="tooltip"]').tooltip('destroy');
        $('[data-toggle="tooltip"]').tooltip({"trigger": "hover", container: "body"});
        if (typeof modal_show_function === "function") {
            modal_show_function();
        }
    });

    $(".modal").on('hidden.bs.modal', function () {
        modal_open = false;
        $(".loading").css("display", "none");
        $('div[role="tooltip"]').remove();
        //$("html,body").disable_fields(false);
    });
    var hotkey_arr = ["text", "number", "password", "search"];
    var hotkey_inputs = "";
    $.each(hotkey_arr, function (index, value) {
        hotkey_inputs += "input[type='" + value + "']:focus";
        if (parseInt(index) !== parseInt(hotkey_arr.length) - 1) {
            hotkey_inputs += ",";
        }
    });
    var onBarcodeScan;
    var global_code = "";
    var global_timeout = 0;
    window.onkeypress = function (event) {

        var button_container;
        if (parseInt($("body").find(".modal.in").length) > 0) {
            button_container = $($("body").find(".modal.in:last"));
        } else {
            button_container = $(".main-content");
        }
        if (parseInt(event.charCode) >= 48 && parseInt(event.charCode) <= 57) {
            var char = String.fromCharCode(event.charCode);

            if (global_code == '') {
                setTimeout(function () {
                    global_code = "";
                    global_timeout = 0;
                }, 1000);
            }

            if (parseInt(global_timeout) === 0) {
                var $d = new Date();
                global_timeout = $d.getTime();
            }
            global_code += char;


        }

        if (event.keyCode == 13) {

            var temp = global_code;
            var $d = new Date(), $interval = $d.getTime() - global_timeout;

            global_code = "";
            global_timeout = 0;
            /*   
             if (parseInt($(button_container).find("button:focus,textarea:focus").length) === 0) {
             $(button_container).find(".save_btn").click();
             var input_focus = $($(button_container).find("input:focus")[0]);
             if (parseInt(input_focus.length) > 0 && $(input_focus).attr("name") === "search_box") {
             $($(input_focus).closest("[data-container='search']")[0]).find("[name='srch_btn']").click();
             
             }
             
             event.preventDefault();
             }
             */
            if (parseInt($(button_container).find("button:focus,textarea:focus").length) === 0) {
                var input_focus = $($(button_container).find("input:focus")[0]);

                if (parseInt(input_focus.length) > 0 && $(input_focus).attr("name") === "search_box") {
                    $($(input_focus).closest("[data-container='search']")[0]).find("[name='srch_btn']").click();

                    event.preventDefault();
                    return;
                }


            }

            if ($interval < 1000) {


                if (typeof onBarcodeScan === "function") {
                    onBarcodeScan(temp);

                }
                event.preventDefault();
//                else {
//                    if (parseInt($(button_container).find("input:focus,textarea:focus").length) > 0) {
//                        $("input:focus,textarea:focus").eq(0).val(temp);
//
//                        var input_focus = $($(button_container).find("input:focus")[0]);
//                        if (parseInt(input_focus.length) > 0 && $(input_focus).attr("name") === "search_box") {
//
//                            $($(input_focus).closest("[data-container='search']")[0]).find("[name='srch_btn']").click();
//                        }
//
//                    } else {
//                        console.log(temp);
//                    }
//
//                }


            } else {

                if (parseInt($(button_container).find("button:focus,textarea:focus").length) === 0) {
                    $(button_container).find(".save_btn").click();
                    event.preventDefault();
                }

            }


        }



        if (event.keyCode == 27) {
            $(button_container).find(".esc_btn").click();
        }
        if (event.charCode == 121) {

            if (parseInt($(button_container).find("button:focus," + hotkey_inputs + ",select:focus,textarea:focus").length) === 0) {
                $(button_container).find(".yes_btn").click();
            }
        }
        if (event.charCode == 110) {
            if (parseInt($(button_container).find("button:focus," + hotkey_inputs + ",select:focus,textarea:focus").length) === 0) {
                $(button_container).find(".no_btn").click();
            }
        }


    }
    var deleted_rows = {};
    var editted_rows = {};
    var original_fields = {};
    var cancel_row = false;
    var global_search_value = "";
    var deselected_searches = [];
    function reloadFieldCustoms(item) {

        //"length":10,"recordsTotal":2,"recordsDisplay":2,"serverSide":true}
        //page_change_control [for changes pages-the one with input field]
        //page_button [controls for first-1 previous-2 next-3 last-4]
        //srch_btn [button for search click]
        //search_select [multiselect for search]
        //search_box [input field]
        //hide_cols [multiselect for hiding columns]
        //refresh_button [refresh button]
        var columns = $(item).DataTable().settings().init().columns;
        var column_defs = $(item).DataTable().settings().init().columnDefs;
        var not_searchables = [];
        var not_orderables = [];
        $.each(column_defs, function (index, value) {
            if (typeof value.orderable !== "undefined" && !value.orderable) {
                var targets = (typeof value.targets === "undefined") ? value.aTargets : value.targets;
                not_orderables = $.merge(not_orderables, targets);
            }
            if (typeof value.searchable !== "undefined" && !value.searchable) {
                var targets = (typeof value.targets === "undefined") ? value.aTargets : value.targets;
                not_searchables = $.merge(not_searchables, targets);
            }

        });

        var custom_table = $(item).closest(".customtable")[0];
        $(custom_table).find("[name='search_select']").html("");
        $(custom_table).find("[name='hide_cols']").html("");
        $(custom_table).find("[name='search_box']").removeClass('x onX').val('').change();
        $(custom_table).find("[name='search_select']").multiselect('destroy');
        // $.fn.serverTable.pause_reload[item.unique_id] = true;
        // $(item).DataTable().columns().search('').draw();

        var hidden_indexes = (($(item).DataTable().columns(".display_col")["0"]));
        $.each(columns, function (index, value) {

            if (parseInt($.inArray(index, not_searchables)) === -1) {

                var option = document.createElement("option");
                option.setAttribute("value", index);
                option.innerHTML = value.name;
                $(custom_table).find("[name='search_select']").append(option);
            }
        });
        $(custom_table).find("[name='hide_cols']").multiselect("destroy");
        var deselect_orders = [];
        var order_by = [];
        $.each(columns, function (index, value) {
            if (parseInt($.inArray(index, not_orderables)) === -1 && parseInt($.inArray(index, hidden_indexes)) === -1) {
                var visible = $(item).DataTable().column(index).visible();
                if (!visible) {
                    deselect_orders.push(index);
                } else {
                    order_by.push(index);
                }
                var option = document.createElement("option");
                option.setAttribute("value", index);
                option.innerHTML = value.name;
                $(custom_table).find("[name='hide_cols']").append(option);
            }
        });

        $(custom_table).find("[name='search_select']").multiselect({
            enableCaseInsensitiveFiltering: true,
            selectAllValue: -1,
            selectAllText: '<?= $all ?>', includeSelectAllOption: true, enableFiltering: true,
            onSelectAll: function () {

            }, onDeselectAll: function () {

            }, onChange: function (option, checked) {


            }});
        $(custom_table).find("[name='hide_cols']").multiselect({
            enableCaseInsensitiveFiltering: true,
            buttonClass: "default_ordering",
            selectAllValue: -1,
            selectAllText: '<?= $all ?>', includeSelectAllOption: true, enableFiltering: true,
            onSelectAll: function () {

            }, onDeselectAll: function () {

            }, onChange: function (option, checked) {


            }});

        $(custom_table).find("[name='hide_cols']").on("change", function () {

            var hidden_cols = [];
            if ($(this).val() === null) {
                $(this).multiselect("selectAll", false);
            } else {
                var not_selected = $(this).find("option:not(:selected)");
                $(not_selected).each(function (index, value) {
                    hidden_cols.push($(value).attr("value"));
                });
            }

            if (parseInt(hidden_cols.length) > 0) {
                $(item).DataTable().columns().visible(true).columns(hidden_cols).visible(false);
                var order = $(item).DataTable().order()[0];

                var check = $(item).DataTable().column(order[0]).visible();
                if (!check) {
                    var value = $(this).find("option:selected:eq(0)").attr("value");
                    $(item).DataTable().order([value, order[1]]).draw();
                }

            } else {
                $(item).DataTable().columns().visible(true);
            }
        });

        $(custom_table).find("[name='search_select']").multiselect("selectAll", false);
        $(custom_table).find("[name='hide_cols']").multiselect("selectAll", false);
        if (parseInt(deselect_orders.length) > 0) {
            $(custom_table).find("[name='hide_cols']").multiselect("deselect", deselect_orders);
        }

        $(custom_table).find("[name='srch_btn']").on("click", function () {

            var container = $(this).closest(".customtable")[0];
            var value = $(container).find("[name='search_select']").val();
            if (value === null) {
                $(container).find("[name='search_select']").multiselect("selectAll", false);
                value = $(container).find("[name='search_select']").val();
            }
            var search_value = $(container).find("[name='search_box']").val();

            $(item).DataTable().columns(value).search(search_value).draw();


        });

    }

    var field_table_custom_id = 0;
    $.fn.fieldTable = function (options) {
        var data_options = {};
        if (typeof options !== "undefined") {
            data_options = options;
        }
        var settings = $.extend({}, $.fn.fieldTable.default_settings, data_options);
        var not_searchable = [];
        var not_orderable = [];
        var column_defs = [];
        if (settings.searching || typeof settings.searching === "undefined") {
            var index = 0;
            $(this).find("thead").find("th").each(function () {
                if ($(this).hasClass("nsearch")) {
                    not_searchable.push(index);
                }
                index++;
            });
        }
        if (settings.ordering || typeof settings.ordering === "undefined") {
            var index = 0;
            $(this).find("thead").find("th").each(function () {
                if ($(this).hasClass("norder")) {
                    not_orderable.push(index);
                }
                index++;
            });
        }

        if (parseInt(not_searchable.length) > 0) {
            column_defs.push({"searchable": false, "targets": not_searchable});
        }
        if (parseInt(not_orderable.length) > 0) {
            column_defs.push({"orderable": false, "targets": not_orderable});
        }

        if (parseInt(column_defs.length) > 0) {

            if (typeof data_options.columnDefs !== "undefined" && parseInt(data_options.columnDefs.length) > 0) {
                var no_searchable = false;
                var no_orderable = false;
                $.each(data_options.columnDefs, function (index, value) {
                    var key = Object.keys(value);
                    if (parseInt($.inArray("searchable"), key) !== -1) {

                        data_options.columnDefs[index]["targets"] = $.merge(data_options.columnDefs[index]["targets"], not_searchable);
                    } else if (parseInt($.inArray("orderable"), key) !== -1) {
                        data_options.columnDefs[index]["targets"] = $.merge(data_options.columnDefs[index]["targets"], not_orderable);
                    } else {
                        if (parseInt($.inArray("searchable"), key) === -1) {
                            no_searchable = true;
                        }
                        if (parseInt($.inArray("orderable"), key) === -1) {
                            no_orderable = true;
                        }

                    }
                });
                if (no_orderable) {
                    data_options.columnDefs.push({"orderable": false, "targets": not_orderable});
                }
                if (no_searchable) {
                    data_options.columnDefs.push({"searchable": false, "targets": not_searchable});
                }
                column_defs = data_options.columnDefs;

            }

            settings.columnDefs = column_defs;

        }
        var index = $($(this).closest(".table-container")[0]).parent().children("div").index($(this).closest(".table-container")[0]);
        var fields = $($($(this).closest(".table-container")[0]).parent().children("div")[parseInt(index) - 1]);


        var table = this;
        var item = this;
        settings.drawCallback = function (callback_settings) {

            var api = new $.fn.dataTable.Api(callback_settings);
            if (typeof item.unique_id !== "undefined") {
                $(table).find("thead").find("th").removeAttr("style");
                var custom_table = $(item).closest(".customtable")[0];
                var info_page = $(item).DataTable().page.info();
                var language = $(item).DataTable().settings().init().language;
                //_PAGE_ _PAGES_
                var new_info = "";
                new_info = new_info.toString().replace("_PAGES_", parseFloat(parseFloat(info_page.pages)).formatMoney(0, ".", ","));
                var length_menu = language.lengthMenuCustom.toString().replace("_START_", parseFloat(parseFloat(info_page.start) + 1).formatMoney(0, ".", ","));
                length_menu = length_menu.toString().replace("_END_", parseFloat(parseFloat(info_page.end)).formatMoney(0, ".", ","));
                length_menu = length_menu.toString().replace("_TOTAL_", parseFloat(parseFloat(info_page.recordsDisplay)).formatMoney(0, ".", ","));
                if (parseFloat(info_page.recordsDisplay) != parseFloat(info_page.recordsTotal)) {
                    length_menu += "" + language.infoFiltered.toString().replace("_MAX_", parseFloat(info_page.recordsTotal).formatMoney(0, ".", ","));
                }
                $(custom_table).find("[name='page_details_label']").html(length_menu);
                if (parseFloat(info_page.recordsDisplay) === 0) {
                    var empty_info = language.infoEmpty;
                    if (parseFloat(info_page.recordsTotal) > 0) {
                        empty_info += "&nbsp;" + language.infoFiltered.toString().replace("_MAX_", parseFloat(info_page.recordsTotal).formatMoney(0, ".", ","));
                    }
                    $(custom_table).find("[name='page_details_label']").html(empty_info);
                }
                $(".default_ordering").parent().attr("data-toggle", 'tooltip');
                $(".default_ordering").parent().attr("data-placement", 'bottom');
                $(".default_ordering").parent().attr("title", '<?= $hide_columns ?>');
                $(".default_ordering").removeAttr("title");
                //     $("[data-toggle='tooltip']").tooltip("destroy");
                $("[data-toggle='tooltip']").tooltip({
                    trigger: 'hover', container: "body"
                });

                if (!$.fn.fieldTable.reloadCustoms[item.unique_id]) {

                    reloadFieldCustoms(item);
                    $.fn.fieldTable.reloadCustoms[item.unique_id] = true;
                }
            }
        };



        settings.createdRow = function (row, data, dataIndex) {
            var columns = $(table).DataTable().settings().init().columns;
            if (typeof columns !== "undefined") {
                columns = $.map(columns, function (n, i) {
                    return n.data;
                })
            }

            if (!cancel_row && typeof fields !== "undefined" && !(data instanceof Array) && data instanceof Object && (parseInt(data.id) > 0)) {
                var index = 0;

                var container_main;
                if ($('body').hasClass(".modal.in")) {
                    container_main = $("body").find(".modal.in");
                } else {
                    container_main = $("body");
                }

                var main_index = $(container_main).find("div[name='field_container']").index(fields);
                if (original_fields[main_index] === undefined) {
                    original_fields[main_index] = {};
                }
                $(fields).find("input[type!='search'],textarea,select,[name='extra_col']").each(function () {

                    if ($(this).is("input") && !$(this).hasClass("ignore-add") && parseInt($.inArray($(this).attr("type"), ["text", "number"])) !== -1) {
                        var input = document.createElement("input");
                        var attributes = $(this).prop("attributes");
                        $.each(attributes, function () {
                            $(input).attr(this.name, this.value);
                        });
                        if (typeof $(this).attr("data-status") !== "undefined") {
                            $(input).addClass("nimp");
                        } else {
                            $(input).removeClass("nimp");
                        }

                        var keys = columns;
                        $(input).addClass("for-edit");

                        if (original_fields[main_index][data.id] === undefined) {
                            original_fields[main_index][data.id] = {"values": [], "changes": 0};
                        }

                        original_fields[main_index][data.id]["values"].push(data[keys[parseInt(index) + 1]]);
                        $(input).on("change", function () {
                            var node = $(table).DataTable().row($(this).parents("tr")).node();
                            var id = $(table).DataTable().row($(this).parents("tr")).data().id;
                            var index = $(node).find(".for-edit").index($(this));
                            var value = original_fields[main_index][id].values[index];
                            var input_value = $(this).val();
                            var table_container = $(this).closest(".table-container")[0];

                            var index_arr = $($(container_main).find(".table-container")).index(table_container);
                            if ($(this).attr("type") === "number") {
                                value = parseFloat(value);
                                input_value = parseFloat(input_value);
                            }

                            if (value !== input_value) {
                                $($(this).closest("td")[0]).addClass("has-change");
                                original_fields[main_index][id].changes = $(node).find("td.has-change").length;
                                if (editted_rows[index_arr] === undefined) {
                                    editted_rows[index_arr] = [];
                                }
                                if (parseInt($.inArray(id, editted_rows[index_arr])) === -1) {
                                    editted_rows[index_arr].push(id);
                                }
                            } else {
                                $($(this).closest("td")[0]).removeClass("has-change");
                                original_fields[main_index][id].changes = $(node).find("td.has-change").length;
                                if (parseInt(original_fields[main_index][id].changes) === 0) {
                                    if (parseInt($.inArray(id, editted_rows[index_arr])) !== -1) {
                                        editted_rows[index_arr].splice($.inArray(id, editted_rows[index_arr]), 1);
                                    }
                                }
                            }

                        });

                        $(input).val(data[keys[parseInt(index) + 1]]);
                        $(row).find("td:eq(" + (parseInt(index) + 1) + ")").html(input);
                        if ($(this).hasClass("datepicker")) {
                            $(row).find("input").datepicker({dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true});
                        }
                        index++;
                    } else if ($(this).is("select") && !$(this).parent().hasClass("ignore-add")) {

                        if ($(this).hasClass("dropdown")) {
                            var keys = columns;
                            if (original_fields[main_index][data.id] === undefined) {
                                original_fields[main_index][data.id] = {"values": [], "changes": 0};
                            }
                            original_fields[main_index][data.id]["values"].push(data[keys[parseInt(index) + 1]]);

                            var value = $(row).find("td:eq(" + (parseInt(index) + 1) + ")").html();
                            var extra_col = "";
                            if (typeof $(this).attr("data-status") != "undefined") {
                                extra_col = "nimp";
                            }
                            var extra_attr = "";
                            if (typeof ($(this).attr("data-name")) !== "undefined") {
                                extra_attr = "data-name='" + $(this).attr("data-name") + "'";
                            }
                            var container;
                            if (parseInt($('body').find(".modal.in").length) > 0) {
                                container = $('body').find(".modal.in").find(".details-container,[name='field_container']");
                            } else {
                                container = $('body').find(".details-container,[name='field_container']");
                            }
                            if (typeof $(this).attr("data-type") !== "undefined" && $(this).attr("data-type") === "multiple") {
                                $(row).find("td:eq(" + (parseInt(index) + 1) + ")").html("<span><select data-type='multiple' data-val='" + value + "' " + extra_attr + " class='for-edit " + extra_col + " select2-field ajax' data-placeholder='" + $(this).attr("data-placeholder") + "'></select></span>");
                            } else {
                                $(row).find("td:eq(" + (parseInt(index) + 1) + ")").html("<select data-val='" + value + "' " + extra_attr + " class='for-edit " + extra_col + " select2-field ajax' data-placeholder='" + $(this).attr("data-placeholder") + "'></select>");
                            }

                            var select = $(row).find("td:eq(" + (parseInt(index) + 1) + ")").find("select");
                            var select_index = $(container).find(".select2-field.ajax").index($(this));

                            if (typeof $(select).attr("data-type") === "undefined") {
                                $(row).find("td:eq(" + (parseInt(index) + 1) + ")").find("select").ajaxSelect2(select2_ajax[select_index]);
                            } else {
                                var select_options;
                                if (typeof select2_ajax[select_index] === "Object") {
                                    select2_ajax[select_index].link = function (item) {
                                        changeMultiple(table, item, main_index, container_main)
                                    }
                                    select_options = select2_ajax[select_index];
                                } else {

                                    select_options = {url: select2_ajax[select_index], link: function (item) {
                                            changeMultiple(table, item, main_index, container_main)
                                        }};
                                }

                                $(row).find("td:eq(" + (parseInt(index) + 1) + ")").find("select").ajaxSelect2(select_options);

                            }
                            if (typeof $(select).attr("data-type") === "undefined") {
                                $(row).find("td:eq(" + (parseInt(index) + 1) + ")").find("select").on("change", function () {
                                    var node = $(table).DataTable().row($(this).parents("tr")).node();
                                    var id = $(table).DataTable().row($(this).parents("tr")).data().id;
                                    var index = $(node).find(".for-edit").index($(this));
                                    var value = original_fields[main_index][id].values[index];
                                    var table_container = $(this).closest(".table-container")[0];
                                    var index_arr = $($(container_main).find(".table-container")).index(table_container);
                                    var select_value = $(this).val();
                                    if (value !== select_value) {
                                        $($(this).closest("td")[0]).addClass("has-change");
                                        if (editted_rows[index_arr] === undefined) {
                                            editted_rows[index_arr] = [];
                                        }
                                        original_fields[main_index][id].changes = $(node).find("td.has-change").length;
                                        if (parseInt($.inArray(id, editted_rows[index_arr])) === -1) {
                                            editted_rows[index_arr].push(id);
                                        }
                                    } else {
                                        $($(this).closest("td")[0]).removeClass("has-change");
                                        original_fields[main_index][id].changes = $(node).find("td.has-change").length;
                                        if (parseInt(original_fields[main_index][id].changes) === 0) {
                                            if (parseInt($.inArray(id, editted_rows[index_arr])) !== -1) {
                                                editted_rows[index_arr].splice($.inArray(id, editted_rows[index_arr]), 1);
                                            }
                                        }
                                    }

                                });
                            }

                            index++;
                        } else {
                            var extra_attr = "";

                            if (typeof ($(this).attr("data-name")) !== "undefined") {
                                extra_attr = "data-name='" + $(this).attr("data-name") + "'";
                            }
                            var value = $(row).find("td:eq(" + (parseInt(index) + 2) + ")").html();
                            $(row).find("td:eq(" + (parseInt(index) + 2) + ")").html("<input " + extra_attr + " type='hidden' value='" + value + "'>");
                            index += 2;


                        }
                    } else if ($(this).is("textarea") && !$(this).hasClass("ignore-add")) {
                        var keys = columns;
                        if (original_fields[main_index][data.id] === undefined) {
                            original_fields[main_index][data.id] = {"values": [], "changes": 0};
                        }
                        original_fields[main_index][data.id]["values"].push(data[keys[parseInt(index) + 1]]);
                        var textarea = document.createElement("textarea");
                        var attributes = $(this).prop("attributes");
                        $.each(attributes, function () {
                            $(textarea).attr(this.name, this.value);
                        });
                        $(textarea).addClass("for-edit");
                        $(textarea).on("change", function () {
                            var node = $(table).DataTable().row($(this).parents("tr")).node();
                            var id = $(table).DataTable().row($(this).parents("tr")).data().id;
                            var index = $(node).find(".for-edit").index($(this));
                            var value = original_fields[main_index][id].values[index];
                            var table_container = $(this).closest(".table-container")[0];
                            var index_arr = $($(container_main).find(".table-container")).index(table_container);
                            if (value !== $(this).val()) {
                                $($(this).closest("td")[0]).addClass("has-change");
                                if (editted_rows[index_arr] === undefined) {
                                    editted_rows[index_arr] = [];
                                }
                                original_fields[main_index][id].changes = $(node).find("td.has-change").length;
                                if (parseInt($.inArray(id, editted_rows[index_arr])) === -1) {
                                    editted_rows[index_arr].push(id);
                                }
                            } else {
                                $($(this).closest("td")[0]).removeClass("has-change");
                                original_fields[main_index][id].changes = $(node).find("td.has-change").length;
                                if (parseInt(original_fields[main_index][id].changes) === 0) {
                                    if (parseInt($.inArray(id, editted_rows[index_arr])) !== -1) {
                                        editted_rows[index_arr].splice($.inArray(id, editted_rows[index_arr]), 1);
                                    }
                                }
                            }

                        });
                        if (typeof $(this).attr("data-status") !== "undefined") {
                            $(textarea).addClass("nimp");
                        }

                        $(textarea).val(data[keys[(parseInt(index) + 1)]].toString().replace(/\\n/g, "\n"));
                        $(textarea).css("width", "100%");
                        $(row).find("td:eq(" + (parseInt(index) + 1) + ")").html(textarea);
                        index++;
                    } else if ($(this).attr("name") === "extra_col" && !$(this).hasClass("ignore-add")) {
                        index++;
                    }

                });
                var keys = columns;
                if (parseInt($.inArray("button", keys)) !== -1) {
                    var button = document.createElement("button");
                    button.setAttribute("class", "btn btn-danger btn-xs for_delete");
                    $(button).on("click", function () {
                        var active_modal = $("body").find(".modal.in").length;
                        var container;
                        if (parseInt(active_modal) > 0) {
                            container = $("body").find(".modal.in");
                        } else {
                            container = $("body");
                        }
                        var table_container = $(this).closest(".table-container")[0];
                        var index = $(container).find(".table-container").index(table_container);

                        if (typeof deleted_rows[index] === "undefined") {
                            deleted_rows[index] = [];
                        }

                        deleted_rows[index].push($(table).DataTable().row($(this).parents("tr")).data().id);
                        if (typeof options.delete_function !== "undefined") {
                            options.delete_function($(this), $(table));
                        }
                        $(table).DataTable().row($(this).parents("tr")).remove().draw();

                        resetUniques(table)
                    });
                    $(button).html("<i class='fa fa-trash'></i>&nbsp;<?= $delete ?>");
                    $(row).find("td:eq(" + $.inArray("button", keys) + ")").html(button);
                }

                resetUniques(table);

            }
            if (add_field_mode) {

                var counter = 0;

                var vals = global_fields.vals;
                var node_index = $($(table).DataTable().$('[name="new_fields"]')[0]).parents("tr");
                var row = $(table).DataTable().row(node_index).node();
                $(table).DataTable().$("[name='new_fields']").each(function () {

                    if ($(this).hasClass("datepicker")) {
                        $(this).datepicker({dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true});
                        var format = JSON.parse($(this).attr("format"));
                        if (checkDate(vals[counter], format.delimiter, format.date_format)) {
                            $(this).val(vals[counter]);
                        } else {
                            $(this).datepicker("setDate", new Date());
                        }
                    } else if ($(this).is('select')) {

                        if ($(this).attr("name") !== "multiple_select") {

                            var final_index = $(this).attr("data-index");
                            $(this).attr("data-val", vals[counter]);

                            $(this).ajaxSelect2(select2_ajax[final_index]);
                        }

                    } else if ($(this).is("button")) {
                        if ($(this).hasClass("for_delete")) {
                            $(this).on("click", function () {

                                if (typeof options.delete_function !== "undefined") {
                                    options.delete_function($(this), $(table));
                                }
                                var node = $(table).DataTable().row($(this).parents("tr")).node();
                                $(node).find(".datepicker").each(function () {
                                    $(this).datepicker("destroy");
                                });
                                resetUniques(table);
                                $(table).DataTable().row($(this).parents("tr")).remove().draw();
                            });
                        }
                    } else {

                        $(this).val(vals[counter]);

                    }
                    counter++;
                });


                $(table).DataTable().$("[name='new_fields']").removeAttr("name", "new_fields");
                clearFields(global_fields, {reset: false});
                $(global_fields).find("[name='direct_add']").trigger("change");
                add_field_mode = false;
                global_fields = undefined;

            }




            if (typeof options.created_row_function !== "undefined") {
                options.created_row_function(row, data, dataIndex, $(table));
            }




            if (typeof field_row_initialize === "function") {

                field_row_initialize(row, table);
            }

        }

        var check_custom = $(this).closest("div.customtable");
        if (parseInt(check_custom.length) > 0) {

            var check_loaded = $(check_custom).find("div.table-pager");
            var buttons = $(check_custom).find(".button-box");
            $.each(check_custom.children("div"), function (index, value) {
                var check_table = $(value).find("table[name='dataTable']")[0];
                if (parseInt($(check_table).length) > 0) {
                    field_table_custom_id++;

                    if (typeof item.unique_id === "undefined") {
                        item.unique_id = field_table_custom_id;
                        $.fn.fieldTable.reloadCustoms[item.unique_id] = false;
                    }
                    settings.dom = "r";
                    if (parseInt(check_loaded.length) > 0) {
                        $(check_loaded).remove();
                    }
                    $(this).prepend("<div class='table_toolbar'><div class='table-pager'></div></div>");
                    var contents = document.createElement("table");
                    contents.setAttribute("class", "pg-table");
                    var pager_tbody = document.createElement("tbody");
                    var tr_page_main = document.createElement("tr");
                    var td_left = document.createElement("td");
                    td_left.setAttribute("width", "35%");
                    td_left.setAttribute("class", "pg-data");
                    td_left.setAttribute("align", "left");
                    var settings_table = document.createElement("table");
                    settings_table.setAttribute("class", "setting");
                    var settings_tbody = document.createElement("tbody");
                    var settings_button_tr = document.createElement("tr");
                    settings_button_tr.setAttribute("name", "button-container");
                    $(buttons).find("button").each(function (index_button, button) {

                        var td_button = document.createElement("td");
                        $(td_button).append(button);
                        $(settings_button_tr).append(td_button);
                    });
                    if (typeof settings.mode === "undefined") {
                        var default_buttons = [{"button": "<select  name='hide_cols' class='table_cols col-select select_table' multiple='multiple'></select>"}];
                        $.each(default_buttons, function (index, value) {
                            var td_button = document.createElement("td");
                            $(td_button).append(value.button);
                            $(settings_button_tr).append(td_button);
                        });

                        $(settings_tbody).append(settings_button_tr);
                    }
                    $(settings_table).append(settings_tbody);
                    $(td_left).append(settings_table);
                    var td_center = document.createElement("td");
                    td_center.setAttribute("class", "pg-data");
                    td_center.setAttribute("width", "30%");
                    td_center.setAttribute("align", "center");
                    var table_page = document.createElement("table");
                    table_page.setAttribute("class", "pages");
                    table_page.setAttribute("name", "pagination_controls")
                    var tbody_page = document.createElement("tbody");
                    var tr_page = document.createElement("tr");
                    var pagination_controls = [
                        {"button": "<span name='page_details_label'></span>"}
                    ];
                    $.each(pagination_controls, function (index, value) {
                        var td = document.createElement("td");
                        if (typeof value.class !== "undefined") {
                            td.setAttribute("class", value.class);
                        }
                        td.innerHTML = (value.button);
                        tr_page.appendChild(td);
                    });
                    tbody_page.appendChild(tr_page);
                    table_page.appendChild(tbody_page);
                    td_center.appendChild(table_page);
                    var td_right = document.createElement("td");
                    td_right.setAttribute("class", "pg-data");
                    td_right.setAttribute("align", "right");
                    td_right.setAttribute("width", "35%");
                    var search_table = document.createElement("table");
                    search_table.setAttribute("class", "pages");
                    search_table.setAttribute("name", "search_controls");
                    var search_tbody = document.createElement("tbody");
                    var search_tr = document.createElement("tr");
                    var search_td = document.createElement("td");
                    var div_search = document.createElement("div");
                    div_search.setAttribute("class", "search");
                    div_search.setAttribute('data-container', "search");
                    var div_search_select = document.createElement("div");
                    div_search_select.setAttribute("class", "pull-right");
                    var search_select = document.createElement("select");
                    search_select.setAttribute("class", "col-select search_col");
                    search_select.setAttribute("name", "search_select");
                    search_select.setAttribute("multiple", "multiple");
                    div_search_select.appendChild(search_select);
                    div_search.appendChild(div_search_select);
                    var div_search_bar = document.createElement("div");
                    div_search_bar.setAttribute("class", "input-group searchbar pull-right");
                    var search_bar = document.createElement("input");
                    search_bar.setAttribute("type", "text");
                    search_bar.setAttribute("name", "search_box");
                    search_bar.setAttribute("class", "clearable form-control input-sm inputsearch srch_value");
                    search_bar.setAttribute("placeholder", "Search...");
                    var search_button_div = document.createElement("div");
                    search_button_div.setAttribute("class", "input-group-btn");
                    var search_button = document.createElement("button");
                    search_button.setAttribute("class", "btn btn-primary btn-flat btn-sm srchbtn");
                    search_button.setAttribute("name", "srch_btn");
                    var search_button_icon = document.createElement("i");
                    search_button_icon.setAttribute("class", "glyphicon glyphicon-search");
                    search_button.appendChild(search_button_icon);
                    search_button_div.appendChild(search_button);
                    div_search_bar.appendChild(search_bar);
                    div_search_bar.appendChild(search_button_div);
                    div_search.appendChild(div_search_bar);
                    search_td.appendChild(div_search);
                    search_tr.appendChild(search_td);
                    search_tbody.appendChild(search_tr);
                    search_table.appendChild(search_tbody);
                    if (typeof settings.mode === "undefined") {
                        td_right.appendChild(search_table);
                    }
                    tr_page_main.appendChild(td_left);
                    tr_page_main.appendChild(td_center);
                    tr_page_main.appendChild(td_right);
                    pager_tbody.appendChild(tr_page_main);
                    contents.appendChild(pager_tbody);
                    $(buttons).remove();
                    if (typeof settings.mode !== "undefined") {
                        $(check_custom).find("[name='alternate_search']").remove();
                        $(check_custom).prepend('<div name="alternate_search" data-container="search" class="table-top">\n\
                        <div class="row"><div class="col-sm-10 col-md-8 col-lg-10 no-padding">\n\
                <form class="form-horizontal"><div class="form-group"><label class="control-label col-sm-4 \n\
col-lg-5">search:</label><div class="col-sm-8 col-lg-7"><input  \n\
type="text" class="clearable form-control input-sm inputsearch srch_value" name="search_box" placeholder="Search..." \n\
aria-describedby="basic-addon2"></div></div></form></div><div class="col-sm-2 col-md-4 col-lg-2 \n\
no-padding"><button name="srch_btn" class="btn btn-primary btn-flat btn-sm srchbtn pull-left" \n\
type="submit"><i class="glyphicon glyphicon-search"></i></button><div \n\
class="pull-left"><select  name="search_select"  \n\
class="col-select search_col pull" multiple="multiple"></select></div><div class="clearfix"></div> \n\
</div> </div></div>');
                    }

                    $(check_custom).find(".table-pager").append(contents);

//                    $.each(settings.lengthMenu, function (length_index, length_value) {
//                        var option = document.createElement("option");
//                        option.setAttribute("value", length_value);
//                        option.innerHTML = length_value;
//                        $(check_custom).find("[name='page_length']").append(option);
//                    });
//                    $(check_custom).find("[name='page_length']").on("change", function () {
//                        $.fn.serverTable.pause_reload[item.unique_id] = true;
//                        $(item).DataTable().page.len($(this).val()).draw();
//                    });
//                    $(check_custom).find("[name='refresh_button']").on("click", function () {
//                        $.fn.serverTable.pause_reload[item.unique_id] = true;
//                        $(item).DataTable().ajax.reload(null, false);
//                    });
//                    $(check_custom).find("[name='page_button']").on("click", function () {
//                        var mode = "";
//                        switch (parseInt($(this).attr("value"))) {
//                            case 1:
//                                mode = "first";
//                                break;
//                            case 2:
//                                mode = "previous";
//                                break;
//                            case 3:
//                                mode = "next";
//                                break;
//                            case 4:
//                                mode = "last";
//                                break;
//
//                        }
//                        $.fn.serverTable.pause_reload[item.unique_id] = true;
//                        $(item).DataTable().page(mode).draw('page');
//                    });
                }
            });
        }

        if ($.fn.DataTable.isDataTable(this)) {
            $(this).DataTable().clear().draw();
            $(this).DataTable().destroy();
            $(this).find(".display_col").removeClass("display_col");
        }
        var language = $.extend(true, {}, settings.language);
        language.info = "<?= $info_message_field_table ?>";
        settings.language = language;
        $(this).DataTable(settings);


    };
    $.fn.fieldTable.reloadCustoms = {};
    function changeMultiple(table, item, main_index, container_main) {
        if (parseInt($(item).closest("span:visible").length) === 0) {
            return;
        }
        var node = $(table).DataTable().row($(item).parents("tr")).node();
        var id = $(table).DataTable().row($(item).parents("tr")).data().id;
        var index = -1;
        if (typeof $(this).attr("name") !== "undefined" && $(item).attr("name") === "multiple_select") {

            index = $(node).find(".for-edit").index($(item).closest(".multi-container").find("select:eq(0)"));
        } else {
            index = $(node).find(".for-edit").index($(item));
        }
        var value = JSON.parse(original_fields[main_index][id].values[index]);

        if (parseInt(value.length) === 0) {
            value = "0";
        } else {
            value = value.sort().join(",");
        }
        var table_container = $(item).closest(".table-container")[0];
        var index_arr = $($(container_main).find(".table-container")).index(table_container);

        var select_value = (typeof $(item).val() === "Array") ? $(item).val().sort().join(",") : $(item).val().toString();


        if (value !== select_value) {
            $($(item).closest("td")[0]).addClass("has-change");
            if (editted_rows[index_arr] === undefined) {
                editted_rows[index_arr] = [];
            }
            original_fields[main_index][id].changes = $(node).find("td.has-change").length;
            if (parseInt($.inArray(id, editted_rows[index_arr])) === -1) {
                editted_rows[index_arr].push(id);
            }
        } else {
            $($(item).closest("td")[0]).removeClass("has-change");
            original_fields[main_index][id].changes = $(node).find("td.has-change").length;

            if (parseInt(original_fields[main_index][id].changes) === 0) {
                editted_rows[index_arr].splice($.inArray(id, editted_rows[index_arr]), 1);
            }
        }

    }
    $.fn.fieldTable.default_settings = {autoWidth: false, language: datatable_language_options, paging: false};
    $.fn.serverTable = function (options) {
        var data_options = {};
        if (typeof options !== "undefined") {
            data_options = options;
        }

        var settings = $.extend({}, $.fn.serverTable.default_settings, data_options);

        var not_searchable = [];
        var not_orderable = [];
        var column_defs = [];
        if (!settings.searching || typeof settings.searching === "undefined") {
            var index = 0;
            $(this).find("thead").find("th").each(function () {

                if ($(this).hasClass("nsearch")) {

                    not_searchable.push(index);
                }
                index++;
            });
        }

        if (!settings.ordering || typeof settings.ordering === "undefined") {
            var index = 0;
            $(this).find("thead").find("th").each(function () {
                if ($(this).hasClass("norder")) {
                    not_orderable.push(index);
                }
                index++;
            });
        }

        if (parseInt(not_searchable.length) > 0) {
            column_defs.push({"searchable": false, "aTargets": not_searchable});
        }
        if (parseInt(not_orderable.length) > 0) {
            column_defs.push({"orderable": false, "aTargets": not_orderable});
        }
        if (parseInt(column_defs.length) > 0) {

            if (typeof data_options.columnDefs !== "undefined" && parseInt(data_options.columnDefs.length) > 0) {
                var no_searchable = false;
                var no_orderable = false;
                $.each(data_options.columnDefs, function (index, value) {
                    var key = Object.keys(value);
                    if (parseInt($.inArray("searchable"), key) !== -1) {

                        data_options.columnDefs[index]["targets"] = $.merge(data_options.columnDefs[index]["targets"], not_searchable);
                    } else if (parseInt($.inArray("orderable"), key) !== -1) {
                        data_options.columnDefs[index]["targets"] = $.merge(data_options.columnDefs[index]["targets"], not_orderable);
                    } else {
                        if (parseInt($.inArray("searchable"), key) === -1) {
                            no_searchable = true;
                        }
                        if (parseInt($.inArray("orderable"), key) === -1) {
                            no_orderable = true;
                        }

                    }
                });
                if (no_orderable) {
                    data_options.columnDefs.push({"orderable": false, "targets": not_orderable});
                }
                if (no_searchable) {
                    data_options.columnDefs.push({"searchable": false, "targets": not_searchable});
                }
                column_defs = data_options.columnDefs;
            }

            settings.columnDefs = column_defs;
        }
        var item = this;
        $.fn.serverTable.unique_id++;
        item.unique_id = $.fn.serverTable.unique_id;

        if (typeof $.fn.serverTable.pause_reload[item.unique_id] === "undefined") {
            $.fn.serverTable.pause_reload[item.unique_id] = false;
        }
        if (typeof $.fn.serverTable.ajax_parms[item.unique_id] === "undefined") {
            $.fn.serverTable.ajax_parms[item.unique_id] = [];
        }

        if ($.fn.DataTable.isDataTable($(this))) {
            $(this).unbind();
            $(this).DataTable().clear();
            $(this).DataTable().destroy();
        }
        var url = settings.url;
        var extra_data = {};
        if (typeof settings.extra_data !== "undefined") {
            var extra_data = settings.extra_data;
        }
        var method = "GET";
        if (typeof options.method !== "undefined") {
            method = "POST";
        }

        settings.ajax = {url: url, method: method, data: function (d) {

                var cancel_search = [];
                var check_cols = $(item).DataTable().settings().init().columns;
                $.each(check_cols, function (index) {
                    var visible = $(item).DataTable().column(index).visible();
                    if (!visible) {
                        cancel_search.push(check_cols[index].data.toString().replace(/o_|`/g, ""));
                    }
                });
                extra_data.cancel_search = cancel_search;
                var keys = Object.keys(extra_data);
                $.each(keys, function (index, value) {
                    d[value] = extra_data[value];
                });
            }
        };
        settings.drawCallback = function (callback_settings) {

            var api = new $.fn.dataTable.Api(callback_settings);
            var custom_table = $(item).closest(".customtable")[0];

            var info_page = $(item).DataTable().page.info();
            var language = $(item).DataTable().settings().init().language;
            //_PAGE_ _PAGES_
            var new_info = "";
            var value = 0;
            value = parseFloat(parseFloat(info_page.page) + 1);
            if (parseFloat(info_page.recordsDisplay) === 0) {
                new_info = language.info.toString().replace("_PAGE_", "0");
            } else {
                new_info = language.info.toString().replace("_PAGE_", "<input onkeypress='numberOnly(event,2)' name='page_input_control' type='text' value='" + value + "' class='page-input'>");
            }
            new_info = new_info.toString().replace("_PAGES_", parseFloat(parseFloat(info_page.pages)).formatMoney(0, ".", ","));
            var length_menu = language.lengthMenuCustom.toString().replace("_START_", parseFloat(parseFloat(info_page.start) + 1).formatMoney(0, ".", ","));
            length_menu = length_menu.toString().replace("_END_", parseFloat(parseFloat(info_page.end)).formatMoney(0, ".", ","));
            length_menu = length_menu.toString().replace("_TOTAL_", parseFloat(parseFloat(info_page.recordsDisplay)).formatMoney(0, ".", ","));
            if (parseFloat(info_page.recordsDisplay) != parseFloat(info_page.recordsTotal)) {
                length_menu += "" + language.infoFiltered.toString().replace("_MAX_", parseFloat(info_page.recordsTotal).formatMoney(0, ".", ","));
            }
            $(custom_table).find("[name='page_details_label']").html(length_menu);
            $(custom_table).find("[name='page_change_control']").html(new_info);
            $(custom_table).find("[name='page_change_control']").find("[name='page_input_control']").on("change", function () {
                $.fn.serverTable.pause_reload[item.unique_id] = true;
                $(item).DataTable().page(parseFloat($(this).val()) - 1).draw(false);
            });

            if (!$.fn.serverTable.pause_reload[item.unique_id]) {

                reloadCustomFields(item);

            } else {
                $.fn.serverTable.pause_reload[item.unique_id] = false;
            }
            $(custom_table).find("[name='page_button']").css("display", "");

            if (parseFloat(info_page.recordsDisplay) === 0) {
                $(custom_table).find("[name='page_button']").css("display", "none");
                var empty_info = language.infoEmpty;
                if (parseFloat(info_page.recordsTotal) > 0) {
                    empty_info += "&nbsp;" + language.infoFiltered.toString().replace("_MAX_", parseFloat(info_page.recordsTotal).formatMoney(0, ".", ","));
                }
                $(custom_table).find("[name='page_details_label']").html(empty_info);
            }
            $(".default_ordering").parent().attr("data-toggle", 'tooltip');
            $(".default_ordering").parent().attr("data-placement", 'bottom');
            $(".default_ordering").parent().attr("title", '<?= $hide_columns ?>');
            $(".default_ordering").removeAttr("title");
            //     $("[data-toggle='tooltip']").tooltip("destroy");
            $("[data-toggle='tooltip']").tooltip({
                trigger: 'hover', container: "body"
            });

            if (typeof settings.onDraw === "function") {
                settings.onDraw();
            }
        };
        settings.rowCallback = function (row, data, index) {
            if (typeof settings.onRowAdd === "function") {
                settings.onRowAdd(row, data);
            }
        }

        delete settings.url;
        delete settings.extra_data;
        var check_custom = $(this).closest("div.customtable");
        if (parseInt(check_custom.length) > 0) {
            var check_loaded = $(check_custom).find("div.table-pager");
            var buttons = $(check_custom).find(".button-box");
            $.each(check_custom.children("div"), function (index, value) {
                var check_table = $(value).find("table[name='dataTable']")[0];
                if (parseInt($(check_table).length) > 0) {
                    $.fn.serverTable.ajax_parms[item.unique_id] = [];
                    $.fn.serverTable.pause_reload[item.unique_id] = false;
                    settings.dom = "r";
                    if (parseInt(check_loaded.length) > 0) {
                        $(check_loaded).remove();
                    }
                    $(this).prepend("<div class='table_toolbar'><div class='table-pager'></div></div>");
                    var contents = document.createElement("table");
                    contents.setAttribute("class", "pg-table");
                    var pager_tbody = document.createElement("tbody");
                    var tr_page_main = document.createElement("tr");
                    var td_left = document.createElement("td");
                    td_left.setAttribute("width", "15%");
                    td_left.setAttribute("class", "pg-data");
                    td_left.setAttribute("align", "left");
                    var settings_table = document.createElement("table");
                    settings_table.setAttribute("class", "setting");
                    var settings_tbody = document.createElement("tbody");
                    var settings_button_tr = document.createElement("tr");
                    settings_button_tr.setAttribute("name", "button-container");
                    $(buttons).find("button").each(function (index_button, button) {

                        var td_button = document.createElement("td");
                        $(td_button).append(button);
                        $(settings_button_tr).append(td_button);
                    });
                    if (typeof settings.mode === "undefined") {
                        var default_buttons = [{"button": "<select  name='hide_cols' class='table_cols col-select select_table' multiple='multiple'></select>"}
                            , {"button": "<a data-toggle='tooltip' data-placement='bottom' title='<?= $refresh ?>' name='refresh_button' href='javascript:void(0)'><i class='fa fa-refresh color-teal'></i></a>"}];
                        $.each(default_buttons, function (index, value) {
                            var td_button = document.createElement("td");
                            $(td_button).append(value.button);
                            $(settings_button_tr).append(td_button);
                        });

                        $(settings_tbody).append(settings_button_tr);
                    }
                    $(settings_table).append(settings_tbody);
                    $(td_left).append(settings_table);
                    var td_center = document.createElement("td");
                    td_center.setAttribute("class", "pg-data");
                    td_center.setAttribute("align", "center");
                    var table_page = document.createElement("table");
                    table_page.setAttribute("class", "pages");
                    table_page.setAttribute("name", "pagination_controls")
                    var tbody_page = document.createElement("tbody");
                    var tr_page = document.createElement("tr");
                    var pagination_controls = [
                        {"button": "<a href='javascript:void(0)' class='First_Page' value='1' name='page_button' data-toggle='tooltip' data-placement='bottom' title='First Page'><i class='fa fa-angle-double-left color-default'></i></a>"},
                        {"button": "<a href='javascript:void(0)' class='Prev_Page' value='2' name='page_button' data-toggle='tooltip' data-placement='bottom' title='Previous Page'><i class='fa fa-angle-left color-default'></i></a>"},
                        {class: "pagenum", "button": "<span name='page_change_control'></span>", class: "filter-text"},
                        {"button": "<a href='javascript:void(0)' class='Next_Page' value='3' name='page_button' data-toggle='tooltip' data-placement='bottom' title='Next Page'><i class='fa fa-angle-right color-default'></i></a>"},
                        {"button": "<a href='javascript:void(0)' class='Last_Page' value='4' name='page_button' data-toggle='tooltip' data-placement='bottom' title='Last Page'><i class='fa fa-angle-double-right color-default'></i></a>"},
                        {"button": "<span class='separator'></span>"},
                        {"button": "<select name='page_length' class='page-list row_limit'></select>"},
                        {"button": "<span name='page_details_label'></span>", class: "filter-text"}
                    ];
                    $.each(pagination_controls, function (index, value) {
                        var td = document.createElement("td");
                        if (typeof value.class !== "undefined") {
                            td.setAttribute("class", value.class);
                        }
                        td.innerHTML = (value.button);
                        tr_page.appendChild(td);
                    });
                    tbody_page.appendChild(tr_page);
                    table_page.appendChild(tbody_page);
                    td_center.appendChild(table_page);
                    var td_right = document.createElement("td");
                    td_right.setAttribute("class", "pg-data");
                    td_right.setAttribute("align", "right");
                    var search_table = document.createElement("table");
                    search_table.setAttribute("class", "pages");
                    search_table.setAttribute("name", "search_controls");
                    var search_tbody = document.createElement("tbody");
                    var search_tr = document.createElement("tr");
                    var search_td = document.createElement("td");
                    var div_search = document.createElement("div");
                    div_search.setAttribute("class", "search");
                    div_search.setAttribute("data-container", "search");
                    var div_search_select = document.createElement("div");
                    div_search_select.setAttribute("class", "pull-right");
                    var search_select = document.createElement("select");
                    search_select.setAttribute("class", "col-select search_col");
                    search_select.setAttribute("name", "search_select");
                    search_select.setAttribute("multiple", "multiple");
                    div_search_select.appendChild(search_select);
                    div_search.appendChild(div_search_select);
                    var div_search_bar = document.createElement("div");
                    div_search_bar.setAttribute("class", "input-group searchbar pull-right");
                    var search_bar = document.createElement("input");
                    search_bar.setAttribute("type", "text");
                    search_bar.setAttribute("name", "search_box");
                    search_bar.setAttribute("class", "clearable form-control input-sm inputsearch srch_value");
                    search_bar.setAttribute("placeholder", "Search...");
                    var search_button_div = document.createElement("div");
                    search_button_div.setAttribute("class", "input-group-btn");
                    var search_button = document.createElement("button");
                    search_button.setAttribute("class", "btn btn-primary btn-flat btn-sm srchbtn");
                    search_button.setAttribute("name", "srch_btn");
                    var search_button_icon = document.createElement("i");
                    search_button_icon.setAttribute("class", "glyphicon glyphicon-search");
                    search_button.appendChild(search_button_icon);
                    search_button_div.appendChild(search_button);
                    div_search_bar.appendChild(search_bar);
                    div_search_bar.appendChild(search_button_div);
                    div_search.appendChild(div_search_bar);
                    search_td.appendChild(div_search);
                    search_tr.appendChild(search_td);
                    search_tbody.appendChild(search_tr);
                    search_table.appendChild(search_tbody);
                    if (typeof settings.mode === "undefined") {
                        td_right.appendChild(search_table);
                    }
                    tr_page_main.appendChild(td_left);
                    tr_page_main.appendChild(td_center);
                    tr_page_main.appendChild(td_right);
                    pager_tbody.appendChild(tr_page_main);
                    contents.appendChild(pager_tbody);
                    $(buttons).remove();
                    if (typeof settings.mode !== "undefined") {
                        $(check_custom).find("[name='alternate_search']").remove();
                        $(check_custom).prepend('<div data-container="search" name="alternate_search" class="table-top">\n\
                        <div class="row"><div class="col-sm-10 col-md-8 col-lg-10 no-padding">\n\
                <form class="form-horizontal"><div class="form-group"><label class="control-label col-sm-4 \n\
col-lg-5">search:</label><div class="col-sm-8 col-lg-7"><input  \n\
type="text" class="clearable form-control input-sm inputsearch srch_value" name="search_box" placeholder="Search..." \n\
aria-describedby="basic-addon2"></div></div></form></div><div class="col-sm-2 col-md-4 col-lg-2 \n\
no-padding"><button name="srch_btn" class="btn btn-primary btn-flat btn-sm srchbtn pull-left" \n\
type="submit"><i class="glyphicon glyphicon-search"></i></button><div \n\
class="pull-left"><select  name="search_select"  \n\
class="col-select search_col pull" multiple="multiple"></select></div><div class="clearfix"></div> \n\
</div> </div></div>');
                    }
                    $(check_custom).find(".table-pager").append(contents);

                    $.each(settings.lengthMenu, function (length_index, length_value) {
                        var option = document.createElement("option");
                        option.setAttribute("value", length_value);
                        option.innerHTML = length_value;
                        $(check_custom).find("[name='page_length']").append(option);
                    });
                    $(check_custom).find("[name='page_length']").on("change", function () {
                        $.fn.serverTable.pause_reload[item.unique_id] = true;
                        $(item).DataTable().page.len($(this).val()).draw();
                    });
                    $(check_custom).find("[name='refresh_button']").on("click", function () {
                        $.fn.serverTable.pause_reload[item.unique_id] = true;
                        $(item).DataTable().ajax.reload(null, false);
                    });
                    $(check_custom).find("[name='page_button']").on("click", function () {
                        var mode = "";
                        switch (parseInt($(this).attr("value"))) {
                            case 1:
                                mode = "first";
                                break;
                            case 2:
                                mode = "previous";
                                break;
                            case 3:
                                mode = "next";
                                break;
                            case 4:
                                mode = "last";
                                break;

                        }
                        $.fn.serverTable.pause_reload[item.unique_id] = true;
                        $(item).DataTable().page(mode).draw('page');
                    });
                }
            });
        }

        $(this).DataTable(settings);

        $(this).DataTable().on("xhr", function () {
            var old_ajax_parms = $.fn.serverTable.ajax_parms[item.unique_id];

            var old_order = "";
            if (parseInt(old_ajax_parms.length) > 0) {
                old_order = old_ajax_parms.order[0]["column"] + " " + old_ajax_parms.order[0]["dir"];
            }
            var data = $(item).DataTable().ajax.params();

            var new_order = data.order[0]["column"] + " " + data.order[0]["dir"];
            if (old_order !== new_order && old_order !== "") {

                $.fn.serverTable.pause_reload[item.unique_id] = true;
            }

            var visible = $(item).DataTable().column(data.order[0]["column"]).visible();
            var order_by = 0;
            var not_visible = $(item).DataTable().columns(".display_col")["0"];
            $.each(data.columns, function (order_index, order_value) {

                if ($(item).DataTable().column(order_index).visible() && parseInt($.inArray(parseInt(order_index), not_visible)) === -1) {

                    order_by = order_index;
                    return false;
                }
            });
            $.fn.serverTable.ajax_parms[item.unique_id] = data;
            if (!visible) {

                $.fn.serverTable.pause_reload[item.unique_id] = true;
                $(item).DataTable().order([order_by, "asc"]).draw();
            }
        });
    }

    function reloadCustomFields(item) {

        //"length":10,"recordsTotal":2,"recordsDisplay":2,"serverSide":true}
        //page_change_control [for changes pages-the one with input field]
        //page_button [controls for first-1 previous-2 next-3 last-4]
        //srch_btn [button for search click]
        //search_select [multiselect for search]
        //search_box [input field]
        //hide_cols [multiselect for hiding columns]
        //refresh_button [refresh button]

        var columns = $.fn.serverTable.ajax_parms[item.unique_id].columns;
        var custom_table = $(item).closest(".customtable")[0];
        $(custom_table).find("[name='search_select']").html("");
        $(custom_table).find("[name='hide_cols']").html("");
        $(custom_table).find("[name='search_box']").removeClass('x onX').val('').change();
        $(custom_table).find("[name='search_select']").multiselect('destroy');
        $.fn.serverTable.pause_reload[item.unique_id] = true;
        // $(item).DataTable().columns().search('').draw();

        $.each(columns, function (index, value) {
            if (value.searchable) {

                var option = document.createElement("option");
                option.setAttribute("value", index);
                option.innerHTML = value.name;
                $(custom_table).find("[name='search_select']").append(option);
            }
        });
        $(custom_table).find("[name='hide_cols']").multiselect("destroy");
        $.each(columns, function (index, value) {
            if (value.orderable) {
                var option = document.createElement("option");
                option.setAttribute("value", index);
                option.innerHTML = value.name;
                $(custom_table).find("[name='hide_cols']").append(option);
            }
        });
        $(custom_table).find("[name='search_select']").multiselect({
            enableCaseInsensitiveFiltering: true,
            selectAllValue: -1,
            selectAllText: '<?= $all ?>', includeSelectAllOption: true, enableFiltering: true,
            onSelectAll: function () {

            }, onDeselectAll: function () {

            }, onChange: function (option, checked) {


            }});
        $(custom_table).find("[name='hide_cols']").multiselect({
            enableCaseInsensitiveFiltering: true,
            buttonClass: "default_ordering",
            selectAllValue: -1,
            selectAllText: '<?= $all ?>', includeSelectAllOption: true, enableFiltering: true,
            onSelectAll: function () {

            }, onDeselectAll: function () {

            }, onChange: function (option, checked) {


            }});
        $(custom_table).find("[name='hide_cols']").on("change", function () {

            var hidden_cols = [];
            $(custom_table).find("[name='search_select']").find("option").prop("disabled", false);
            var deselect_search_cols = [];
            var search_value = "";
            $.each($.fn.serverTable.ajax_parms[item.unique_id]["columns"], function (index_ajax, value_ajax) {
                var value = $.fn.serverTable.ajax_parms[item.unique_id]["columns"][index_ajax]["search"]["value"];
                if (value !== "") {
                    search_value = value;
                    return false;
                }
            });
            if ($(this).val() === null) {
                $(this).multiselect("selectAll", false);
            } else {
                var not_selected = $(this).find("option:not(:selected)");
                $(not_selected).each(function (index, value) {
                    if (columns[$(value).attr("value")].data.toString().substring(0, 2) !== "o_") {
                        $(custom_table).find("[name='search_select']").find("option[value='" + $(value).attr("value") + "']").prop("disabled", true);
                        deselect_search_cols.push($(value).attr("value"));
                    } else {
                        $(custom_table).find("[name='search_select']").find("option[value='" + (parseInt($(value).attr("value")) + 1) + "']").prop("disabled", true);
                        deselect_search_cols.push(parseInt($(value).attr("value")) + 1);
                    }
                    hidden_cols.push($(value).attr("value"));
                });
            }

            $(custom_table).find("[name='search_select']").multiselect("refresh");
            if (parseInt(deselect_search_cols.length) > 0) {

                $(custom_table).find("[name='search_select']").multiselect("deselect", deselect_search_cols);
            }
            $.fn.serverTable.pause_reload[item.unique_id] = true;
            if (parseInt(hidden_cols.length) > 0) {
                $(item).DataTable().columns().visible(true).columns(hidden_cols).visible(false).draw(false);
            } else {
                $(item).DataTable().columns().visible(true).draw(false);
            }
            if (parseInt(deselect_search_cols.length) > 0) {
                if ($(custom_table).find("select[name='search_select']").val() == null) {
                    $(custom_table).find("select[name='search_select']").multiselect("selectAll", false);
                }
                $.fn.serverTable.pause_reload[item.unique_id] = true;
                $(item).DataTable().columns().search("").columns($(custom_table).find("[name='search_select']").val()).search(search_value).draw(false);
            }
        });
        $(custom_table).find("[name='search_select']").multiselect("selectAll", false);
        $(custom_table).find("[name='hide_cols']").multiselect("selectAll", false);
        $(custom_table).find("[name='srch_btn']").on("click", function () {

            var container = $(this).closest(".customtable")[0];
            var value = $(container).find("[name='search_select']").val();
            if (value === null) {
                $(container).find("[name='search_select']").multiselect("selectAll", false);
                value = $(container).find("[name='search_select']").val();
            }
            $.fn.serverTable.pause_reload[item.unique_id] = true;
            $($(container).find("table[name='dataTable']")).DataTable().columns().search('').columns(value).search($(container).find("input[name='search_box']").val()).draw();
        });
    }

    /*Server Table settings*/
    $.fn.serverTable.ajax_parms = {};
    $.fn.serverTable.pause_reload = {};
    $.fn.serverTable.unique_id = 0;
    //fixedHeader: {header: true, headerOffset: 50},
    $.fn.serverTable.default_settings = {processing: true, serverSide: true, autoWidth: false, language: datatable_language_options, pageLength: 10, lengthMenu: [10, 25, 50, 75, 100]};
    $('.close').on('click', function () {

        if ($(this).parent().hasClass("alert")) {
            $(this).parent().hide(1000);
        }
    });
    function numberOnly(item, type) {
        if (parseInt(type) === 1) {
            if (!(item.charCode === 0 || item.charCode === 46 || (item.charCode >= 48 && item.charCode <= 57))) {

                item.preventDefault();
            }
        } else {
            if (!(item.charCode === 0 || (item.charCode >= 48 && item.charCode <= 57))) {

                item.preventDefault();
            }
        }

    }
    var enforceModalFocusFn = $.fn.modal.Constructor.prototype.enforceFocus;
    $.fn.modal.Constructor.prototype.enforceFocus = function () {
    };
    $('.modal').on('hidden', function () {
        $.fn.modal.Constructor.prototype.enforceFocus = enforceModalFocusFn;
    });
    $('.modal').modal({keyboard: false, backdrop: "static", show: false});
    Number.prototype.formatMoney = function (c, d, t) {
        var n = this,
                c = isNaN(c = Math.abs(c)) ? 2 : c,
                d = d == undefined ? "." : d,
                t = t == undefined ? "," : t,
                s = n < 0 ? "-" : "",
                i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
                j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    };
    function getDetailFields(div) {
        var arr = [];
        $(div).find("input,select,textarea").each(function () {
            if ($(this).is("input") && !$(this).hasClass("ignore-add") && parseInt($.inArray($(this).attr("type"), detail_input_types)) !== -1) {
                if (parseInt($.inArray($(this).attr("type"), ["checkbox", "radio"])) !== -1) {
                    arr.push(($(this).prop("checked")) ? 1 : 0);
                } else {
                    arr.push($(this).val());
                }
            } else if ($(this).is("select") && !$(this).parent().hasClass("ignore-add")) {
                if ($(this).hasClass("select2-field")) {
                    if (typeof $(this).attr("data-type") === "undefined") {
                        arr.push($(this).val());
                    } else {
                        if ($(this).attr("data-type") === "multiple") {
                            if ($(this).parent().css("display") === "none") {
                                arr.push(JSON.stringify($(this).closest(".multi-container").find("select[name='multiple_select']").val()))
                            } else {
                                arr.push(JSON.stringify([]));
                            }
                        }
                    }
                }
            } else if ($(this).is("textarea") && !$(this).hasClass("ignore-add")) {
                arr.push($(this).val().toString().replace(/\n/g, "\\n"));
            }

        });
        return arr;
    }
    function getTableFields(table, mode) {
        var arr = [];
        var columns = $(table).DataTable().settings().init().columns;
        if (typeof columns === "undefined" || typeof columns[0].data === "undefined") {
            if (parseInt($(table).DataTable().$("tr").length) > 0) {
                $(table).DataTable().$("tr").each(function () {
                    arr.push(getDetailFields($(this)));
                });
            } else {
                arr = [];
            }
        } else {

            if (parseInt($(table).DataTable().$("tr").length) > 0) {
                $(table).DataTable().$("tr").each(function () {
                    var field_arr = getDetailFields($(this));
                    var id = $(table).DataTable().row(this).data().id;
                    // if (parseInt(id) !== 0) {
                    if (parseInt(mode) === 1) {
                        field_arr.push(id);
                    }

                    // }
                    var container;
                    if (parseInt($("body").find(".modal.in").length) > 0) {
                        container = $("body").find(".modal.in");
                    } else {
                        container = $("body");
                    }
                    var table_container = $(table).closest(".table-container")[0];
                    var index_arr = $($(container).find(".table-container")).index(table_container);
                    if (parseInt(id) === 0 || parseInt($.inArray(id, editted_rows[index_arr])) !== -1) {
                        arr.push(field_arr);
                    }
                });
            } else {
                arr = [];
            }

        }

        return arr;
    }

    function getFields(form, mode) {
        var all_fields = [];
        $(form).find("div").each(function () {
            if ($(this).hasClass("details-container")) {
                all_fields.push(getDetailFields($(this)));
            } else if ($(this).hasClass("table-container")) {

                var table = $(this).find("table")[0];
                all_fields.push(getTableFields(table, mode));
            }

        });
        return all_fields;
    }

    $.fn.hasScrollBar = function () {

        return this.get(0).scrollHeight > this.get(0).clientHeight;
    }

    function getDefaultOption(options) {

        var default_object = {};
        switch ((options.split(":")[0])) {

            case "success":
                default_object = $.extend(true, {}, $.fn.display_alert.success_defaults);
                break;
            case "failed":
                default_object = $.extend(true, {}, $.fn.display_alert.server_problem_defaults);
                break;
            default:

                if ($.fn.display_alert["has_" + options.split(":")[0] + "_defaults"] === undefined) {
                    break;
                }

                if (typeof options.split(":")[1] === "undefined") {
                    $.fn.display_alert["has_" + options.split(":")[0] + "_defaults"].show_link = false;
                } else {
                    if (parseInt(options.split(":")[1]) === 1) {
                        $.fn.display_alert["has_" + options.split(":")[0] + "_defaults"].show_link = true;
                    }
                }
                default_object = $.extend(true, {}, $.fn.display_alert["has_" + options.split(":")[0] + "_defaults"]);
                break;
        }

        return default_object;
    }
    //Alert Function
    $.fn.display_alert = function (options) {
        if (typeof options !== "object") {
            $(this).attr("data-event", options);
        } else {
            $(this).removeAttr("data-event");
        }
        options = (typeof options !== "object") ? options = getDefaultOption(options) : options;
        options.alert_class = (typeof options.alert_class === "undefined") ? $(this).attr("class") : options.alert_class;
        options.show_link = (typeof options.show_link === "undefined") ? false : options.show_link;
        options.message = (typeof options.message === "undefined") ? $(this).find("span").html() : options.message;
        options.message += (typeof options.max !== "undefined") ? "&nbsp;<strong>" + parseFloat(options.max).formatMoney(0, ".", ",") + "</strong>" : "";
        var container;
        if (options.show_link) {
            options.message += "&nbsp;<a class='link_error' href='javascript:void(0)'>(<?= $lerror ?>)</a>";
            container = $(this).parent();
            var tabcontent = $(this).closest(".tab-container")[0];
            if (parseInt($(tabcontent).length) > 0) {
                container = $($(this).closest(".tab-container")[0]).parent();
            }

        }
        $(this).attr("class", options.alert_class);
        $(this).find("span").html(options.message);
        $(this).find(".link_error").on("click", function () {
            var check_modal = $(this).closest(".modal-body")[0];
            if (parseInt($(check_modal).length) > 0) {
                container = check_modal;
            } else {
                container = $("html,body");
            }
            $(container).animate({scrollTop: $(container).find(".has-error").offset().top}, 1000);
        });
        $(this).css("display", "");
        var alert_div = this;
        if (typeof options.hasTimeLimit !== undefined && !isNaN(parseInt(options.hasTimeLimit))) {
            setTimeout(function () {
                $(alert_div).find(".close").click();
            }, options.hasTimeLimit);
        }

    }
    //Add Row Function
    function resetUniques(table) {
        var table_container = $(table).closest('.table-container');
        var container = $('body').find(".details-container,[name='field_container']");
        var modal_body = ($(table_container).parent().closest(".modal-body"));
        if (parseInt(modal_body.length) > 0) {
            container = $(modal_body).find(".details-container,[name='field_container']");
        }
        var select;
        var div_index = parseInt($(table_container).parent().children("div").index($(table_container)));
        if ($($(table_container).parent().children("div")[parseInt(div_index) - 1]).attr("name") === "field_container") {
            select = $($(table_container).parent().children("div")[parseInt(div_index) - 1]).find("select.unique");
        } else {
            select = $(table_container).parent().find("div[name='field_container']").find("select.unique");
        }
        $(select).each(function () {
            var index = $(container).find(".select2-field.ajax").index($(this));
            $(this).ajaxSelect2(select2_ajax[index]);
        });
    }
    var add_field_mode = false;
    var global_fields;

    $.fn.addFieldRow = function (fields) {
        global_fields = fields;

        if (checkFields(fields).status) {
            return;
        }
        var columns = $(this).DataTable().settings().init().columns;
        var arr;
        if (typeof columns === "undefined") {
            arr = [];
        } else {
            arr = {};
        }
        var vals = [];
        (typeof columns === "undefined") ? arr.push(0) : arr[columns[0].data] = 0;
        var index = 0;

        $(fields).find("input,select,textarea,[name='extra_col']").each(function () {
            var container;
            if (!$(this).hasClass("datepicker")) {
                container = $(this).parent().clone();
            } else {
                container = $(this).datepicker("destroy").removeAttr("id").parent().clone();
            }

            if ($(this).is("input") && !$(this).hasClass("ignore-add") && parseInt($.inArray($(this).attr("type"), input_types)) !== -1) {

                if (typeof $(container).find("input").attr("data-status") === "undefined") {
                    $(container).find("input").removeClass("nimp").attr("name", "new_fields");
                } else {
                    $(container).find("input").removeAttr("data-status").attr("name", "new_fields");
                }

                (typeof columns === "undefined") ? arr.push($(container).html()) : arr[columns[(parseInt(index) + 1)].data] = $(container).html();
                if ($(this).attr("type") === "number" && (isNaN(parseInt($(this).val())) || parseInt($(this).val()) < 0)) {
                    vals.push(0);
                } else {
                    vals.push($(this).val());
                }
                index++;
            } else if ($(this).is("select") && !$(this).parent().hasClass("ignore-add")) {
                var class_name = "";
                var extra_attr = "";
                var extra_status = "";
                if (typeof $(this).attr("data-type") === "undefined" && !$(this).hasClass("dropdown") && $(this).attr("name") !== "multiple_select") {

                    if (typeof columns === "undefined") {

                        if (typeof $(this).attr("data-status") !== "undefined") {
                            class_name = "class='nimp'";
                        }
                        var value = "N/A";
                        if ($(this).val() !== null) {
                            value = $(this).find("option:selected").html();
                        }
                        if (typeof $(this).attr("data-name") !== "undefined") {
                            extra_attr = "data-name='" + $(this).attr("data-name") + "'";
                        }
                        arr.push(value);
                        arr.push("<input " + extra_attr + " " + class_name + " type='hidden' name='new_fields'>");
                    } else {

                        if (typeof $(this).attr("data-status") !== "undefined") {
                            class_name = "class='nimp'";
                        }
                        var value = "N/A";
                        if ($(this).val() !== null) {
                            value = $(this).find("option:selected").html();
                        }
                        if (typeof $(this).attr("data-name") !== "undefined") {
                            extra_attr = "data-name='" + $(this).attr("data-name") + "'";
                        }
                        arr[columns[(parseInt(index) + 1)].data] = value;
                        arr[columns[(parseInt(index) + 2)].data] = "<input " + class_name + " " + extra_attr + " type='hidden' name='new_fields'>";
                    }

                    vals.push($(this).val());
                    index += 2;
                } else {
                    if ($(this).attr("name") !== "multiple_select") {

                        if (typeof $(this).attr("data-status") !== "undefined") {
                            extra_status = "nimp";
                        }

                        if (typeof $(this).attr("data-name") !== "undefined") {
                            extra_attr = "data-name='" + $(this).attr("data-name") + "'";
                        }
                        var container = $('body');
                        if (parseInt($('body').find(".modal.in").length) > 0) {
                            container = $('body').find(".modal.in");
                        }
                        var select_index = $(container).find(".select2-field.ajax").index($(this));
                        if (typeof columns === "undefined") {
                            if (typeof $(this).attr("data-type") !== "undefined" && $(this).attr("data-type") === "multiple") {
                                arr.push("<span><select data-index='" + select_index + "' " + extra_attr + "  name='new_fields' class='select2-field ajax " + extra_status + "' data-container='in-table' data-type='multiple' data-placeholder='" + $(this).attr("data-placeholder") + "'></select></span>");
                                var span = $(this).closest(".multi-container").find("span:visible");
                                if ($(span).find("select").prop("multiple")) {
                                    vals.push(JSON.stringify($(span).find("select").val()));
                                } else {
                                    vals.push(JSON.stringify([]));
                                }
                            } else {


                                arr.push("<select data-index='" + select_index + "'  " + extra_attr + " name= 'new_fields' class = 'select2-field ajax " + extra_status + "' data-container='in-table' data-placeholder = '" + $(this).attr("data-placeholder") + "' > < /select>");
                                vals.push($(this).val());
                            }
                        } else {
                            if (typeof $(this).attr("data-type") !== "undefined" && $(this).attr("data-type") === "multiple") {
                                arr[columns[(parseInt(index) + 1)].data] = ("<span><select data-index='" + select_index + "'  " + extra_attr + " class='" + extra_status + " select2-field ajax' data-container='in-table' name='new_fields' data-type='multiple' data-placeholder='" + $(this).attr("data-placeholder") + "'></select></span>");
                                var span = $(this).closest(".multi-container").find("span:visible");
                                if ($(span).find("select").prop("multiple")) {
                                    vals.push(JSON.stringify($(span).find("select").val()));
                                } else {
                                    vals.push(JSON.stringify([]));
                                }
                            } else {
                                arr[columns[(parseInt(index) + 1)].data] = ("<select " + extra_attr + " data-index='" + select_index + "' name='new_fields' class='" + extra_status + " select2-field ajax' data-container='in-table' data-placeholder='" + $(this).attr("data-placeholder") + "'></select>");
                                vals.push($(this).val());
                            }
                        }


                        index++;
                    }
                }
            } else if ($(this).is("textarea") && !$(this).hasClass("ignore-add")) {
                if (typeof $(container).find("textarea").attr("data-status") === "undefined") {
                    $(container).find("textarea").removeClass("nimp").css("width", "100%").attr("name", "new_fields");
                } else {
                    $(container).find("textarea").removeAttr("data-status").css("width", "100%").attr("name", "new_fields");
                }

                (typeof columns === "undefined") ? arr.push($(container).html()) : arr[columns[(parseInt(index) + 1)].data] = $(container).html();
                vals.push($(this).val());
                index++;
            } else if ($(this).attr("name") === "extra_col" && !$(this).hasClass("ignore-add")) {
                (typeof columns === "undefined") ? arr.push("") : arr[columns[(parseInt(index) + 1)].data] = "";
                // vals.push($(this).val());
                index++;
            }

        });
        (typeof columns === "undefined") ? arr.push("<button name='new_fields' class='for_delete btn btn-danger btn-xs'><i class='fa fa-trash'></i>&nbsp;<?= $delete ?></button>")
                : arr[columns[(parseInt(columns.length) - 1)].data] = "<button name='new_fields' class='for_delete btn btn-danger btn-xs'><i class='fa fa-trash'></i>&nbsp;<?= $delete ?></button>";
        add_field_mode = true;
        global_fields.vals = vals;

        $(this).DataTable().row.add(arr).draw();

    };
    var queued_counter = 0;
    function clearFields(item, options) {

        queued_counter = 0;

        if (typeof options === "undefined" || typeof options.reset === "undefined" || options.reset) {
            deleted_rows = {};
            editted_rows = {};
            original_fields = {};
        }
        var cancel_ajax = false;
        if (typeof options !== "undefined") {
            if (typeof options.select_load !== "undefined" && options.select_load) {
                cancel_ajax = true;
            }
        }

        $(".alert").css("display", "none");
        $(".has-error").removeClass("has-error");
        $(item).children().each(function () {
            if ($(this).hasClass("tab-container")) {
                var ul = $($(this).children("ul[class='nav nav-tabs']")[0]);
                var content = $($(this).children("div.tab-content")[0]);
                $(ul).children("li").removeClass("active");
                $(content).children("div").removeClass("in active");
                $($(ul).children("li")[0]).addClass("active");
                $($(content).children("div")[0]).addClass("in active");
                $(content).children().each(function () {
                    clearFields($(this), options);
                });
            } else if ($(this).hasClass("table-container")) {

                var table = $(this).find("table");
                if ($.fn.DataTable.isDataTable("#" + $(table).attr("id"))) {
                    $("#" + $(table).attr("id")).DataTable().clear().draw();
                }
            } else {
                var conditions = "input,select,textarea";


                $(this).find(conditions).each(function () {
                    if ($(this).is("input") && $(this).attr("type") !== "search") {
                        if ($(this).hasClass("datepicker")) {
                            $(this).datepicker("destroy");
                            $(this).removeAttr("id");
                            $(this).datepicker({dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true});
                            if (!$(this).hasClass("no-default")) {
                                $(this).datepicker("setDate", new Date());
                            }
                        } else if ($(this).attr("type") === "checkbox") {
                            $(this).prop("checked", false);
                        } else {
                            $(this).val("");
                        }
                    } else if ($(this).is("select")) {

                        var container;

                        if (parseInt($(item).closest(".modal").length) > 0) {
                            container = $(item).closest(".modal-body").find(".details-container,[name='field_container']");
                        } else {
                            container = $('body').find(".details-container,[name='field_container']");
                        }
                        var dropdown = $(this);
                        if (!cancel_ajax && $(this).hasClass("ajax") && parseInt(select2_ajax.length) > 0) {

                            var index = -1;
                            var counter = 0;

                            $.each($(container).find(".select2-field.ajax"), function (index_select, val) {
                                if (typeof $(val).attr("data-container") === "undefined") {
                                    if ($(dropdown).is(val)) {
                                        index = counter;
                                        return;
                                    } else {
                                        counter++;
                                    }
                                }
                            });

                            if (typeof options !== "undefined" && typeof options.select2_queued !== "undefined" && options.select2_queued) {
                                var qajax_option;
                                if (typeof select2_ajax[index] === "object") {
                                    qajax_option = $.extend(true, {}, select2_ajax[index]);
                                } else {
                                    qajax_option = select2_ajax[index];
                                }
                                queued_ajaxs.push({element: this, options: qajax_option});
                            } else {

                                $(this).ajaxSelect2(select2_ajax[index]);
                            }
                        } else {
                            if ($(this).hasClass("select2-field") && !$(this).hasClass("ajax")) {
                                $(this).ajaxSelect2();
                            }
                        }
                    } else if ($(this).is("textarea")) {
                        $(this).val("");
                    }
                });
            }

        });

        if (typeof options !== "undefined" && typeof options.select2_queued !== "undefined" && options.select2_queued) {
            var options = queued_ajaxs[0].options;
            if (typeof options === "object") {
                options.queued = true;
            } else {
                options = {"url": options, queued: true};
            }
            $(queued_ajaxs[0].element).ajaxSelect2(options);

        }
        $("[data-toggle='tooltip']").tooltip("destroy");
        $("[data-toggle='tooltip']").tooltip({
            trigger: 'hover', container: "body"
        });

    }
    function checkValues(datatable, value, selected_index) {
        var flag = false;
        $(datatable).DataTable().$('tr').each(function () {
            var value_input = $(this).find("input[type='hidden']:eq(" + selected_index + ")").val();
            if (parseInt(value_input) === parseInt(value)) {
                flag = true;
                return false;
            }
        });
        return flag;
    }
    var stop_load = false;
    $.fn.ajaxSelect2 = function (option) {
        var url = "";

        var change_function = undefined;
        var queued = false;
        if (typeof option === "object") {
            url = option.url;
            queued = option.queued;
            change_function = option.link;

        } else {
            url = option;
        }
        if (typeof change_function !== "undefined") {
            $(this).unbind();
        }

        var item = this;
        if (!$(this).hasClass("ajax")) {

            if (typeof $(item).attr("data-type") !== "undefined") {
                var data_default = "0";
                if (typeof $(item).attr("data-default") !== "undefined") {
                    data_default = $(item).attr("data-default");
                }

                $(item).find("option[value='" + data_default + "']").remove();
                var data_all = "<?= $all ?>";
                if (typeof $(item).attr("data-all") !== "undefined") {
                    data_all = $(item).attr("data-all");
                }
                $($(item).find("option")[0]).before("<option value='" + data_default + "'>" + data_all + "</option>");
                var options = $(item).find("option").clone();
                $(item).select2({width: "100%"});

                $(item).closest(".multi-container").find("select[name='multiple_select']").parent().remove();
                $(item).parent().css("display", "");
                $(item).closest(".multi-container").append("<span><select  multiple  name='multiple_select' data-placeholder='" + $(item).attr("data-placeholder") + "'></select></span>");
                var multi_select = $(item).closest(".multi-container").find("select[name='multiple_select']");
                $.each(options, function () {
                    $(multi_select).append($(this));
                });
                $(multi_select).select2({width: "100%"});
                $(multi_select).select2("val", "");
                $(multi_select).parent().css("display", "none");
                $(item).select2("val", data_default);
                $(item).on("change", function () {
                    var value = $(this).val();
                    if (parseInt(value) !== parseInt(data_default)) {
                        $(this).closest(".multi-container").find("select[name='multiple_select']").select2("val", value);
                        $(this).parent().css("display", "none");
                        $(this).closest(".multi-container").find("select[name='multiple_select']").parent().css("display", "");
                        var select_index = $(this).closest(".multi-container").find("select[name='multiple_select']");
                        if (typeof change_function !== "undefined") {
                            change_function(select_index);
                        }

                        $("body").children("span.select2-container").css("display", "");
                    }


                });
                $(multi_select).on("select2:unselect", function (e) {
                    if (!e.params.originalEvent) {
                        return;
                    }

                    e.params.originalEvent.stopPropagation();
                });
                $(multi_select).on("select2:open", function (e) {
                    if (parseInt($("body").children("span.select2-container").css('left').toString().replace(/px/g, "")) === 0) {
                        $("body").children("span.select2-container").css("display", "none");
                    } else {
                        $("body").children("span.select2-container").css("display", "");
                    }
                });

                $(multi_select).on("change", function () {
                    var value = $(this).val();
                    var check_arr = parseInt($.inArray(data_default, value));
                    if (check_arr !== -1) {
                        $(this).closest(".multi-container").find(".select2-field.field").select2("val", data_default);
                        $(this).parent().css("display", "none");
                        $(this).closest(".multi-container").find(".select2-field.field").parent().css("display", "");
                        var select_index = $(this).closest(".multi-container").find(".select2-field.field");

                        if (typeof change_function !== "undefined") {

                            change_function(select_index);
                        }

                    } else {
                        if (value === null) {

                            $(this).closest(".multi-container").find(".select2-field.field").select2("val", data_default);
                            $(this).parent().css("display", "none");
                            $(this).closest(".multi-container").find(".select2-field.field").parent().css("display", "");
                            var select_index = $(this).closest(".multi-container").find(".select2-field.field");
                            if (typeof change_function !== "undefined") {
                                change_function(select_index);
                            }

                            //  $("body").children("span.select2-container").css("display", "none");


                        } else {
                            select_index = $(this);
                            if (typeof change_function !== "undefined") {
                                change_function(select_index);
                            }

                        }

                    }
                });
            } else {
                if ($(item).hasClass("no-clear")) {
                    $(item).select2({width: "100%"});
                } else {
                    $(item).select2({width: "100%", allowClear: true});

                }

                if (typeof $(item).attr("data-default") !== "undefined") {
                    $(item).select2("val", $(item).attr("data-default"));
                } else {
                    $(item).select2("val", "");
                }
            }
            return;
        } else {
            $(this).attr("disabled", "disabled");
        }

        var json = $.getJSON(url, function (data) {
            if (typeof data !== "Object" && parseInt(data) === -1) {
                document.location.reload();
            }
            $(item).html("");
            if (typeof $(item).attr("data-type") !== "undefined") {
                var option = document.createElement("option");
                option.setAttribute("value", 0);
                if (typeof ($(item).closest(".multi-container").attr("data-all")) !== "undefined") {
                    option.innerHTML = $(item).closest(".multi-container").attr("data-all");
                } else {
                    option.innerHTML = "<?= $all ?>";
                }
                $(item).append(option);
            }

            var optgroup_arr = [];
            var reference_column = "";


            if (data instanceof Object && !(data instanceof Array)) {
                optgroup_arr = data.optgroup;
                reference_column = data.reference_column;
                data = data.values;
            }

            var arr_optgroup = {};
            if (parseInt(optgroup_arr.length) > 0) {
                $.each(optgroup_arr, function (index, value) {
                    var opt = document.createElement("optgroup");
                    opt.setAttribute("label", value["name"]);
                    arr_optgroup[value["id"]] = opt;
                });
            }
            for (var y = 0; y < data.length; y++) {
                var option = document.createElement("option");
                if ($(item).hasClass("unique")) {

                    var table = $(item).closest("div[name='field_container']");
                    var index = parseInt($(table).parent().children("div").index(table));
                    var datatable;
                    if ($($(table).parent().children("div")[index + 1]).hasClass("table-container")) {
                        datatable = $($(table).parent().children("div")[index + 1]).find("table");
                    } else {
                        datatable = $(table).parent().find(".table-container").find("table");
                    }
                    var hidden_index = 0;
                    var selected_index = 0;
                    $(table).find(".select2-field.ajax").each(function () {
                        if (!$(this).hasClass("dropdown")) {
                            if ($(this).is($(item))) {
                                selected_index = hidden_index;
                            }
                            hidden_index++;
                        }
                    })
                    if (checkValues(datatable, data[y].id, selected_index)) {

                        option.setAttribute("disabled", "disabled");
                    }


                }
                option.setAttribute("json", JSON.stringify(data[y]));
                option.setAttribute("value", data[y].id);

                option.innerHTML = data[y].name;
                if (parseInt(Object.keys(arr_optgroup).length) > 0) {
                    $(arr_optgroup[data[y][reference_column]]).append(option);
                } else {
                    $(item).append(option);
                }
            }
            if (parseInt(Object.keys(arr_optgroup).length) > 0) {
                var groups = Object.keys(arr_optgroup);
                $.each(groups, function (index, value) {
                    if (arr_optgroup[value].innerHTML !== "") {
                        $(item).append(arr_optgroup[value]);
                    }
                });

            }


            if (typeof $(item).attr("data-type") !== "undefined") {
                $(item).select2({width: "100%"});
                var options = $(item).children().clone();
                $(item).closest(".multi-container").find("select[name='multiple_select']").parent().remove();
                $(item).parent().css("display", "");
                $(item).closest(".multi-container").append("<span><select  multiple  name='multiple_select' data-placeholder='" + $(item).attr("data-placeholder") + "'></select></span>");
                var multi_select = $(item).closest(".multi-container").find("select[name='multiple_select']");
                $.each(options, function () {
                    $(multi_select).append($(this));
                });
                $(multi_select).select2({width: "100%"});
                $(multi_select).select2("val", "");
                $(multi_select).parent().css("display", "none");
                $(item).select2("val", 0);

                $(item).on("change", function () {
                    var value = $(this).val();
                    if (parseInt(value) !== 0) {
                        $(this).closest(".multi-container").find("select[name='multiple_select']").select2("val", value);
                        $(this).parent().css("display", "none");
                        $(this).closest(".multi-container").find("select[name='multiple_select']").parent().css("display", "");
                        var select_index = $(this).closest(".multi-container").find("select[name='multiple_select']");
                        if (typeof change_function !== "undefined") {
                            change_function(select_index);
                        }

                    }


                });
                $(multi_select).on("select2:open", function (e) {
                    if (parseInt($("body").children("span.select2-container").css('left').toString().replace(/px/g, "")) === 0) {
                        $("body").children("span.select2-container").css("display", "none");
                    } else {
                        $("body").children("span.select2-container").css("display", "");
                    }
                });
                $(multi_select).on("select2:unselect", function (e) {
                    if (!e.params.originalEvent) {
                        return;
                    }

                    e.params.originalEvent.stopPropagation();
                });
                $(multi_select).on("change", function () {
                    var value = $(this).val();
                    var check_arr = parseInt($.inArray("0", value));
                    if (check_arr !== -1) {
                        $(this).closest(".multi-container").find(".select2-field.ajax").select2("val", 0);
                        $(this).parent().css("display", "none");
                        $(this).closest(".multi-container").find(".select2-field.ajax").parent().css("display", "");
                        var select_index = $(this).closest(".multi-container").find(".select2-field.ajax");
                        if (typeof change_function !== "undefined") {
                            change_function(select_index);
                        }

                    } else {
                        if (value === null) {
                            $(this).closest(".multi-container").find(".select2-field.ajax").select2("val", 0);
                            $(this).parent().css("display", "none");
                            $(this).closest(".multi-container").find(".select2-field.ajax").parent().css("display", "");
                            var select_index = $(this).closest(".multi-container").find(".select2-field.ajax");
                            if (typeof change_function !== "undefined") {
                                change_function(select_index);
                            }

                        } else {
                            select_index = $(this);
                            if (typeof change_function !== "undefined") {
                                change_function(select_index);
                            }

                        }

                    }
                });

            } else {
                if (typeof change_function !== "undefined") {
                    $(item).on("change", function () {
                        change_function($(item));
                    });
                }
                if ($(item).hasClass("no-clear")) {
                    $(item).select2({width: "100%"});
                } else {
                    $(item).select2({width: "100%", allowClear: true});
                }
                $(item).select2("val", "");
            }

            if (typeof $(item).attr("data-val") !== "undefined") {

                var value = $(item).attr("data-val");
                if (typeof $(item).attr("data-type") !== "undefined" || $(item).hasClass("no-clear")) {
                    $(item).select2({width: "100%"});
                } else {
                    $(item).select2({width: "100%", allowClear: true});
                }
                if (typeof $(item).attr("data-type") !== "undefined" && $(item).attr("data-type") === "multiple") {
                    if (typeof JSON.parse(value) === "object") {

                        if (parseInt(JSON.parse(value).length) === 0) {
                            $(item).removeAttr("data-val");
                            $(item).closest(".multi-container").find("select[name='multiple_select']").select2("val", 0);
                        } else {
                            $(item).removeAttr("data-val");
                            $(item).closest(".multi-container").find("select[name='multiple_select']").select2("val", JSON.parse(value));
                            $(item).parent().css("display", "none");
                            $(item).closest(".multi-container").find("select[name='multiple_select']").parent().css("display", "");
                        }
                    }

                } else {

                    $(item).select2("val", value);
                    $(item).removeAttr("data-val");
                }


            }
            if ($(item).hasClass("disable")) {
                $(item).removeClass("disable");
            } else {
                $(item).removeAttr("disabled");

            }
        });

        if (queued) {

            $.when(json).done(function () {
                if ((parseInt(queued_ajaxs.length) - 1) > queued_counter) {
                    queued_counter++;
                    var options = queued_ajaxs[queued_counter].options;
                    if (typeof options === "object") {
                        options.queued = true;
                    } else {
                        options = {"url": options, queued: true};
                    }
                    $(queued_ajaxs[queued_counter].element).ajaxSelect2(options);

                } else {

                    queued_counter = 0;
                    queued_ajaxs = [];
                    var check_is_global = false;
                    if (typeof is_add_global !== "undefined") {
                        check_is_global = is_add_global;
                    }
                    if (check_is_global) {
                        end_queue_function_global();
                    }
                    if (!check_is_global) {
                        end_queue_function();
                    }
                }

            });
        }
    }



    function checkDate(date_value, delimiter, format) {

        var date = date_value.toString().split(delimiter);
        var format_string = format.toString().split(delimiter);
        var index = {};

        if (date_value !== "") {
            for (var y = 0; y < format_string.length; y++) {
                if (parseInt(date[y].length) === parseInt(format_string[y].length)) {
                    if (format_string[y] === "mm") {
                        index.month = y;
                    } else if (format_string[y] === "yyyy") {
                        index.year = y;
                    } else if (format_string[y] === "dd") {
                        index.day = y;
                    }
                }
            }
        }
        var converted_date = new Date(date[index.year], (parseInt(date[index.month]) - 1), date[index.day]);
        return parseInt(Object.keys(index).length) > 0 && converted_date && (parseInt(converted_date.getMonth()) + 1) === parseInt(date[index.month]);
    }
    function checkFields(item) {
        $(".alert").css("display", "none");
        $(".has-error").removeClass("has-error");
        $(".dropdown-error").css("border-color", "");
        var flag = false;
        var alert_type = "";
        $(item).find("input,select,textarea").each(function () {

            var value = $.trim($(this).val());
            if ($(this).is("input") && parseInt($.inArray($(this).attr("type"), input_types)) !== -1) {
                if ($(this).parent().attr("class") !== "bootstrap-tagsinput") {
                    if ($(this).hasClass("datepicker")) {

                        var format = JSON.parse($(this).attr("format"));
                        if ((value === "" && !$(this).hasClass("nimp")) || (!$(this).hasClass("nimp") && !checkDate(value, format.delimiter, format.date_format))) {
                            $(this).parent().addClass("has-error");
                            alert_type = "date";
                            flag = true;
                            return false;
                        }
                    } else {
                        if ($(this).attr("type") === "number") {
                            if ($(this).hasClass("nimp")) {
                                if ($(this).val() !== "") {
                                    if (isNaN(parseInt(value)) || parseInt(value) < 0) {
                                        $(this).parent().addClass("has-error");
                                        alert_type = "invalid_number";
                                        flag = true;
                                        return false;
                                    } else {
                                        if (typeof $(this).attr("max") !== "undefined" && parseFloat($(this).attr("max")) < parseFloat($(this).val())) {
                                            $(this).parent().addClass("has-error");
                                            $.fn.display_alert.has_invalid_max_number_defaults.max = parseFloat($(this).attr("max")).formatMoney(0, ".", ",");
                                            alert_type = "invalid_max_number";
                                            flag = true;
                                            return false;
                                        }

                                    }

                                }
                            } else {
                                if (isNaN(parseInt(value)) || (parseInt(value) < 0 && typeof $(this).attr("has-negative") === "undefined")) {
                                    $(this).parent().addClass("has-error");
                                    alert_type = "invalid_number";
                                    flag = true;
                                    return false;
                                } else {
                                    if (typeof $(this).attr("max") !== "undefined" && parseFloat($(this).attr("max")) < parseFloat($(this).val())) {
                                        $(this).parent().addClass("has-error");
                                        $.fn.display_alert.has_invalid_max_number_defaults.max = parseFloat($(this).attr("max")).formatMoney(0, ".", ",");
                                        alert_type = "invalid_max_number";
                                        flag = true;
                                        return false;
                                    }
                                }
                            }



                        } else if (($(this).attr("type") === "text" || $(this).attr("type") === "password") && (value === "" && !$(this).hasClass("nimp"))) {
                            $(this).parent().addClass("has-error");
                            alert_type = "blank";
                            flag = true;
                            return false;
                        }
                    }
                }
            } else if ($(this).is("select") && (value === null && !$(this).hasClass("nimp") || value === "" && !$(this).hasClass("nimp"))) {
                if ($(this).attr("name") === "multiple_select") {
                    if ($(this).parent().css("display") !== "none") {
                        var selection = $(this).parent().find("span[class='selection']")[0];
                        $($(selection).children("span")[0]).css("border-color", "#a94442");
                        $($(selection).children("span")[0]).addClass("dropdown-error");
                        alert_type = "blank";
                        flag = true;
                        return false;
                    }
                } else {
                    var selection = $(this).parent().find("span[class='selection']")[0];
                    $($(selection).children("span")[0]).css("border-color", "#a94442");
                    $($(selection).children("span")[0]).addClass("dropdown-error");
                    alert_type = "blank";
                    flag = true;
                    return false;
                }
            } else if ($(this).is("textarea") && (value === "" && !$(this).hasClass("nimp"))) {
                $(this).parent().addClass("has-error");
                alert_type = "blank";
                flag = true;
                return false;
            }

        });
        var check_modal = $($(item).closest(".modal-body")[0]);
        if (flag) {

            var alert_div = $(item).find(".alert");
            if (parseInt($(alert_div).length) > 0) {
                var container;
                if (parseInt(check_modal.length) > 0) {
                    container = check_modal;
                } else {
                    container = 'body,html';//$(alert_div).parent();
                }
                var tabcontent = $(alert_div).closest(".tab-container")[0];
                if (parseInt($(tabcontent).length) > 0) {
                    container = $($(alert_div).closest(".tab-container")[0]).parent();
                }

                //container = $(container).hasScrollBar() ? container : "html,body";
                if ($(container).hasScrollBar()) {
                    alert_type += ":1";
                }

                $(alert_div).display_alert(alert_type);
                $(container).animate({scrollTop: $(alert_div).offset().top}, 1000);
            }
        }
        return flag ? {status: true, alert_type: alert_type} : {status: false};
    }
    function checkTable(item) {
        $(".alert").css("display", "none");
        $(".has-error").removeClass("has-error");
        $(".dropdown-error").css("border-color", "");
        var flag = false;
        var alert_type = "";
        var table;
        if ($(item).hasClass("customtable") || parseInt($(item).find(".customtable").length) > 0) {
            table = $(item).find("table[name='dataTable']");
        } else {
            table = $(item).find("table");
        }

        if ($.fn.DataTable.isDataTable("#" + $(table).attr("id"))) {

            $('#' + $(table).attr("id")).DataTable().$("tr").each(function () {

                if (checkFields($(this)).status) {
                    alert_type = checkFields($(this)).alert_type;
                    flag = true;
                    return false;
                }
            });
            if (!flag) {

                if (parseInt($('#' + $(table).attr("id")).DataTable().$("tr").length) === 0 && !$('#' + $(table).attr("id")).hasClass("nimp")) {
                    alert_type = "blank_table";
                    flag = true;
                }
            }

        }
        var check_modal = $($(item).closest(".modal-body")[0]);
        if (flag) {
            var alert_div = $(item).find(".alert:eq(0)");
            if (parseInt($(alert_div).length) > 0) {
                var container = $(alert_div).parent();
                if (parseInt(check_modal.length) > 0) {
                    container = check_modal;
                } else {
                    container = 'html,body'//$(alert_div).parent();
                }

                var tab_content = $(alert_div).closest(".tab-container")[0];
                if (parseInt($(tab_content).length) > 0) {
                    container = $($(alert_div).closest(".tab-container")[0]).parent();
                }

                if (alert_type !== "blank_table") {

                    alert_type = $(container).hasScrollBar() ? alert_type + "" + ":1" : alert_type;
                }

                $(alert_div).display_alert(alert_type);
                $(container).animate({scrollTop: $(alert_div).offset().top}, 1000);
            }

        }
        return flag ? true : false;
    }

    function checkPage(item) {
        $(".alert").css("display", "none");
        $(".has-error").removeClass("has-error");
        var flag = false;
        $(item).children("div").each(function () {
            if ($(this).hasClass("details-container")) {

                if (checkFields($(this)).status) {
                    flag = true;
                    return false;
                }
            } else if ($(this).hasClass("table-container")) {

                if (checkTable($(this))) {
                    flag = true;
                    return false;
                }
            } else if ($(this).hasClass("tab-container")) {
                if (checkTabs($(this))) {
                    flag = true;
                    return false;
                }
            }

        });
        if (flag) {
            var alert_type = $(item).find(".alert:visible");
            if (parseInt($(alert_type).length) > 0) {
                var container;
                if (parseInt($(item).hasClass("modal-body").length) > 0) {
                    container = $(item);
                } else {
                    container = 'body,html' //$(alert_type).parent();
                }
                var tab_content = $(alert_type).closest(".tab-container")[0];
                if (parseInt($(tab_content).length) > 0) {
                    container = $(tab_content).parent();
                }

                if ($(container).hasScrollBar()) {
                    if (typeof $(alert_type).attr("data-event") !== "undefined" && parseInt($(alert_type).attr("data-event").split(":").length) === 1) {
                        $(alert_type).display_alert($(alert_type).attr("data-event") + ":1");
                    }
                    $(container).animate({scrollTop: $(alert_type).offset().top}, 1000);
                }

            }


        }
        return flag ? true : false;
    }
    function checkTabs(item) {
        $(".alert").css("display", "none");
        $(".has-error").removeClass("has-error");
        $(".dropdown-error").css("border-color", "");
        var index;
        var ul = $(item).children("ul[class='nav nav-tabs']")[0];
        var tabcontent = $(item).children("div.tab-content")[0];
        $(ul).children("li").removeClass("active");
        $(tabcontent).children("div").removeClass("in active");
        $($(ul).children("li")[0]).addClass("active");
        $($(tabcontent).children("div")[0]).addClass("in active");
        var flag = false;
        $(tabcontent).children("div").each(function () {
            var divs = $(this).children("div");
            var tab_content_flag = false;
            var tab_content = $(this);
            $(divs).each(function () {
                if ($(this).hasClass("table-container")) {

                    if (checkTable($(this))) {
                        tab_content_flag = true;
                        return false;
                    }
                } else if ($(this).hasClass("tab-container")) {
                    if (checkTabs($(this))) {
                        tab_content_flag = true;
                        return false;
                    }
                } else if ($(this).hasClass("details-container")) {
                    if (checkFields($(this)).status) {
                        tab_content_flag = true;
                        return false;
                    }
                }

            });
            if (tab_content_flag) {
                index = $($(tabcontent).children("div")).index(tab_content);
                $(ul).children("li").removeClass("active");
                $(tabcontent).children("div").removeClass("in active");
                $($(ul).children("li")[index]).addClass("active");
                $($(tabcontent).children("div")[index]).addClass("in active");
                flag = true;
                return false;
            }

        });
        var check_modal = $($(item).closest(".modal-body")[0]);
        if (flag) {
            var alert_type = $($(tabcontent).children("div")[index]).find(".alert:visible");
            if (parseInt($(alert_type).length) > 0) {
                var container;
                if (parseInt(check_modal.length) > 0) {
                    container = check_modal;
                } else {
                    container = 'body,html'// $(alert_type).parent();
                }
                var tab_content = $(alert_type).closest(".tab-container")[0];
                if (parseInt($(tab_content).length) > 0) {
                    container = $(tab_content).parent();
                }
                if ($(container).hasScrollBar()) {
                    if (typeof $(alert_type).attr("data-event") !== "undefined" && parseInt($(alert_type).attr("data-event").split(":").length) === 1) {
                        $(alert_type).display_alert($(alert_type).attr("data-event") + ":1");
                    }
                    $(container).animate({scrollTop: $(alert_type).offset().top}, 1000);
                }

            }


        }
        return flag ? true : false;
    }
    $('.language').on("click", function () {
        if (typeof $(this).attr("value") !== "undefined") {
            $.post("/changeLanguage", {id: $(this).attr("value")}, function (data) {
                if (data === 1) {
                    document.location.reload();
                }
            }, 'json');
        }
    });
    $.fn.display_alert.server_problem_defaults = {message: "<i class='fa fa-exclamation-circle'></i>&nbsp;<?= $server_error ?>", alert_class: "alert alert-danger"};
    $.fn.display_alert.has_blank_defaults = {message: "<i class='fa fa-exclamation-circle'></i>&nbsp;<?= $error_blank ?>", alert_class: "alert alert-danger"};
    $.fn.display_alert.has_blank_table_defaults = {message: "<i class='fa fa-exclamation-circle'></i>&nbsp;<?= $tdata ?>", alert_class: "alert alert-danger"};
    $.fn.display_alert.has_invalid_number_defaults = {message: "<i class='fa fa-exclamation-circle'></i>&nbsp;<?= $invalid_number ?>", alert_class: "alert alert-danger"};
    $.fn.display_alert.has_invalid_max_number_defaults = {max: 0, message: "<i class='fa fa-exclamation-circle'></i>&nbsp;<?= $cannot_exceed_max ?>", alert_class: "alert alert-danger"};
    $.fn.display_alert.has_date_defaults = {message: "<i class='fa fa-exclamation-circle'></i>&nbsp;<?= $invalid_date ?>", alert_class: "alert alert-danger"};
    $.fn.display_alert.success_defaults = {"hasTimeLimit": 2000, message: "<i class='fa fa-check'></i>&nbsp;<?= $module_name ?> <?= $ssaved ?>", alert_class: "alert alert-success"};

</script>