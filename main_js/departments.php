<script>
    var clone_error = $.extend(true, {}, $.fn.display_alert.server_problem_defaults);
    var clone_success = $.extend(true, {}, $.fn.display_alert.success_defaults);

    $('#nav_departments').addClass('active');
    var mode = -1;
    var global_id = 0;
    $('#table_multiple').fieldTable({searching: false, ordering: false});

    $('#abutton').on("click", function () {
        mode = 0;
        $('#dataModal').disable_fields(false);
        clearFields($('#dataModal').find(".modal-body"));
        $('#dataModal').find("[name='title']").html("<?= $add ?>");
        $('#dataModal').modal("show");
    });


    $('#abutton_multiple').on("click", function () {
        mode = 0;
        $('#dataModal').disable_fields(false);
        clearFields($('#dataModal_Multiple').find(".modal-body"));
        $('#dataModal_Multiple').find("[name='title']").html("<?= $add ?>(<?= $multiple ?>)");
        $('#dataModal_Multiple').modal("show");
    });
    $('#add_multiple').on("click", function () {

        $('#table_multiple').addFieldRow($('#dataModal_Multiple').find("div[name='field_container']"));

    });

    $('#save').on("click", function () {
        if (checkFields($('#dataModal').find(".details-container")[0]).status) {
            return;
        }

        var arr = getFields($('#dataModal'), mode);

        $('#dataModal').disable_fields(true);

        $.post("/saveDepartment", {deleted_fields: deleted_rows, id: global_id, uid: "<?= $_SESSION[getSessionName()]["id"] ?>", arr: arr, mode: mode}, function (data) {
            if (data === 1) {
                $($(".main-content").find(".alert")[0]).display_alert("success");
                $('#dataModal').modal('hide');
                $('#datatable').DataTable().ajax.reload(null, false);
            } else if (data === 0) {
                $($('#dataModal').find(".alert")[0]).display_alert("failed");
                $('#dataModal').disable_fields(false);
            } else if (data === -1) {
                $($('#dataModal').find(".alert")[0]).display_alert({"message": "<i class='fa fa-exclamation-circle'></i>&nbsp;<?= $department_error ?>", "alert_class": "alert alert-danger"});
                $('#dataModal').disable_fields(false);
            }
        }, 'json');
    });

    $("#save_multiple").on("click", function () {
        if (checkTable($('#dataModal_Multiple').find(".table-container"))) {
            return;
        }
        if (checkDuplicates([1], $('#dataModal_Multiple').find(".table-container").find("table")[0])) {
            $('#dataModal_Multiple').find(".table-container").find(".alert").display_alert({message: "<i class='fa fa-exclamation-circle'></i>&nbsp;<?= $duplicate_error ?>", alert_class: "alert alert-danger"});
            return;
        }
        var arr = getFields($('#dataModal_Multiple'));

        $('#dataModal_Multiple').disable_fields(true);
        $.post("/saveDepartmentMultiple", {id: global_id, uid: "<?= $_SESSION[getSessionName()]["id"] ?>", arr: arr}, function (data) {
            if (data === 1) {
                $($(".main-content").find(".alert")[0]).display_alert("success");
                $('#dataModal_Multiple').modal('hide');
                $('#datatable').DataTable().ajax.reload(null, false);
            } else if (data === 0) {
                $($('#dataModal_Multiple').find(".alert")[0]).display_alert("failed");
                $('#dataModal_Multiple').disable_fields(false);
            } else if (data === -1) {
                $($('#dataModal_Multiple').find(".alert")[0]).display_alert({"message": "<i class='fa fa-exclamation-circle'></i>&nbsp;<?= $department_error2 ?>", "alert_class": "alert alert-danger"});
                $('#dataModal_Multiple').disable_fields(false);
            }
        }, 'json');
    });
    $('#datatable').serverTable({
        url: "/getAllDepartments", "columns": [
            {"data": "id"},
            {"data": "name", name: "<?= $name ?>"},
            {"data": "button"}
        ]});

    function editDepartment(item) {
        mode = 1;
        $('#dataModal').find("[name='title']").html("<?= $edit ?>");
        var row_details = $('#datatable').DataTable().row($(item).parents("tr")).data();
        global_id = row_details.id;
        $(".main-content").disable_fields(true);
        $(item).find(".loading").css("display", "");
        clearFields($('#dataModal').find(".modal-body"));
        $.getJSON("/loadDepartment", {id: row_details.id}, function (data) {
            $("#dataModal").loadDetails(data[0]);
            $('#dataModal').modal('show');
        });
    }

    function deleteDepartment(item) {
        clearFields($('#deleteModal'));
        var row_details = $('#datatable').DataTable().row($(item).parents("tr")).data();
        global_id = row_details.id;
        $('#deleteModal').find("[name='message']").html("<?= $delete_message ?> <strong><?= $module_name ?>:&nbsp;" + row_details.name + "</strong>?");
        $('#deleteModal').modal("show");
    }
    $('#delete_yes').on("click", function () {
        $('#deleteModal').disable_fields(true);
        $.post("/deleteData", {mode: 2, id: global_id}, function (data) {
            if (data === 1) {
                clone_success.message = "<?= $module_name . ' ' . $sdeleted ?>"
                $($(".main-content").find(".alert")[0]).display_alert(clone_success);
                $('#deleteModal').disable_fields(false);
                $('#deleteModal').modal('hide');
                $('#datatable').DataTable().ajax.reload(null, false);
            } else if (data === 0) {
                $($("#deleteModal").find(".alert")[0]).display_alert("failed");
                $('#deleteModal').disable_fields(false);
            } else if (data === -1) {
                clone_error.message = "<?= $conflict_found ?>";
                $($("#deleteModal").find(".alert")[0]).display_alert(clone_error);
                $('#deleteModal').disable_fields(false);
            }

        }, 'json');
    });
</script>