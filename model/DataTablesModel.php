<?php

class DataTablesModel{

    function __construct() {
        
    }
	
	function getAllContact_types() {
        include './lib/language.php';
        $col_arr = array("`id`", "`name`");
        $filters = getServer_filters($col_arr, $_GET);
        
        $arr = array();
        $query_base = "select " . implode(",", $col_arr) . " from (SELECT * FROM `contact_types`)final";

        $data = prepareTable($query_base . " " . $filters["search"] . " " . $filters["order"] . " " . $filters["limit"], $arr);
        $total_records = count(prepareTable($query_base, $arr));
        $records_filters = count(prepareTable($query_base . " " . $filters["search"], $arr));

        $draw = $_GET["draw"];
        for ($x = 0; $x < count($data); $x++) {
            $button = "";
                $button = "<button onclick='editContact_type(this)' data-toggle='tooltip' data-placement='bottom' title='$edit' class='btn btn-primary btn-sm'><i class='fa fa-edit'></i>" . $load_tag . "</button>&nbsp;";
				if($data[$x]['id']!=0){
					$button .= "<button onclick='deleteContact_type(this)' data-toggle='tooltip' data-placement='bottom' title='$delete' class='btn btn-danger btn-sm'><i class='fa fa-trash'></i></button>&nbsp;";
				}
            if ($button == "") {
                $button = "<label class='label label-default'>$none</label>";
            }
            $data[$x]["button"] = $button;
        }
        return json_encode(array("draw" => $draw, "recordsTotal" => $total_records, "recordsFiltered" => $records_filters, "data" => $data));
    }
	
	function getAllDepartments() {
        include './lib/language.php';
        $col_arr = array("`id`", "`name`");
        $filters = getServer_filters($col_arr, $_GET);
        
        $arr = array();
        $query_base = "select " . implode(",", $col_arr) . " from (SELECT * FROM `departments`)final";

        $data = prepareTable($query_base . " " . $filters["search"] . " " . $filters["order"] . " " . $filters["limit"], $arr);
        $total_records = count(prepareTable($query_base, $arr));
        $records_filters = count(prepareTable($query_base . " " . $filters["search"], $arr));

        $draw = $_GET["draw"];
        for ($x = 0; $x < count($data); $x++) {
            $button = "";
                $button = "<button onclick='editDepartment(this)' data-toggle='tooltip' data-placement='bottom' title='$edit' class='btn btn-primary btn-sm'><i class='fa fa-edit'></i>" . $load_tag . "</button>&nbsp;";
				if($data[$x]['id']!=0){
					$button .= "<button onclick='deleteDepartment(this)' data-toggle='tooltip' data-placement='bottom' title='$delete' class='btn btn-danger btn-sm'><i class='fa fa-trash'></i></button>&nbsp;";
				}
            if ($button == "") {
                $button = "<label class='label label-default'>$none</label>";
            }
            $data[$x]["button"] = $button;
        }
        return json_encode(array("draw" => $draw, "recordsTotal" => $total_records, "recordsFiltered" => $records_filters, "data" => $data));
    }
	
	function getAllCurrencys() {
        include './lib/language.php';
        $col_arr = array("`id`", "`name`", "`symbol`");
        $filters = getServer_filters($col_arr, $_GET);
        
        $arr = array();
        $query_base = "select " . implode(",", $col_arr) . " from (SELECT * FROM `currencys`)final";

        $data = prepareTable($query_base . " " . $filters["search"] . " " . $filters["order"] . " " . $filters["limit"], $arr);
        $total_records = count(prepareTable($query_base, $arr));
        $records_filters = count(prepareTable($query_base . " " . $filters["search"], $arr));

        $draw = $_GET["draw"];
        for ($x = 0; $x < count($data); $x++) {
            $button = "";
                $button = "<button onclick='editCurrency(this)' data-toggle='tooltip' data-placement='bottom' title='$edit' class='btn btn-primary btn-sm'><i class='fa fa-edit'></i>" . $load_tag . "</button>&nbsp;";
				if($data[$x]['id']!=0){
					$button .= "<button onclick='deleteCurrency(this)' data-toggle='tooltip' data-placement='bottom' title='$delete' class='btn btn-danger btn-sm'><i class='fa fa-trash'></i></button>&nbsp;";
				}
            if ($button == "") {
                $button = "<label class='label label-default'>$none</label>";
            }
            $data[$x]["button"] = $button;
        }
        return json_encode(array("draw" => $draw, "recordsTotal" => $total_records, "recordsFiltered" => $records_filters, "data" => $data));
    }

	
    function getAllEmployees() {
        include './lib/language.php';
        $col_arr = array("`id`", "`name`", "`username`", "`department`");
		$filters = getServer_filters($col_arr, $_POST);
		
        $arr = array();
        $query_base = "select " . implode(",", $col_arr) . " from (
			SELECT A.*,B.`name` 'department' FROM `employees` A
			LEFT JOIN `departments` B ON B.`id`=A.`id_department`
		)final";

        $data = prepareTable($query_base . " " . $filters["search"] . " " . $filters["order"] . " " . $filters["limit"], $arr);
        $total_records = count(prepareTable($query_base, $arr));
        $records_filters = count(prepareTable($query_base . " " . $filters["search"], $arr));

        $draw = $_POST["draw"];
        for ($x = 0; $x < count($data); $x++) {
            $button = "";
                $button .= "<button onclick='editEmployee(this)' data-toggle='tooltip' data-placement='bottom' title='$edit' class='btn btn-primary btn-sm'><i class='fa fa-edit'></i>" . $load_tag . "</button>&nbsp;";
				if($data[$x]['id']!=0){
					$button .= "<button onclick='deleteEmployee(this)' data-toggle='tooltip' data-placement='bottom' title='$delete' class='btn btn-danger btn-sm'><i class='fa fa-trash'></i></button>&nbsp;";
				}
            $data[$x]["button"] = $button;
        }
        return json_encode(array("draw" => $draw, "recordsTotal" => $total_records, "recordsFiltered" => $records_filters, "data" => $data));
    }
	
	
    function getAllCustomers() {
        include './lib/language.php';
        $col_arr = array("`id`", "`name`", "`contact_person`", "`address`", "`balance`");
		$filters = getServer_filters($col_arr, $_POST);
		
        $arr = array();
        $query_base = "select " . implode(",", $col_arr) . " from (SELECT * FROM `customers`)final";

        $data = prepareTable($query_base . " " . $filters["search"] . " " . $filters["order"] . " " . $filters["limit"], $arr);
        $total_records = count(prepareTable($query_base, $arr));
        $records_filters = count(prepareTable($query_base . " " . $filters["search"], $arr));

        $draw = $_POST["draw"];
        for ($x = 0; $x < count($data); $x++) {
            $button = "";
                $button .= "<button onclick='editCustomer(this)' data-toggle='tooltip' data-placement='bottom' title='$edit' class='btn btn-primary btn-sm'><i class='fa fa-edit'></i>" . $load_tag . "</button>&nbsp;";
				if($data[$x]['id']!=0){
					$button .= "<button onclick='deleteCustomer(this)' data-toggle='tooltip' data-placement='bottom' title='$delete' class='btn btn-danger btn-sm'><i class='fa fa-trash'></i></button>&nbsp;";
				}
            $data[$x]["button"] = $button;
        }
        return json_encode(array("draw" => $draw, "recordsTotal" => $total_records, "recordsFiltered" => $records_filters, "data" => $data));
    }
	
    function getAllSuppliers() {
        include './lib/language.php';
        $col_arr = array("`id`", "`name`","`currency`", "`contact_person`", "`address`", "`balance`");
		$filters = getServer_filters($col_arr, $_POST);
		
        $arr = array();
        $query_base = "select " . implode(",", $col_arr) . " from (
			SELECT A.*,concat(B.`name`,' (',B.`symbol`,')')'currency' FROM `suppliers` A
			LEFT JOIN `currencys` B ON B.`id`=A.`id_currency`
		)final";

        $data = prepareTable($query_base . " " . $filters["search"] . " " . $filters["order"] . " " . $filters["limit"], $arr);
        $total_records = count(prepareTable($query_base, $arr));
        $records_filters = count(prepareTable($query_base . " " . $filters["search"], $arr));

        $draw = $_POST["draw"];
        for ($x = 0; $x < count($data); $x++) {
            $button = "";
                $button .= "<button onclick='editSupplier(this)' data-toggle='tooltip' data-placement='bottom' title='$edit' class='btn btn-primary btn-sm'><i class='fa fa-edit'></i>" . $load_tag . "</button>&nbsp;";
				if($data[$x]['id']!=0){
					$button .= "<button onclick='deleteSupplier(this)' data-toggle='tooltip' data-placement='bottom' title='$delete' class='btn btn-danger btn-sm'><i class='fa fa-trash'></i></button>&nbsp;";
				}
            $data[$x]["button"] = $button;
        }
        return json_encode(array("draw" => $draw, "recordsTotal" => $total_records, "recordsFiltered" => $records_filters, "data" => $data));
    }
	
	
    function getAllPlastics() {
        include './lib/language.php';
        $col_arr = array("`id`", "`specification`", "`minimum`", "`starting_qty`","`price`");
		$filters = getServer_filters($col_arr, $_POST);
		
        $arr = array();
        $query_base = "select " . implode(",", $col_arr) . " from (SELECT * FROM `plastics`)final";

        $data = prepareTable($query_base . " " . $filters["search"] . " " . $filters["order"] . " " . $filters["limit"], $arr);
        $total_records = count(prepareTable($query_base, $arr));
        $records_filters = count(prepareTable($query_base . " " . $filters["search"], $arr));

        $draw = $_POST["draw"];
        for ($x = 0; $x < count($data); $x++) {
            $button = "";
                $button .= "<button onclick='editPlastic(this)' data-toggle='tooltip' data-placement='bottom' title='$edit' class='btn btn-primary btn-sm'><i class='fa fa-edit'></i>" . $load_tag . "</button>&nbsp;";
				if($data[$x]['id']!=0){
					$button .= "<button onclick='deletePlastic(this)' data-toggle='tooltip' data-placement='bottom' title='$delete' class='btn btn-danger btn-sm'><i class='fa fa-trash'></i></button>&nbsp;";
				}
            $data[$x]["button"] = $button;
        }
        return json_encode(array("draw" => $draw, "recordsTotal" => $total_records, "recordsFiltered" => $records_filters, "data" => $data));
    }
	
	
    function getAllAdhesives() {
        include './lib/language.php';
        $col_arr = array("`id`", "`name`", "`type`", "`minimum`", "`starting_qty`","`price`");
		$filters = getServer_filters($col_arr, $_POST);
		
        $arr = array();
        $query_base = "select " . implode(",", $col_arr) . " from (SELECT * FROM `adhesives`)final";

        $data = prepareTable($query_base . " " . $filters["search"] . " " . $filters["order"] . " " . $filters["limit"], $arr);
        $total_records = count(prepareTable($query_base, $arr));
        $records_filters = count(prepareTable($query_base . " " . $filters["search"], $arr));

        $draw = $_POST["draw"];
        for ($x = 0; $x < count($data); $x++) {
            $button = "";
                $button .= "<button onclick='editAdhesive(this)' data-toggle='tooltip' data-placement='bottom' title='$edit' class='btn btn-primary btn-sm'><i class='fa fa-edit'></i>" . $load_tag . "</button>&nbsp;";
				if($data[$x]['id']!=0){
					$button .= "<button onclick='deleteAdhesive(this)' data-toggle='tooltip' data-placement='bottom' title='$delete' class='btn btn-danger btn-sm'><i class='fa fa-trash'></i></button>&nbsp;";
				}
            $data[$x]["button"] = $button;
        }
        return json_encode(array("draw" => $draw, "recordsTotal" => $total_records, "recordsFiltered" => $records_filters, "data" => $data));
    }
	
	
    function getAllChemicals() {
        include './lib/language.php';
        $col_arr = array("`id`", "`name`", "`type`", "`minimum`", "`starting_qty`","`price`");
		$filters = getServer_filters($col_arr, $_POST);
		
        $arr = array();
        $query_base = "select " . implode(",", $col_arr) . " from (SELECT * FROM `chemicals`)final";

        $data = prepareTable($query_base . " " . $filters["search"] . " " . $filters["order"] . " " . $filters["limit"], $arr);
        $total_records = count(prepareTable($query_base, $arr));
        $records_filters = count(prepareTable($query_base . " " . $filters["search"], $arr));

        $draw = $_POST["draw"];
        for ($x = 0; $x < count($data); $x++) {
            $button = "";
                $button .= "<button onclick='editChemical(this)' data-toggle='tooltip' data-placement='bottom' title='$edit' class='btn btn-primary btn-sm'><i class='fa fa-edit'></i>" . $load_tag . "</button>&nbsp;";
				if($data[$x]['id']!=0){
					$button .= "<button onclick='deleteChemical(this)' data-toggle='tooltip' data-placement='bottom' title='$delete' class='btn btn-danger btn-sm'><i class='fa fa-trash'></i></button>&nbsp;";
				}
            $data[$x]["button"] = $button;
        }
        return json_encode(array("draw" => $draw, "recordsTotal" => $total_records, "recordsFiltered" => $records_filters, "data" => $data));
    }
	
	
    function getAllInks() {
        include './lib/language.php';
        $col_arr = array("`id`", "`name`", "`type`", "`minimum`", "`starting_qty`","`price`");
		$filters = getServer_filters($col_arr, $_POST);
		
        $arr = array();
        $query_base = "select " . implode(",", $col_arr) . " from (SELECT * FROM `inks`)final";

        $data = prepareTable($query_base . " " . $filters["search"] . " " . $filters["order"] . " " . $filters["limit"], $arr);
        $total_records = count(prepareTable($query_base, $arr));
        $records_filters = count(prepareTable($query_base . " " . $filters["search"], $arr));

        $draw = $_POST["draw"];
        for ($x = 0; $x < count($data); $x++) {
            $button = "";
                $button .= "<button onclick='editInk(this)' data-toggle='tooltip' data-placement='bottom' title='$edit' class='btn btn-primary btn-sm'><i class='fa fa-edit'></i>" . $load_tag . "</button>&nbsp;";
				if($data[$x]['id']!=0){
					$button .= "<button onclick='deleteInk(this)' data-toggle='tooltip' data-placement='bottom' title='$delete' class='btn btn-danger btn-sm'><i class='fa fa-trash'></i></button>&nbsp;";
				}
            $data[$x]["button"] = $button;
        }
        return json_encode(array("draw" => $draw, "recordsTotal" => $total_records, "recordsFiltered" => $records_filters, "data" => $data));
    }
	
    function getAllCylinders() {
        include './lib/language.php';
        $col_arr = array("`id`", "`color`","`size`", "`minimum`", "`starting_qty`", "`price`");
		$filters = getServer_filters($col_arr, $_POST);
		
        $arr = array();
        $query_base = "select " . implode(",", $col_arr) . " from (SELECT * FROM `cylinders`)final";

        $data = prepareTable($query_base . " " . $filters["search"] . " " . $filters["order"] . " " . $filters["limit"], $arr);
        $total_records = count(prepareTable($query_base, $arr));
        $records_filters = count(prepareTable($query_base . " " . $filters["search"], $arr));

        $draw = $_POST["draw"];
        for ($x = 0; $x < count($data); $x++) {
            $button = "";
                $button .= "<button onclick='editCylinder(this)' data-toggle='tooltip' data-placement='bottom' title='$edit' class='btn btn-primary btn-sm'><i class='fa fa-edit'></i>" . $load_tag . "</button>&nbsp;";
				if($data[$x]['id']!=0){
					$button .= "<button onclick='deleteCylinder(this)' data-toggle='tooltip' data-placement='bottom' title='$delete' class='btn btn-danger btn-sm'><i class='fa fa-trash'></i></button>&nbsp;";
				}
            $data[$x]["button"] = $button;
        }
        return json_encode(array("draw" => $draw, "recordsTotal" => $total_records, "recordsFiltered" => $records_filters, "data" => $data));
    }
	
	
	
    function getAllProducts() {
        include './lib/language.php';
        $col_arr = array("`id`", "`name`", "`date`", "`customer`", "`plastic`");
		$filters = getServer_filters($col_arr, $_POST);
		
        $arr = array();
        $query_base = "select " . implode(",", $col_arr) . " from (
			SELECT PR.*,C.`name` 'customer',
			(SELECT GROUP_CONCAT(DISTINCT `specification` SEPARATOR '|') 'x' FROM `plastics` WHERE `id` IN (SELECT `id_plastic` FROM `product_has_plastics` WHERE `id_product`=PR.`id`)) 'plastic' 
			FROM `products`PR
			LEFT JOIN `customers` C ON PR.`id_customer`=C.`id`
		)final";

        $data = prepareTable($query_base . " " . $filters["search"] . " " . $filters["order"] . " " . $filters["limit"], $arr);
        $total_records = count(prepareTable($query_base, $arr));
        $records_filters = count(prepareTable($query_base . " " . $filters["search"], $arr));

        $draw = $_POST["draw"];
        for ($x = 0; $x < count($data); $x++) {
            $button = "";
                $button .= "<button onclick='editProduct(this)' data-toggle='tooltip' data-placement='bottom' title='$edit' class='btn btn-primary btn-sm'><i class='fa fa-edit'></i>" . $load_tag . "</button>&nbsp;";
				if($data[$x]['id']!=0){
					$button .= "<button onclick='deleteProduct(this)' data-toggle='tooltip' data-placement='bottom' title='$delete' class='btn btn-danger btn-sm'><i class='fa fa-trash'></i></button>&nbsp;";
				}
            $data[$x]["button"] = $button;
        }
        return json_encode(array("draw" => $draw, "recordsTotal" => $total_records, "recordsFiltered" => $records_filters, "data" => $data));
    }
	
    function getAllCustomerPO() {
        include './lib/language.php';
        $col_arr = array("`id`", "`date`", "`customer`", "`po_number`", "`status`");
		$filters = getServer_filters($col_arr, $_POST);
		
        $arr = array();
        $query_base = "select " . implode(",", $col_arr) . " from (
			SELECT CP.*,C.`name` 'customer' FROM `customerpos` CP
			LEFT JOIN `customers` C ON C.`id`=CP.`id_customer`
		)final";

        $data = prepareTable($query_base . " " . $filters["search"] . " " . $filters["order"] . " " . $filters["limit"], $arr);
        $total_records = count(prepareTable($query_base, $arr));
        $records_filters = count(prepareTable($query_base . " " . $filters["search"], $arr));

        $draw = $_POST["draw"];
        for ($x = 0; $x < count($data); $x++) {
            $button = "";
                $button .= "<button onclick='editCustomerPO(this)' data-toggle='tooltip' data-placement='bottom' title='$edit' class='btn btn-primary btn-sm'><i class='fa fa-edit'></i>" . $load_tag . "</button>&nbsp;";
				if($data[$x]['id']!=0){
					$button .= "<button onclick='deleteCustomerPO(this)' data-toggle='tooltip' data-placement='bottom' title='$delete' class='btn btn-danger btn-sm'><i class='fa fa-trash'></i></button>&nbsp;";
				}
				$status = prepareTable("SELECT sum(`status`) 'sum', count(*)'count' FROM `customerpo_has_products` WHERE `id_customerpo`=?",array($data[$x]["id"]));
				
            $data[$x]["button"] = $button;
			if($status[0]['count']==0){
				$percentage = $status[0]['count'];
			}else{
				$percentage = ($status[0]['sum']/$status[0]['count'])*100;
			}
			//var_dump($percentage);
			if($percentage==0){
				$data[$x]["status"] = "<span class='label label-default'>{$pending}</span>";
			}if($percentage==100){
				$data[$x]["status"] = "<span class='label label-success'>{$all_delivered}</span>";
			}else{
				$data[$x]["status"] = "<div class='progress'><div class='progress-bar btn-success' role='progressbar' style='width: {$percentage}%;' aria-valuenow='{$percentage}' aria-valuemin='0' aria-valuemax='100'><span>{$status[0]['sum']} {$of} {$status[0]['count']} {$job_name} {$done}</span></div></div>";
			}
        }
        return json_encode(array("draw" => $draw, "recordsTotal" => $total_records, "recordsFiltered" => $records_filters, "data" => $data));
    }
	
    function getAllJobOrder() {
        include './lib/language.php';
        $col_arr = array("`id`", "`date`", "`customer`", "`jo_number`", "`po_number`", "`po_qty`", "`status`","`product`");
		$filters = getServer_filters($col_arr, $_POST);
		
        $arr = array();
        $query_base = "select " . implode(",", $col_arr) . " from (
			SELECT JO.*,C.`name` 'customer',P.`name` `product` FROM `joborders` JO
			LEFT JOIN `customers` C ON C.`id`=JO.`id_customer`
			LEFT JOIN `products` P ON P.`id`=JO.`id_product`
		)final";

        $data = prepareTable($query_base . " " . $filters["search"] . " " . $filters["order"] . " " . $filters["limit"], $arr);
        $total_records = count(prepareTable($query_base, $arr));
        $records_filters = count(prepareTable($query_base . " " . $filters["search"], $arr));

        $draw = $_POST["draw"];
        for ($x = 0; $x < count($data); $x++) {
            $button = "";
				if($data[$x]['status']!=0){
					$button .= "<button onclick='logProductions(this)' data-toggle='tooltip' data-placement='bottom' title='$view_logs' class='btn btn-primary btn-sm'><i class='fa fa-th-list '></i>" . $load_tag . "</button>&nbsp;";
				}
                $button .= "<button onclick='editJobOrder(this)' data-toggle='tooltip' data-placement='bottom' title='$edit' class='btn btn-primary btn-sm'><i class='fa fa-edit'></i>" . $load_tag . "</button>&nbsp;";
				if($data[$x]['id']!=0){
					$button .= "<button onclick='deleteJobOrder(this)' data-toggle='tooltip' data-placement='bottom' title='$delete' class='btn btn-danger btn-sm'><i class='fa fa-trash'></i></button>&nbsp;";
				}
            $data[$x]["button"] = $button;
			if($data[$x]["status"]==0){
				$data[$x]["status"] = "<span class='label label-default'>{$pending}</span>";
			}elseif($data[$x]["status"]==1){
				$data[$x]["status"] = "<span class='label label-default'>{$none_delivered}</span>";
			}elseif($data[$x]["status"]==2){
				$data[$x]["status"] = "<span class='label label-warning'>{$partially_delivered}</span>";
			}elseif($data[$x]["status"]==3){
				$data[$x]["status"] = "<span class='label label-success'>{$completed}</span>";
			}
		/* $percentage = prepareTable("SELECT ((sum(`status`)/count(*))*100) 'percentage',sum(`status`)'sum',count(*)'total' FROM `joborder_has_plastics` WHERE `id_joborder`=?",array($data[$x]["id"]))[0];
            $data[$x]["status"] = "<div class='progress'><div class='progress-bar btn-success' role='progressbar' style='width: {$percentage['percentage']}%;' aria-valuenow='{$percentage['percentage']}' aria-valuemin='0' aria-valuemax='100'><span>{$percentage['sum']} of {$percentage['total']}</span></div></div>"; */
        }
        return json_encode(array("draw" => $draw, "recordsTotal" => $total_records, "recordsFiltered" => $records_filters, "data" => $data));
    }
	
    function getAllJobOrderProductions() {
        include './lib/language.php';
		$id = 0;
		if(isset($_POST['from'])){
			$id = $_POST['from'];
		}
        $col_arr = array("`id`", "`date`", "`pd_roll`", "`pd_weight`", "`ld_roll`", "`ld_weight`", "`sd_roll`", "`sd_weight`", "`bf_roll`", "`bf_weight`", "`lod_dr_number`", "`lod_wieght`", "`remarks`");
		$filters = getServer_filters($col_arr, $_POST);
        $arr = array();
        $query_base = "select " . implode(",", $col_arr) . " from (
			SELECT * FROM `joborder_has_productions` 
			WHERE `id_joborder`={$id}
		)final";

        $data = prepareTable($query_base . " " . $filters["search"] . " " . $filters["order"] . " " . $filters["limit"], $arr);
        $total_records = count(prepareTable($query_base, $arr));
        $records_filters = count(prepareTable($query_base . " " . $filters["search"], $arr));

        $draw = $_POST["draw"];
        for ($x = 0; $x < count($data); $x++) {
            $button = "";
				/* if($data[$x]['status']!=0){
					$button .= "<button onclick='logProductions(this)' data-toggle='tooltip' data-placement='bottom' title='$view_logs' class='btn btn-primary btn-sm'><i class='fa fa-th-list '></i>" . $load_tag . "</button>&nbsp;";
				}
                $button .= "<button onclick='editJobOrder(this)' data-toggle='tooltip' data-placement='bottom' title='$edit' class='btn btn-primary btn-sm'><i class='fa fa-edit'></i>" . $load_tag . "</button>&nbsp;";
				if($data[$x]['id']!=0){
					$button .= "<button onclick='deleteJobOrder(this)' data-toggle='tooltip' data-placement='bottom' title='$delete' class='btn btn-danger btn-sm'><i class='fa fa-trash'></i></button>&nbsp;";
				} */
            $data[$x]["button"] = $button;
        }
        return json_encode(array("draw" => $draw, "recordsTotal" => $total_records, "recordsFiltered" => $records_filters, "data" => $data));
    }
	
	
	
}

