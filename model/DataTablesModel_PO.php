<?php

class DataTablesModel_PO{

    function __construct() {
        
    }
	
//Adhesives	
    function getAllInAdhesives() {
        include './lib/language.php';
        $col_arr = array("`id`", "`name`", "`qty`");
		$filters = getServer_filters($col_arr, $_POST);
		
        $arr = array();
        $query_base = "select " . implode(",", $col_arr) . " from (
			SELECT A.*,B.`name` FROM `in_adhesives` A
			LEFT JOIN `adhesives` B ON A.`id_adhesive` = B.`id`
			)final";

        $data = prepareTable($query_base . " " . $filters["search"] . " " . $filters["order"] . " " . $filters["limit"], $arr);
        $total_records = count(prepareTable($query_base, $arr));
        $records_filters = count(prepareTable($query_base . " " . $filters["search"], $arr));

        $draw = $_POST["draw"];
        for ($x = 0; $x < count($data); $x++) {
            $button = "";
                $button .= "<button onclick='editAdhesive(this)' data-toggle='tooltip' data-placement='bottom' title='$edit' class='btn btn-primary btn-sm'><i class='fa fa-edit'></i>" . $load_tag . "</button>&nbsp;";
				if($data[$x]['id']!=0){
					$button .= "<button onclick='deleteAdhesive(this)' data-toggle='tooltip' data-placement='bottom' title='$delete' class='btn btn-danger btn-sm'><i class='fa fa-trash'></i></button>&nbsp;";
				}
            $data[$x]["button"] = $button;
        }
        return json_encode(array("draw" => $draw, "recordsTotal" => $total_records, "recordsFiltered" => $records_filters, "data" => $data));
    }
	
    function getAllInAdhesivesLogs() {
        include './lib/language.php';
        $col_arr = array("`id`", "`issued_on`", "`name`", "`typed`", "`initial_qty`", "`qty`", "`remaining_qty`");
		$filters = getServer_filters($col_arr, $_POST);
		
        $arr = array();
        $query_base = "select " . implode(",", $col_arr) . " from (
			SELECT A.*,C.`name`,(CASE A.`type` WHEN 0 THEN '{$in}' ELSE '{$out}' END) 'typed' FROM `in_adhesive_has_logs` A
			LEFT JOIN `adhesives` C ON A.`id_adhesive` = C.`id`
			)final";

        $data = prepareTable($query_base . " " . $filters["search"] . " " . $filters["order"] . " " . $filters["limit"], $arr);
        $total_records = count(prepareTable($query_base, $arr));
        $records_filters = count(prepareTable($query_base . " " . $filters["search"], $arr));

        $draw = $_POST["draw"];
        for ($x = 0; $x < count($data); $x++) {
            $button = "";
                $button .= "<button onclick='editAdhesive(this)' data-toggle='tooltip' data-placement='bottom' title='$edit' class='btn btn-primary btn-sm'><i class='fa fa-edit'></i>" . $load_tag . "</button>&nbsp;";
				if($data[$x]['id']!=0){
					$button .= "<button onclick='deleteAdhesive(this)' data-toggle='tooltip' data-placement='bottom' title='$delete' class='btn btn-danger btn-sm'><i class='fa fa-trash'></i></button>&nbsp;";
				}
            $data[$x]["button"] = $button;
        }
        return json_encode(array("draw" => $draw, "recordsTotal" => $total_records, "recordsFiltered" => $records_filters, "data" => $data));
    }
	
//Plastics	
    function getAllInPlastics() {
        include './lib/language.php';
        $col_arr = array("`id`", "`name`", "`qty`");
		$filters = getServer_filters($col_arr, $_POST);
		
        $arr = array();
        $query_base = "select " . implode(",", $col_arr) . " from (
			SELECT A.*,B.`specification` 'name' FROM `in_plastics` A
			LEFT JOIN `plastics` B ON A.`id_plastic` = B.`id`
			)final";

        $data = prepareTable($query_base . " " . $filters["search"] . " " . $filters["order"] . " " . $filters["limit"], $arr);
        $total_records = count(prepareTable($query_base, $arr));
        $records_filters = count(prepareTable($query_base . " " . $filters["search"], $arr));

        $draw = $_POST["draw"];
        for ($x = 0; $x < count($data); $x++) {
            $button = "";
                $button .= "<button onclick='editPlastic(this)' data-toggle='tooltip' data-placement='bottom' title='$edit' class='btn btn-primary btn-sm'><i class='fa fa-edit'></i>" . $load_tag . "</button>&nbsp;";
				if($data[$x]['id']!=0){
					$button .= "<button onclick='deletePlastic(this)' data-toggle='tooltip' data-placement='bottom' title='$delete' class='btn btn-danger btn-sm'><i class='fa fa-trash'></i></button>&nbsp;";
				}
            $data[$x]["button"] = $button;
        }
        return json_encode(array("draw" => $draw, "recordsTotal" => $total_records, "recordsFiltered" => $records_filters, "data" => $data));
    }
	
    function getAllInPlasticsLogs() {
        include './lib/language.php';
        $col_arr = array("`id`", "`issued_on`", "`name`", "`typed`", "`initial_qty`", "`qty`", "`remaining_qty`");
		$filters = getServer_filters($col_arr, $_POST);
		
        $arr = array();
        $query_base = "select " . implode(",", $col_arr) . " from (
			SELECT A.*,C.`specification` 'name',(CASE A.`type` WHEN 0 THEN '{$in}' ELSE '{$out}' END) 'typed' FROM `in_plastic_has_logs` A
			LEFT JOIN `plastics` C ON A.`id_plastic` = C.`id`
			)final";

        $data = prepareTable($query_base . " " . $filters["search"] . " " . $filters["order"] . " " . $filters["limit"], $arr);
        $total_records = count(prepareTable($query_base, $arr));
        $records_filters = count(prepareTable($query_base . " " . $filters["search"], $arr));

        $draw = $_POST["draw"];
        for ($x = 0; $x < count($data); $x++) {
            $button = "";
                $button .= "<button onclick='editPlastic(this)' data-toggle='tooltip' data-placement='bottom' title='$edit' class='btn btn-primary btn-sm'><i class='fa fa-edit'></i>" . $load_tag . "</button>&nbsp;";
				if($data[$x]['id']!=0){
					$button .= "<button onclick='deletePlastic(this)' data-toggle='tooltip' data-placement='bottom' title='$delete' class='btn btn-danger btn-sm'><i class='fa fa-trash'></i></button>&nbsp;";
				}
            $data[$x]["button"] = $button;
        }
        return json_encode(array("draw" => $draw, "recordsTotal" => $total_records, "recordsFiltered" => $records_filters, "data" => $data));
    }
	
	
//Inks	
    function getAllInInks() {
        include './lib/language.php';
        $col_arr = array("`id`", "`name`", "`qty`");
		$filters = getServer_filters($col_arr, $_POST);
		
        $arr = array();
        $query_base = "select " . implode(",", $col_arr) . " from (
			SELECT A.*,B.`name` FROM `in_inks` A
			LEFT JOIN `inks` B ON A.`id_ink` = B.`id`
			)final";

        $data = prepareTable($query_base . " " . $filters["search"] . " " . $filters["order"] . " " . $filters["limit"], $arr);
        $total_records = count(prepareTable($query_base, $arr));
        $records_filters = count(prepareTable($query_base . " " . $filters["search"], $arr));

        $draw = $_POST["draw"];
        for ($x = 0; $x < count($data); $x++) {
            $button = "";
                $button .= "<button onclick='editInk(this)' data-toggle='tooltip' data-placement='bottom' title='$edit' class='btn btn-primary btn-sm'><i class='fa fa-edit'></i>" . $load_tag . "</button>&nbsp;";
				if($data[$x]['id']!=0){
					$button .= "<button onclick='deleteInk(this)' data-toggle='tooltip' data-placement='bottom' title='$delete' class='btn btn-danger btn-sm'><i class='fa fa-trash'></i></button>&nbsp;";
				}
            $data[$x]["button"] = $button;
        }
        return json_encode(array("draw" => $draw, "recordsTotal" => $total_records, "recordsFiltered" => $records_filters, "data" => $data));
    }
	
    function getAllInInksLogs() {
        include './lib/language.php';
        $col_arr = array("`id`", "`issued_on`", "`name`", "`typed`", "`initial_qty`", "`qty`", "`remaining_qty`");
		$filters = getServer_filters($col_arr, $_POST);
		
        $arr = array();
        $query_base = "select " . implode(",", $col_arr) . " from (
			SELECT A.*,C.`name`,(CASE A.`type` WHEN 0 THEN '{$in}' ELSE '{$out}' END) 'typed' FROM `in_ink_has_logs` A
			LEFT JOIN `inks` C ON A.`id_ink` = C.`id`
			)final";

        $data = prepareTable($query_base . " " . $filters["search"] . " " . $filters["order"] . " " . $filters["limit"], $arr);
        $total_records = count(prepareTable($query_base, $arr));
        $records_filters = count(prepareTable($query_base . " " . $filters["search"], $arr));

        $draw = $_POST["draw"];
        for ($x = 0; $x < count($data); $x++) {
            $button = "";
                $button .= "<button onclick='editInk(this)' data-toggle='tooltip' data-placement='bottom' title='$edit' class='btn btn-primary btn-sm'><i class='fa fa-edit'></i>" . $load_tag . "</button>&nbsp;";
				if($data[$x]['id']!=0){
					$button .= "<button onclick='deleteInk(this)' data-toggle='tooltip' data-placement='bottom' title='$delete' class='btn btn-danger btn-sm'><i class='fa fa-trash'></i></button>&nbsp;";
				}
            $data[$x]["button"] = $button;
        }
        return json_encode(array("draw" => $draw, "recordsTotal" => $total_records, "recordsFiltered" => $records_filters, "data" => $data));
    }
	
	
//Cylinders	
    function getAllInCylinders() {
        include './lib/language.php';
        $col_arr = array("`id`", "`name`", "`qty`");
		$filters = getServer_filters($col_arr, $_POST);
		
        $arr = array();
        $query_base = "select " . implode(",", $col_arr) . " from (
			SELECT A.*,B.`color` 'name' FROM `in_cylinders` A
			LEFT JOIN `cylinders` B ON A.`id_cylinder` = B.`id`
			)final";

        $data = prepareTable($query_base . " " . $filters["search"] . " " . $filters["order"] . " " . $filters["limit"], $arr);
        $total_records = count(prepareTable($query_base, $arr));
        $records_filters = count(prepareTable($query_base . " " . $filters["search"], $arr));

        $draw = $_POST["draw"];
        for ($x = 0; $x < count($data); $x++) {
            $button = "";
                $button .= "<button onclick='editCylinder(this)' data-toggle='tooltip' data-placement='bottom' title='$edit' class='btn btn-primary btn-sm'><i class='fa fa-edit'></i>" . $load_tag . "</button>&nbsp;";
				if($data[$x]['id']!=0){
					$button .= "<button onclick='deleteCylinder(this)' data-toggle='tooltip' data-placement='bottom' title='$delete' class='btn btn-danger btn-sm'><i class='fa fa-trash'></i></button>&nbsp;";
				}
            $data[$x]["button"] = $button;
        }
        return json_encode(array("draw" => $draw, "recordsTotal" => $total_records, "recordsFiltered" => $records_filters, "data" => $data));
    }
	
    function getAllInCylindersLogs() {
        include './lib/language.php';
        $col_arr = array("`id`", "`issued_on`", "`name`", "`typed`", "`initial_qty`", "`qty`", "`remaining_qty`");
		$filters = getServer_filters($col_arr, $_POST);
		
        $arr = array();
        $query_base = "select " . implode(",", $col_arr) . " from (
			SELECT A.*,C.`color` 'name',(CASE A.`type` WHEN 0 THEN '{$in}' ELSE '{$out}' END) 'typed' FROM `in_cylinder_has_logs` A
			LEFT JOIN `cylinders` C ON A.`id_cylinder` = C.`id`
			)final";

        $data = prepareTable($query_base . " " . $filters["search"] . " " . $filters["order"] . " " . $filters["limit"], $arr);
        $total_records = count(prepareTable($query_base, $arr));
        $records_filters = count(prepareTable($query_base . " " . $filters["search"], $arr));

        $draw = $_POST["draw"];
        for ($x = 0; $x < count($data); $x++) {
            $button = "";
                $button .= "<button onclick='editCylinder(this)' data-toggle='tooltip' data-placement='bottom' title='$edit' class='btn btn-primary btn-sm'><i class='fa fa-edit'></i>" . $load_tag . "</button>&nbsp;";
				if($data[$x]['id']!=0){
					$button .= "<button onclick='deleteCylinder(this)' data-toggle='tooltip' data-placement='bottom' title='$delete' class='btn btn-danger btn-sm'><i class='fa fa-trash'></i></button>&nbsp;";
				}
            $data[$x]["button"] = $button;
        }
        return json_encode(array("draw" => $draw, "recordsTotal" => $total_records, "recordsFiltered" => $records_filters, "data" => $data));
    }
	
	
//Chemicals
    function getAllInChemicals() {
        include './lib/language.php';
        $col_arr = array("`id`", "`name`", "`qty`");
		$filters = getServer_filters($col_arr, $_POST);
		
        $arr = array();
        $query_base = "select " . implode(",", $col_arr) . " from (
			SELECT A.*,B.`name` FROM `in_chemicals` A
			LEFT JOIN `chemicals` B ON A.`id_chemical` = B.`id`
			)final";

        $data = prepareTable($query_base . " " . $filters["search"] . " " . $filters["order"] . " " . $filters["limit"], $arr);
        $total_records = count(prepareTable($query_base, $arr));
        $records_filters = count(prepareTable($query_base . " " . $filters["search"], $arr));

        $draw = $_POST["draw"];
        for ($x = 0; $x < count($data); $x++) {
            $button = "";
                $button .= "<button onclick='editChemical(this)' data-toggle='tooltip' data-placement='bottom' title='$edit' class='btn btn-primary btn-sm'><i class='fa fa-edit'></i>" . $load_tag . "</button>&nbsp;";
				if($data[$x]['id']!=0){
					$button .= "<button onclick='deleteChemical(this)' data-toggle='tooltip' data-placement='bottom' title='$delete' class='btn btn-danger btn-sm'><i class='fa fa-trash'></i></button>&nbsp;";
				}
            $data[$x]["button"] = $button;
        }
        return json_encode(array("draw" => $draw, "recordsTotal" => $total_records, "recordsFiltered" => $records_filters, "data" => $data));
    }
	
    function getAllInChemicalsLogs() {
        include './lib/language.php';
        $col_arr = array("`id`", "`issued_on`", "`name`", "`typed`", "`initial_qty`", "`qty`", "`remaining_qty`");
		$filters = getServer_filters($col_arr, $_POST);
		
        $arr = array();
        $query_base = "select " . implode(",", $col_arr) . " from (
			SELECT A.*,C.`name`,(CASE A.`type` WHEN 0 THEN '{$in}' ELSE '{$out}' END) 'typed' FROM `in_chemical_has_logs` A
			LEFT JOIN `chemicals` C ON A.`id_chemical` = C.`id`
			)final";

        $data = prepareTable($query_base . " " . $filters["search"] . " " . $filters["order"] . " " . $filters["limit"], $arr);
        $total_records = count(prepareTable($query_base, $arr));
        $records_filters = count(prepareTable($query_base . " " . $filters["search"], $arr));

        $draw = $_POST["draw"];
        for ($x = 0; $x < count($data); $x++) {
            $button = "";
                $button .= "<button onclick='editChemical(this)' data-toggle='tooltip' data-placement='bottom' title='$edit' class='btn btn-primary btn-sm'><i class='fa fa-edit'></i>" . $load_tag . "</button>&nbsp;";
				if($data[$x]['id']!=0){
					$button .= "<button onclick='deleteChemical(this)' data-toggle='tooltip' data-placement='bottom' title='$delete' class='btn btn-danger btn-sm'><i class='fa fa-trash'></i></button>&nbsp;";
				}
            $data[$x]["button"] = $button;
        }
        return json_encode(array("draw" => $draw, "recordsTotal" => $total_records, "recordsFiltered" => $records_filters, "data" => $data));
    }
	
	
	
	
}

