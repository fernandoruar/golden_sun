<?php

class MainModel {

    function __construct() {
        ;
    }

    function rrmdir($dir) {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (is_dir($dir . "/" . $object))
                        $this->rrmdir($dir . "/" . $object);
                    else
                        unlink($dir . "/" . $object);
                }
            }
            rmdir($dir);
        }else if (is_file($dir)) {
            unlink($dir);
        }
    }

    function checkAccount() {
        $session_details = prepareTable("SELECT
`id`,`name`,`username`

from `employees` where `username`=? and (`password`)=(?);
", array($_POST["username"], $_POST["password"]));
        if (count($session_details) > 0) {
            $_SESSION[getSessionName()] = $session_details[0];
        }

        return json_encode((count($session_details) > 0) ? 1 : 0);
    }

    function addData($data_arr, $field_arr, $table_name, $check_array) {
        $queries = array();
        $arrays = array();
        foreach ($check_array as $rows) {
            $table_check = $field_arr[$rows["index"]]["table_name"];
            $cols = $rows["col_index"];
            $strict_status = true;
            if (isset($rows["strict"])) {
                $strict_status = $rows["strict"];
            }
            $delimeter = "";
            if ($strict_status) {
                $delimeter = "and";
            } else {
                $delimeter = "or";
            }
            $conditions = array();
            $values = $data_arr[$rows["index"]];

            foreach ($cols as $inner_cols) {
                $conditions[] = $field_arr[$rows["index"]]["columns"][$inner_cols] . "=?";
            }
            if (is_array($values[0])) {
                $check_counter = 0;
                foreach ($values as $value_arr) {
                    $varr = array();
                    foreach ($cols as $inner_cols) {
                        $varr[] = $data_arr[$rows["index"]][$check_counter][$inner_cols];
                    }
                    $queries[] = "select

(case when count(*)>0 then null else 0 end) 'status'

from `$table_check`
where " . implode(" $delimeter ", $conditions);

                    $arrays[] = $varr;
                    $check_counter++;
                }
            } else {
                $varr = array();
                foreach ($cols as $inner_cols) {
                    $varr[] = $data_arr[$rows["index"]][$inner_cols];
                }
                $queries[] = "select

(case when count(*)>0 then null else 0 end) 'status'

from `$table_check`
where " . implode(" $delimeter ", $conditions);

                $arrays[] = $varr;
            }
        }
        $queries[] = "set @`id_reference`:=(SELECT `AUTO_INCREMENT`
FROM  INFORMATION_SCHEMA.TABLES
WHERE TABLE_SCHEMA = '" . getTableName() . "'
AND   TABLE_NAME   = '" . $table_name . "');";
        $arrays[] = array();


        for ($x = 0; $x < count($field_arr); $x++) {
            if (isset($data_arr[$x]) && count($data_arr[$x]) > 0) {
                $values = array();
                $columns = array();
                foreach ($field_arr[$x]["columns"] as $crows) {
                    if (is_array($crows)) {
                        $columns[] = $crows["col_name"];
                    } else {
                        $columns[] = $crows;
                    }
                }
                if (isset($field_arr[$x]["on_duplicate"])) {
                    $columns[] = "`id`";
                }
                $query_string = "insert into `{$field_arr[$x]["table_name"]}`(" . implode(",", $columns) . ") values ";

                if (!is_array($data_arr[$x][0])) {

                    $query_cols = array();
                    $current_value = $data_arr[$x];
                    $findex = 0;
                    foreach ($field_arr[$x]["columns"] as $crows) {
                        if (is_array($crows)) {
                            if (isset($crows["query"])) {
                                $query_cols[] = "({$crows["query"]})";
                            } else {
                                $query_cols[] = "?";
                            }
                            if (isset($crows["values"])) {
                                array_splice($current_value, $findex, 0, $crows["values"]);
                                $findex += count($crows["values"]);
                            } else {
                                $questions = explode("?", $crows["query"]);
                                if (count($questions) > 0) {
                                    $findex += count($questions);
                                } else {
                                    $findex++;
                                }
                            }
                        } else {
                            $query_cols[] = "?";
                            $findex++;
                        }
                    }
                    if (isset($field_arr[$x]["on_duplicate"])) {
                        $on_update = $field_arr[$x]["on_duplicate"];
                        $update = $on_update["update"];
                        $where_indexes = $on_update["col_index"];
                        $on_update_vals = array();
                        $where_cols = array();
                        foreach ($where_indexes as $rows) {
                            $where_cols[] = $field_arr[$x]["columns"][$rows] . "=?";
                            $on_update_vals[] = $data_arr[$x][$rows];
                        }
                        $queries[] = "set @`id_on_duplicate`:=(select {$on_update["col_name"]} from {$field_arr[$x]["table_name"]} where " . implode(",", $where_cols) . ");";
                        $arrays[] = $on_update_vals;
                        $query_cols[] = "(select @`id_on_duplicate`)";
                    }
                    $query_string .= "(" . implode(",", $query_cols) . ")";

                    $values = $current_value;
                    if (isset($field_arr[$x]["on_duplicate"])) {
                        $query_string .= " on duplicate key update ";
                        $on_update = $field_arr[$x]["on_duplicate"];
                        $update = $on_update["update"];
                        $where_indexes = $on_update["col_index"];
                        $on_update_vals = array();
                        $where_cols = array();
                        foreach ($where_indexes as $rows) {
                            $where_cols[] = $field_arr[$x]["columns"][$rows] . "=?";
                            $on_update_vals[] = $data_arr[$x][$rows];
                        }
                        $update_cols = array();
                        foreach ($update as $urows) {
                            $ustring = $urows["col_name"] . "=";
                            if (isset($urows["query"])) {
                                $ustring .= "(" . $urows["query"] . ")";
                            } else {
                                $ustring .= "?";
                            }
                            $update_cols[] = $ustring;
                            if (isset($urows["values"])) {
                                $values = array_merge($values, $urows["values"]);
                            }
                        }
                        $query_string .= implode(",", $update_cols);
                        $queries[] = "select {$on_update["col_name"]} into @`id_reference` from {$field_arr[$x]["table_name"]} where " . implode(",", $where_cols);
                        $arrays[] = $on_update_vals;
                    }
                } else {
                    $counter = 0;

                    foreach ($data_arr[$x] as $inner_rows) {
                        $query_cols = array();
                        $findex = 0;
                        $reference_used = false;
                        foreach ($field_arr[$x]["columns"] as $crows) {
                            if (is_array($crows)) {
                                if (isset($crows["is_reference"]) && $crows["is_reference"] && !$reference_used) {
                                    $query_cols[] = "(select @`id_reference`)";
                                    $reference_used = true;
                                } else {
                                    if (isset($crows["query"])) {
                                        $query_cols[] = "({$crows["query"]})";
                                    } else {
                                        $query_cols[] = "?";
                                    }
                                    if (isset($crows["values"])) {
                                        array_splice($inner_rows, $findex, 0, $crows["values"]);
                                        $findex += count($crows["values"]);
                                    } else {
                                        $questions = array();
                                        if (isset($crows["query"])) {
                                            $questions = explode("?", $crows["query"]);
                                        }
                                        if (count($questions) > 0) {
                                            $findex += count($questions);
                                        } else {
                                            $findex++;
                                        }
                                    }
                                }
                            } else {
                                if ($crows == "`id_" . substr($table_name, 0, -1) . "`" && !$reference_used) {
                                    $query_cols[] = "(select @`id_reference`)";
                                    $reference_used = true;
                                } else {
                                    $query_cols[] = "?";
                                }
                                $findex++;
                            }
                        }
                        $values = array_merge($values, $inner_rows);
                        $query_string .= "(" . implode(",", $query_cols) . ")";
                        if ($counter != count($data_arr[$x]) - 1) {
                            $query_string .= ",";
                        }
                        $counter++;
                    }
                }
                $queries[] = $query_string;
                $arrays[] = $values;
            }
        }

        return json_encode(transactStatement($queries, $arrays));
    }

    function editData($data_arr, $field_arr, $deleted_arr, $tables, $check_arr, $ref_id) {
        $queries = array();
        $arrays = array();
        foreach ($check_arr as $rows) {
            $table_check = $field_arr[$rows["index"]]["table_name"];
            $cols = $rows["col_index"];
            $conditions = array();
            $values = $data_arr[$rows["index"]];
            $strict_status = true;
            if (isset($rows["strict"])) {
                $strict_status = $rows["strict"];
            }
            $delimeter = "";
            if ($strict_status) {
                $delimeter = "and";
            } else {
                $delimeter = "or";
            }
            foreach ($cols as $inner_cols) {
                $conditions[] = $field_arr[$rows["index"]]["columns"][$inner_cols] . "=?";
            }
            $econdition = " and " . $rows["col_name"] . "!=?";
            if (is_array($values[0])) {
                $check_counter = 0;
                foreach ($values as $value_arr) {
                    $varr = array();
                    foreach ($cols as $inner_cols) {
                        $varr[] = $data_arr[$rows["index"]][$check_counter][$inner_cols];
                    }
                    $varr[] = $ref_id;
                    $queries[] = "select

(case when count(*)>0 then null else 0 end) 'status'

from `$table_check`
where (" . implode(" $delimeter ", $conditions) . ") $econdition";

                    $arrays[] = $varr;
                    $check_counter++;
                }
            } else {
                $varr = array();
                foreach ($cols as $inner_cols) {
                    $varr[] = $data_arr[$rows["index"]][$inner_cols];
                }
                $varr[] = $ref_id;
                $queries[] = "select

(case when count(*)>0 then null else 0 end) 'status'

from `$table_check`
where (" . implode(" $delimeter ", $conditions) . ") $econdition";

                $arrays[] = $varr;
            }
        }
        for ($x = 0; $x < count($tables); $x++) {
            if (isset($deleted_arr[$x])) {
                for ($y = 0; $y < count($deleted_arr[$x]); $y++) {
                    $queries[] = "delete from `{$tables[$x]}` where `id`=?";
                    $arrays[] = array($deleted_arr[$x][$y]);
                }
            }
        }
        for ($x = 0; $x < count($field_arr); $x++) {
            if (isset($data_arr[$x]) && count($data_arr[$x]) > 0) {
                $current_data = $data_arr[$x];
                $current_fields = $field_arr[$x];


                if (!is_array($current_data[0])) {
                    $fields = array();
                    $fcounter = 0;
                    for ($y = 0; $y < count($current_fields["columns"]); $y++) {
                        if (is_array($current_fields["columns"][$y])) {
                            $query = (isset($current_fields["columns"][$y]["query"])) ? $current_fields["columns"][$y]["query"] : "?";
                            $fields[] = $current_fields["columns"][$y]["col_name"] . "=" . $query;
                            if (isset($current_fields["columns"][$y]["values"])) {
                                array_splice($current_data, $fcounter, 0, $current_fields["columns"][$y]["values"]);
                                $fcounter += count($current_fields["columns"][$y]["values"]);
                            } else {
                                $questions = explode("?", $query);
                                if (count($questions) > 0) {
                                    $fcounter += count($questions);
                                } else {
                                    $fcounter++;
                                }
                            }
                        } else {
                            $fields[] = $current_fields["columns"][$y] . "=?";
                            $fcounter++;
                        }
                    }
                    $query_string = "update `{$current_fields["table_name"]}` set " . implode(",", $fields);
                    $where = $current_fields["where"];
                    $warray = array();
                    foreach ($where["fields"] as $warr) {
                        $warray[] = $warr . "=?";
                    }
                    if (isset($where["values"])) {
                        $current_data = array_merge($current_data, $where["values"]);
                    }
                    $query_string .= " where " . implode("and", $warray);
                    $queries[] = $query_string;
                    $arrays[] = $current_data;
                } else {
                    $update_inserts = "";
                    $inserts = "";
                    $all_values_update = array();
                    $all_values = array();

                    for ($z = 0; $z < count($current_data); $z++) {

                        $current_values = $current_data[$z];
                        $id = $current_values[count($current_values) - 1];
                        $index = -1;
                        if ($id == 0) {
                            $index = 0;
                            $current_values[count($current_values) - 1] = $ref_id;
                        } else {
                            $index = 1;
                        }
                        $col_fields = array();
                        $questions = array();
                        $field_count = 0;
                        foreach ($current_fields["columns"][$index] as $crows) {
                            if (is_array($crows)) {
                                $col_fields[] = $crows["col_name"];
                                if (isset($crows["query"])) {
                                    $questions[] = $crows["query"];
                                }
                                if (isset($crows["values"])) {
                                    $questions[] = "?";
                                    array_splice($current_values, $field_count, 0, $crows["values"]);
                                    $field_count += count($crows["values"]);
                                } else {
                                    $questionsz = explode("?", $crows["query"]);
                                    if (count($questionsz) > 0) {
                                        $field_count += count($questionsz);
                                    } else {
                                        $field_count++;
                                    }
                                }
                            } else {
                                $col_fields[] = $crows;
                                $questions[] = "?";
                                $field_count++;
                            }
                        }

                        if ($id == 0) {

                            if ($inserts == "") {

                                $inserts = "insert into `{$current_fields["table_name"]}` (" . implode(",", $col_fields) . ") values (" . implode(",", $questions) . ")";
                            } else {
                                $inserts .= ",(" . implode(",", $questions) . ")";
                            }
                            $all_values = array_merge($all_values, $current_values);
                        } else {
                            if ($update_inserts == "") {

                                $update_inserts = "insert into `{$current_fields["table_name"]}` (" . implode(",", $col_fields) . ") values (" . implode(",", $questions) . ")";
                            } else {
                                $update_inserts .= ",(" . implode(",", $questions) . ")";
                            }$all_values_update = array_merge($all_values_update, $current_values);
                        }
                    }
                    if (count($all_values) > 0) {
                        $queries[] = $inserts;
                        $arrays[] = $all_values;
                    }
                    if (count($all_values_update) > 0) {
                        $cols = array_search("`id`", $current_fields["columns"][1]);
                        $cfields = $current_fields["columns"][1];
                        unset($cfields[$cols]);
                        $fcols = $cfields;
                        $fvalues = array();
                        foreach ($fcols as $rr) {
                            if (is_array($rr)) {
                                $fvalues[] = $rr["col_name"] . "=values(" . $rr["col_name"] . ")";
                            } else {
                                $fvalues[] = $rr . "=values(" . $rr . ")";
                            }
                        }
                        $update_inserts .= " on duplicate key update " . implode(",", $fvalues);

                        $queries[] = $update_inserts;
                        $arrays[] = $all_values_update;
                    }
                }
            }
        }

        return json_encode(transactStatement($queries, $arrays));
    }

}
