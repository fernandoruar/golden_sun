CREATE DATABASE  IF NOT EXISTS `golden_sun` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `golden_sun`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: golden_sun
-- ------------------------------------------------------
-- Server version	5.7.18-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `adhesive_has_logs`
--

DROP TABLE IF EXISTS `adhesive_has_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adhesive_has_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_adhesive` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `minimum` int(11) NOT NULL,
  `starting_qty` int(11) NOT NULL,
  `old_price` double(11,2) NOT NULL,
  `price` double(11,2) NOT NULL,
  `issued_by` int(11) DEFAULT NULL,
  `issued_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `adhesive_has_logs`
--

LOCK TABLES `adhesive_has_logs` WRITE;
/*!40000 ALTER TABLE `adhesive_has_logs` DISABLE KEYS */;
INSERT INTO `adhesive_has_logs` VALUES (1,7,'Adhesive A','Adhesive A',123,123,123.00,123.00,1,'2018-06-14 08:14:43'),(2,7,'Adhesive A1','Adhesive A',123,123,123.00,123.00,1,'2018-06-14 08:14:47'),(7,4,'Adhesive 1','1',1,1,1.00,1.00,1,'2018-06-19 05:24:11'),(8,4,'Adhesive 1','1',2,1,1.00,1.00,1,'2018-06-19 05:25:38'),(9,5,'Adhesive 2','2',2,2,2.00,2.00,1,'2018-06-19 06:35:32'),(10,7,'Adhesive A','Adhesive A',123,123,123.00,123.00,1,'2018-06-19 06:35:57'),(11,10,'Adhesive A','Adhesive A',123,12,123.00,123.00,1,'2018-06-19 06:36:08'),(12,6,'Adhesive A','Type A',123,123,123.00,123.00,1,'2018-06-19 06:36:26'),(13,8,'Adhesive B','123',123,123,123.00,123.00,1,'2018-06-19 06:36:45');
/*!40000 ALTER TABLE `adhesive_has_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `adhesives`
--

DROP TABLE IF EXISTS `adhesives`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adhesives` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `minimum` int(11) NOT NULL,
  `starting_qty` int(11) NOT NULL,
  `price` double(11,2) NOT NULL,
  `issued_by` int(11) DEFAULT NULL,
  `issued_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` int(11) DEFAULT NULL,
  `modified_on` timestamp NULL DEFAULT NULL,
  `conflicts` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `adhesives`
--

LOCK TABLES `adhesives` WRITE;
/*!40000 ALTER TABLE `adhesives` DISABLE KEYS */;
INSERT INTO `adhesives` VALUES (4,'Adhesive 1','1',3,1,1.00,0,'2018-05-18 09:49:41',1,'2018-06-19 05:25:38',4),(5,'Adhesive 2','2',2,4,2.00,1,'2018-05-18 09:49:50',1,'2018-06-19 06:35:32',2),(6,'Adhesive A','Type A',1231,1231,123.00,1,'2018-06-14 08:14:06',1,'2018-06-19 06:36:26',0),(7,'Adhesive C','Adhesive C',111,122,123.00,1,'2018-06-14 08:14:25',1,'2018-06-19 06:35:57',0),(8,'Adhesive B','123',1,121,123.00,0,'2018-06-14 08:16:02',1,'2018-06-19 06:36:45',1),(10,'Adhesive D','Adhesive D',123,12,123.00,1,'2018-06-18 04:50:24',1,'2018-06-19 06:36:08',0);
/*!40000 ALTER TABLE `adhesives` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`adhesives_AFTER_INSERT` AFTER INSERT ON `adhesives` FOR EACH ROW
BEGIN
	INSERT INTO `in_adhesives`(`id_adhesive`,`qty`) VALUES(new.`id`,new.`starting_qty`);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`adhesives_AFTER_UPDATE` AFTER UPDATE ON `adhesives` FOR EACH ROW
BEGIN


	if(new.`name`!=old.`name` OR new.`type`!=old.`type` OR new.`minimum`!=old.`minimum` OR new.`starting_qty`!=old.`starting_qty` OR new.`price`!=old.`price`) then 
	insert into `adhesive_has_logs`  
	(`issued_on`,`issued_by`,`id_adhesive`,`name`,`type`,`minimum`,`starting_qty`,`old_price`,`price`)
	values 
	(new.`modified_on`,new.`modified_by`,new.`id`,old.`name`,old.`type`,old.`minimum`,old.`starting_qty`,old.`price`,new.`price`);

		if((SELECT count(*) FROM `in_adhesives` WHERE `id_adhesive`=new.`id`)=0)then
			INSERT INTO `in_adhesives`(`id_adhesive`,`qty`,`issued_by`) VALUES(new.`id`,new.`starting_qty`,new.`modified_by`);
		else
			UPDATE `in_adhesives` SET `qty`= (`qty`-old.`starting_qty`+new.`starting_qty`), `issued_by`=new.`modified_by` WHERE `id_adhesive`=new.`id`;
		end if;
	end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`adhesives_BEFORE_DELETE` BEFORE DELETE ON `adhesives` FOR EACH ROW
BEGIN
	DELETE FROM `in_adhesives` WHERE `id_adhesive`=old.`id`;
	DELETE FROM `in_adhesive_has_logs` WHERE `id_adhesive`=old.`id`;
	DELETE FROM `adhesive_has_logs` WHERE `id_adhesive`=old.`id`;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `chemical_has_logs`
--

DROP TABLE IF EXISTS `chemical_has_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chemical_has_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_chemical` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `minimum` int(11) NOT NULL,
  `starting_qty` int(11) NOT NULL,
  `old_price` double(11,2) NOT NULL,
  `price` double(11,2) NOT NULL,
  `issued_by` int(11) DEFAULT NULL,
  `issued_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chemical_has_logs`
--

LOCK TABLES `chemical_has_logs` WRITE;
/*!40000 ALTER TABLE `chemical_has_logs` DISABLE KEYS */;
INSERT INTO `chemical_has_logs` VALUES (1,4,'My Chemical 1','asd',12312,123123,12313.00,12313.00,1,'2018-06-14 08:16:47'),(2,4,'My Chemical 12','asd',12312,123123,12313.00,12313.00,1,'2018-06-14 08:16:52'),(3,4,'My Chemical 1','asd',12312,123123,12313.00,12313.00,1,'2018-06-18 04:50:07'),(4,1,'Chemical 1','1',11,1,1.00,1.00,1,'2018-06-20 05:05:10'),(5,2,'Chemical 2','2',2,2,2.00,2.00,1,'2018-06-20 05:05:14'),(6,3,'My Chemical 1','asd',123,123,123.00,123.00,1,'2018-06-20 05:05:32'),(7,4,'My Chemical 2','asd',12312,123123,12313.00,12313.00,1,'2018-06-20 05:05:44'),(8,5,'My Chemical 2','123',123,123,123.00,123.00,1,'2018-06-20 05:06:02'),(9,8,'My Chemical 1','123',123,123,123.00,123.00,1,'2018-06-20 05:06:09');
/*!40000 ALTER TABLE `chemical_has_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chemicals`
--

DROP TABLE IF EXISTS `chemicals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chemicals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `minimum` int(11) NOT NULL,
  `starting_qty` int(11) NOT NULL,
  `price` double(11,2) NOT NULL,
  `issued_by` int(11) DEFAULT NULL,
  `issued_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` int(11) DEFAULT NULL,
  `modified_on` timestamp NULL DEFAULT NULL,
  `conflicts` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chemicals`
--

LOCK TABLES `chemicals` WRITE;
/*!40000 ALTER TABLE `chemicals` DISABLE KEYS */;
INSERT INTO `chemicals` VALUES (1,'Chemical 1','1',11,11,1.00,0,'2018-05-18 09:49:16',1,'2018-06-20 05:05:10',5),(2,'Chemical 2','2',2,22,2.00,1,'2018-05-18 09:49:23',1,'2018-06-20 05:05:14',3),(3,'My Chemical 3','asd',123,12,123.00,1,'2018-06-14 08:16:28',1,'2018-06-20 05:05:32',0),(4,'My Chemical 4','asd',12312,15,12313.00,1,'2018-06-14 08:16:43',1,'2018-06-20 05:05:49',0),(5,'My Chemical 2','123',123,10,123.00,0,'2018-06-14 08:17:37',1,'2018-06-20 05:06:02',0),(8,'My Chemical 1','123',123,5,123.00,1,'2018-06-18 05:06:20',1,'2018-06-20 05:06:09',0);
/*!40000 ALTER TABLE `chemicals` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`chemicals_AFTER_INSERT` AFTER INSERT ON `chemicals` FOR EACH ROW
BEGIN

	INSERT INTO `in_chemicals`(`id_chemical`,`qty`) VALUES(new.`id`,new.`starting_qty`);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`chemicals_AFTER_UPDATE` AFTER UPDATE ON `chemicals` FOR EACH ROW
BEGIN


	if(new.`name`!=old.`name` OR new.`type`!=old.`type` OR new.`minimum`!=old.`minimum` OR new.`starting_qty`!=old.`starting_qty` OR new.`price`!=old.`price`) then 
	insert into `chemical_has_logs`  
	(`issued_on`,`issued_by`,`id_chemical`,`name`,`type`,`minimum`,`starting_qty`,`old_price`,`price`)
	values 
	(new.`modified_on`,new.`modified_by`,new.`id`,old.`name`,old.`type`,old.`minimum`,old.`starting_qty`,old.`price`,new.`price`);

		if((SELECT count(*) FROM `in_chemicals` WHERE `id_chemical`=new.`id`)=0)then
			INSERT INTO `in_chemicals`(`id_chemical`,`qty`,`issued_by`) VALUES(new.`id`,new.`starting_qty`,new.`modified_by`);
		else
			UPDATE `in_chemicals` SET `qty`= (`qty`-old.`starting_qty`+new.`starting_qty`), `issued_by`=new.`modified_by` WHERE `id_chemical`=new.`id`;
		end if;
	end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`chemicals_BEFORE_DELETE` BEFORE DELETE ON `chemicals` FOR EACH ROW
BEGIN

	DELETE FROM `in_chemicals` WHERE `id_chemical`=old.`id`;
	DELETE FROM `in_chemical_has_logs` WHERE `id_chemical`=old.`id`;
	DELETE FROM `chemical_has_logs` WHERE `id_chemical`=old.`id`;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `contact_type_has_logs`
--

DROP TABLE IF EXISTS `contact_type_has_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact_type_has_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `issued_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `issued_by` int(11) DEFAULT NULL,
  `id_contact_type` int(11) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact_type_has_logs`
--

LOCK TABLES `contact_type_has_logs` WRITE;
/*!40000 ALTER TABLE `contact_type_has_logs` DISABLE KEYS */;
INSERT INTO `contact_type_has_logs` VALUES (2,'2018-05-17 09:52:35',0,5,'Address'),(3,'2018-05-17 09:52:48',0,5,'Addresss'),(4,'2018-05-17 09:53:03',0,5,'Addressssss');
/*!40000 ALTER TABLE `contact_type_has_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contact_types`
--

DROP TABLE IF EXISTS `contact_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `issued_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `issued_by` int(11) DEFAULT NULL,
  `modified_on` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `conflicts` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact_types`
--

LOCK TABLES `contact_types` WRITE;
/*!40000 ALTER TABLE `contact_types` DISABLE KEYS */;
INSERT INTO `contact_types` VALUES (5,'Address','2018-05-17 07:40:13',0,'2018-05-17 17:53:08',0,0),(6,'E-Mail','2018-05-17 07:40:40',0,'0000-00-00 00:00:00',NULL,0),(7,'Mobile','2018-06-14 07:32:27',0,'0000-00-00 00:00:00',NULL,0),(8,'Email Address','2018-06-14 07:32:53',0,'0000-00-00 00:00:00',NULL,0),(9,'Fax','2018-06-14 07:32:53',0,'0000-00-00 00:00:00',NULL,0);
/*!40000 ALTER TABLE `contact_types` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`contact_types_AFTER_UPDATE` AFTER UPDATE ON `contact_types` FOR EACH ROW
BEGIN

if(new.`name`!=old.`name`) then 
insert into `contact_type_has_logs`  
(`issued_on`,`issued_by`,`id_contact_type`,`name`)
values 
(new.`modified_on`,new.`modified_by`,new.`id`,old.`name`);

end if;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`contact_types_AFTER_DELETE` AFTER DELETE ON `contact_types` FOR EACH ROW
BEGIN
	DELETE FROM `contact_type_has_logs` WHERE `id_contact_type`=old.`id`;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `currency_has_logs`
--

DROP TABLE IF EXISTS `currency_has_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `currency_has_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `issued_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `issued_by` int(11) DEFAULT NULL,
  `id_currency` int(11) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `symbol` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `currency_has_logs`
--

LOCK TABLES `currency_has_logs` WRITE;
/*!40000 ALTER TABLE `currency_has_logs` DISABLE KEYS */;
INSERT INTO `currency_has_logs` VALUES (4,'2018-05-17 07:56:36',0,8,'Philippine Peso','₱'),(5,'2018-05-17 07:56:57',0,8,'pHp','PhP');
/*!40000 ALTER TABLE `currency_has_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `currencys`
--

DROP TABLE IF EXISTS `currencys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `currencys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `symbol` varchar(45) NOT NULL,
  `issued_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `issued_by` int(11) DEFAULT NULL,
  `modified_on` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `conflicts` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `currencys`
--

LOCK TABLES `currencys` WRITE;
/*!40000 ALTER TABLE `currencys` DISABLE KEYS */;
INSERT INTO `currencys` VALUES (8,'Philippine Peso','₱','2018-05-17 07:56:20',0,'2018-05-17 15:56:57',0,2),(9,'US Dollar','$','2018-05-18 07:42:41',0,'0000-00-00 00:00:00',NULL,3),(10,'Euro','€','2018-05-18 07:42:41',0,'0000-00-00 00:00:00',NULL,0),(11,'Pound','£','2018-05-18 07:42:41',0,'0000-00-00 00:00:00',NULL,0),(13,'Hongkong Dollar','HKD','2018-06-14 07:31:49',0,'0000-00-00 00:00:00',NULL,0),(14,'Won','KRW','2018-06-14 07:31:49',0,'0000-00-00 00:00:00',NULL,0);
/*!40000 ALTER TABLE `currencys` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`currencys_AFTER_UPDATE` AFTER UPDATE ON `currencys` FOR EACH ROW
BEGIN


if(new.`name`!=old.`name` OR new.`symbol`!=old.`symbol`) then 
insert into `currency_has_logs`  
(`issued_on`,`issued_by`,`id_currency`,`name`,`symbol`)
values 
(new.`modified_on`,new.`modified_by`,new.`id`,old.`name`,old.`symbol`);

end if;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`currencys_AFTER_DELETE` AFTER DELETE ON `currencys` FOR EACH ROW
BEGIN
	DELETE FROM `currency_has_logs` WHERE `id_currency`=old.`id`;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `customer_has_logs`
--

DROP TABLE IF EXISTS `customer_has_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_has_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_customer` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `contact_person` varchar(100) NOT NULL,
  `address` longtext NOT NULL,
  `balance` double(11,2) NOT NULL DEFAULT '0.00',
  `issued_by` int(11) DEFAULT NULL,
  `issued_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` int(11) DEFAULT NULL,
  `modified_on` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_has_logs`
--

LOCK TABLES `customer_has_logs` WRITE;
/*!40000 ALTER TABLE `customer_has_logs` DISABLE KEYS */;
INSERT INTO `customer_has_logs` VALUES (1,1,'1','2','3',4.00,1,'2018-05-18 05:16:55',NULL,NULL),(2,6,'2','2','2',2.00,1,'2018-05-18 05:17:14',NULL,NULL),(3,5,'3','2','3',4.00,1,'2018-05-18 05:17:35',NULL,NULL),(6,9,'Customer JC3','JC','Quezon',4500.00,1,'2018-06-14 07:28:11',NULL,NULL);
/*!40000 ALTER TABLE `customer_has_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customerpo_has_logs`
--

DROP TABLE IF EXISTS `customerpo_has_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customerpo_has_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_customerpo` int(11) NOT NULL,
  `date` date NOT NULL,
  `id_customer` int(11) NOT NULL,
  `po_number` varchar(100) NOT NULL,
  `issued_by` int(11) DEFAULT NULL,
  `issued_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customerpo_has_logs`
--

LOCK TABLES `customerpo_has_logs` WRITE;
/*!40000 ALTER TABLE `customerpo_has_logs` DISABLE KEYS */;
INSERT INTO `customerpo_has_logs` VALUES (1,7,'2018-05-31',6,'23',1,'2018-06-04 09:05:57'),(5,6,'2018-05-31',1,'1',1,'2018-06-14 09:46:37'),(8,6,'2018-05-31',8,'1',1,'2018-06-14 09:52:02'),(9,6,'2018-05-31',7,'1',1,'2018-06-14 09:52:42'),(10,6,'2018-05-31',8,'1',1,'2018-06-18 03:45:16'),(11,6,'2018-05-31',7,'1',1,'2018-06-18 03:45:25'),(12,6,'2018-05-31',9,'1',1,'2018-06-18 03:46:07'),(13,6,'2018-05-31',9,'2',1,'2018-06-18 03:48:42'),(14,6,'2018-06-01',9,'3',1,'2018-06-18 03:50:49'),(15,6,'2018-06-01',1,'111',1,'2018-06-18 03:52:55');
/*!40000 ALTER TABLE `customerpo_has_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customerpo_has_products`
--

DROP TABLE IF EXISTS `customerpo_has_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customerpo_has_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_customerpo` int(11) DEFAULT NULL,
  `id_product` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `price` double(11,2) DEFAULT '0.00',
  `status` int(11) DEFAULT '0',
  `issued_by` int(11) DEFAULT NULL,
  `issued_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` int(11) DEFAULT NULL,
  `modified_on` timestamp NULL DEFAULT NULL,
  `conflicts` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customerpo_has_products`
--

LOCK TABLES `customerpo_has_products` WRITE;
/*!40000 ALTER TABLE `customerpo_has_products` DISABLE KEYS */;
INSERT INTO `customerpo_has_products` VALUES (33,7,1,555,44.00,0,1,'2018-05-31 07:22:26',1,'2018-06-18 04:16:20',0),(34,7,3,200,24.00,0,1,'2018-05-31 07:22:26',1,'2018-05-31 07:22:27',0),(52,6,3,250,2.00,0,1,'2018-06-04 09:00:35',1,'2018-06-18 04:14:58',0),(56,6,2,42,51.00,0,1,'2018-06-04 09:26:01',1,'2018-06-04 09:26:01',0),(59,9,2,5,10.00,0,1,'2018-06-14 08:20:47',1,'2018-06-14 08:20:54',0),(62,12,2,1,100.00,0,1,'2018-06-18 04:55:02',1,'2018-06-18 04:55:03',0);
/*!40000 ALTER TABLE `customerpo_has_products` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`customerpo_has_products_AFTER_INSERT` AFTER INSERT ON `customerpo_has_products` FOR EACH ROW
BEGIN
        
		UPDATE `products` set `conflicts`=`conflicts`+1 WHERE `id`=new.`id_product`;
        
        INSERT INTO `joborders` (
			`po_number`,
            `date`,
            `id_customer`,
            `id_product`,
            `id_customerpo_product`, 
            `po_qty`, 
            `issued_by`
        ) 
        VALUES(
			(SELECT `po_number` FROM `customerpos`  WHERE `id`=new.`id_customerpo`),
            (SELECT `date` FROM `customerpos` WHERE `id`=new.`id_customerpo`),
            (SELECT `id_customer` FROM `customerpos`  WHERE `id`=new.`id_customerpo`),
            new.`id_product`,
            new.`id`,
            new.`qty`,
            new.`issued_by`
		);

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`customerpo_has_products_AFTER_UPDATE` AFTER UPDATE ON `customerpo_has_products` FOR EACH ROW
BEGIN

        
	if(new.`id_product`!=old.`id_product` OR new.`qty`!=old.`qty` OR new.`price`!=old.`price`) then 
		insert into `customerpo_product_has_logs`  
		(`issued_on`,`issued_by`,`id_customerpo_product`,`id_product`,`qty`,`price`)
		values 
		(new.`modified_on`,new.`modified_by`,new.`id`,old.`id_product`,old.`qty`,old.`price`);
        
        
		UPDATE `joborders` SET `id_product` = new.`id_product`, `po_qty`=new.`qty`, `issued_by`=new.`modified_by`  WHERE `id_customerpo_product`=new.`id`;
	end if;
    
    
	if(new.`id_product`!=old.`id_product`) then
		UPDATE `products` set `conflicts`=`conflicts`+1 WHERE `id`=new.`id_product`;
		UPDATE `products` set `conflicts`=`conflicts`-1 WHERE `id`=old.`id_product`;
    end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`customerpo_has_products_BEFORE_DELETE` BEFORE DELETE ON `customerpo_has_products` FOR EACH ROW
BEGIN

		DELETE FROM `joborders` WHERE `id_customerpo_product`=old.`id`;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`customerpo_has_products_AFTER_DELETE` AFTER DELETE ON `customerpo_has_products` FOR EACH ROW
BEGIN

        
		DELETE FROM `customerpo_product_has_logs` WHERE `id_customerpo_product`=old.`id`;
        
		UPDATE `products` set `conflicts`=`conflicts`-1 WHERE `id`=old.`id_product`;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `customerpo_product_has_logs`
--

DROP TABLE IF EXISTS `customerpo_product_has_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customerpo_product_has_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_customerpo_product` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `price` double(11,2) NOT NULL DEFAULT '0.00',
  `issued_by` int(11) DEFAULT NULL,
  `issued_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customerpo_product_has_logs`
--

LOCK TABLES `customerpo_product_has_logs` WRITE;
/*!40000 ALTER TABLE `customerpo_product_has_logs` DISABLE KEYS */;
INSERT INTO `customerpo_product_has_logs` VALUES (1,52,3,42,52.00,1,'2018-06-04 09:21:06'),(2,52,3,23,41.00,1,'2018-06-04 09:21:16'),(3,52,3,200,100.00,1,'2018-06-04 09:22:02'),(4,59,2,5,0.00,1,'2018-06-14 08:20:54'),(11,52,3,100,100.00,1,'2018-06-18 04:11:10'),(12,52,3,220,100.00,1,'2018-06-18 04:14:58'),(13,33,1,100,23.00,1,'2018-06-18 04:15:43'),(14,33,1,1233,23.00,1,'2018-06-18 04:16:20');
/*!40000 ALTER TABLE `customerpo_product_has_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customerpos`
--

DROP TABLE IF EXISTS `customerpos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customerpos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `id_customer` int(11) NOT NULL,
  `po_number` varchar(100) NOT NULL,
  `issued_by` int(11) DEFAULT NULL,
  `issued_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` int(11) DEFAULT NULL,
  `modified_on` timestamp NULL DEFAULT NULL,
  `conflicts` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customerpos`
--

LOCK TABLES `customerpos` WRITE;
/*!40000 ALTER TABLE `customerpos` DISABLE KEYS */;
INSERT INTO `customerpos` VALUES (6,'2018-06-12',5,'12333',1,'2018-05-31 07:21:07',1,'2018-06-18 03:52:55',0,0),(7,'2018-05-31',6,'230',1,'2018-05-31 07:22:26',1,'2018-06-18 04:16:20',0,0),(9,'2018-06-15',1,'123456789',1,'2018-06-14 08:20:47',1,'2018-06-14 08:20:54',0,0),(10,'2018-06-14',5,'21143432',1,'2018-06-14 08:23:41',1,'2018-06-14 08:23:42',0,0),(12,'2018-06-18',1,'123abc',1,'2018-06-18 04:55:02',1,'2018-06-18 04:55:03',0,0);
/*!40000 ALTER TABLE `customerpos` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`customerpos_AFTER_INSERT` AFTER INSERT ON `customerpos` FOR EACH ROW
BEGIN
    UPDATE `customers` set `conflicts`=`conflicts`+1 WHERE `id`=new.`id_customer`;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`customerpos_AFTER_UPDATE` AFTER UPDATE ON `customerpos` FOR EACH ROW
BEGIN


	if(new.`date`!=old.`date` OR new.`id_customer`!=old.`id_customer` OR new.`po_number`!=old.`po_number`) then 
		insert into `customerpo_has_logs`  
		(`issued_on`,`issued_by`,`id_customerpo`,`date`,`id_customer`,`po_number`)
		values 
		(new.`modified_on`,new.`modified_by`,new.`id`,old.`date`,old.`id_customer`,old.`po_number`);
        
        UPDATE `joborders` SET `po_number`=new.`po_number`,`id_customer`=new.`id_customer` WHERE `po_number`=old.`po_number`;
	end if;
    
    

	if(new.`id_customer`!=old.`id_customer`) then
		UPDATE `customers` set `conflicts`=`conflicts`+1 WHERE `id`=new.`id_customer`;
		UPDATE `customers` set `conflicts`=`conflicts`-1 WHERE `id`=old.`id_customer`;
    end if;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`customerpos_AFTER_DELETE` AFTER DELETE ON `customerpos` FOR EACH ROW
BEGIN


	DELETE FROM `customerpo_has_logs` WHERE `id_customerpo`=old.`id`;
	DELETE FROM `customerpo_has_products` WHERE `id_customerpo`=old.`id`;
    
    UPDATE `customers` set `conflicts`=`conflicts`-1 WHERE `id`=old.`id_customer`;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `contact_person` varchar(100) NOT NULL,
  `address` longtext NOT NULL,
  `balance` double(11,2) NOT NULL DEFAULT '0.00',
  `issued_by` int(11) DEFAULT NULL,
  `issued_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` int(11) DEFAULT NULL,
  `modified_on` timestamp NULL DEFAULT NULL,
  `conflicts` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` VALUES (1,'Customer 1','Customer Person','Customer Address',4000.00,1,'2018-05-18 05:04:10',1,'2018-06-14 09:04:42',5),(5,'Customer 3','Customer Person 3','Customer Address 3',700000.00,0,'2018-05-18 05:13:02',1,'2018-05-18 05:17:35',4),(6,'Customer 2','Customer Person 2','Customer  Address 2',20000.00,0,'2018-05-18 05:13:02',1,'2018-05-18 05:17:14',8),(7,'Customer JC1','JC','Dasma Cavite',0.00,1,'2018-06-14 07:23:35',1,'2018-06-14 07:23:36',0),(8,'Customer JC2','JC','Manila',50000.00,0,'2018-06-14 07:24:22',NULL,NULL,0),(9,'Customer JC2','JC','Manila',50000.00,0,'2018-06-14 07:24:22',1,'2018-06-14 07:55:55',0),(10,'Customer A','A','A',5200.00,1,'2018-06-18 04:36:44',1,'2018-06-18 04:37:06',0);
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`customers_AFTER_UPDATE` AFTER UPDATE ON `customers` FOR EACH ROW
BEGIN


if(new.`name`!=old.`name` OR new.`contact_person`!=old.`contact_person` OR new.`address`!=old.`address` OR new.`balance`!=old.`balance`) then 
insert into `customer_has_logs`  
(`issued_on`,`issued_by`,`id_customer`,`name`,`contact_person`,`address`,`balance`)
values 
(new.`modified_on`,new.`modified_by`,new.`id`,old.`name`,old.`contact_person`,old.`address`,old.`balance`);

end if;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`customers_AFTER_DELETE` AFTER DELETE ON `customers` FOR EACH ROW
BEGIN
	DELETE FROM `customer_has_logs` WHERE `id_customer`=old.`id`;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `cylinder_has_logs`
--

DROP TABLE IF EXISTS `cylinder_has_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cylinder_has_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_cylinder` int(11) NOT NULL,
  `color` varchar(100) NOT NULL,
  `size` varchar(100) NOT NULL,
  `minimum` int(11) NOT NULL,
  `starting_qty` int(11) NOT NULL,
  `old_price` double(11,2) NOT NULL,
  `price` double(11,2) NOT NULL,
  `issued_by` int(11) DEFAULT NULL,
  `issued_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cylinder_has_logs`
--

LOCK TABLES `cylinder_has_logs` WRITE;
/*!40000 ALTER TABLE `cylinder_has_logs` DISABLE KEYS */;
INSERT INTO `cylinder_has_logs` VALUES (3,3,'Cylinder 2','2',2,2,2.00,2.00,1,'2018-06-18 04:49:32'),(4,3,'123','Cylinder 123',2,2,2.00,2.00,1,'2018-06-20 05:14:03'),(5,4,'123','Cylinder 1',123,123,23.00,23.00,1,'2018-06-20 05:14:09'),(6,9,'123','Cylinder 123',123,123,123.00,123.00,1,'2018-06-20 05:14:17'),(7,11,'123','Cylinder 123',123,123,123.00,123.00,1,'2018-06-20 05:14:24'),(8,9,'4','Cylinder 123',4,123,123.00,123.00,1,'2018-06-20 05:14:30'),(9,12,'1234','Cylinder 123',123,123,123.00,123.00,1,'2018-06-20 05:14:37'),(10,2,'Cylinder 1','1',1,1,1.00,1.00,1,'2018-06-20 05:14:45'),(11,8,'Cylinder 21','2',2,2,2.00,2.00,1,'2018-06-20 05:14:52'),(12,7,'Cylinder 3','3',3,1000,1000.00,1000.00,1,'2018-06-20 05:14:57');
/*!40000 ALTER TABLE `cylinder_has_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cylinders`
--

DROP TABLE IF EXISTS `cylinders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cylinders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `color` varchar(100) NOT NULL,
  `size` varchar(100) NOT NULL,
  `minimum` int(11) NOT NULL,
  `starting_qty` int(11) NOT NULL,
  `price` double(11,2) NOT NULL,
  `issued_by` int(11) DEFAULT NULL,
  `issued_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` int(11) DEFAULT NULL,
  `modified_on` timestamp NULL DEFAULT NULL,
  `conflicts` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cylinders`
--

LOCK TABLES `cylinders` WRITE;
/*!40000 ALTER TABLE `cylinders` DISABLE KEYS */;
INSERT INTO `cylinders` VALUES (2,'Cylinder 1','1',1,7,1.00,1,'2018-05-18 10:00:02',1,'2018-06-20 05:14:45',1),(3,'1','Cylinder 123',2,22,2.00,1,'2018-05-18 10:00:17',1,'2018-06-20 05:14:03',1),(4,'2','Cylinder 1',123,3,23.00,1,'2018-06-14 08:18:14',1,'2018-06-20 05:14:09',1),(7,'Cylinder 3','3',3,9,1000.00,0,'2018-06-18 04:49:12',1,'2018-06-20 05:14:57',0),(8,'Cylinder 21','2',2,8,2.00,0,'2018-06-18 04:49:12',1,'2018-06-20 05:14:52',0),(9,'4','Cylinder 123',4,4,123.00,1,'2018-06-18 04:49:14',1,'2018-06-20 05:14:30',0),(11,'5','Cylinder 123',5,5,123.00,1,'2018-06-18 05:06:41',1,'2018-06-20 05:14:24',0),(12,'6','Cylinder 123',123,6,123.00,1,'2018-06-18 05:22:21',1,'2018-06-20 05:14:37',0);
/*!40000 ALTER TABLE `cylinders` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`cylinders_AFTER_INSERT` AFTER INSERT ON `cylinders` FOR EACH ROW
BEGIN

	INSERT INTO `in_cylinders`(`id_cylinder`,`qty`) VALUES(new.`id`,new.`starting_qty`);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`cylinders_AFTER_UPDATE` AFTER UPDATE ON `cylinders` FOR EACH ROW
BEGIN

	if(new.`color`!=old.`color` OR new.`size`!=old.`size` OR new.`minimum`!=old.`minimum` OR new.`starting_qty`!=old.`starting_qty` OR new.`price`!=old.`price`) then 
	insert into `cylinder_has_logs`  
	(`issued_on`,`issued_by`,`id_cylinder`,`color`,`size`,`minimum`,`starting_qty`,`old_price`,`price`)
	values 
	(new.`modified_on`,new.`modified_by`,new.`id`,old.`color`,old.`size`,old.`minimum`,old.`starting_qty`,old.`price`,new.`price`);

		if((SELECT count(*) FROM `in_cylinders` WHERE `id_cylinder`=new.`id`)=0)then
			INSERT INTO `in_cylinders`(`id_cylinder`,`qty`,`issued_by`) VALUES(new.`id`,new.`starting_qty`,new.`modified_by`);
		else
			UPDATE `in_cylinders` SET `qty`= (`qty`-old.`starting_qty`+new.`starting_qty`), `issued_by`=new.`modified_by` WHERE `id_cylinder`=new.`id`;
		end if;
	end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`cylinders_BEFORE_DELETE` BEFORE DELETE ON `cylinders` FOR EACH ROW
BEGIN

	DELETE FROM `in_cylinders` WHERE `id_cylinder`=old.`id`;
	DELETE FROM `in_cylinder_has_logs` WHERE `id_cylinder`=old.`id`;
	DELETE FROM `cylinder_has_logs` WHERE `id_cylinder`=old.`id`;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `department_has_logs`
--

DROP TABLE IF EXISTS `department_has_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `department_has_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `issued_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `issued_by` int(11) DEFAULT NULL,
  `id_department` int(11) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `department_has_logs`
--

LOCK TABLES `department_has_logs` WRITE;
/*!40000 ALTER TABLE `department_has_logs` DISABLE KEYS */;
INSERT INTO `department_has_logs` VALUES (1,'2018-06-14 07:59:05',0,3,'D1'),(2,'2018-06-14 07:59:13',0,3,'D12');
/*!40000 ALTER TABLE `department_has_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `departments`
--

DROP TABLE IF EXISTS `departments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `departments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `issued_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `issued_by` int(11) DEFAULT NULL,
  `modified_on` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `conflicts` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departments`
--

LOCK TABLES `departments` WRITE;
/*!40000 ALTER TABLE `departments` DISABLE KEYS */;
INSERT INTO `departments` VALUES (3,'D1','2018-05-17 07:40:24',0,'2018-06-14 15:59:13',0,0),(6,'Admin','2018-05-17 09:35:26',0,'0000-00-00 00:00:00',NULL,1),(7,'QA Dept','2018-06-14 07:34:09',0,'0000-00-00 00:00:00',NULL,0),(8,'HR Dept','2018-06-14 07:34:38',0,'0000-00-00 00:00:00',NULL,1),(9,'IT Dept','2018-06-14 07:34:38',0,'0000-00-00 00:00:00',NULL,0),(11,'Dev Dept','2018-06-14 08:38:46',0,'0000-00-00 00:00:00',NULL,0);
/*!40000 ALTER TABLE `departments` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`departments_AFTER_UPDATE` AFTER UPDATE ON `departments` FOR EACH ROW
BEGIN

if(new.`name`!=old.`name`) then 
insert into `department_has_logs`  
(`issued_on`,`issued_by`,`id_department`,`name`)
values 
(new.`modified_on`,new.`modified_by`,new.`id`,old.`name`);

end if;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`departments_AFTER_DELETE` AFTER DELETE ON `departments` FOR EACH ROW
BEGIN

	DELETE FROM `department_has_logs` WHERE `id_department`=old.`id`;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `employee_has_logs`
--

DROP TABLE IF EXISTS `employee_has_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employee_has_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `issued_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `issued_by` int(11) DEFAULT NULL,
  `id_employee` int(11) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `id_department` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee_has_logs`
--

LOCK TABLES `employee_has_logs` WRITE;
/*!40000 ALTER TABLE `employee_has_logs` DISABLE KEYS */;
INSERT INTO `employee_has_logs` VALUES (4,'2018-05-17 09:47:26',1,0,'ADMIN','admin','1234',0),(5,'2018-05-17 09:50:06',1,0,'ADMIN','admin','1234',6),(12,'2018-06-18 04:41:45',1,22,'Employee A','aaa','aaa',6);
/*!40000 ALTER TABLE `employee_has_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employees`
--

DROP TABLE IF EXISTS `employees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `id_department` int(11) NOT NULL,
  `issued_by` int(11) DEFAULT NULL,
  `issued_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` int(11) DEFAULT NULL,
  `modified_on` timestamp NULL DEFAULT NULL,
  `conflicts` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employees`
--

LOCK TABLES `employees` WRITE;
/*!40000 ALTER TABLE `employees` DISABLE KEYS */;
INSERT INTO `employees` VALUES (0,'ADMIN','admin','1234',6,1,'2018-04-25 05:37:12',1,'2018-05-17 09:50:06',0),(21,'Employee JC1','Employee JC1','1234',1,1,'2018-06-14 08:43:21',1,'2018-06-14 08:43:22',0),(22,'Employee A','aaa','aaa',8,1,'2018-06-18 04:39:40',1,'2018-06-18 04:41:45',0);
/*!40000 ALTER TABLE `employees` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`employees_conflict_AFTER_INSERT` AFTER INSERT ON `employees` FOR EACH ROW
BEGIN
	UPDATE `departments` set `conflicts`=`conflicts`+1 WHERE `id`=new.`id_department`;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`employees_AFTER_UPDATE` AFTER UPDATE ON `employees` FOR EACH ROW
BEGIN

if(new.`name`!=old.`name` OR new.`username`!=old.`username` OR new.`password`!=old.`password` OR new.`id_department`!=old.`id_department`) then 
insert into `employee_has_logs`  
(`issued_on`,`issued_by`,`id_employee`,`name`,`username`,`password`,`id_department`)
values 
(new.`modified_on`,new.`modified_by`,new.`id`,old.`name`,old.`username`,old.`password`,old.`id_department`);

end if;


/*Conflicts*/
    if(old.`id_department`!=new.`id_department`) then
		UPDATE `departments` set `conflicts`=`conflicts`-1 WHERE `id`=old.`id_department`;
		UPDATE `departments` set `conflicts`=`conflicts`+1 WHERE `id`=new.`id_department`;
	end if;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`employees_conflict_AFTER_DELETE` AFTER DELETE ON `employees` FOR EACH ROW
BEGIN
	DELETE FROM `employee_has_logs` WHERE `id_employee`=old.`id`;
	UPDATE `departments` set `conflicts`=`conflicts`-1 WHERE `id`=old.`id_department`;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `in_adhesive_has_logs`
--

DROP TABLE IF EXISTS `in_adhesive_has_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `in_adhesive_has_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_adhesive` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `initial_qty` int(11) DEFAULT NULL,
  `remaining_qty` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `issued_by` int(11) DEFAULT NULL,
  `issued_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `conflicts` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `in_adhesive_has_logs`
--

LOCK TABLES `in_adhesive_has_logs` WRITE;
/*!40000 ALTER TABLE `in_adhesive_has_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `in_adhesive_has_logs` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`in_adhesive_has_logs_AFTER_INSERT` AFTER INSERT ON `in_adhesive_has_logs` FOR EACH ROW
BEGIN
	if(new.`type`=0) then
		UPDATE `in_adhesives` SET `qty`= `qty` + new.`qty`, `modified_by`=new.`issued_by`, `modified_on`=new.`issued_on` WHERE `id_adhesive`=new.`id_adhesive`;
	elseif(new.`type`=1)then
		UPDATE `in_adhesives` SET `qty`= `qty` - new.`qty`, `modified_by`=new.`issued_by`, `modified_on`=new.`issued_on` WHERE `id_adhesive`=new.`id_adhesive`;
    end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`in_adhesive_has_logs_BEFORE_DELETE` BEFORE DELETE ON `in_adhesive_has_logs` FOR EACH ROW
BEGIN

	if(old.`type`=0) then
		UPDATE `in_adhesives` SET `qty`= `qty` - old.`qty` WHERE `id_adhesive`=old.`id_adhesive`;
	elseif(old.`type`=1)then
		UPDATE `in_adhesives` SET `qty`= `qty` + old.`qty` WHERE `id_adhesive`=old.`id_adhesive`;
    end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `in_adhesives`
--

DROP TABLE IF EXISTS `in_adhesives`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `in_adhesives` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_adhesive` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `issued_by` int(11) DEFAULT NULL,
  `issued_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` int(11) DEFAULT NULL,
  `modified_on` timestamp NULL DEFAULT NULL,
  `conflicts` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `in_adhesives`
--

LOCK TABLES `in_adhesives` WRITE;
/*!40000 ALTER TABLE `in_adhesives` DISABLE KEYS */;
INSERT INTO `in_adhesives` VALUES (1,4,1,1,'2018-06-19 05:24:10',0,'2018-06-20 04:13:28',0),(2,5,4,1,'2018-06-19 06:35:31',NULL,NULL,0),(3,7,122,1,'2018-06-19 06:35:57',NULL,NULL,0),(4,10,12,1,'2018-06-19 06:36:07',NULL,NULL,0),(5,6,1231,1,'2018-06-19 06:36:26',NULL,NULL,0),(6,8,121,1,'2018-06-19 06:36:44',NULL,NULL,0);
/*!40000 ALTER TABLE `in_adhesives` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `in_chemical_has_logs`
--

DROP TABLE IF EXISTS `in_chemical_has_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `in_chemical_has_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_chemical` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `initial_qty` int(11) DEFAULT NULL,
  `remaining_qty` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `issued_by` int(11) DEFAULT NULL,
  `issued_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `conflicts` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `in_chemical_has_logs`
--

LOCK TABLES `in_chemical_has_logs` WRITE;
/*!40000 ALTER TABLE `in_chemical_has_logs` DISABLE KEYS */;
INSERT INTO `in_chemical_has_logs` VALUES (1,1,24,11,35,0,0,'2018-06-20 05:06:42',0),(2,2,53,22,75,0,0,'2018-06-20 05:06:42',0),(3,1,15,35,20,1,0,'2018-06-20 05:06:57',0),(4,2,15,75,60,1,0,'2018-06-20 05:06:57',0);
/*!40000 ALTER TABLE `in_chemical_has_logs` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`in_chemical_has_logs_AFTER_INSERT` AFTER INSERT ON `in_chemical_has_logs` FOR EACH ROW
BEGIN

	if(new.`type`=0) then
		UPDATE `in_chemicals` SET `qty`= `qty` + new.`qty`, `modified_by`=new.`issued_by`, `modified_on`=new.`issued_on` WHERE `id_chemical`=new.`id_chemical`;
	elseif(new.`type`=1)then
		UPDATE `in_chemicals` SET `qty`= `qty` - new.`qty`, `modified_by`=new.`issued_by`, `modified_on`=new.`issued_on` WHERE `id_chemical`=new.`id_chemical`;
    end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`in_chemical_has_logs_BEFORE_DELETE` BEFORE DELETE ON `in_chemical_has_logs` FOR EACH ROW
BEGIN

	if(old.`type`=0) then
		UPDATE `in_chemicals` SET `qty`= `qty` - old.`qty` WHERE `id_chemical`=old.`id_chemical`;
	elseif(old.`type`=1)then
		UPDATE `in_chemicals` SET `qty`= `qty` + old.`qty` WHERE `id_chemical`=old.`id_chemical`;
    end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `in_chemicals`
--

DROP TABLE IF EXISTS `in_chemicals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `in_chemicals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_chemical` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `issued_by` int(11) DEFAULT NULL,
  `issued_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` int(11) DEFAULT NULL,
  `modified_on` timestamp NULL DEFAULT NULL,
  `conflicts` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `in_chemicals`
--

LOCK TABLES `in_chemicals` WRITE;
/*!40000 ALTER TABLE `in_chemicals` DISABLE KEYS */;
INSERT INTO `in_chemicals` VALUES (1,1,20,1,'2018-06-20 05:05:10',0,'2018-06-20 05:06:57',0),(2,2,60,1,'2018-06-20 05:05:14',0,'2018-06-20 05:06:57',0),(3,3,12,1,'2018-06-20 05:05:31',NULL,NULL,0),(4,4,15,1,'2018-06-20 05:05:43',NULL,NULL,0),(5,5,10,1,'2018-06-20 05:06:02',NULL,NULL,0),(6,8,5,1,'2018-06-20 05:06:09',NULL,NULL,0);
/*!40000 ALTER TABLE `in_chemicals` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `in_cylinder_has_logs`
--

DROP TABLE IF EXISTS `in_cylinder_has_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `in_cylinder_has_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_cylinder` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `initial_qty` int(11) DEFAULT NULL,
  `remaining_qty` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `issued_by` int(11) DEFAULT NULL,
  `issued_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `conflicts` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `in_cylinder_has_logs`
--

LOCK TABLES `in_cylinder_has_logs` WRITE;
/*!40000 ALTER TABLE `in_cylinder_has_logs` DISABLE KEYS */;
INSERT INTO `in_cylinder_has_logs` VALUES (3,3,23,22,45,0,0,'2018-06-20 05:24:18',0),(4,4,42,3,45,0,0,'2018-06-20 05:24:18',0),(5,3,5,45,40,1,0,'2018-06-20 05:24:31',0),(6,4,15,45,30,1,0,'2018-06-20 05:24:31',0);
/*!40000 ALTER TABLE `in_cylinder_has_logs` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`in_cylinder_has_logs_AFTER_INSERT` AFTER INSERT ON `in_cylinder_has_logs` FOR EACH ROW
BEGIN

	if(new.`type`=0) then
		UPDATE `in_cylinders` SET `qty`= `qty` + new.`qty`, `modified_by`=new.`issued_by`, `modified_on`=new.`issued_on` WHERE `id_cylinder`=new.`id_cylinder`;
	elseif(new.`type`=1)then
		UPDATE `in_cylinders` SET `qty`= `qty` - new.`qty`, `modified_by`=new.`issued_by`, `modified_on`=new.`issued_on` WHERE `id_cylinder`=new.`id_cylinder`;
    end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`in_cylinder_has_logs_BEFORE_DELETE` BEFORE DELETE ON `in_cylinder_has_logs` FOR EACH ROW
BEGIN

	if(old.`type`=0) then
		UPDATE `in_cylinders` SET `qty`= `qty` - old.`qty` WHERE `id_cylinder`=old.`id_cylinder`;
	elseif(old.`type`=1)then
		UPDATE `in_cylinders` SET `qty`= `qty` + old.`qty` WHERE `id_cylinder`=old.`id_cylinder`;
    end if;
	
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `in_cylinders`
--

DROP TABLE IF EXISTS `in_cylinders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `in_cylinders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_cylinder` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `issued_by` int(11) DEFAULT NULL,
  `issued_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` int(11) DEFAULT NULL,
  `modified_on` timestamp NULL DEFAULT NULL,
  `conflicts` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `in_cylinders`
--

LOCK TABLES `in_cylinders` WRITE;
/*!40000 ALTER TABLE `in_cylinders` DISABLE KEYS */;
INSERT INTO `in_cylinders` VALUES (1,3,40,1,'2018-06-20 05:14:03',0,'2018-06-20 05:24:31',0),(2,4,30,1,'2018-06-20 05:14:09',0,'2018-06-20 05:24:31',0),(3,9,4,1,'2018-06-20 05:14:16',NULL,NULL,0),(4,11,5,1,'2018-06-20 05:14:23',NULL,NULL,0),(5,12,6,1,'2018-06-20 05:14:36',NULL,NULL,0),(6,2,7,1,'2018-06-20 05:14:44',0,'2018-06-20 05:18:35',0),(7,8,8,1,'2018-06-20 05:14:52',NULL,NULL,0),(8,7,9,1,'2018-06-20 05:14:56',NULL,NULL,0);
/*!40000 ALTER TABLE `in_cylinders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `in_ink_has_logs`
--

DROP TABLE IF EXISTS `in_ink_has_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `in_ink_has_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_ink` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `initial_qty` int(11) DEFAULT NULL,
  `remaining_qty` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `issued_by` int(11) DEFAULT NULL,
  `issued_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `conflicts` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `in_ink_has_logs`
--

LOCK TABLES `in_ink_has_logs` WRITE;
/*!40000 ALTER TABLE `in_ink_has_logs` DISABLE KEYS */;
INSERT INTO `in_ink_has_logs` VALUES (1,1,24,11,35,0,0,'2018-06-20 05:32:03',0),(2,2,43,22,65,0,0,'2018-06-20 05:32:03',0),(3,3,1,101,100,1,0,'2018-06-20 05:32:22',0),(4,4,2,102,100,1,0,'2018-06-20 05:32:22',0);
/*!40000 ALTER TABLE `in_ink_has_logs` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`in_ink_has_logs_AFTER_INSERT` AFTER INSERT ON `in_ink_has_logs` FOR EACH ROW
BEGIN

	if(new.`type`=0) then
		UPDATE `in_inks` SET `qty`= `qty` + new.`qty`, `modified_by`=new.`issued_by`, `modified_on`=new.`issued_on` WHERE `id_ink`=new.`id_ink`;
	elseif(new.`type`=1)then
		UPDATE `in_inks` SET `qty`= `qty` - new.`qty`, `modified_by`=new.`issued_by`, `modified_on`=new.`issued_on` WHERE `id_ink`=new.`id_ink`;
    end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`in_ink_has_logs_BEFORE_DELETE` BEFORE DELETE ON `in_ink_has_logs` FOR EACH ROW
BEGIN

	if(old.`type`=0) then
		UPDATE `in_inks` SET `qty`= `qty` - old.`qty` WHERE `id_ink`=old.`id_ink`;
	elseif(old.`type`=1)then
		UPDATE `in_inks` SET `qty`= `qty` + old.`qty` WHERE `id_ink`=old.`id_ink`;
    end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `in_inks`
--

DROP TABLE IF EXISTS `in_inks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `in_inks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_ink` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `issued_by` int(11) DEFAULT NULL,
  `issued_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` int(11) DEFAULT NULL,
  `modified_on` timestamp NULL DEFAULT NULL,
  `conflicts` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `in_inks`
--

LOCK TABLES `in_inks` WRITE;
/*!40000 ALTER TABLE `in_inks` DISABLE KEYS */;
INSERT INTO `in_inks` VALUES (1,1,35,1,'2018-06-20 05:30:43',0,'2018-06-20 05:32:03',0),(2,2,65,1,'2018-06-20 05:30:49',0,'2018-06-20 05:32:03',0),(3,3,100,1,'2018-06-20 05:31:02',0,'2018-06-20 05:32:22',0),(4,4,100,1,'2018-06-20 05:31:11',0,'2018-06-20 05:32:22',0),(5,5,201,1,'2018-06-20 05:31:19',NULL,NULL,0),(6,6,301,1,'2018-06-20 05:31:25',NULL,NULL,0);
/*!40000 ALTER TABLE `in_inks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `in_plastic_has_logs`
--

DROP TABLE IF EXISTS `in_plastic_has_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `in_plastic_has_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_plastic` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `initial_qty` int(11) DEFAULT NULL,
  `remaining_qty` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `issued_by` int(11) DEFAULT NULL,
  `issued_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `conflicts` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `in_plastic_has_logs`
--

LOCK TABLES `in_plastic_has_logs` WRITE;
/*!40000 ALTER TABLE `in_plastic_has_logs` DISABLE KEYS */;
INSERT INTO `in_plastic_has_logs` VALUES (1,2,120,10,130,0,0,'2018-06-20 05:41:00',0),(2,11,23,11,34,0,0,'2018-06-20 05:41:00',0),(3,2,15,130,115,1,0,'2018-06-20 05:41:33',0),(4,11,4,34,30,1,0,'2018-06-20 05:41:33',0),(5,2,111,115,226,0,0,'2018-06-20 05:48:07',0),(6,11,222,30,252,0,0,'2018-06-20 05:48:07',0);
/*!40000 ALTER TABLE `in_plastic_has_logs` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`in_plastic_has_logs_AFTER_INSERT` AFTER INSERT ON `in_plastic_has_logs` FOR EACH ROW
BEGIN

	if(new.`type`=0) then
		UPDATE `in_plastics` SET `qty`= `qty` + new.`qty`, `modified_by`=new.`issued_by`, `modified_on`=new.`issued_on` WHERE `id_plastic`=new.`id_plastic`;
	elseif(new.`type`=1)then
		UPDATE `in_plastics` SET `qty`= `qty` - new.`qty`, `modified_by`=new.`issued_by`, `modified_on`=new.`issued_on` WHERE `id_plastic`=new.`id_plastic`;
    end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`in_plastic_has_logs_BEFORE_DELETE` BEFORE DELETE ON `in_plastic_has_logs` FOR EACH ROW
BEGIN

	if(old.`type`=0) then
		UPDATE `in_plastics` SET `qty`= `qty` - old.`qty` WHERE `id_plastic`=old.`id_plastic`;
	elseif(old.`type`=1)then
		UPDATE `in_plastics` SET `qty`= `qty` + old.`qty` WHERE `id_plastic`=old.`id_plastic`;
    end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `in_plastics`
--

DROP TABLE IF EXISTS `in_plastics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `in_plastics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_plastic` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `issued_by` int(11) DEFAULT NULL,
  `issued_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` int(11) DEFAULT NULL,
  `modified_on` timestamp NULL DEFAULT NULL,
  `conflicts` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `in_plastics`
--

LOCK TABLES `in_plastics` WRITE;
/*!40000 ALTER TABLE `in_plastics` DISABLE KEYS */;
INSERT INTO `in_plastics` VALUES (1,12,11,NULL,'2018-06-20 05:39:37',NULL,NULL,0),(2,2,216,1,'2018-06-20 05:39:49',0,'2018-06-20 05:48:07',0),(3,11,252,1,'2018-06-20 05:39:54',0,'2018-06-20 05:48:07',0),(4,3,12,1,'2018-06-20 05:40:06',NULL,NULL,0),(5,8,13,1,'2018-06-20 05:40:20',NULL,NULL,0),(6,9,14,1,'2018-06-20 05:40:29',NULL,NULL,0),(7,5,101,1,'2018-06-20 05:40:38',NULL,NULL,0);
/*!40000 ALTER TABLE `in_plastics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ink_has_logs`
--

DROP TABLE IF EXISTS `ink_has_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ink_has_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_ink` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `minimum` int(11) NOT NULL,
  `starting_qty` int(11) NOT NULL,
  `old_price` double(11,2) NOT NULL,
  `price` double(11,2) NOT NULL,
  `issued_by` int(11) DEFAULT NULL,
  `issued_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ink_has_logs`
--

LOCK TABLES `ink_has_logs` WRITE;
/*!40000 ALTER TABLE `ink_has_logs` DISABLE KEYS */;
INSERT INTO `ink_has_logs` VALUES (1,4,'Ink A','asd',123,123,123.00,123.00,1,'2018-06-14 08:11:46'),(2,4,'Ink A1','asd',123,123,123.00,123.00,1,'2018-06-14 08:11:50'),(3,1,'Ink 1','1',1,1,1.00,1.00,1,'2018-06-20 05:30:44'),(4,2,'Ink 2','2',2,2,2.00,2.00,1,'2018-06-20 05:30:50'),(5,3,'Ink A','Type A',10,10,10.00,10.00,1,'2018-06-20 05:31:03'),(6,4,'Ink A','asd',123,123,123.00,123.00,1,'2018-06-20 05:31:11'),(7,5,'Ink B','123',123,123,123.00,123.00,1,'2018-06-20 05:31:20'),(8,6,'Ink C','123',123,123,123.00,123.00,1,'2018-06-20 05:31:26');
/*!40000 ALTER TABLE `ink_has_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inks`
--

DROP TABLE IF EXISTS `inks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `minimum` int(11) NOT NULL,
  `starting_qty` int(11) NOT NULL,
  `price` double(11,2) NOT NULL,
  `issued_by` int(11) DEFAULT NULL,
  `issued_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` int(11) DEFAULT NULL,
  `modified_on` timestamp NULL DEFAULT NULL,
  `conflicts` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inks`
--

LOCK TABLES `inks` WRITE;
/*!40000 ALTER TABLE `inks` DISABLE KEYS */;
INSERT INTO `inks` VALUES (1,'Ink 1','1',1,11,1.00,0,'2018-05-18 01:50:05',1,'2018-06-20 05:30:44',5),(2,'Ink 2','2',2,22,2.00,1,'2018-05-18 01:50:14',1,'2018-06-20 05:30:50',3),(3,'Ink A1','Type A',10,101,10.00,1,'2018-06-14 08:11:21',1,'2018-06-20 05:31:03',0),(4,'Ink A2','asd',123,102,123.00,1,'2018-06-14 08:11:31',1,'2018-06-20 05:31:11',0),(5,'Ink B','123',123,201,123.00,0,'2018-06-14 08:13:36',1,'2018-06-20 05:31:20',0),(6,'Ink C','123',123,301,123.00,0,'2018-06-14 08:13:36',1,'2018-06-20 05:31:26',1);
/*!40000 ALTER TABLE `inks` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`inks_AFTER_INSERT` AFTER INSERT ON `inks` FOR EACH ROW
BEGIN

	INSERT INTO `in_inks`(`id_ink`,`qty`) VALUES(new.`id`,new.`starting_qty`);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`inks_AFTER_UPDATE` AFTER UPDATE ON `inks` FOR EACH ROW
BEGIN


	if(new.`name`!=old.`name` OR new.`type`!=old.`type` OR new.`minimum`!=old.`minimum` OR new.`starting_qty`!=old.`starting_qty` OR new.`price`!=old.`price`) then 
	insert into `ink_has_logs`  
	(`issued_on`,`issued_by`,`id_ink`,`name`,`type`,`minimum`,`starting_qty`,`old_price`,`price`)
	values 
	(new.`modified_on`,new.`modified_by`,new.`id`,old.`name`,old.`type`,old.`minimum`,old.`starting_qty`,old.`price`,new.`price`);

		if((SELECT count(*) FROM `in_inks` WHERE `id_ink`=new.`id`)=0)then
			INSERT INTO `in_inks`(`id_ink`,`qty`,`issued_by`) VALUES(new.`id`,new.`starting_qty`,new.`modified_by`);
		else
			UPDATE `in_inks` SET `qty`= (`qty`-old.`starting_qty`+new.`starting_qty`), `issued_by`=new.`modified_by` WHERE `id_ink`=new.`id`;
		end if;
	end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`inks_BEFORE_DELETE` BEFORE DELETE ON `inks` FOR EACH ROW
BEGIN

	DELETE FROM `in_inks` WHERE `id_ink`=old.`id`;
	DELETE FROM `in_ink_has_logs` WHERE `id_ink`=old.`id`;
	DELETE FROM `ink_has_logs` WHERE `id_ink`=old.`id`;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `joborder_has_logs`
--

DROP TABLE IF EXISTS `joborder_has_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `joborder_has_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_joborder` int(11) DEFAULT NULL,
  `recieved_by` varchar(100) DEFAULT NULL,
  `noted_by` varchar(100) DEFAULT NULL,
  `jo_number` varchar(100) DEFAULT NULL,
  `cylinder_number` varchar(100) DEFAULT NULL,
  `barcode` varchar(100) DEFAULT NULL,
  `weight_roll` varchar(100) DEFAULT NULL,
  `meter_roll` varchar(100) DEFAULT NULL,
  `winding_direction` int(11) DEFAULT NULL,
  `prepared_by` varchar(100) DEFAULT NULL,
  `filled_up_by` varchar(100) DEFAULT NULL,
  `approved_by` varchar(100) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `issued_by` int(11) DEFAULT NULL,
  `issued_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `joborder_has_logs`
--

LOCK TABLES `joborder_has_logs` WRITE;
/*!40000 ALTER TABLE `joborder_has_logs` DISABLE KEYS */;
INSERT INTO `joborder_has_logs` VALUES (1,16,'2','3','1','5','7','6','8',4,NULL,NULL,NULL,0,1,'2018-06-01 06:51:07'),(2,16,'3','4','2','5','6','7','8',4,NULL,NULL,NULL,0,1,'2018-06-01 09:59:01'),(3,17,NULL,NULL,NULL,'0',NULL,'0','0',0,NULL,NULL,NULL,0,1,'2018-06-04 09:18:07'),(5,34,NULL,NULL,NULL,'0',NULL,'0','0',0,NULL,NULL,NULL,0,1,'2018-06-04 09:32:47'),(6,16,'33','44','22','5','66','77','88',3,NULL,NULL,NULL,0,1,'2018-06-01 09:59:01'),(7,33,'23','23','23','0','','0','0',0,'','','',0,1,'2018-06-05 03:38:36'),(8,33,'23','23','23','0','','0','0',0,'','','',1,1,'2018-06-05 03:38:36'),(9,33,'23','23','23','0','','0','0',0,'','','',2,1,'2018-06-05 03:38:36'),(10,33,'23','23','23','0','','0','0',0,'','','',3,1,'2018-06-05 03:38:36'),(11,33,'23','23','23','0','','0','0',0,'','','',1,1,'2018-06-05 03:38:36'),(12,33,'23','23','23','0','','0','0',0,'','','',3,1,'2018-06-05 03:38:36'),(13,33,'23','23','23','0','','0','0',0,'','','',2,1,'2018-06-05 03:38:36'),(14,33,'23','23','23','0','','0','0',0,'','','',3,1,'2018-06-05 03:38:36'),(15,33,'23','23','23','0','','0','0',0,'','','',2,1,'2018-06-05 03:38:36'),(16,33,'23','23','23','0','','0','0',0,'','','',1,1,'2018-06-05 03:38:36'),(17,33,'23','23','23','0','','0','0',0,'','','',3,1,'2018-06-05 03:38:36'),(18,33,'23','23','23','0','','0','0',0,'','','',2,1,'2018-06-05 03:38:36'),(19,33,'23','23','23','0','','0','0',0,'','','',3,1,'2018-06-05 03:38:36'),(20,33,'23','23','23','0','','0','0',0,'','','',2,1,'2018-06-05 03:38:36'),(21,33,'23','23','23','0','','0','0',0,'','','',3,1,'2018-06-05 03:38:36'),(22,33,'23','23','23','0','','0','0',0,'','','',2,1,'2018-06-05 03:38:36'),(23,33,'23','23','23','0','','0','0',0,'','','',3,1,'2018-06-05 03:38:36'),(24,33,'23','23','23','0','','0','0',0,'','','',2,1,'2018-06-05 03:38:36'),(25,33,'23','23','23','0','','0','0',0,'','','',3,1,'2018-06-05 03:38:36'),(26,33,'23','23','23','0','','0','0',0,'','','',2,1,'2018-06-05 03:38:36'),(27,33,'23','23','23','0','','0','0',0,'','','',3,1,'2018-06-05 03:38:36'),(28,33,'23','23','23','0','','0','0',0,'','','',2,1,'2018-06-05 03:38:36'),(29,33,'23','23','23','0','','0','0',0,'','','',3,1,'2018-06-05 03:38:36'),(30,33,'23','23','23','0','','0','0',0,'','','',1,1,'2018-06-05 03:38:36'),(31,33,'23','23','23','0','','0','0',0,'','','',2,1,'2018-06-05 03:38:36'),(32,33,'23','23','23','0','','0','0',0,'','','',3,1,'2018-06-05 03:38:36'),(33,33,'23','23','23','0','','0','0',0,'','','',2,1,'2018-06-05 03:38:36'),(34,33,'23','23','23','0','','0','0',0,'','','',3,1,'2018-06-05 03:38:36'),(35,33,'23','23','23','0','','0','0',0,'','','',1,1,'2018-06-05 03:38:36'),(36,33,'23','23','23','0','','0','0',0,'','','',3,1,'2018-06-05 03:38:36'),(37,33,'23','23','23','0','','0','0',0,'','','',1,1,'2018-06-05 03:38:36'),(38,33,'23','23','23','0','','0','0',0,'','','',2,1,'2018-06-05 03:38:36'),(39,33,'23','23','23','0','','0','0',0,'','','',3,1,'2018-06-05 03:38:36'),(40,33,'23','23','23','0','','0','0',0,'','','',1,1,'2018-06-05 03:38:36'),(41,33,'23','23','23','0','','0','0',0,'','','',3,1,'2018-06-05 03:38:36'),(42,33,'23','23','23','0','','0','0',0,'','','',2,1,'2018-06-05 03:38:36'),(43,17,'a','b','','0','6','5','7',2,'c','d','e',0,1,'2018-06-06 03:34:59'),(44,17,'a','b','','0','6','5','7',2,'c','d','e',1,1,'2018-06-06 03:35:21'),(45,17,'a','b','24','0','6','5','7',2,'c','d','e',1,1,'2018-06-06 03:35:21'),(46,16,'33','44','22','5','66','77','88',3,NULL,NULL,NULL,1,1,'2018-06-01 09:59:01'),(47,16,'33','44','22','5','66','77','88',3,NULL,NULL,NULL,2,1,'2018-06-01 09:59:01'),(48,17,'a','b','24','0','6','5','7',2,'c','d','e',2,1,'2018-06-06 03:35:21'),(49,33,'23','23','23','0','','0','0',0,'','','',1,1,'2018-06-05 03:38:36'),(50,34,'A','B','222','0','44','22','33',0,'C','D','E',0,1,'2018-06-06 05:16:01'),(51,34,'A','B','222','0','44','22','33',0,'C','D','E',1,1,'2018-06-06 05:16:01'),(52,16,'33','44','22','5','66','77','88',3,NULL,NULL,NULL,3,1,'2018-06-01 09:59:01'),(53,17,'a','b','24','0','6','5','7',2,'c','d','e',3,1,'2018-06-06 03:35:21'),(54,33,'23','23','23','0','','0','0',0,'','','',3,1,'2018-06-05 03:38:36'),(55,34,'A','B','222','0','44','22','33',0,'C','D','E',2,1,'2018-06-06 05:16:01'),(56,34,'A','B','222','0','44','22','33',0,'C','D','E',1,1,'2018-06-06 05:16:01'),(57,34,'A','B','222','0','44','22','33',0,'C','D','E',2,1,'2018-06-06 05:16:01'),(58,34,'A','B','222','0','44','22','33',0,'C','D','E',1,1,'2018-06-06 05:16:01'),(59,34,'A','B','222','0','44','22','33',0,'C','D','E',2,1,'2018-06-06 05:16:01'),(60,16,'33','44','22','5','66','77','88',3,NULL,NULL,NULL,1,1,'2018-06-01 09:59:01'),(61,16,'33','44','22','5','66','77','88',3,NULL,NULL,NULL,2,1,'2018-06-01 09:59:01'),(62,16,'33','44','22','5','66','77','88',3,NULL,NULL,NULL,3,1,'2018-06-01 09:59:01'),(63,33,'23','23','23','0','','0','0',0,'','','',1,1,'2018-06-05 03:38:36'),(64,33,'23','23','23','0','','0','0',0,'','','',2,1,'2018-06-05 03:38:36'),(65,33,'23','23','23','0','','0','0',0,'','','',3,1,'2018-06-05 03:38:36'),(66,34,'A','B','222','0','44','22','33',0,'C','D','E',1,1,'2018-06-06 05:16:01'),(67,34,'A','B','222','0','44','22','33',0,'C','D','E',2,1,'2018-06-06 05:16:01'),(68,33,'23','23','23','0','','0','0',0,'','','',1,1,'2018-06-05 03:38:36'),(69,33,'23','23','23','0','','0','0',0,'','','',2,1,'2018-06-05 03:38:36'),(70,16,'33','44','22','5','66','77','88',3,NULL,NULL,NULL,1,1,'2018-06-01 09:59:01'),(71,16,'33','44','22','5','66','77','88',3,NULL,NULL,NULL,2,1,'2018-06-01 09:59:01'),(72,17,'a','b','24','0','6','5','7',2,'c','d','e',1,1,'2018-06-06 03:35:21'),(73,17,'a','b','24','0','6','5','7',2,'c','d','e',2,1,'2018-06-06 03:35:21'),(74,16,'33','44','22','5','66','77','88',3,NULL,NULL,NULL,1,1,'2018-06-01 09:59:01'),(75,17,'a','b','24','0','6','5','7',2,'c','d','e',1,1,'2018-06-06 03:35:21'),(76,17,'a','b','24','0','6','5','7',2,'c','d','e',2,1,'2018-06-06 03:35:21'),(77,17,'a','b','24','0','6','5','7',2,'c','d','e',3,1,'2018-06-06 03:35:21'),(78,17,'a','b','24','0','6','5','7',2,'c','d','e',2,1,'2018-06-06 03:35:21'),(79,17,'a','b','24','0','6','5','7',2,'c','d','e',3,1,'2018-06-06 03:35:21'),(80,34,'A','B','222','0','44','22','33',0,'C','D','E',3,1,'2018-06-06 05:16:01'),(81,34,'A','B','222','0','44','22','33',0,'C','D','E',2,1,'2018-06-06 05:16:01'),(82,33,'23','23','23','0','','0','0',0,'','','',3,1,'2018-06-05 03:38:36'),(83,33,'23','23','23','0','','0','0',0,'','','',2,1,'2018-06-05 03:38:36'),(84,16,'33','44','22','5','66','77','88',3,NULL,NULL,NULL,2,1,'2018-06-01 09:59:01'),(85,17,'a','b','24','0','6','5','7',2,'c','d','e',2,1,'2018-06-06 03:35:21'),(86,16,'33','44','22','5','66','77','88',3,NULL,NULL,NULL,1,1,'2018-06-01 09:59:01'),(87,16,'33','44','22','5','66','77','88',3,NULL,NULL,NULL,2,1,'2018-06-01 09:59:01'),(88,16,'33','44','22','5','66','77','88',3,NULL,NULL,NULL,1,1,'2018-06-01 09:59:01'),(89,16,'33','44','22','5','66','77','88',3,NULL,NULL,NULL,2,1,'2018-06-01 09:59:01'),(90,16,'33','44','22','5','66','77','88',3,NULL,NULL,NULL,1,1,'2018-06-01 09:59:01'),(91,16,'33','44','22','5','66','77','88',3,NULL,NULL,NULL,2,1,'2018-06-01 09:59:01'),(92,16,'33','44','22','5','66','77','88',3,NULL,NULL,NULL,1,1,'2018-06-01 09:59:01'),(93,16,'33','44','22','5','66','77','88',3,NULL,NULL,NULL,2,1,'2018-06-01 09:59:01'),(94,17,'a','b','24','0','6','5','7',2,'c','d','e',1,1,'2018-06-06 03:35:21'),(95,17,'a','b','24','0','6','5','7',2,'c','d','e',2,1,'2018-06-06 03:35:21'),(96,17,'a','b','24','0','6','5','7',2,'c','d','e',3,1,'2018-06-06 03:35:21'),(97,17,'a','b','24','0','6','5','7',2,'c','d','e',2,1,'2018-06-06 03:35:21'),(98,17,'a','b','24','0','6','5','7',2,'c','d','e',3,1,'2018-06-06 03:35:21'),(99,17,'a','b','24','0','6','5','7',2,'c','d','e',2,1,'2018-06-18 04:08:08'),(100,17,'b','c','25','0','666','555','777',2,'D','e','f',2,1,'2018-06-18 04:08:08');
/*!40000 ALTER TABLE `joborder_has_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `joborder_has_plastics`
--

DROP TABLE IF EXISTS `joborder_has_plastics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `joborder_has_plastics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_joborder` int(11) NOT NULL DEFAULT '0',
  `id_plastic` int(11) NOT NULL DEFAULT '0',
  `id_product` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `type_size` varchar(100) DEFAULT NULL,
  `size` double(11,2) DEFAULT NULL,
  `issued_by` int(11) DEFAULT NULL,
  `issued_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` int(11) DEFAULT NULL,
  `modified_on` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `joborder_has_plastics`
--

LOCK TABLES `joborder_has_plastics` WRITE;
/*!40000 ALTER TABLE `joborder_has_plastics` DISABLE KEYS */;
INSERT INTO `joborder_has_plastics` VALUES (4,16,2,NULL,666,'555',0.00,NULL,'2018-05-31 07:22:26',1,'2018-06-05 03:18:15',1),(5,16,3,NULL,888,'777',0.00,NULL,'2018-05-31 07:22:26',1,'2018-06-05 03:18:15',1),(7,17,3,NULL,42,'23',0.00,NULL,'2018-05-31 07:22:26',1,'2018-06-18 04:08:08',1),(8,1,2,2,NULL,NULL,NULL,2,'2018-06-04 08:59:18',NULL,NULL,0),(9,1,3,2,NULL,NULL,NULL,2,'2018-06-04 08:59:18',NULL,NULL,0),(11,33,3,3,425,'23',NULL,1,'2018-06-04 09:00:35',1,'2018-06-05 03:38:36',1),(12,34,2,2,123,'123',NULL,1,'2018-06-04 09:26:01',1,'2018-06-18 03:49:31',1),(13,34,3,2,321,'321',NULL,1,'2018-06-04 09:26:01',1,'2018-06-18 03:49:31',1),(20,37,2,2,NULL,NULL,NULL,1,'2018-06-18 04:55:02',NULL,NULL,0),(21,37,3,2,NULL,NULL,NULL,1,'2018-06-18 04:55:02',NULL,NULL,0);
/*!40000 ALTER TABLE `joborder_has_plastics` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`joborder_has_plastics_AFTER_INSERT` AFTER INSERT ON `joborder_has_plastics` FOR EACH ROW
BEGIN

		UPDATE `plastics` set `conflicts`=`conflicts`+1 WHERE `id`=new.`id_plastic`;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`joborder_has_plastics_AFTER_UPDATE` AFTER UPDATE ON `joborder_has_plastics` FOR EACH ROW
BEGIN


	if(new.`id_plastic`!=old.`id_plastic` OR new.`qty`!=old.`qty` OR new.`size`!=old.`size` OR new.`type_size`!=old.`type_size`) then 
		insert into `joborder_plastic_has_logs`  
		(`issued_on`,`issued_by`,`id_joborder_plastic`,`id_plastic`,`id_product`,`qty`,`size`,`type_size`)
		values 
		(new.`modified_on`,new.`modified_by`,new.`id`,old.`id_plastic`,old.`id_product`,old.`qty`,old.`size`,old.`type_size`);
	end if;
    
    if((SELECT count(*) FROM `joborder_has_plastics` WHERE `status`='0' AND `id_joborder`=new.`id_joborder`)=0) then
		UPDATE `joborders` SET `status`='1' WHERE `id`=new.`id_joborder`;
    end if;
    
	if(new.`id_plastic`!=old.`id_plastic`) then
		UPDATE `plastics` set `conflicts`=`conflicts`+1 WHERE `id`=new.`id_plastic`;
		UPDATE `plastics` set `conflicts`=`conflicts`-1 WHERE `id`=old.`id_plastic`;
    end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`joborder_has_plastics_AFTER_DELETE` AFTER DELETE ON `joborder_has_plastics` FOR EACH ROW
BEGIN

		UPDATE `plastics` set `conflicts`=`conflicts`-1 WHERE `id`=old.`id_plastic`;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `joborder_has_productions`
--

DROP TABLE IF EXISTS `joborder_has_productions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `joborder_has_productions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_joborder` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `pd_roll` double(11,2) NOT NULL DEFAULT '0.00',
  `pd_weight` double(11,2) NOT NULL DEFAULT '0.00',
  `ld_roll` double(11,2) NOT NULL DEFAULT '0.00',
  `ld_weight` double(11,2) NOT NULL DEFAULT '0.00',
  `sd_roll` double(11,2) NOT NULL DEFAULT '0.00',
  `sd_weight` double(11,2) NOT NULL DEFAULT '0.00',
  `bf_roll` double(11,2) NOT NULL DEFAULT '0.00',
  `bf_weight` double(11,2) NOT NULL DEFAULT '0.00',
  `lod_dr_number` varchar(100) NOT NULL DEFAULT '0.00',
  `lod_wieght` double(11,2) NOT NULL DEFAULT '0.00',
  `remarks` longtext,
  `status` int(11) NOT NULL DEFAULT '0',
  `issued_by` int(11) DEFAULT NULL,
  `issued_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` int(11) DEFAULT NULL,
  `modified_on` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `joborder_has_productions`
--

LOCK TABLES `joborder_has_productions` WRITE;
/*!40000 ALTER TABLE `joborder_has_productions` DISABLE KEYS */;
INSERT INTO `joborder_has_productions` VALUES (10,34,'2018-06-21',1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00,'0.00',0.00,'1',0,0,'2018-06-08 05:23:26',0,'2018-06-08 05:23:26'),(11,34,'2018-06-15',2.00,2.00,22.00,2.00,2.00,2.00,2.00,2.00,'0.00',0.00,'2',0,0,'2018-06-08 05:23:55',0,'2018-06-08 05:23:55'),(12,33,'2018-06-01',1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00,'0.00',0.00,'1',0,0,'2018-06-08 05:25:42',0,'2018-06-08 05:25:43'),(13,33,'2018-06-08',2.00,2.00,2.00,2.00,2.00,2.00,2.00,2.00,'0.00',0.00,'2',0,0,'2018-06-08 05:25:42',0,'2018-06-08 05:25:43'),(14,33,'2018-06-15',3.00,3.00,3.00,3.00,3.00,3.00,3.00,3.00,'0.00',0.00,'3',0,0,'2018-06-08 05:25:42',0,'2018-06-08 05:25:43'),(24,16,'2018-06-07',11.00,22.00,33.00,44.00,55.00,66.00,77.00,88.00,'0.00',0.00,'123',0,0,'2018-06-14 05:47:46',0,'2018-06-14 05:47:47'),(25,16,'2018-06-21',23.00,42.00,213.00,41.00,23.00,42.00,52.00,23.00,'0.00',0.00,'222',0,0,'2018-06-14 05:48:08',0,'2018-06-14 05:48:09'),(26,17,'2018-06-22',1.00,11.00,11.00,1.00,223.00,1242.00,111.00,11222.00,'0.00',0.00,'2321',0,0,'2018-06-14 06:02:07',0,'2018-06-14 06:02:08'),(27,17,'2018-06-20',4.00,2.00,5.00,2.00,444.00,444.00,555.00,666.00,'0.00',0.00,'dassda',2,0,'2018-06-14 06:02:07',0,'2018-06-14 06:02:08'),(28,16,'2018-06-15',23.00,42.00,52.00,123.00,444.00,555.00,666.00,777.00,'0.00',0.00,'2223',0,0,'2018-06-14 06:18:13',0,'2018-06-14 06:18:14');
/*!40000 ALTER TABLE `joborder_has_productions` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`joborder_has_productions_AFTER_INSERT` AFTER INSERT ON `joborder_has_productions` FOR EACH ROW
BEGIN
	
	DECLARE countering INT;
    
    SET @countering = (SELECT count(*) FROM `joborder_has_productions` WHERE `id_joborder`=new.`id_joborder`);
    
    IF(@countering=0)THEN
		UPDATE `joborders` SET `status`=1 WHERE `id`=new.`id_joborder`;
	ELSEIF(@countering>=1)THEN
		SET @countering = (SELECT count(*) FROM `joborder_has_productions` WHERE `id_joborder`=new.`id_joborder` AND `status`=0);
        
        IF(@countering>=1)THEN
			SET @countering = (SELECT count(*) FROM `joborder_production_has_deliverys` WHERE `id_joborder_production` IN (SELECT `id` FROM `joborder_has_productions` WHERE `id_joborder`=new.`id_joborder`));
            IF(@countering = 0)THEN
				UPDATE `joborders` SET `status`=1 WHERE `id`=new.`id_joborder`;
            ELSE
				UPDATE `joborders` SET `status`=2 WHERE `id`=new.`id_joborder`;
            END IF;
        ELSEIF(@countering=0)THEN
			UPDATE `joborders` SET `status`=3 WHERE `id`=new.`id_joborder`;
		END IF;
    END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`joborder_has_productions_AFTER_UPDATE` AFTER UPDATE ON `joborder_has_productions` FOR EACH ROW
BEGIN
	
	DECLARE countering INT;
    
    SET @countering = (SELECT count(*) FROM `joborder_has_productions` WHERE `id_joborder`=new.`id_joborder`);
    
    IF(@countering=0)THEN
		UPDATE `joborders` SET `status`=1 WHERE `id`=new.`id_joborder`;
	ELSEIF(@countering>=1)THEN
		SET @countering = (SELECT count(*) FROM `joborder_has_productions` WHERE `id_joborder`=new.`id_joborder` AND `status`=0);
        
        IF(@countering>=1)THEN
			SET @countering = (SELECT count(*) FROM `joborder_production_has_deliverys` WHERE `id_joborder_production` IN (SELECT `id` FROM `joborder_has_productions` WHERE `id_joborder`=new.`id_joborder`));
            IF(@countering = 0)THEN
				UPDATE `joborders` SET `status`=1 WHERE `id`=new.`id_joborder`;
            ELSE
				UPDATE `joborders` SET `status`=2 WHERE `id`=new.`id_joborder`;
            END IF;
        ELSEIF(@countering=0)THEN
			UPDATE `joborders` SET `status`=3 WHERE `id`=new.`id_joborder`;
		END IF;
    END IF;
    
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`joborder_has_productions_AFTER_DELETE` AFTER DELETE ON `joborder_has_productions` FOR EACH ROW
BEGIN
	
	DECLARE countering INT;
    
    SET @countering = (SELECT count(*) FROM `joborder_has_productions` WHERE `id_joborder`=old.`id_joborder`);
    
    IF(@countering=0)THEN
		UPDATE `joborders` SET `status`=1 WHERE `id`=old.`id_joborder`;
	ELSEIF(@countering>=1)THEN
		SET @countering = (SELECT count(*) FROM `joborder_has_productions` WHERE `id_joborder`=old.`id_joborder` AND `status`=0);
        
        IF(@countering>=1)THEN
			SET @countering = (SELECT count(*) FROM `joborder_production_has_deliverys` WHERE `id_joborder_production` IN (SELECT `id` FROM `joborder_has_productions` WHERE `id_joborder`=old.`id_joborder`));
            IF(@countering = 0)THEN
				UPDATE `joborders` SET `status`=1 WHERE `id`=old.`id_joborder`;
            ELSE
				UPDATE `joborders` SET `status`=2 WHERE `id`=old.`id_joborder`;
            END IF;
        ELSEIF(@countering=0)THEN
			UPDATE `joborders` SET `status`=3 WHERE `id`=old.`id_joborder`;
		END IF;
    END IF;
    DELETE FROM `joborder_production_has_deliverys` WHERE `id_joborder_production`=old.`id`;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `joborder_plastic_has_logs`
--

DROP TABLE IF EXISTS `joborder_plastic_has_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `joborder_plastic_has_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_joborder_plastic` int(11) DEFAULT '0',
  `id_joborder` int(11) DEFAULT '0',
  `id_plastic` int(11) DEFAULT '0',
  `id_product` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT '0',
  `size` double(11,2) DEFAULT '0.00',
  `type_size` varchar(100) DEFAULT NULL,
  `issued_by` int(11) DEFAULT NULL,
  `issued_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `joborder_plastic_has_logs`
--

LOCK TABLES `joborder_plastic_has_logs` WRITE;
/*!40000 ALTER TABLE `joborder_plastic_has_logs` DISABLE KEYS */;
INSERT INTO `joborder_plastic_has_logs` VALUES (1,4,0,2,NULL,0,0.00,NULL,1,'2018-06-05 03:18:15'),(2,5,0,3,NULL,0,0.00,NULL,1,'2018-06-05 03:18:15'),(3,7,0,3,NULL,0,0.00,NULL,1,'2018-06-06 03:34:59');
/*!40000 ALTER TABLE `joborder_plastic_has_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `joborder_production_has_deliverys`
--

DROP TABLE IF EXISTS `joborder_production_has_deliverys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `joborder_production_has_deliverys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dr_no` varchar(100) NOT NULL,
  `id_joborder_production` int(11) NOT NULL,
  `deliver_to` varchar(100) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `address` longtext,
  `terms` longtext,
  `qty` double(11,2) NOT NULL DEFAULT '0.00',
  `unit` varchar(100) DEFAULT NULL,
  `price` double(11,2) DEFAULT NULL,
  `issued_by` int(11) DEFAULT NULL,
  `issued_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` int(11) DEFAULT NULL,
  `modified_on` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `joborder_production_has_deliverys`
--

LOCK TABLES `joborder_production_has_deliverys` WRITE;
/*!40000 ALTER TABLE `joborder_production_has_deliverys` DISABLE KEYS */;
INSERT INTO `joborder_production_has_deliverys` VALUES (11,'1',27,'1','2018-06-14','1','1',66.00,NULL,333.00,0,'2018-06-14 06:31:07',0,'2018-06-14 06:31:08',2),(12,'1',26,'1','2018-06-14','1','1',42.00,NULL,213.00,0,'2018-06-14 06:31:07',0,'2018-06-14 06:31:08',1),(13,'2',27,'1','2018-06-15','1','1',50.00,NULL,50.00,0,'2018-06-14 06:32:06',0,'2018-06-14 06:32:07',2),(14,'2',26,'1','2018-06-15','1','1',200.00,NULL,30.00,0,'2018-06-14 06:32:06',0,'2018-06-14 06:32:07',1),(15,'3',27,'2','2018-06-16','2','2',60.00,NULL,300.00,0,'2018-06-14 06:32:47',0,'2018-06-14 06:32:48',2),(21,'4',27,'4','2018-06-21','4','4',490.00,NULL,100.00,0,'2018-06-14 08:15:50',0,'2018-06-14 08:15:50',2),(22,'4',26,'4','2018-06-21','4','4',500.00,NULL,20.00,0,'2018-06-14 08:15:50',0,'2018-06-14 08:15:50',1);
/*!40000 ALTER TABLE `joborder_production_has_deliverys` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`joborder_production_has_deliverys_AFTER_INSERT` AFTER INSERT ON `joborder_production_has_deliverys` FOR EACH ROW
BEGIN
	DECLARE qty_holder int;
	DECLARE total_qty int;
    SET @qty_holder = (SELECT sum(`qty`) FROM `joborder_production_has_deliverys` WHERE `id_joborder_production`=new.`id_joborder_production`);
    
		UPDATE `joborder_has_productions` SET `status`=1 WHERE `id`=new.`id_joborder_production`;
		UPDATE `joborder_has_productions` SET `status`=0 WHERE `id`=new.`id_joborder_production`;
    
    IF(new.`status`=1) THEN
		SET @total_qty = (SELECT `sd_weight` FROM `joborder_has_productions` WHERE `id`=new.`id_joborder_production`);
	ELSEIF(new.`status`=2) THEN
		SET @total_qty = (SELECT `bf_weight` FROM `joborder_has_productions` WHERE `id`=new.`id_joborder_production`);
	END IF;
    
    IF(@qty_holder=@total_qty) THEN
		UPDATE `joborder_has_productions` SET `status`=new.`status` WHERE `id`=new.`id_joborder_production`;
	ELSE
		UPDATE `joborder_has_productions` SET `status`=0 WHERE `id`=new.`id_joborder_production`;
	END IF;
    
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`joborder_production_has_deliverys_AFTER_DELETE` AFTER DELETE ON `joborder_production_has_deliverys` FOR EACH ROW
BEGIN

	DECLARE qty_holder int;
	DECLARE total_qty int;
    SET @qty_holder = (SELECT sum(`qty`) FROM `joborder_production_has_deliverys` WHERE `id_joborder_production`=old.`id_joborder_production`);
    
    
    IF(old.`status`=1) THEN
		SET @total_qty = (SELECT `sd_weight` FROM `joborder_has_productions` WHERE `id`=old.`id_joborder_production`);
	ELSEIF(old.`status`=2) THEN
		SET @total_qty = (SELECT `bf_weight` FROM `joborder_has_productions` WHERE `id`=old.`id_joborder_production`);
	END IF;
    
    IF(@qty_holder=@total_qty) THEN
		UPDATE `joborder_has_productions` SET `status`=old.`status` WHERE `id`=old.`id_joborder_production`;
	ELSE
		UPDATE `joborder_has_productions` SET `status`=0 WHERE `id`=old.`id_joborder_production`;
	END IF;
    
    
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `joborders`
--

DROP TABLE IF EXISTS `joborders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `joborders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_customer` int(11) NOT NULL DEFAULT '0',
  `id_product` int(11) NOT NULL DEFAULT '0',
  `id_customerpo_product` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `date` date NOT NULL DEFAULT '0000-00-00',
  `recieved_by` varchar(100) DEFAULT NULL,
  `noted_by` varchar(100) DEFAULT NULL,
  `thickness` double(11,2) DEFAULT NULL,
  `jo_number` varchar(100) DEFAULT NULL,
  `po_number` varchar(100) NOT NULL,
  `po_qty` int(11) NOT NULL,
  `barcode` varchar(100) DEFAULT NULL,
  `cylinder_number` varchar(100) NOT NULL DEFAULT '0',
  `weight_roll` varchar(100) NOT NULL DEFAULT '0',
  `meter_roll` varchar(100) NOT NULL DEFAULT '0',
  `winding_direction` int(11) NOT NULL DEFAULT '0',
  `prepared_by` varchar(100) DEFAULT NULL,
  `filled_up_by` varchar(100) DEFAULT NULL,
  `approved_by` varchar(100) DEFAULT NULL,
  `issued_by` int(11) DEFAULT NULL,
  `issued_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` int(11) DEFAULT NULL,
  `modified_on` timestamp NULL DEFAULT NULL,
  `conflicts` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `joborders`
--

LOCK TABLES `joborders` WRITE;
/*!40000 ALTER TABLE `joborders` DISABLE KEYS */;
INSERT INTO `joborders` VALUES (16,6,1,33,1,'2018-05-31','33','44',NULL,'22','230',555,'66','5','77','88',3,NULL,NULL,NULL,1,'2018-05-31 07:22:26',1,'2018-06-01 09:59:01',0),(17,6,3,34,1,'2018-12-01','b','c',100.00,'25','230',200,'666','0','555','777',2,'D','e','f',NULL,'2018-05-31 07:22:26',1,'2018-06-18 04:08:08',0),(33,5,3,52,1,'2018-05-31','23','23',1.00,'23','12333',250,'','0','0','0',0,'','','',1,'2018-06-04 09:00:35',1,'2018-06-05 03:38:36',0),(34,5,2,56,1,'2018-05-24','A','B',111.00,'222','12333',42,'44','0','22','33',0,'C','D','E',1,'2018-06-04 09:26:01',1,'2018-06-18 03:49:31',0),(37,1,2,62,0,'2018-06-18',NULL,NULL,NULL,NULL,'123abc',1,NULL,'0','0','0',0,NULL,NULL,NULL,1,'2018-06-18 04:55:02',NULL,NULL,0);
/*!40000 ALTER TABLE `joborders` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`joborders_AFTER_INSERT` AFTER INSERT ON `joborders` FOR EACH ROW
BEGIN

		UPDATE `products` set `conflicts`=`conflicts`+1 WHERE `id`=new.`id_product`;
		UPDATE `customers` set `conflicts`=`conflicts`+1 WHERE `id`=new.`id_customer`;
     
        INSERT INTO `joborder_has_plastics`(`id_joborder`,`id_plastic`,`id_product`,`issued_by`) 
	(SELECT `id_joborder`,`id_plastic`,`id_product`,`iby` FROM `product_has_plastics` PP
    JOIN (SELECT new.`id` 'id_joborder',new.`issued_by` 'iby')x WHERE  PP.`id_product`=new.`id_product`);
           
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`joborders_AFTER_UPDATE` AFTER UPDATE ON `joborders` FOR EACH ROW
BEGIN


	if(new.`recieved_by`!=old.`recieved_by` OR new.`noted_by`!=old.`noted_by` OR new.`jo_number`!=old.`jo_number` OR new.`barcode`!=old.`barcode` OR new.`cylinder_number`!=old.`cylinder_number` OR new.`weight_roll`!=old.`weight_roll` OR new.`meter_roll`!=old.`meter_roll` OR new.`winding_direction`!=old.`winding_direction` OR new.`prepared_by`!=old.`prepared_by` OR new.`filled_up_by`!=old.`filled_up_by` OR new.`approved_by`!=old.`approved_by` OR new.`status`!=old.`status`) then 
		insert into `joborder_has_logs`  
		(`issued_on`,`issued_by`,`id_joborder`,`recieved_by`,`noted_by`,`jo_number`,`barcode`,`cylinder_number`,`weight_roll`,`meter_roll`,`winding_direction`,`prepared_by`,`filled_up_by`,`approved_by`,`status`)
		values 
		(new.`modified_on`,new.`modified_by`,new.`id`,old.`recieved_by`,old.`noted_by`,old.`jo_number`,old.`barcode`,old.`cylinder_number`,old.`weight_roll`,old.`meter_roll`,old.`winding_direction`,old.`prepared_by`,old.`filled_up_by`,old.`approved_by`,old.`status`);
		DELETE FROM `joborder_has_productions` WHERE `id_joborder`=old.`id`;
    end if;
    

	if(new.`id_product`!=old.`id_product`) then
		UPDATE `products` set `conflicts`=`conflicts`+1 WHERE `id`=new.`id_product`;
		UPDATE `products` set `conflicts`=`conflicts`-1 WHERE `id`=old.`id_product`;
    end if;
    
	if(new.`id_customer`!=old.`id_customer`) then
		UPDATE `customers` set `conflicts`=`conflicts`+1 WHERE `id`=new.`id_customer`;
		UPDATE `customers` set `conflicts`=`conflicts`-1 WHERE `id`=old.`id_customer`;
    end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`joborders_AFTER_DELETE` AFTER DELETE ON `joborders` FOR EACH ROW
BEGIN

		DELETE FROM `joborder_has_logs` WHERE `id_joborder`=old.`id`;
		DELETE FROM `joborder_has_plastics` WHERE `id_joborder`=old.`id`;
		DELETE FROM `joborder_has_productions` WHERE `id_joborder`=old.`id`;
        
        
		UPDATE `products` set `conflicts`=`conflicts`-1 WHERE `id`=old.`id_product`;
		UPDATE `customers` set `conflicts`=`conflicts`-1 WHERE `id`=old.`id_customer`;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `plastic_has_logs`
--

DROP TABLE IF EXISTS `plastic_has_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plastic_has_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_plastic` int(11) NOT NULL,
  `specification` varchar(100) NOT NULL,
  `minimum` int(11) NOT NULL,
  `starting_qty` int(11) NOT NULL,
  `old_price` double(11,2) NOT NULL,
  `price` double(11,2) NOT NULL,
  `issued_by` int(11) DEFAULT NULL,
  `issued_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plastic_has_logs`
--

LOCK TABLES `plastic_has_logs` WRITE;
/*!40000 ALTER TABLE `plastic_has_logs` DISABLE KEYS */;
INSERT INTO `plastic_has_logs` VALUES (3,5,'Aluminum 1',100,100,100.00,100.00,1,'2018-06-14 08:06:14'),(4,2,'Plastic 1',1,1,1.00,12.00,1,'2018-06-18 04:50:53'),(5,2,'Plastic 1',12,12,12.00,12.00,1,'2018-06-20 05:39:49'),(6,11,'Plastic 12',123,123,123.00,123.00,1,'2018-06-20 05:39:55'),(7,3,'Plastic 2',2,2,2.00,2.00,1,'2018-06-20 05:40:06'),(8,8,'Plastic 2',123,123,123.00,123.00,1,'2018-06-20 05:40:20'),(9,9,'Plastic 2',123,123,123.00,123.00,1,'2018-06-20 05:40:30'),(10,5,'Plastic A',100,100,100.00,100.00,1,'2018-06-20 05:40:38'),(11,2,'Plastic 1',12,10,12.00,12.00,1,'2018-06-20 05:49:35');
/*!40000 ALTER TABLE `plastic_has_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plastics`
--

DROP TABLE IF EXISTS `plastics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plastics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `specification` varchar(100) NOT NULL,
  `minimum` int(11) NOT NULL,
  `starting_qty` int(11) NOT NULL,
  `price` double(11,2) NOT NULL,
  `issued_by` int(11) DEFAULT NULL,
  `issued_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` int(11) DEFAULT NULL,
  `modified_on` timestamp NULL DEFAULT NULL,
  `conflicts` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plastics`
--

LOCK TABLES `plastics` WRITE;
/*!40000 ALTER TABLE `plastics` DISABLE KEYS */;
INSERT INTO `plastics` VALUES (2,'Plastic 1',12,0,12.00,1,'2018-05-18 10:08:58',1,'2018-06-20 05:49:35',8),(3,'Plastic 22',2,12,2.00,0,'2018-05-18 10:09:09',1,'2018-06-20 05:40:06',11),(5,'Plastic A',100,101,100.00,1,'2018-06-14 08:02:33',1,'2018-06-20 05:40:38',1),(8,'Plastic 32',123,13,123.00,1,'2018-06-18 05:04:11',1,'2018-06-20 05:40:20',0),(9,'Plastic 42',123,14,123.00,1,'2018-06-18 05:04:55',1,'2018-06-20 05:40:30',0),(11,'Plastic 12',123,11,123.00,1,'2018-06-18 05:20:18',1,'2018-06-20 05:39:55',0),(12,'Plastic New',11,11,11.00,1,'2018-06-20 05:39:37',1,'2018-06-20 05:39:38',0);
/*!40000 ALTER TABLE `plastics` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`plastics_AFTER_INSERT` AFTER INSERT ON `plastics` FOR EACH ROW
BEGIN

	INSERT INTO `in_plastics`(`id_plastic`,`qty`) VALUES(new.`id`,new.`starting_qty`);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`plastics_AFTER_UPDATE` AFTER UPDATE ON `plastics` FOR EACH ROW
BEGIN

	if(new.`specification`!=old.`specification` OR new.`minimum`!=old.`minimum` OR new.`starting_qty`!=old.`starting_qty` OR new.`price`!=old.`price`) then 
	insert into `plastic_has_logs`  
	(`issued_on`,`issued_by`,`id_plastic`,`specification`,`minimum`,`starting_qty`,`old_price`,`price`)
	values 
	(new.`modified_on`,new.`modified_by`,new.`id`,old.`specification`,old.`minimum`,old.`starting_qty`,old.`price`,new.`price`);

		if((SELECT count(*) FROM `in_plastics` WHERE `id_plastic`=new.`id`)=0)then
			INSERT INTO `in_plastics`(`id_plastic`,`qty`,`issued_by`) VALUES(new.`id`,new.`starting_qty`,new.`modified_by`);
		else
			UPDATE `in_plastics` SET `qty`= (`qty`-old.`starting_qty`+new.`starting_qty`), `issued_by`=new.`modified_by` WHERE `id_plastic`=new.`id`;
		end if;
	end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`plastics_BEFORE_DELETE` BEFORE DELETE ON `plastics` FOR EACH ROW
BEGIN
	DELETE FROM `in_plastics` WHERE `id_plastic`=old.`id`;
	DELETE FROM `in_plastic_has_logs` WHERE `id_plastic`=old.`id`;
	DELETE FROM `plastic_has_logs` WHERE `id_plastic`=old.`id`;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `product_has_logs`
--

DROP TABLE IF EXISTS `product_has_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_has_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_product` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `id_customer` varchar(100) NOT NULL,
  `id_ink` json NOT NULL,
  `id_adhesive` json NOT NULL,
  `id_chemical` json NOT NULL,
  `id_cylinder` int(11) NOT NULL,
  `issued_by` int(11) DEFAULT NULL,
  `issued_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_has_logs`
--

LOCK TABLES `product_has_logs` WRITE;
/*!40000 ALTER TABLE `product_has_logs` DISABLE KEYS */;
INSERT INTO `product_has_logs` VALUES (1,1,'1','2018-05-23','6','[\"1\", \"2\"]','[\"4\"]','[\"2\"]',1,1,'2018-05-28 03:21:04'),(2,1,'Job Name 1','2018-05-23','6','[\"1\", \"2\"]','[\"4\"]','[\"2\"]',1,1,'2018-05-31 03:02:54'),(3,2,'Job Name 2','2018-05-28','1','[\"1\"]','[\"5\"]','[\"1\", \"2\"]',1,1,'2018-05-28 03:21:32'),(4,3,'Job Name 3','2018-05-28','6','[\"1\", \"2\"]','[\"4\"]','[\"1\"]',1,1,'2018-05-28 03:21:58'),(5,5,'Job Name 4','2018-05-31','1','[\"1\"]','[\"4\"]','[\"1\"]',1,1,'2018-05-31 07:48:44'),(6,1,'Job Name 1','2018-05-23','6','[\"1\", \"2\"]','[\"4\"]','[\"2\"]',2,1,'2018-06-14 09:21:41'),(7,6,'Sample Product Customer 1','2018-06-18','1','[\"1\"]','[\"4\"]','[\"1\"]',2,1,'2018-06-18 04:47:11'),(8,6,'Sample Product Customer 1','2018-06-19','1','[\"1\"]','[\"4\"]','[\"1\"]',2,1,'2018-06-18 04:47:16'),(9,6,'Sample Product Customer 11','2018-06-19','1','[\"1\"]','[\"4\"]','[\"1\"]',2,1,'2018-06-18 04:47:28');
/*!40000 ALTER TABLE `product_has_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_has_plastics`
--

DROP TABLE IF EXISTS `product_has_plastics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_has_plastics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_product` int(11) NOT NULL,
  `id_plastic` int(11) NOT NULL,
  `size` varchar(100) NOT NULL,
  `thickness` varchar(100) NOT NULL,
  `issued_by` int(11) DEFAULT NULL,
  `issued_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` int(11) DEFAULT NULL,
  `modified_on` timestamp NULL DEFAULT NULL,
  `conflicts` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_has_plastics`
--

LOCK TABLES `product_has_plastics` WRITE;
/*!40000 ALTER TABLE `product_has_plastics` DISABLE KEYS */;
INSERT INTO `product_has_plastics` VALUES (2,1,3,'2','2',1,'2018-05-23 09:11:48',1,'2018-05-23 09:11:49',0),(4,2,2,'1','1',1,'2018-05-28 03:21:31',1,'2018-05-28 03:21:32',0),(5,2,3,'2','2',1,'2018-05-28 03:21:31',1,'2018-05-28 03:21:32',0),(6,3,3,'3','3',1,'2018-05-28 03:21:58',1,'2018-05-28 03:21:58',0),(8,5,2,'1','1',1,'2018-05-31 07:48:44',1,'2018-05-31 07:48:45',0),(9,5,3,'2','2',1,'2018-05-31 07:48:44',1,'2018-05-31 07:48:45',0),(10,1,2,'42','555',1,'2018-06-04 09:24:58',1,'2018-06-04 09:24:58',0),(18,5,5,'q','q',1,'2018-06-14 08:15:55',1,'2018-06-14 08:15:56',0),(19,6,2,'1','1',1,'2018-06-18 04:47:04',1,'2018-06-18 04:47:05',0),(20,6,3,'2','2',1,'2018-06-18 04:47:39',1,'2018-06-18 04:47:39',0);
/*!40000 ALTER TABLE `product_has_plastics` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`product_has_plastics_AFTER_INSERT` AFTER INSERT ON `product_has_plastics` FOR EACH ROW
BEGIN
	UPDATE `plastics` set `conflicts`=`conflicts`+1 WHERE `id`=new.`id_plastic`;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`product_has_plastics_AFTER_UPDATE` AFTER UPDATE ON `product_has_plastics` FOR EACH ROW
BEGIN


	if(new.`id_plastic`!=old.`id_plastic` OR new.`size`!=old.`size` OR new.`thickness`!=old.`thickness`) then 
	insert into `product_plastic_has_logs`  
	(`issued_on`,`issued_by`,`id_product_plastic`,`id_plastic`,`size`,`thickness`)
	values 
	(new.`modified_on`,new.`modified_by`,new.`id`,old.`id_plastic`,old.`size`,old.`thickness`);

	end if;

	UPDATE `plastics` set `conflicts`=`conflicts`-1 WHERE `id`=old.`id_plastic`;
	UPDATE `plastics` set `conflicts`=`conflicts`+1 WHERE `id`=new.`id_plastic`;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`product_has_plastics_AFTER_DELETE` AFTER DELETE ON `product_has_plastics` FOR EACH ROW
BEGIN

	UPDATE `plastics` set `conflicts`=`conflicts`-1 WHERE `id`=old.`id_plastic`;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `product_plastic_has_logs`
--

DROP TABLE IF EXISTS `product_plastic_has_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_plastic_has_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_product_plastic` int(11) NOT NULL,
  `id_plastic` int(11) NOT NULL,
  `size` varchar(100) NOT NULL,
  `thickness` varchar(100) NOT NULL,
  `issued_by` int(11) DEFAULT NULL,
  `issued_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_plastic_has_logs`
--

LOCK TABLES `product_plastic_has_logs` WRITE;
/*!40000 ALTER TABLE `product_plastic_has_logs` DISABLE KEYS */;
INSERT INTO `product_plastic_has_logs` VALUES (1,1,2,'1','1',1,'2018-06-04 08:30:55');
/*!40000 ALTER TABLE `product_plastic_has_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `id_customer` varchar(100) NOT NULL,
  `id_ink` json NOT NULL,
  `id_adhesive` json NOT NULL,
  `id_chemical` json NOT NULL,
  `id_cylinder` int(11) NOT NULL,
  `issued_by` int(11) DEFAULT NULL,
  `issued_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` int(11) DEFAULT NULL,
  `modified_on` timestamp NULL DEFAULT NULL,
  `conflicts` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idproducts_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,'Job Name 1','2018-05-24','6','[\"1\", \"2\"]','[\"4\"]','[\"2\"]',2,1,'2018-05-23 09:11:48',1,'2018-06-14 09:21:41',7),(2,'Job Name 2','2018-05-28','1','[\"1\"]','[\"5\"]','[\"1\", \"2\"]',3,1,'2018-05-28 03:21:31',1,'2018-05-28 03:21:32',5),(3,'Job Name 3','2018-05-28','6','[\"1\", \"2\"]','[\"4\"]','[\"1\"]',2,1,'2018-05-28 03:21:58',1,'2018-05-28 03:21:58',4),(5,'Job Name 4','2018-05-31','1','[\"1\"]','[\"4\"]','[\"1\"]',2,1,'2018-05-31 07:48:44',1,'2018-06-14 08:15:56',0),(6,'Sample Product Customer 11','2018-06-19','6','[\"1\", \"2\"]','[\"4\", \"5\"]','[\"1\", \"2\"]',3,1,'2018-06-18 04:47:04',1,'2018-06-18 04:47:39',0),(7,'Product A','2018-06-18','6','[\"6\"]','[\"8\"]','[\"1\"]',2,1,'2018-06-18 04:56:16',1,'2018-06-18 05:14:13',0),(8,'Product B','2018-06-18','6','[]','[]','[]',4,1,'2018-06-18 05:14:35',1,'2018-06-18 05:14:35',0);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`products_AFTER_INSERT` AFTER INSERT ON `products` FOR EACH ROW
BEGIN

	/*Conflicts*/
			UPDATE `customers` set `conflicts`=`conflicts`+1 WHERE `id`=new.`id_customer`;
            
            
			UPDATE `inks` set `conflicts`=`conflicts`+1 WHERE not isNull((JSON_SEARCH(new.`id_ink`,'one',`id`)));
			UPDATE `adhesives` set `conflicts`=`conflicts`+1 WHERE not isNull((JSON_SEARCH(new.`id_adhesive`,'one',`id`)));
			UPDATE `chemicals` set `conflicts`=`conflicts`+1 WHERE not isNull((JSON_SEARCH(new.`id_chemical`,'one',`id`)));
            UPDATE `cylinders` set `conflicts`=`conflicts`+1 WHERE `id`=new.`id_cylinder`;
			#UPDATE `cylinders` set `conflicts`=`conflicts`+1 WHERE not isNull((JSON_SEARCH(new.`id_cylinder`,'one',`id`)));

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`products_AFTER_UPDATE` AFTER UPDATE ON `products` FOR EACH ROW
BEGIN

	if(new.`name`!=old.`name` OR new.`date`!=old.`date` OR new.`id_customer`!=old.`id_customer` OR new.`id_ink`!=old.`id_ink` OR new.`id_adhesive`!=old.`id_adhesive` OR new.`id_chemical`!=old.`id_chemical` OR new.`id_cylinder`!=old.`id_cylinder`) then 
	insert into `product_has_logs`  
	(`issued_on`,`issued_by`,`id_product`,`name`,`date`,`id_customer`,`id_ink`,`id_adhesive`,`id_chemical`,`id_cylinder`)
	values 
	(new.`modified_on`,new.`modified_by`,new.`id`,old.`name`,old.`date`,old.`id_customer`,old.`id_ink`,old.`id_adhesive`,old.`id_chemical`,old.`id_cylinder`);

	end if;


	/*Conflicts*/
        if(old.`id_customer`!=new.`id_customer`) then
			UPDATE `customers` set `conflicts`=`conflicts`-1 WHERE `id`=old.`id_customer`;
			UPDATE `customers` set `conflicts`=`conflicts`+1 WHERE `id`=new.`id_customer`;
		end if;
        
		if(old.`id_ink`!=new.`id_ink`) then
			UPDATE `inks` set `conflicts`=`conflicts`-1 WHERE not isNull((JSON_SEARCH(old.`id_ink`,'one',`id`)));
			UPDATE `inks` set `conflicts`=`conflicts`+1 WHERE not isNull((JSON_SEARCH(new.`id_ink`,'one',`id`)));
		end if;
        
		if(old.`id_adhesive`!=new.`id_adhesive`) then
			UPDATE `adhesives` set `conflicts`=`conflicts`-1 WHERE not isNull((JSON_SEARCH(old.`id_adhesive`,'one',`id`)));
			UPDATE `adhesives` set `conflicts`=`conflicts`+1 WHERE not isNull((JSON_SEARCH(new.`id_adhesive`,'one',`id`)));
		end if;
        
		if(old.`id_chemical`!=new.`id_chemical`) then
			UPDATE `chemicals` set `conflicts`=`conflicts`-1 WHERE not isNull((JSON_SEARCH(old.`id_chemical`,'one',`id`)));
			UPDATE `chemicals` set `conflicts`=`conflicts`+1 WHERE not isNull((JSON_SEARCH(new.`id_chemical`,'one',`id`)));
		end if;
        
		if(old.`id_cylinder`!=new.`id_cylinder`) then
            UPDATE `cylinders` set `conflicts`=`conflicts`+1 WHERE `id`=new.`id_cylinder`;
            UPDATE `cylinders` set `conflicts`=`conflicts`-1 WHERE `id`=old.`id_cylinder`;
			#UPDATE `cylinders` set `conflicts`=`conflicts`-1 WHERE not isNull((JSON_SEARCH(old.`id_cylinder`,'one',`id`)));
			#UPDATE `cylinders` set `conflicts`=`conflicts`+1 WHERE not isNull((JSON_SEARCH(new.`id_cylinder`,'one',`id`)));
		end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`products_AFTER_DELETE` AFTER DELETE ON `products` FOR EACH ROW
BEGIN
		DELETE FROM `product_has_logs` WHERE `id_product`=old.`id`; 
		DELETE FROM `product_has_plastics` WHERE `id_product`=old.`id`; 
    
	/*Conflicts*/
			UPDATE `customers` set `conflicts`=`conflicts`-1 WHERE `id`=old.`id_customer`;
			UPDATE `inks` set `conflicts`=`conflicts`-1 WHERE not isNull((JSON_SEARCH(old.`id_ink`,'one',`id`)));
			UPDATE `adhesives` set `conflicts`=`conflicts`-1 WHERE not isNull((JSON_SEARCH(old.`id_adhesive`,'one',`id`)));
			UPDATE `chemicals` set `conflicts`=`conflicts`-1 WHERE not isNull((JSON_SEARCH(old.`id_chemical`,'one',`id`)));
            UPDATE `cylinders` set `conflicts`=`conflicts`-1 WHERE `id`=old.`id_cylinder`;
			#UPDATE `cylinders` set `conflicts`=`conflicts`-1 WHERE not isNull((JSON_SEARCH(old.`id_cylinder`,'one',`id`)));
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `supplier_has_logs`
--

DROP TABLE IF EXISTS `supplier_has_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supplier_has_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_supplier` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `id_currency` int(11) NOT NULL,
  `contact_person` varchar(100) NOT NULL,
  `address` longtext NOT NULL,
  `balance` double(11,2) NOT NULL DEFAULT '0.00',
  `issued_by` int(11) DEFAULT NULL,
  `issued_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` int(11) DEFAULT NULL,
  `modified_on` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supplier_has_logs`
--

LOCK TABLES `supplier_has_logs` WRITE;
/*!40000 ALTER TABLE `supplier_has_logs` DISABLE KEYS */;
INSERT INTO `supplier_has_logs` VALUES (1,8,'Supplier JC1',13,'asd','asd',12312.00,1,'2018-06-14 07:36:01',NULL,NULL),(2,9,'Supplier JC2',8,'Jayce','Imus',85000.00,1,'2018-06-14 07:37:27',NULL,NULL),(3,9,'Supplier JC1',8,'Jayce','Imus',85000.00,1,'2018-06-14 07:37:36',NULL,NULL);
/*!40000 ALTER TABLE `supplier_has_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `suppliers`
--

DROP TABLE IF EXISTS `suppliers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `suppliers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `id_currency` varchar(45) NOT NULL,
  `contact_person` varchar(100) NOT NULL,
  `address` longtext NOT NULL,
  `balance` double(11,2) NOT NULL DEFAULT '0.00',
  `issued_by` int(11) DEFAULT NULL,
  `issued_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` int(11) DEFAULT NULL,
  `modified_on` timestamp NULL DEFAULT NULL,
  `conflicts` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `suppliers`
--

LOCK TABLES `suppliers` WRITE;
/*!40000 ALTER TABLE `suppliers` DISABLE KEYS */;
INSERT INTO `suppliers` VALUES (3,'Supplier 1','8','Supplier','Supplier',10000.00,0,'2018-05-18 07:43:31',NULL,NULL,0),(4,'Supplier 2','9','Supplier','Supplier',2000.00,0,'2018-05-18 07:43:31',1,'2018-06-14 09:03:31',0),(7,'Supplier JC1','9','JC','Dasma',150000.00,1,'2018-06-14 07:35:23',1,'2018-06-14 07:35:23',0),(8,'Supplier JC1','9','JC','asd',12312.00,1,'2018-06-14 07:35:42',1,'2018-06-14 07:36:33',0),(9,'Supplier JC2','8','Jayce','Imus',85000.00,0,'2018-06-14 07:37:20',1,'2018-06-14 07:37:36',0);
/*!40000 ALTER TABLE `suppliers` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`suppliers_AFTER_INSERT` AFTER INSERT ON `suppliers` FOR EACH ROW
BEGIN
    UPDATE `currencys` set `conflicts`=`conflicts`+1 WHERE `id`=new.`id_currency`;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`suppliers_AFTER_UPDATE` AFTER UPDATE ON `suppliers` FOR EACH ROW
BEGIN

	if(new.`name`!=old.`name` OR new.`contact_person`!=old.`contact_person` OR new.`address`!=old.`address` OR new.`balance`!=old.`balance` OR new.`id_currency`!=old.`id_currency`) then 
	insert into `supplier_has_logs`  
	(`issued_on`,`issued_by`,`id_supplier`,`name`,`contact_person`,`address`,`balance`,`id_currency`)
	values 
	(new.`modified_on`,new.`modified_by`,new.`id`,old.`name`,old.`contact_person`,old.`address`,old.`balance`,old.`id_currency`);

	end if;


	/*Conflicts*/
    if(old.`id_currency`!=new.`id_currency`) then
		UPDATE `currencys` set `conflicts`=`conflicts`-1 WHERE `id`=old.`id_currency`;
		UPDATE `currencys` set `conflicts`=`conflicts`+1 WHERE `id`=new.`id_currency`;
	end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `golden_sun`.`suppliers_AFTER_DELETE` AFTER DELETE ON `suppliers` FOR EACH ROW
BEGIN

	DELETE FROM `supplier_has_logs` WHERE `id_supplier`=old.`id`;
    UPDATE `currencys` set `conflicts`=`conflicts`-1 WHERE `id`=old.`id_currency`;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Dumping events for database 'golden_sun'
--

--
-- Dumping routines for database 'golden_sun'
--
/*!50003 DROP PROCEDURE IF EXISTS `sp_split` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_split`(IN toSplit text, IN target char(255))
BEGIN
	# Temp table variables
	SET @tableName = 'tmpSplit';
	SET @fieldName = 'variable';

	# Dropping table
	SET @sql := CONCAT('DROP TABLE IF EXISTS ', @tableName);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;

	# Creating table TEMPORARY
	SET @sql := CONCAT('CREATE TABLE ', @tableName, ' (', @fieldName, ' VARCHAR(1000))');
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;

	# Preparing toSplit
	SET @vars := toSplit;
	SET @vars := CONCAT("('", REPLACE(@vars, ", ", "'),('"), "')");

	# Inserting values
	SET @sql := CONCAT('INSERT INTO ', @tableName, ' VALUES ', @vars);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;

	# Returning record set, or inserting into optional target
	IF target IS NULL THEN
		SET @sql := CONCAT('SELECT TRIM(`', @fieldName, '`) AS `', @fieldName, '` FROM ', @tableName);
	ELSE
		SET @sql := CONCAT('INSERT INTO ', target, ' SELECT TRIM(`', @fieldName, '`) FROM ', @tableName);
	END IF;

	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
		
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-20 13:56:14
