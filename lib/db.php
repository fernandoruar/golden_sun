<?php

include 'medoo.php';
global $database;
$server_host = explode(":", $_SERVER['HTTP_HOST']);
global $db_name;
global $session_name;
global $host_port;

$host_port = $server_host[1];


if ($server_host[1] == 1000) {
    $db_name = "golden_sun";
    $session_name = "golden_sun_user";
}

$database = new medoo([
    'database_type' => 'mysql',
    'database_name' => $db_name,
    'server' => 'localhost',
    'username' => 'root',
    'password' => '',
        ]);
		
function getUser($key = null) {
    $user = fromSession("user");
    if (isset($user)) {
        if ($key == null) {
            return $user;
        } else {
            if (isset($user[$key])) {
                return $user[$key];
            } else {
                return null;
            }
        }
    } else {
        return null;
    }
}

function getTableName() {
    global $db_name;
    return $db_name;
}

function getSessionName() {
    global $session_name;
    return $session_name;
}

function getHost_name() {
    global $host_port;
    return $host_port;
}

function getImage_name($arr) {
    $image_name = "";
    if ($arr["image"] != "") {
        if ($arr["type"] == 1) {
            $image_name = "http://" . getInit("image_link") . $arr["id_product"] . "/" . $arr["image"];
        } else if ($arr["type"] == 2) {
            $image_name = "http://" . getInit("image_link") . "{$arr["id_product"]}/colors/{$arr["id_color"]}/" . $arr["image"];
        } else if ($arr["type"] == 3) {
            $image_name = "http://" . getInit("image_link") . "{$arr["id_product"]}/sizes/{$arr["id_size"]}/" . $arr["image"];
        } else if ($arr["type"] == 4) {
            $image_name = "http://" . getInit("image_link") . "{$arr["id_product"]}/colors/{$arr["id_color"]}/sizes/{$arr["id_size"]}/" . $arr["image"];
        }
    }
    return $image_name;
}

function permission_dropdown($arr) {
    $check = -1;
    $check_arr = prepareTable("select

coalesce(json_unquote(json_extract(json_extract(`permission`,substring_index(json_unquote(json_search(`permission`
,'one',?,null,'$**.id')),'.',1)
),'$.value')),-1) 'check'
from `employees`

where `id`=?", $arr);
    if (count($check_arr) > 0) {
        $check = $check_arr[0]["check"];
    }
    return json_decode($check);
}

function check_permission($arr) {
    $check = 0;
    if ($arr[0] != "") {
        $check_arr = prepareTable("select


(case when isNull(json_search(`permission`,'one',?)) then 0 else 1 end) 'check'
from `employees`

where `id`=?", $arr);
        if (count($check_arr) > 0) {
            $check = $check_arr[0]["check"];
        }
    }
    return $check;
}

function getInit($id) {
    global $host_port;
    $init = [
        "localhost" => $_SERVER['HTTP_HOST'],
        "dir" => $_SERVER['DOCUMENT_ROOT'],
        "product_image" => "./public/products/" . $host_port . "/",
        "image_link" => "localhost:" . $host_port . "/public/products/" . $host_port . "/",
        "image_replace" => "localhost:" . $host_port
    ];
    return $init[$id];
}

//Explicit Commands
function info() {
    global $database;
    print_r($database->info());
}

function currentdatetime() {
    date_default_timezone_set('Asia/Manila');
    return date("YmdHis");
}

function allowOrDie($data) {
    $user = fromSession("user");
    if ($data == "user") {
        if ($user == null) {
            sendTo("logout");
        }
    } else if (!in_array($user["position"], $data)) {
        sendTo("logout");
    }
}

function linkTo($data) {
    if ($data == "back") {
        return $_SERVER['HTTP_REFERER'];
    } else {
        return "http://" . getInit('localhost') . "/" . $data;
    }
}

function sendTo($data) {
    if ($data == "back") {
        header('Location: ' . $_SERVER['HTTP_REFERER']);
    } else {
        header("Location: http://" . getInit('localhost') . "/" . $data);
    }
    die();
}

function getPage($data) {
    $dir = $_SERVER['DOCUMENT_ROOT'];
    include $dir . "/view/" . $data . ".php";
}

function linkPage($data) {
    $dir = $_SERVER['DOCUMENT_ROOT'];
    return $dir . "/view/" . $data . ".php";
}

function linkPublic($data) {
    $dir = $_SERVER['DOCUMENT_ROOT'];
    return "http://" . getInit("localhost") . "/public/" . $data;
}

//////////////////////////
// ADMIN FUNCTIONS
//////////////////////////

function randomKey() {
    $key = "";
    $salt = rand(100000, 999999);
    $x = 0;
    while ($x < 1) {
        $key = md5(sha1(crypt($key, $salt)));
        $x++;
    }
    return $key;
}

function selectTable($table, $data = null) {
    global $database;

    if ($data == null) {
        $data = $database->select($table, "*");
    } else {
        $data = $database->select($table, "*", $data);
    }
    if (!isset($data)) {
        return [];
    } else {
        return $data;
    }
}

function selectTableQ($query) {
    global $database;
    $data = $database->selectQ($query);

    if (!isset($data)) {
        return [];
    } else {
        return $data;
    }
}

function prepareTable($query, $array) {
    global $database;
    return $database->preparedStatement($query, $array);
}

function deleteTable($table, $data) {
    global $database;

    if (is_array($data)) {
        $data = $database->delete($table, $data);
    } else {
        $data = $database->delete($table, ["id" => $data]);
    }
    if (!isset($data)) {
        return null;
    } else {
        return $data;
    }
}

function getTable($table, $data) {
    global $database;
    try {
        if (is_array($data)) {
            $data = $database->get($table, "*", $data);
        } else {
            $data = $database->get($table, "*", ["id" => $data]);
        }
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
    if (!isset($data)) {
        return null;
    } else {
        return $data;
    }
}

function insertTable($table, $data) {
    global $database;
    $data = $database->insert($table, $data);
    if (!isset($data)) {
        return null;
    } else {
        return $data;
    }
}

function updateTable($table, $data, $id) {
    global $database;
    $data["date_updated"] = currentdatetime();
    if (is_array($data) && !is_array($id)) {
        $data = $database->update($table, $data, ["id" => $id]);
    } else if (is_array($data) && is_array($id)) {
        $data = $database->update($table, $data, $id);
    } else {
        $data = "invalid parameters";
    }
    if (!isset($data)) {
        return null;
    } else {
        return $data;
    }
}

//////////////////////////
// HELPER FUNCTIONS
//////////////////////////

function toSession($name, $data) {
    if (session_status() == 1) {
        session_start();
    }
    $_SESSION[$name] = $data;
    return $_SESSION[$name];
}

function fromSession($name) {
    if (session_status() == 1) {
        session_start();
    }
    if (isset($_SESSION[$name])) {
        return $_SESSION[$name];
    } else {
        return null;
    }
}

function stopSession() {
    session_cache_expire();
    session_destroy();
    toSession("user", null);
}

function getGet($key = null) {
    if (isset($_GET)) {
        $g = $_GET;
        if ($key != null) {
            if (isset($_GET[$key])) {
                return $g[$key];
            } else {
                return null;
            }
        } else {
            return $g;
        }
    } else {
        return [];
    }
}

function getPost($key = null) {
    if (isset($_POST)) {
        $p = $_POST;
        if ($key != null) {
            if (isset($_POST[$key])) {
                return $p[$key];
            } else {
                return null;
            }
        } else {
            return $p;
        }
    } else {
        return [];
    }
}

function fileUpload($dir, $file, $id) {
    $target_dir = "./public/" . $dir;
    $name = "picture." . explode(".", basename($file["name"]))[1];
    $uploadfile = $target_dir . $id . "_" . $name;
//    $uploadfile = $target_dir . $name;
//    $fileType = pathinfo($uploadfile, PATHINFO_EXTENSION);
    try {
        if (move_uploaded_file($file['tmp_name'], $uploadfile)) {
            //echo "File is valid, and was successfully uploaded.\n";
        } else {
            //echo "Possible file upload attack!\n";
            //  echo move_uploaded_file($file['tmp_name'], $uploadfile);
        }
    } catch (Exception $exc) {
        print_r($_FILES);
        echo $exc->getTraceAsString();
    }

    return $id . "_" . $name;
}

function transactStatement($query, $array) {
    global $database;
    return $database->transactions($query, $array);
}
