<style>
    .solid_bottom {
        height: 20px;
        margin-top: 8px;
        font-size: 15px;
        padding-left: 10px;
    }
    .table_set {
        display: table;
    }
    .portrait {
        max-width: 100%;
    }
    .h20 {
        height: 20px;
    }
    @media print {
        @page {
            size: A4 portrait;
        }
    }
</style>
<div class="sheet_container portrait">
    <div class="row">
        <div class="col-xs-12">
            <h2 class="text-center"><b>SUMMARY REPORT</b></h2>
        </div>
        <div class="col-xs-12">
            <table class="mt10">
                <tr>
                    <td>
                        <label>COMPANY : </label>
                    </td>
                    <td>
                        <div type="text" class="solid_bottom" style="width:400px"><p>Test</p></div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>SIZE / ROLLS : </label>
                    </td>
                    <td>
                        <div type="text" class="solid_bottom" style="width:400px"><p>Test</p></div>
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-xs-4">
            <div class="b_all p_all_5" style="height:470px; padding-top:21px;">
                PRINTING OPERATOR
                <div class="row">
                    <div class="col-xs-12">
                        <table class="w_100" style="display:table;">
                            <tr>
                                <td width="30%"></td>
                                <td width="35%"><label>Kls.</label></td>
                                <td width="35%"><label>Pesos</label></td>
                            </tr>
                            <tr>
                                <td><fl>PRIMARY</fl></td>
                                <td class="solid_bottom"><div type="text" class="mt10"><p>Test</p></div></td>
                                <td class="solid_bottom"><div type="text" class="mt10"><p>Test</p></div></td>
                            </tr>
                            <tr>
                                <td><fl>COLOR</fl></td>
                                <td class="solid_bottom"><div type="text" class="mt10"><p>Test</p></div></td>
                                <td class="solid_bottom"><div type="text" class="mt10"><p>Test</p></div></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><label style="margin-top:20px;">Kls.</label></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td><fl>PAINT</fl></td>
                                <td class="solid_bottom"><div type="text" class="mt10"><p>Test</p></div></td>
                                <td class="solid_bottom"><div type="text" class="mt10"><p>Test</p></div></td>
                            </tr>
                            <tr>
                                <td><fl>SOLVENT</fl></td>
                                <td class="solid_bottom"><div type="text" class="mt10"><p>Test</p></div></td>
                                <td class="solid_bottom"><div type="text" class="mt10"><p>Test</p></div></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><label style="margin-top:20px;">SUM</label></td>
                                <td><label>SUM</label></td>
                            </tr>
                            <tr>
                                <td><fl>TOTAL</fl></td>
                                <td class="solid_bottom"><div type="text" class="mt10"><p>Test</p></div></td>
                                <td class="solid_bottom"><div type="text" class="mt10"><p>Test</p></div></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-4">
            <div class="b_all p_all_5" style="height:470px; padding-top:21px;">
                LAMINATION OPERATOR
                <div class="row">
                    <div class="col-xs-12">
                        <table class="w_100" style="display:table;">
                            <tr>
                                <td width="30%"></td>
                                <td width="35%"><label>Kls.</label></td>
                                <td width="35%"><label>Pesos</label></td>
                            </tr>
                            <tr>
                                <td><fl>COLOR FILM</fl></td>
                                <td class="solid_bottom"><div type="text" class="mt10"><p>Test</p></div></td>
                                <td class="solid_bottom"><div type="text" class="mt10"><p>Test</p></div></td>
                            </tr>
                            <tr>
                                <td><fl>CPP /VMCPP</fl></td>
                                <td class="solid_bottom"><div type="text" class="mt10"><p>Test</p></div></td>
                                <td class="solid_bottom"><div type="text" class="mt10"><p>Test</p></div></td>
                            </tr>
                            <tr>
                                <td><fl>VMPET</fl></td>
                                <td class="solid_bottom"><div type="text" class="mt10"><p>Test</p></div></td>
                                <td class="solid_bottom"><div type="text" class="mt10"><p>Test</p></div></td>
                            </tr>
                            <tr>
                                <td><fl>PE</fl></td>
                                <td class="solid_bottom"><div type="text" class="mt10"><p>Test</p></div></td>
                                <td class="solid_bottom"><div type="text" class="mt10"><p>Test</p></div></td>
                            </tr>
                            <tr>
                                <td><fl>FINISH</fl></td>
                                <td class="solid_bottom"><div type="text" class="mt10"><p>Test</p></div></td>
                                <td class="solid_bottom"><div type="text" class="mt10"><p>Test</p></div></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><label style="margin-top:20px;">Kls.</label></td>
                                <td><label>Pesos</label></td>
                            </tr>
                            <tr>
                                <td><fl>GLUE / HD</fl></td>
                                <td class="solid_bottom"><div type="text" class="mt10"><p>Test</p></div></td>
                                <td class="solid_bottom"><div type="text" class="mt10"><p>Test</p></div></td>
                            </tr>
                            <tr>
                                <td><fl>EAC</fl></td>
                                <td class="solid_bottom"><div type="text" class="mt10"><p>Test</p></div></td>
                                <td class="solid_bottom"><div type="text" class="mt10"><p>Test</p></div></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><label style="margin-top:20px;">SUM</label></td>
                                <td><label>SUM</label></td>
                            </tr>
                            <tr>
                                <td><fl>TOTAL</fl></td>
                                <td class="solid_bottom"><div type="text" class="mt10"><p>Test</p></div></td>
                                <td class="solid_bottom"><div type="text" class="mt10"><p>Test</p></div></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-4">
            <div class="b_all p_all_5" style="height:470px;">
                <div class="row">
                    <div class="col-xs-12">
                        <table class="w_100" style="display:table;">
                            <tr>
                                <td colspan="3">
                                    <div style="width:100px; display:inline;">SLITTING</div> 
                                    <div id="slitting_val" class="h30 solid_bottom"><fl>Long Name Long Name</fl></div>
                                </td>
                            </tr>
                            <tr>
                                <td width="10%"></td>
                                <td width="45%"><label>ROLLS</label></td>
                                <td width="45%"><label>KGS</label></td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td class="solid_bottom"><div type="text" class="mt10"><p>Test</p></div></td>
                                <td class="solid_bottom"><div type="text" class="mt10"><p>Test</p></div></td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td class="solid_bottom"><div type="text" class="mt10"><p>Test</p></div></td>
                                <td class="solid_bottom"><div type="text" class="mt10"><p>Test</p></div></td>
                            </tr>
                            <tr>
                                <td colspan="3"><hr class="mt40 hid"></td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <div style="width:100px; display:inline;">DELIVERY NO.</div> 
                                    <div id="deliverynum_val" class="h30 solid_bottom"><fl>123456789123465789</fl></div>
                                </td>
                            </tr>
                            <tr>
                                <td width="10%"></td>
                                <td width="45%"><label>ROLLS</label></td>
                                <td width="45%"><label>KGS</label></td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td class="solid_bottom"><div type="text" class="mt10"><p>Test</p></div></td>
                                <td class="solid_bottom"><div type="text" class="mt10"><p>Test</p></div></td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td class="solid_bottom"><div type="text" class="mt10"><p>Test</p></div></td>
                                <td class="solid_bottom"><div type="text" class="mt10"><p>Test</p></div></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>