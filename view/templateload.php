<?php

include "./lib/language.php";
if ($head == 'header') {

	$admin_name = '';
	$admin_id = '';
	if(!isset($_SESSION[getSessionName()])) {
	  header('Location: /login');
	  exit();
	} else {
	  $admin_id =$_SESSION[getSessionName()]['id'];
	  $admin_name = $_SESSION[getSessionName()]['name'];
	}
    include linkPage("template/header");
    include linkPage("$tablename");
    include linkPage("template/footer");
    include "./main_js/main.php";
    include "./main_js/$tablename.php";

    
} 

elseif ($head == 'header2') {

	$admin_name = '';
	$admin_id = '';
	if(!isset($_SESSION[getSessionName()])) {
	  header('Location: /login');
	  exit();
	} else {
	  $admin_id =$_SESSION[getSessionName()]['id'];
	  $admin_name = $_SESSION[getSessionName()]['name'];
	}
    include linkPage("template/header2");
    include linkPage("$tablename");
    include linkPage("template/footer2");
    include "./main_js/main.php";
    include "./main_js/$tablename.php";

    
} 

else if ($head == 'print_header') {
    include linkPage("template/print_header");
    include linkPage("$tablename");
    include linkPage("template/print_footer");
    include "./main_js/$tablename.php";
    
} 

else if ($head == 'login_header') {
    include linkPage("template/login_header");
    include linkPage("$tablename");
    include linkPage("template/login_footer");
    include "./main_js/main.php";
    include "./main_js/$tablename.php";
    
} 


?>


