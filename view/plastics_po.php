<div class="content-wrapper">
    <section class="content" id="section" data="50"> 
        <div class="row">
            <div class="col-md-12">
                <div class="box box-default">
                    <div class="box-header">
                        <h3 class="box-title"><?=$inventory.' - '.$plastics?></h3>
                    </div>
                    <div class="box-body">
						<div class="tab-container tabs-box">
							<ul class="nav nav-tabs">
								<li class="active"><a href="#details" data-toggle="tab"><?= $details ?></a></li>
								<li><a href="#logs" data-toggle="tab"><?= $logs ?></a></li>
							</ul>
							<div class="tab-content">
								<div id="details" class="tab tab-pane fade in active">
									<div class="customtable mt20">
										<div class="table-scroll">
											<div class="table-height">
												<table data-order='[["1","asc"]]' name="dataTable" id="table_plastic_po" class="table dataTable table-bordered table-striped table-hover">
													<thead>
														<th data-class-name="display_col" width="10%" class="nsearch norder" ></th>
														<th data-class-name="text-center" width="10%"><?= $name ?></th>
														<th data-class-name="text-center" width="10%"><?= $qty?></th>
														<th data-class-name="display_col" width="10%" class="norder nsearch"><?= $actions ?></th>
													</thead>
													<tbody>
													</tbody>
												</table>
											</div>                   
										</div>                                     
									</div>
								</div>
								<div id="logs" class="tab tab-pane fade">
									<div class="customtable mt20">
										<div class="button-box">
											<button id="abutton_multiple" type="button" class="btn"><img src="./public/images/multiple.png" class="multiplebtn"/></button>
										</div>
										<div class="table-scroll">
											<div class="table-height">
												<div class='alert alert-danger' style="display:none">
													<a class='close'>&times;</a>
													<center><span></span></center>
												</div>
												<table data-order='[["1","asc"]]' name="dataTable" id="table_plastic_logs" class="table dataTable table-bordered table-striped table-hover">
													<thead>
														<th data-class-name="display_col" width="10%" class="nsearch norder" ></th>
														<th data-class-name="text-center" width="10%"><?= $date ?></th>
														<th data-class-name="text-center" width="10%"><?= $name ?></th>
														<th data-class-name="text-center" width="10%"><?= $type ?></th>
														<th data-class-name="text-center" width="10%"><?= $initial_qty?></th>
														<th data-class-name="text-center" width="10%"><?= $qty?></th>
														<th data-class-name="text-center" width="10%"><?= $final_qty?></th>
														<th data-class-name="display_col" width="10%" class="norder nsearch"><?= $actions ?></th>
													</thead>
													<tbody>
													</tbody>
												</table>
											</div>                   
										</div>                                     
									</div>
								</div>
							</div>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
</div>


<div class="modal fade lgModal" id="dataModal_logs" role="dialog">
    <div class="modal-dialog modal-lg " style='width:1300px'>

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" name="title"></h4>
            </div>
            <div class="modal-body">
                <center class="color-red" style="margin-top:10px;"><i class="fa fa-asterisk"></i>&nbsp;<?= $required_fields ?></center>

                <div name="field_container">
                    <div class='alert alert-danger' style="display:none">
                        <a class='close'>&times;</a>
                        <center><span></span></center>
                    </div>
                    <table class="table table-form">
                        <tr>
                            <td><input type='radio' data_val='0' name='inout' class='pull-right'></td>
                            <td><strong><?= $in ?>:&nbsp;</strong></td>
                        </tr>
                        <tr>
                            <td><input type='radio' data_val='1' name='inout' class='pull-right'></td>
                            <td><strong><?= $out ?>:&nbsp;</strong></td>
                        </tr>
                        <tr>
                            <td colspan='2'><strong><?= $name ?><span class="color-red">*</span>:&nbsp;</strong></td>
                            <td><select id='items' class="unique select2-field ajax"  data-placeholder="<?= $cplastic ?>"></select></td>
                            <td><strong><?= $qty ?><span class="color-red">*</span>:&nbsp;</strong></td>
                            <td><input  class="form-control score" type="number"></td>
                            <td><button id="add_multiple" class="btn btn-primary"><i class="fa fa-plus-circle"></i>&nbsp;<?= $add ?></button></td>
                        </tr>

                    </table>
                </div>
                <div class="table-responsive table-scroll table-container">
                    <div class="table-height">
                        <div class='alert alert-danger' style="display:none">
                            <a class='close'>&times;</a>
                            <center><span></span></center>
                        </div>


                        <table id="table_multiple" class="table table-bordered table-hover table-striped table_style">
                            <thead>
                            <th class="norder nsearch" data-class-name="display_col">ID</th>
                            <th width="10%"><?= $name ?></th>
                            <th class="norder nsearch" data-class-name="display_col"></th>
                            <th width="10%"><?= $qty ?></th>
                            <th width="5%"><?= $actions ?></th>
                            </thead>

                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">              


                <button type="button"  class="esc_btn btn btn-danger pull-left"  data-dismiss="modal"><i class="fa fa-times"></i> &nbsp;<?= $close ?></button>
                <button type="button" id="save_multiple" class="save_btn btn btn-success pull-right"><i class="fa fa-save"></i> <?= $save ?>
                </button>

            </div>

        </div>
    </div>
</div>


<div id="deleteModal" class="modal fade custom-modal primarymodal" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
			<div class='alert alert-danger' style="display:none">
				<a class='close'>&times;</a>
				<center><span></span></center>
			</div>
            <div class="modal-body">
                <h4 name='message'></h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-flat btn-sm pull-left" data-dismiss="modal"><i class="fa fa-close"></i>&nbsp;<?=$cancel?></button><button type="button" id='delete_yes' class="btn btn-success btn-flat btn-sm pull-right"><i class="fa fa-save"></i>&nbsp;<?=$yes?></button>
            </div>
        </div>
    </div>
</div>