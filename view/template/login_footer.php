</div>
</main>

<script>
    $('.lang').on('click', function () {
        chooseLanguage($(this).attr("value"));
    });

    function chooseLanguage(id) {

        $.post("/changeLanguage", {id: id}, function (data) {
            if (data === 1) {
                document.location.reload();
            }

        }, 'json');
    }
    window.onkeypress = function (event) {

        if (event.keyCode == 13) {
            $('#sin').click();
        }

    }
    function directSupplierProduct() {
        window.open("/change_supplier_product");
    }
    function directProduct() {
        window.open("/create_product");
    }
    $(".toggle-side").click(function () {
        $(".cd-side-nav").toggle();

        if ($("#section").attr('data') == '50') {
            $("#section").attr('data', '100');
            $("#section").css('width', '100%');
            $(".cd-main-content .content-wrapper").css({'margin-left': '0', 'padding-left': '0', 'padding-right': '0'});
        } else {
            $("#section").attr('data', '50');
            $("#section").css('width', '100%');
            $(".cd-main-content .content-wrapper").css({'margin-left': '110px', 'padding-left': '5px', 'padding-right': '5px'});
        }
    });

    if (screen.width < 500) {

        $("body").addClass("nohover");
        $("td, th")
                .attr("tabindex", "1")
                .on("touchstart", function () {
                    $(this).focus();
                });

    }

    $(".sidebar-toggle").click(function (e) {
        e.preventDefault();
        $("nav.sidebar, .main-header, .content-wrapper").toggleClass("active");
    });



    var $wrapper = $('#wrapper');

    // theme switcher
    var theme_match = String(window.location).match(/[?&]theme=([a-z0-9]+)/);
    var theme = (theme_match && theme_match[1]) || 'default';
    var themes = ['default', 'legacy', 'bootstrap2', 'bootstrap3'];
    $('head').append('<link rel="stylesheet" href="../public/css/selectize.' + theme + '.css">');

    var $themes = $('<div>').addClass('theme-selector').insertAfter('h1');
    for (var i = 0; i < themes.length; i++) {
        $themes.append('<a href="?theme=' + themes[i] + '"' + (themes[i] === theme ? ' class="active"' : '') + '>' + themes[i] + '</a>');
    }

    // display scripts on the page
    $('script', $wrapper).each(function () {
        var code = this.text;
        if (code && code.length) {
            var lines = code.split('\n');
            var indent = null;

            for (var i = 0; i < lines.length; i++) {
                if (/^[	 ]*$/.test(lines[i]))
                    continue;
                if (!indent) {
                    var lineindent = lines[i].match(/^([ 	]+)/);
                    if (!lineindent)
                        break;
                    indent = lineindent[1];
                }
                lines[i] = lines[i].replace(new RegExp('^' + indent), '');
            }

            var code = $.trim(lines.join('\n')).replace(/	/g, '    ');
            var $pre = $('<pre>').addClass('js').text(code);
            $pre.insertAfter(this);
        }
    });

    // show current input values
    $('select.selectized,input.selectized', $wrapper).each(function () {
        var $container = $('<div>').addClass('value').html('Current Value: ');
        var $value = $('<span>').appendTo($container);
        var $input = $(this);
        var update = function (e) {
            $value.text(JSON.stringify($input.val()));
        }

        $(this).on('change', update);
        update();

        $container.insertAfter($input);
    });


// CLEARABLE INPUT
    function tog(v) {
        return v ? 'addClass' : 'removeClass';
    }
    $(document).on('input', '.clearable', function () {
        $(this)[tog(this.value)]('x');
    }).on('mousemove', '.x', function (e) {
        $(this)[tog(this.offsetWidth - 18 < e.clientX - this.getBoundingClientRect().left)]('onX');
    }).on('touchstart click', '.onX', function (ev) {
        ev.preventDefault();
        $(this).removeClass('x onX').val('').change();
    });





    function mobileViewUpdate() {
        var viewportWidth = $(window).width();
        if (viewportWidth > 1910) {
            $(".table-scroll").removeClass("table-responsive");
        }
    }

    $(window).load(mobileViewUpdate);
    $(window).resize(mobileViewUpdate);


</script>
</body>
</html>
