<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta charset="utf-8" />
        <title><?=$module_name.' - '.$ctitle?></title>
        <meta name="description" content="overview &amp; stats" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
        <link rel="shortcut icon" type="image/png" href="/favicon2.png"/>
        <link rel="shortcut icon" type="image/png" href="http://<?=getInit('localhost')?>/public/images/favicon2.png"/>
        <link rel="apple-touch-icon" sizes="72x72" href="http://<?=getInit('localhost')?>/public/images/favicon2.png">
        <link rel="apple-touch-icon" sizes="114x114" href="http://<?=getInit('localhost')?>/public/images/favicon2.png">

        <link href="http://<?=getInit('localhost')?>/public/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="http://<?=getInit('localhost')?>/public/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <link href="http://<?=getInit('localhost')?>/public/css/style_admin.css" rel="stylesheet" type="text/css"/>
        <link href="http://<?=getInit('localhost')?>/public/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
        <link href="http://<?=getInit('localhost')?>/public/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css"/>
        <link href="http://<?=getInit('localhost')?>/public/css/dropzone.css" rel="stylesheet" type="text/css"/>
        <link href="http://<?=getInit('localhost')?>/public/css/bootstrap-toggle.min.css" rel="stylesheet" type="text/css"/>
        <link href="http://<?=getInit('localhost')?>/public/css/bootstrap-tagsinput.css" rel="stylesheet" type="text/css"/>
        <link href="http://<?=getInit('localhost')?>/public/css/loading.min.css" rel="stylesheet" type="text/css"/>
        <link href="http://<?=getInit('localhost')?>/public/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="http://<?=getInit('localhost')?>/public/icomoon/styles.css" rel="stylesheet" type="text/css"/>
        <link href="http://<?=getInit('localhost')?>/public/css/custom_style.css" rel="stylesheet" type="text/css"/>

        <script src="http://<?=getInit('localhost')?>/public/js/jquery-2.1.1.js" type="text/javascript"></script>
        <script src="http://<?=getInit('localhost')?>/public/js/jquery-ui.js" type="text/javascript"></script>
        <script src="http://<?=getInit('localhost')?>/public/js/jquery.menu-aim.js" type="text/javascript"></script>
        <script src="http://<?=getInit('localhost')?>/public/js/dropzone.js" type="text/javascript"></script>
        <script src="http://<?=getInit('localhost')?>/public/js/jquery.dataTables.min.js" type="text/javascript"></script>
        <script src="http://<?=getInit('localhost')?>/public/js/moment.min.js" type="text/javascript"></script>
        <script src="http://<?=getInit('localhost')?>/public/js/bootstrap-multiselect.js" type="text/javascript"></script>
        <script src="http://<?=getInit('localhost')?>/public/js/bootstrap-tagsinput.min.js" type="text/javascript"></script>
        <script src="http://<?=getInit('localhost')?>/public/js/highcharts.js" type="text/javascript"></script>
        <script src="http://<?=getInit('localhost')?>/public/js/exporting.js" type="text/javascript"></script>
        <script src="http://<?=getInit('localhost')?>/public/js/classie.js" type="text/javascript"></script>
        <script src="http://<?=getInit('localhost')?>/public/js/bootstrap-toggle.min.js" type="text/javascript"></script>
        <script src="http://<?=getInit('localhost')?>/public/js/selectize.js" type="text/javascript"></script>
        <script src="http://<?=getInit('localhost')?>/public/js/modernizr.custom.js" type="text/javascript"></script>
        <script src="http://<?=getInit('localhost')?>/public/js/move-top.js" type="text/javascript"></script>
        <script src="http://<?=getInit('localhost')?>/public/js/easing.js" type="text/javascript"></script>
        <script src="http://<?=getInit('localhost')?>/public/js/jquery.loading.min.js" type="text/javascript"></script>
        <script src="http://<?=getInit('localhost')?>/public/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="http://<?=getInit('localhost')?>/public/js/select2.min.js"></script>
        
    </head>
    
    <body id="thisbody" class="skin-blue">
	