<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta charset="utf-8" />
        <title><?=$module_name.' - '.$ctitle?></title>
        <meta name="description" content="overview &amp; stats" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
        <link rel="stylesheet" href="./public/css/bootstrap.min.css" />
        <link rel="stylesheet" href="./public/font-awesome-4.7.0/css/font-awesome.min.css" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <link rel="stylesheet" href="./public/css/style.css"  />
        <link rel="stylesheet" href="./public/css/jquery.dataTables.min.css"  />
        <link rel="stylesheet" href="./public/css/bootstrap-multiselect.css" type="text/css"/>
        <link rel="stylesheet" href="./public/css/dropzone.css" type="text/css"/>
        <link href="./public/css/bootstrap-toggle.min.css" rel="stylesheet">  
        <link href="./public/css/bootstrap-tagsinput.css" rel="stylesheet">
        <link href="./public/css/loading.min.css" rel="stylesheet">
        <link href="./public/css/jquery-ui.css" rel="stylesheet">
        <link href="./public/icomoon/styles.css" rel="stylesheet">
         <link href="./public/css/fred.css" rel="stylesheet">
        <link href="./public/css/print/printable_golden.css" rel="stylesheet">
        

    </head>
    <body>