<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta charset="utf-8" />
        <title><?=$module_name.' - '.$ctitle?></title>
        <meta name="description" content="overview &amp; stats" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
        <link rel="stylesheet" href="http://<?= getInit("localhost") ?>/public/css/bootstrap.min.css" />
        <link rel="stylesheet" href="http://<?= getInit("localhost") ?>/public/font-awesome-4.7.0/css/font-awesome.min.css" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <link rel="stylesheet" href="http://<?= getInit("localhost") ?>/public/css/style.css"  />
        <link rel="stylesheet" href="http://<?= getInit("localhost") ?>/public/css/jquery.dataTables.min.css"  />
        <link rel="stylesheet" href="http://<?= getInit("localhost") ?>/public/css/bootstrap-multiselect.css" type="text/css"/>
        <link rel="stylesheet" href="http://<?= getInit("localhost") ?>/public/css/dropzone.css" type="text/css"/>
        <link href="http://<?= getInit("localhost") ?>/public/css/bootstrap-toggle.min.css" rel="stylesheet">  
        <link href="http://<?= getInit("localhost") ?>/public/css/bootstrap-tagsinput.css" rel="stylesheet">
        <link href="http://<?= getInit("localhost") ?>/public/css/loading.min.css" rel="stylesheet">
        <link href="http://<?= getInit("localhost") ?>/public/css/jquery-ui.css" rel="stylesheet">

        <link href="http://<?= getInit("localhost") ?>/public/icomoon/styles.css" rel="stylesheet">
        <link href="http://<?= getInit("localhost") ?>/public/css/select2.min_1.css" rel="stylesheet" type="text/css"/>
        <link href="http://<?= getInit("localhost") ?>/public/css/fred.css" rel="stylesheet">
        <link href="http://<?= getInit("localhost") ?>/public/css/print/printable_golden.css" rel="stylesheet">
        
        <script src="http://<?= getInit("localhost") ?>/public/js/jquery.2.1.1.min.js"></script>
        <script src="http://<?= getInit("localhost") ?>/public/js/jquery-ui.js"></script>
        <script src="http://<?= getInit("localhost") ?>/public/js/jquery.menu-aim.js"></script>
        <script src="http://<?= getInit("localhost") ?>/public/js/dropzone.js"></script>
        <script src="http://<?= getInit("localhost") ?>/public/js/jquery.dataTables.min.js"></script>
        <script src="http://<?= getInit("localhost") ?>/public/js/moment.min.js"></script>
        <script type="text/javascript" src="http://<?= getInit("localhost") ?>/public/js/bootstrap-multiselect.js"></script>
        <script type="text/javascript" src="http://<?= getInit("localhost") ?>/public/js/bootstrap-tagsinput.min.js"></script>
        <script src="http://<?= getInit("localhost") ?>/public/js/classie.js"></script> 
        <script src="http://<?= getInit("localhost") ?>/public/js/bootstrap-toggle.min.js"></script>
        <script src="http://<?= getInit("localhost") ?>/public/js/selectize.js"></script>
        <script src="http://<?= getInit("localhost") ?>/public/js/modernizr.custom.js"></script>
        <script src="http://<?= getInit("localhost") ?>/public/js/move-top.js"></script>
        <script src="http://<?= getInit("localhost") ?>/public/js/easing.js"></script>
        <script src="http://<?= getInit("localhost") ?>/public/js/jquery.loading.min.js"></script>
        <script src="http://<?= getInit("localhost") ?>/public/js/bootstrap.min.js"></script>

    </head>
    <body id="thisbody" class="skin-blue">
        <div class="wrapper">
            <header class="cd-main-header">
                <a href="/" data-toggle="tooltip" data-placement="bottom" title="Inventory System" class="cd-logo"><img src="http://<?= getInit("localhost") ?>/public/images/logo.png" class="img-responsive"/></a>
                <!--                <div class="dropdown">
                                    <a class="btn dropdown-toggle cd-logo" id="mode_select"  data-toggle="dropdown"><img src="http://<?= getInit("localhost") ?>/public/images/salary.png" class="img-responsive" style="margin-top: -3px;"/></a>
                                    <ul class="dropdown-menu mode_select" role="menu" aria-labelledby="mode_select">
                                        <label class="mode_select_label">Select Module:</label>
                                        <li role="presentation"><a role="menuitem" tabindex="-1" href="/module/1">Inventory</a></li>
                                        <li role="presentation"><a role="menuitem" tabindex="-1" href="/module/2">Payroll</a></li>
                                        <li role="presentation"><a role="menuitem" tabindex="-1" href="/module/3">Asset Management</a></li>
                                        <li role="presentation" class="divider"></li>
                                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Sample w/ Divider</a></li>    
                                    </ul>
                                </div>-->

                <button type="button" class="toggle-side" data-toggle="collapse" data-target="#cd-nav">
                    <i class="fa fa-bars"></i>
                </button>
                <a href="#0" class="cd-nav-trigger"><span></span></a>
                <nav class="cd-nav">
                    <ul class="cd-top-nav">
                        <!--li class="has-children notif"><a data-toggle="tooltip" data-placement="bottom" title="View Notifications"><i class="fa fa-bell" data-toggle="dropdown"></i><span class="label badge-green">5</span></a>
                            <ul>
                                <li class="header"><a href="/notifications">See all notifications<i class="fa fa-long-arrow-right"></i></a></li>
                                <li class="notiflist">
                                    <ul>
                                        <li>
                                            <a href="/notifications">
                                                <div class="notifbox">   
                                                    <div class="row">
                                                        <div class="col-xs-2 no-padding">
                                                            <div class="notif-icon user"><i class="fa fa-user"></i></div>
                                                        </div>
                                                        <div class="col-xs-10 no-padding">
                                                            <div class="details">  
                                                                <p>John Doe just signed up as an editor. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip</p>
                                                            </div>  
                                                        </div>
                                                    </div>                                                     
                                                </div>
                                                <div class="clearfix"></div>
                                            </a>
                                            <div class="clearfix"></div>
                                        </li>
                                        <li>
                                            <a href="/notifications">
                                                <div class="notifbox">
                                                    <div class="row">
                                                        <div class="col-xs-2 no-padding">
                                                            <div class="notif-icon comment"><i class="fa fa-comment"></i></div>
                                                        </div>
                                                        <div class="col-xs-10 no-padding">
                                                            <div class="details"> 
                                                                <p>New comments. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                                                                <span class="label-badge badge-yellow round">3</span>
                                                            </div>
                                                        </div>
                                                    </div>	
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="/notifications">
                                                <div class="notifbox">
                                                    <div class="row">
                                                        <div class="col-xs-2 no-padding">
                                                            <div class="notif-icon cart"><i class="fa fa-shopping-cart"></i></div>
                                                        </div>
                                                        <div class="col-xs-10 no-padding">
                                                            <div class="details"> 
                                                                <p>New orders</p> 
                                                                <span class="label-badge badge-green round">15</span>
                                                            </div> 
                                                        </div>
                                                    </div> 
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="/notifications">
                                                <div class="notifbox">
                                                    <div class="row">
                                                        <div class="col-xs-2 no-padding">
                                                            <div class="notif-icon user"><i class="fa fa-user"></i></div>
                                                        </div>
                                                        <div class="col-xs-10 no-padding">
                                                            <div class="details">  
                                                                <p>Kate Jones updated the employee list. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip</p>
                                                            </div>    
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="/notifications">
                                                <div class="notifbox">
                                                    <div class="row">
                                                        <div class="col-xs-2 no-padding">
                                                            <div class="notif-icon comment"><i class="fa fa-comment"></i></div>
                                                        </div>
                                                        <div class="col-xs-10 no-padding">
                                                            <div class="details"> 
                                                                <p>New comments. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                                                                <span class="label-badge badge-yellow round">10</span>
                                                            </div>
                                                        </div>
                                                    </div>	
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li-->
                        <li class="has-children language"><a data-toggle="tooltip" data-placement="bottom" title="Choose Language"><i class="fa fa-language" data-toggle="dropdown"></i></a>
                            <ul>
                                <li><a class="lang" id="english" href="javascript:void(0)" onclick="chooseLanguage(1)" value="1">
                                        <div class="clearfix">
                                            <span class="pull-left">english</span>
                                            <span class="pull-right"><img src="http://<?= getInit("localhost") ?>/public/images/usa-flag.png" class="img-responsive" /></span>
                                        </div>
                                    </a>
                                </li>
                                <li><a class="lang" id="chinese" href="javascript:void(0)" onclick="chooseLanguage(2)" value="2">
                                        <div class="clearfix">
                                            <span class="pull-left">simplified chinese</span>
                                            <span class="pull-right"><img src="http://<?= getInit("localhost") ?>/public/images/china-flag.png" class="img-responsive" /></span>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="has-children account">
                            <a href="#0">
                                <img src="http://<?= getInit("localhost") ?>/public/images/avatar0.png" alt="avatar" data-toggle="dropdown">
                                <?= $_SESSION[getSessionName()]["name"] ?>
                            </a>
                            <ul>
                                <li class="userheader">
                                    <img src="http://<?= getInit("localhost") ?>/public/images/avatar0.png" class="img-responsive img-circle"/>
                                    <p>
                                <?= $_SESSION[getSessionName()]["name"] ?></p>
                                </li>
                                <li class="userfooter">                               
                                    <a href="/logout"><button class="btn btn-default btn-flat">Sign out</button></a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </header>
            <main class="cd-main-content">

                <nav class="cd-side-nav">
                    <ul>
                        <li class="has-children overview">
                            <a href="#0">
                                <img src="http://<?=getInit("localhost")?>/public/images/info.png"/>
                                <span><?=$information?></span>
                            </a>
                            <ul>
                                <div class="dropdownlist">                                     
                                    <div class="catbox">
                                        <h3><?=$accounts?></h3> 
                                        <li id="nav_customers"><a href="/customers"><i class=""></i><?=$customers?></a></li>
                                        <li id="nav_suppliers"><a href="/suppliers"><i class=""></i><?=$suppliers?></a></li>
                                        <li id="nav_employees"><a href="/employees"><i class=""></i><?=$employees?></a></li>
                                    </div>                                     
                                    <div class="catbox">
                                        <h3><?=$supplies?></h3> 
                                        <li id="nav_plastics"><a href="/plastics"><i class=""></i><?=$plastics?></a></li>
                                        <li id="nav_inks"><a href="/inks"><i class=""></i><?=$inks?></a></li>
                                        <li id="nav_adhesives"><a href="/adhesives"><i class=""></i><?=$adhesives?></a></li>
                                        <li id="nav_chemicals"><a href="/chemicals"><i class=""></i><?=$chemicals?></a></li>
                                        <li id="nav_cylinders"><a href="/cylinders"><i class=""></i><?=$cylinders?></a></li>
                                    </div>                              
                                    <div class="catbox">
                                        <h3><?=$types_categories?></h3> 
                                        <li id="nav_departments"><a href="/departments"><i class=""></i><?=$departments?></a></li>
                                        <li id="nav_contacttype"><a href="/contact_types"><i class=""></i><?=$contact_types?></a></li>
                                        <li id="nav_currency"><a href="/currency"><i class=""></i><?=$currency?></a></li>
                                    </div>
                                </div>
                            </ul>
                        </li>
                        <li class="has-children overview">
                            <a href="#0">
                                <img src="http://<?=getInit("localhost")?>/public/images/info.png"/>
                                <span><?=$inventory?></span>
                            </a>
                            <ul>
                                <div class="dropdownlist">                           
                                    <div class="catbox">
                                        <h3><?=$inventory?></h3>
                                        <li id="nav_plastics_po"><a href="/plastics_po"><i class=""></i><?=$plastics?></a></li>
                                        <li id="nav_inks_po"><a href="/inks_po"><i class=""></i><?=$inks?></a></li>
                                        <li id="nav_adhesives_po"><a href="/adhesives_po"><i class=""></i><?=$adhesives?></a></li>
                                        <li id="nav_chemicals_po"><a href="/chemicals_po"><i class=""></i><?=$chemicals?></a></li>
                                        <li id="nav_cylinders_po"><a href="/cylinders_po"><i class=""></i><?=$cylinders?></a></li>
                                    </div> 
                                </div>
                            </ul>
                        </li>
                        <li id="nav_products">
                            <a href="/products">
                                <img src="http://<?=getInit("localhost")?>/public/images/inventory.png"/>
                                <span><?=$products?></span>
                            </a>
                        </li>
                        <li  id="nav_customer_po">
                            <a href="/customer_po">
                                <img src="http://<?=getInit("localhost")?>/public/images/info.png"/>
                                <span><?=$customer_po?></span>
                            </a>
                        </li>
                        <li  id="nav_job_order">
                            <a href="/job_orders">
                                <img src="http://<?=getInit("localhost")?>/public/images/info.png"/>
                                <span><?=$job_orders?></span>
                            </a>
                        </li>
                        <!--li class="has-children overview">
                            <a href="#0">
                                <img src="http://<?=getInit("localhost")?>/public/images/sales.png"/>
                                <span><?=$suppliers?></span>
                            </a>
                            <ul>
                                <div class="dropdownlist">                           
                                    <div class="catbox">
                                        <h3><?=$suppliers?></h3> 
                                        <li id="nav_plastics_supplier"><a href="/plastics_supplier"><i class=""></i><?=$plastics?></a></li>
                                        <li id="nav_inks_supplier"><a href="/inks_supplier"><i class=""></i><?=$inks?></a></li>
                                        <li id="nav_adhesives_supplier"><a href="/adhesives_supplier"><i class=""></i><?=$adhesives?></a></li>
                                        <li id="nav_chemicals_supplier"><a href="/chemicals_supplier"><i class=""></i><?=$chemicals?></a></li>
                                        <li id="nav_cylinders_supplier"><a href="/cylinders_supplier"><i class=""></i><?=$cylinders?></a></li>
                                    </div> 
                                </div>
                            </ul>
                        </li>
                        <li class="has-children overview">
                            <a href="#0">
                                <img src="http://<?=getInit("localhost")?>/public/images/sales.png"/>
                                <span><?=$usage?></span>
                            </a>
                            <ul>
                                <div class="dropdownlist">                           
                                    <div class="catbox">
                                        <h3><?=$usage?></h3> 
                                        <li id="nav_plastics_usage"><a href="/plastics_usage"><i class=""></i><?=$plastics?></a></li>
                                        <li id="nav_inks_usage"><a href="/inks_usage"><i class=""></i><?=$inks?></a></li>
                                        <li id="nav_adhesives_usage"><a href="/adhesives_usage"><i class=""></i><?=$adhesives?></a></li>
                                        <li id="nav_chemicals_usage"><a href="/chemicals_usage"><i class=""></i><?=$chemicals?></a></li>
                                        <li id="nav_cylinders_usage"><a href="/cylinders_usage"><i class=""></i><?=$cylinders?></a></li>
                                </div>
                            </ul>
                        </li-->
                        <li class="has-children overview">
                            <a href="#0">
                                <img src="http://<?=getInit("localhost")?>/public/images/report.png"/>
                                <span>Printable</span>
                            </a>
                            <ul>
                                <div class="dropdownlist">                                     
                                    <div class="catbox">
                                        <h3></h3> 
                                        <li id=""><a href="/create_job_order"><i class=""></i>Create Job Order</a></li>
                                        
                                        <li id=""><a href="/costing"><i class=""></i>Costing</a></li>
                                        </div>
                                </div>
                            </ul>
                        </li>
                    </ul> 
                </nav>

