</div>
</main>
<script src="http://<?= getInit("localhost") ?>/public/js/modalEffects.js"></script>
<script src="http://<?= getInit("localhost") ?>/public/js/main.js"></script>
<script src="http://<?= getInit("localhost") ?>/public/js/select2.min.js" type="text/javascript"></script>
<script src="http://<?= getInit("localhost") ?>/public/js/dataTables.fixedHeader.min.js" type="text/javascript"></script>
<script>
    $(".toggle-side").click(function () {
        $(".cd-side-nav").toggle();

        if ($("#section").attr('data') == '50') {
            $("#section").attr('data', '100');
            $("#section").css('width', '100%');
            $(".cd-main-content .content-wrapper").css({'margin-left': '0', 'padding-left': '0', 'padding-right': '0'});
        } else {
            $("#section").attr('data', '50');
            $("#section").css('width', '100%');
            $(".cd-main-content .content-wrapper").css({'margin-left': '110px', 'padding-left': '5px', 'padding-right': '5px'});
        }
    });
    
if (screen.width < 500) {
  
  $("body").addClass("nohover");
  $("td, th")
    .attr("tabindex", "1")
    .on("touchstart", function() {
      $(this).focus();
    });
  
}
    $(document).ready(function () {
        $('.header-select').multiselect({
            includeSelectAllOption: true,
            selectAllValue: 'select-all-value'
        });
        $('#header-select2').multiselect({
            includeSelectAllOption: true,
            selectAllValue: 'select-all-value'
        });
        $('.col-select').multiselect({
            includeSelectAllOption: true,
            columns: 4,
            selectAllValue: 'select-all-value'
        });
        $('#col-select2').multiselect({
            includeSelectAllOption: true,
            selectAllValue: 'select-all-value'
        });
    });

    $(document).ready(function () {
        var defaults = {
            containerID: 'toTop',
            containerHoverID: 'toTopHover',
            scrollSpeed: 1200,
            easingType: 'linear'
        };
        $().UItoTop({easingType: 'easeOutQuart'});
    });

    $(".sidebar-toggle").click(function (e) {
        e.preventDefault();
        $("nav.sidebar, .main-header, .content-wrapper").toggleClass("active");
    });

    $(function () {
        $(".datepicker").datepicker({ dateFormat: 'yyyy-mm-dd' });
    });

    $(document).ready(function () {
        $('[data-toggle="popover"]').popover();
    });  
    $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip({ trigger: "hover" });
});

    $('.lang').on('click', function () {
        chooseLanguage($(this).attr("value"));
    });

    function chooseLanguage(id) {

        $.post("/changeLanguage", {id: id}, function (data) {
            if (data === 1) {
                document.location.reload();
            }

        }, 'json');
    }
    window.onkeypress = function (event) {

        if (event.keyCode == 13) {
            $('#sin').click();
        }

    }

    function mobileViewUpdate() {
        var viewportWidth = $(window).width();
        if (viewportWidth > 1910) {
            $(".table-scroll").removeClass("table-responsive");
        }
    }

    $(window).load(mobileViewUpdate);
    $(window).resize(mobileViewUpdate);

    $(".loading").click(function () {
        $.showLoading({name: 'circle-fade', allowHide: true});
    });
    $('.selectize').selectize({
        sortField: 'text',
        plugins: ['remove_button'],
        delimiter: ',',
        persist: false,
        create: function (input) {
            return {
                value: input,
                text: input
            }
        }
    });

    $(function () {
        var $wrapper = $('#wrapper');

        // theme switcher
        var theme_match = String(window.location).match(/[?&]theme=([a-z0-9]+)/);
        var theme = (theme_match && theme_match[1]) || 'default';
        var themes = ['default', 'legacy', 'bootstrap2', 'bootstrap3'];
        $('head').append('<link rel="stylesheet" href="../public/css/selectize.' + theme + '.css">');

        var $themes = $('<div>').addClass('theme-selector').insertAfter('h1');
        for (var i = 0; i < themes.length; i++) {
            $themes.append('<a href="?theme=' + themes[i] + '"' + (themes[i] === theme ? ' class="active"' : '') + '>' + themes[i] + '</a>');
        }

        // display scripts on the page
        $('script', $wrapper).each(function () {
            var code = this.text;
            if (code && code.length) {
                var lines = code.split('\n');
                var indent = null;

                for (var i = 0; i < lines.length; i++) {
                    if (/^[	 ]*$/.test(lines[i]))
                        continue;
                    if (!indent) {
                        var lineindent = lines[i].match(/^([ 	]+)/);
                        if (!lineindent)
                            break;
                        indent = lineindent[1];
                    }
                    lines[i] = lines[i].replace(new RegExp('^' + indent), '');
                }

                var code = $.trim(lines.join('\n')).replace(/	/g, '    ');
                var $pre = $('<pre>').addClass('js').text(code);
                $pre.insertAfter(this);
            }
        });

        // show current input values
        $('select.selectized,input.selectized', $wrapper).each(function () {
            var $container = $('<div>').addClass('value').html('Current Value: ');
            var $value = $('<span>').appendTo($container);
            var $input = $(this);
            var update = function (e) {
                $value.text(JSON.stringify($input.val()));
            }

            $(this).on('change', update);
            update();

            $container.insertAfter($input);
        });
    });

// CLEARABLE INPUT
    function tog(v) {
        return v ? 'addClass' : 'removeClass';
    }
    $(document).on('input', '.clearable', function () {
        $(this)[tog(this.value)]('x');
    }).on('mousemove', '.x', function (e) {
        $(this)[tog(this.offsetWidth - 18 < e.clientX - this.getBoundingClientRect().left)]('onX');
    }).on('touchstart click', '.onX', function (ev) {
        ev.preventDefault();
        $(this).removeClass('x onX').val('').change();
    });


 /**Table Codes**/
	/**Loads Page Info (Page number and Page count)**/
	var global_pages=0;
	function loadPageInfo(table){
			var info = $('#'+table).DataTable().page.info();
			global_pages = info.pages;
			$('.total_pages[table='+table+']').html(info.pages);
			$('.viewcount[table='+table+']').html("View "+parseInt(info.start+1)+"-"+info.end+" of "+info.recordsTotal);
			$('.pageset[table='+table+']').val(info.page+1);
	}
	
	/*Column Show*/
    $('select.select_table').on('change', function (e) {
        var col = $(this).val();
        if (col == null) {
            $('#'+$(this).attr('table')).DataTable().columns().visible(true, true);
        } else {
            $('#'+$(this).attr('table')).DataTable().columns().visible(false, false);
            $('#'+$(this).attr('table')).DataTable().column($(this).val()).visible(true, true);
        }
		$('#'+$(this).attr('table')).DataTable().ajax.url($('#'+$(this).attr('table')).DataTable().ajax.url()+'?show_cols='+col).load();
    });
	
	/*Column Search*/
    //var global_value="";
	//global_value=cols;
		/**For Search Button Click**/
		$('.srchbtn').on('click',function(){
			var value=$('.srch_value[table='+$(this).attr('table')+']').val();
			var cols=$('.search_col[table='+$(this).attr('table')+']').val();
			$('#'+$(this).attr('table')).DataTable().columns().search("").draw();
			if(cols==null){
				$('#'+$(this).attr('table')).DataTable().columns().search( value ).draw();
			}else{
				$('#'+$(this).attr('table')).DataTable().columns(cols).search( value ).draw();
			}
			loadPageInfo($(this).attr('table'));
		});
		
		/**For KeyPress Enter**/
		$('.srch_value').on('keydown',function(e){
			if (e.keyCode == 13) {
				var value=$('.srch_value[table='+$(this).attr('table')+']').val();
				var cols=$('.search_col[table='+$(this).attr('table')+']').val();
				$('#'+$(this).attr('table')).DataTable().columns().search("").draw();
				if(cols==null){
					$('#'+$(this).attr('table')).DataTable().columns().search( value ).draw();
				}else{
					$('#'+$(this).attr('table')).DataTable().columns(cols).search( value ).draw();
				}
			}
			loadPageInfo($(this).attr('table'));
		});


	/*Page Number of shown data*/
	$('.row_limit').on('change',function(){
		var length = $(this).val();
		$('#'+$(this).attr('table')).DataTable().page.len( length ).draw();
		loadPageInfo($(this).attr('table'));
	});
	
	/*Page Change (next/prev)*/
	$('.Last_Page').on('click',function(){
		$('#'+$(this).attr('table')).dataTable().fnPageChange( 'last' );
		loadPageInfo($(this).attr('table'));
	});
	$('.Next_Page').on('click',function(){
		$('#'+$(this).attr('table')).dataTable().fnPageChange( 'next' );
		loadPageInfo($(this).attr('table'));
	});
	$('.Prev_Page').on('click',function(){
		$('#'+$(this).attr('table')).dataTable().fnPageChange( 'previous' );
		loadPageInfo($(this).attr('table'));
	});
	$('.First_Page').on('click',function(){
		$('#'+$(this).attr('table')).dataTable().fnPageChange( 'first' );
		loadPageInfo($(this).attr('table'));
	});

	/*Page Change Set*/
	/**On KeyPress Enter**/
	$('.pageset').on('keydown',function(e){
		if (e.keyCode == 13) {
			if($(this).val()<0){
				$('#'+$(this).attr('table')).dataTable().fnPageChange(global_pages-global_pages);
			}else if($(this).val()>=0&&$(this).val()<=global_pages){
				$('#'+$(this).attr('table')).dataTable().fnPageChange(parseInt($(this).val()-1));
			}else if($(this).val()>global_pages){
				$('#'+$(this).attr('table')).dataTable().fnPageChange(global_pages-1);
			}
				loadPageInfo($(this).attr('table'));
		}
	});

// CLEARABLE INPUT
    function tog(v) {
        return v ? 'addClass' : 'removeClass';
    }
    $(document).on('input', '.clearable', function () {
        $(this)[tog(this.value)]('x');
    }).on('mousemove', '.x', function (e) {
        $(this)[tog(this.offsetWidth - 18 < e.clientX - this.getBoundingClientRect().left)]('onX');
    }).on('touchstart click', '.onX', function (ev) {
        ev.preventDefault();
        $(this).removeClass('x onX').val('').change();
    });
    ///Allow numbers, dot, backspace, delete, left and right arrow keys
    NumsMe();
    function NumsMe(){
         $('.NumsMe').keypress(function(event) {
            var key = window.event ? event.keyCode : event.which;

            if (key == 8 || key == 46 || key == 37 || key == 39) {
                return true;
            }else if ((key == 99 || key == 97 || key == 118) && event.ctrlKey) {
                return true;
            }else if ((key < 48 || key > 57) ){
                return false;
            }
            else return true;
        });
    }

    ///Allow numbers, single dot, backspace, delete, left and right arrow keys
    scoreme();
    function scoreme(){
         $('.score').keypress(function(event) {
            var key = window.event ? event.keyCode : event.which;

             //console.log('key code is: '+event.which);
            if (key == 8 || key == 37 || key == 39) {
                return true;
            }else if ((key == 99 || key == 97 || key == 118) && event.ctrlKey) {
                return true;
            }
            else if ( (key != 46 || $(this).val().indexOf('.') != -1) && (key < 48 || key > 57) ) {
                return false;
            }
            else return true;
        });
    }
    ///Allow numbers, dot, backspace, delete, left and right arrow keys
    LetterMe();
    function LetterMe(){
         $('.letters').keypress(function(event) {
            var key = window.event ? event.keyCode : event.which;
            if (key == 8 || key == 46 || key == 37 || key == 39) {
                return true;
            }else if ((key == 99 || key == 97 || key == 118) && event.ctrlKey) {
                return true;
            }else if (key > 32 && (key < 65 || key > 90) && (key < 97 || key > 122)) {
                return false;
            }
            else return true;
        });
    }
    
    UnSpaceMe();
    function UnSpaceMe(){
        $('.UnSpaceMe').keypress(function(event){
            var key = window.event ? event.keyCode : event.which;
            if (key === 32 &&  event.target.selectionStart === 0) {
              return false;
            }
            else return true;
        });
    }
    NoKeys();
    function NoKeys(){
        $('.NoKeys').keypress(function(event){
            var key = window.event ? event.keyCode : event.which;
            if (key > 0) {
              return false;
            }
            else return true;
        });
    }
    
</script>
</body>
</html>
