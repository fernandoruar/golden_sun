<div class="content-wrapper  main-content">
    <section class="content" id="section" data="50"> 
        <div class="row">
            <div class="col-md-12">
                <div class="box box-default">
                    <div class="box-header">
                        <h3 class="box-title"><?= $contact_types ?></h3>
                    </div>
                    <div class="box-body">

                        <div class="customtable">
                            <div class="button-box">
                                <button id="abutton" type="button" class="btn"><i class="fa fa-plus color-blue"></i></button>
								<button id="abutton_multiple" type="button" class="btn"><img src="./public/images/multiple.png" class="multiplebtn"/></button>
                            </div>
                            <div class="table-scroll">
                                <div class="table-height">
                                    <div class='alert alert-danger' style="display:none">
                                        <a class='close'>&times;</a>
                                        <center><span></span></center>
                                    </div>
                                    <table data-order='[["1","asc"]]' name="dataTable" id="datatable" class="table dataTable table-bordered table-striped table-hover">
                                        <thead>
                                        <th class="nsearch norder" data-class-name="display_col"></th>
                                        <th width="90%" data-class-name="text-center"><?= $name ?></th>                                 
                                        <th width="10%" class="norder nsearch" data-class-name="text-center"><?= $actions ?></th>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>                   
                            </div>                                       
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
</div>




<div class="modal fade" id="dataModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" name="title"></h4>

            </div>
            <div class="modal-body">
                <center class="color-red" style="margin-top:10px;"><i class="fa fa-asterisk"></i>&nbsp;<?= $required_fields ?></center>

                <div class="details-container">
                    <div class='alert alert-danger' style="display:none">
                        <a class='close'>&times;</a>
                        <center><span></span></center>
                    </div>
                    <table class="table table-form">
                        <tr>
                            <td><?= $name ?><span class="color-red">*</span></td>
                            <td><span><input auto_focus class="form-control" type="text"></span></td>
                        </tr>

                    </table>
                </div>

            </div>
            <div class="modal-footer">              


                <button type="button"  class="esc_btn btn btn-danger pull-left"  data-dismiss="modal"><i class="fa fa-times"></i> &nbsp;<?= $close ?></button>
                <button type="button" id="save" class="save_btn btn btn-success pull-right"><i class="fa fa-save"></i> <?= $save ?>
                </button>

            </div>

        </div>
    </div>
</div>

<div class="modal fade lgModal" id="dataModal_Multiple" role="dialog">
    <div class="modal-dialog lgModal_dialog ">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" name="title"></h4>
            </div>
            <div class="modal-body">
                <center class="color-red" style="margin-top:10px;"><i class="fa fa-asterisk"></i>&nbsp;<?= $required_fields ?></center>

                <div name="field_container">
                    <div class='alert alert-danger' style="display:none">
                        <a class='close'>&times;</a>
                        <center><span></span></center>
                    </div>
                    <table class="table table-form">
                        <tr>
                            <td><strong><?= $name ?><span class="color-red">*</span>:&nbsp;</strong></td>
                            <td><input auto_focus class="form-control" type="text"></td>
                            <td><button id="add_multiple" class="btn btn-primary"><i class="fa fa-plus-circle"></i>&nbsp;<?= $add ?></button></td>
                        </tr>

                    </table>
                </div>
                <div class="table-responsive table-scroll table-container">
                    <div class="table-height">
                        <div class='alert alert-danger' style="display:none">
                            <a class='close'>&times;</a>
                            <center><span></span></center>
                        </div>


                        <table id="table_multiple" class="table table-bordered table-hover table-striped table_style">
                            <thead>
                            <th class="norder nsearch" data-class-name="display_col">ID</th>
                            <th width="90%"><?= $name ?></th>
                            <th width="10%"><?= $actions ?></th>
                            </thead>

                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">              


                <button type="button"  class="esc_btn btn btn-danger pull-left"  data-dismiss="modal"><i class="fa fa-times"></i> &nbsp;<?= $close ?></button>
                <button type="button" id="save_multiple" class="save_btn btn btn-success pull-right"><i class="fa fa-save"></i> <?= $save ?>
                </button>

            </div>

        </div>
    </div>
</div>


<div class="modal fade" id="deleteModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" name="title"><?= $confirmation ?></h4>
            </div>
            <div class="modal-body">
                <div class='alert alert-danger' style="display:none">
                    <a class='close'>&times;</a>
                    <center><span></span></center>
                </div>
                <center><h4 name="message"></h4></center>
            </div>
            <div class="modal-footer">              

                <button type="button" data-dismiss="modal"  class="no_btn btn btn-danger pull-left"><i class="fa fa-remove"></i> &nbsp;<?= $no ?></button>

                <button type="button"  id='delete_yes' class="yes_btn btn btn-success pull-right"><i class="fa fa-check"></i> &nbsp;<?= $yes ?></button>

            </div>

        </div>
    </div>
</div>