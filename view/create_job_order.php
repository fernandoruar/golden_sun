<div class="content-wrapper">
    <section class="content" id="section" data="50"> 
        <div class="row">
            <div class="col-md-12">
                <div class="box box-default">
                    <div class="box-header">
                    </div>
                    <div class="box-body">
                        <div style="margin:20px">
                            <div class="row scroll_x">
                                <div class="col-xs-12">
                                    <div class="sheet_container">
								<div class="details-container">
                                        <div class="row">
                                            <div class="col-xs-12 text-center">
                                                <div class="solid_border">
                                                    <fr class="main_title"><?=$ctitle?></fr>
                                                    <fr class="sub_title"><?=$job_order_sheet?></fr>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <table class="mt10">
                                                    <tr>
                                                        <td>
                                                            <label><?= $date ?> : </label>
                                                        </td>
                                                        <td>
                                                            <input type="text" auto_focus class="datepicker w150 solid_bottom form-control">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label><?= $customer ?> : </label> 
                                                        </td>
                                                        <td class='w400 solid_bottom'>
															<!--select class='select2-field ajax dropdown' data-placeholder="<?= $ccustomer ?>"></select-->
															<label id='customer'></label>
                                                        </td>
                                                    </tr> 
                                                    <tr>
                                                        <td>
                                                            <label><?= $product ?> : </label> 
                                                        </td>
                                                        <td class="w300 solid_bottom">
															<!--select class='select2-field ajax dropdown' data-placeholder="<?= $cproducts ?>"></select-->
															<label id='product'></label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="col-xs-6">
                                                <table class="mt10 pull-right">
                                                    <tr>
                                                        <td>
                                                            <label><?= $jo_number ?> : </label>  
                                                        </td>
                                                        <td>
                                                            <input type="text" class="w150 solid_bottom">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label><?= $po_number ?> : </label> 
                                                        </td>
                                                        <td class="w300 solid_bottom">
                                                            <!--input type="text" id='ponum' class="w300 solid_bottom"-->
															<label id='po_number'></label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label><?= $po_qty ?> : </label>  
                                                        </td>
                                                        <td class="w300 solid_bottom">
                                                            <!--input type="text" id='poqty' class="w300 solid_bottom"-->
															<label id='po_qty'></label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="block">
                                                    <div class="pos_bot">
                                                        <p><b><i><?=$job_order_note?></i></b></p>
                                                    </div>
                                                    <div>
                                                        <table class="mt10 pull-right">
                                                            <tr>
                                                                <td colspan="2" class="b_all">
                                                                    <p class="text-center"><?=$production_acknowlege?></p>
                                                                </td>
                                                            </tr>
                                                            <tr class="b_all">
                                                                <td>
                                                                    <label><?= $recieved_by ?> : </label> 
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="w200">
                                                                </td>
                                                            </tr>
                                                            <tr class="b_all">
                                                                <td>
                                                                    <label><?= $noted_by ?> : </label>  
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="w200">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div>
                                                    <table style="margin-top: -1px">
                                                        <tr>
                                                            <td class="b_all">
                                                                <p class="text-center"><?= $materials ?></p>
                                                            </td>
                                                            <td class="b_all">
                                                                <p class="text-center"><?= $thickness ?></p>
                                                            </td>
                                                            <td class="b_all">
                                                                <p class="text-center"><?= $plastic_type_size ?></p>
                                                            </td>
                                                            <td class="b_all">
                                                                <p class="text-center"><?= $qty ?></p>
                                                            </td>
                                                            <td class="b_all">
                                                                <p class="text-center"><?= $plastic_type_size ?></p>
                                                            </td>
                                                            <td class="b_all">
                                                                <p class="text-center"><?= $qty ?></p>
                                                            </td>
                                                            <td class="b_all">
                                                                <p class="text-center"><?= $plastic_type_size ?></p>
                                                            </td>
                                                            <td class="b_all">
                                                                <p class="text-center"><?= $qty ?></p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="b_all text-center" style="width:260px">
                                                                <!--input type="text" style="width:254px"-->
																<label id='materials'></label>
                                                            </td>
                                                            <td class="b_all">
                                                                <input type="text" style="width:80px">
                                                            </td>
                                                            <td class="b_all">
                                                                <input type="text" id='PTS0' style="width:195px" class='pps'>
                                                            </td>
                                                            <td class="b_all">
                                                                <input type="text" id='QTY0'  style="width:80px" class='pps'>
                                                            </td>
                                                            <td class="b_all">
                                                                <input type="text" id='PTS1' style="width:195px" class='pps'>
                                                            </td>
                                                            <td class="b_all">
                                                                <input type="text" id='QTY1' style="width:80px" class='pps'>
                                                            </td>
                                                            <td class="b_all">
                                                                <input type="text" id='PTS2' style="width:195px" class='pps'>
                                                            </td>
                                                            <td class="b_all">
                                                                <input type="text" id='QTY2'  style="width:80px" class='pps'>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
								<!--div class="details-container">
								</div>
								</div>
								<div class="details-container"-->
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div>
                                                    <table style="margin-top: -5px">
                                                        <tr>
                                                            <td class="b_all" colspan="4">
                                                                <p class="text-center"><?=$winding_direction?></p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="b_x wind_dir">
                                                                <img src="./public/images/wind/wind1.png">
                                                            </td>
                                                            <td class="b_x wind_dir">
                                                                <img src="public/images/wind/wind2.png">
                                                            </td>
                                                            <td class="b_x wind_dir">
                                                                <img src="public/images/wind/wind3.png">
                                                            </td>
                                                            <td class="b_x wind_dir">
                                                                <img src="public/images/wind/wind4.png">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="b_x text-center">
                                                                <label><?= $figure_no ?> 1</label><div class="checkbox_con"><input name='figs' data_val='1' type="radio" style="height: 16px"></div>
                                                            </td>
                                                            <td class="b_x text-center">
                                                                <label><?= $figure_no ?> 2</label><div class="checkbox_con"><input name='figs' data_val='2' type="radio" style="height: 16px"></div>
                                                            </td>
                                                            <td class="b_x text-center">
                                                                <label><?= $figure_no ?> 3</label><div class="checkbox_con"><input name='figs' data_val='3' type="radio" style="height: 16px"></div>
                                                            </td>
                                                            <td class="b_x text-center">
                                                                <label><?= $figure_no ?> 4</label><div class="checkbox_con"><input name='figs' data_val='4' type="radio" style="height: 16px"></div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table style="margin-top: -5px; margin-left: -1px; position: absolute;">
                                                        <tr>
                                                            <td class="b_all">
                                                                <p class="text-center"><?= $cylinder_number ?>.</p>
                                                            </td>
                                                            <td class="b_all">
                                                                <p class="text-center"><?= $weight_roll ?></p>
                                                            </td>

                                                        </tr>
                                                        <tr>
                                                            <td class="b_all text-center">
                                                                <!--input type="text" style="width:331px"-->
																<label id='cylinder_number'></label>
                                                            </td>
                                                            <td class="b_all">
                                                                <input type="text" style="width:154px">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="b_all">
                                                                <p class="text-center"><?= $barcode_number ?>.</p>
                                                            </td>
                                                            <td class="b_all">
                                                                <p class="text-center"><?= $meter_roll ?></p>
                                                            </td>

                                                        </tr>
                                                        <tr>
                                                            <td class="b_all">
                                                                <input type="text" style="width:331px">
                                                            </td>
                                                            <td class="b_all">
                                                                <input type="text" style="width:154px">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div>
                                                    <table id='production_table' style="margin-top: -5px">
                                                        <tr>
                                                            <th class="b_all" rowspan="2">
                                                                <p><?=$prod_date?></p>
                                                            </th>
                                                            <th class="b_all" colspan="2">
                                                                <p><?=$printing_dept?></p>
                                                            </th>
                                                            <th class="b_all" colspan="2">
                                                                <p><?=$lamination_dept?></p>
                                                            </th>
                                                            <th class="b_all" colspan="2">
                                                                <p><?=$slitting_dept?></p>
                                                            </th>
                                                            <th class="b_all" colspan="2">
                                                                <p><?=$bagforming_dept?></p>
                                                            </th>
                                                            <th class="b_all" colspan="2">
                                                                <p><?=$logistic_dept?></p>
                                                            </th>
                                                            <th class="b_all" rowspan='2'>
                                                                <p><?=$remarks?></p>
                                                            </th>
                                                        </tr>
                                                        <tr>
                                                            <th class="b_all">
                                                                <p><?=$roll?></p>
                                                            </th>
                                                            <th class="b_all">
                                                                <p><?=$weight?></p>
                                                            </th>
                                                            <th class="b_all">
                                                                <p><?=$roll?></p>
                                                            </th>
                                                            <th class="b_all">
                                                                <p><?=$weight?></p>
                                                            </th>
                                                            <th class="b_all">
                                                                <p><?=$roll?></p>
                                                            </th>
                                                            <th class="b_all">
                                                                <p><?=$weight?></p>
                                                            </th>
                                                            <th class="b_all">
                                                                <p><?=$roll?></p>
                                                            </th>
                                                            <th class="b_all">
                                                                <p><?=$weight?></p>
                                                            </th>
                                                            <th class="b_all">
                                                                <p>D.R.#</p>
                                                            </th>
                                                            <th class="b_all">
                                                                <p><?=$weight?></p>
                                                            </th>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <label><?=$prepared_by?>:</label>
                                                        </td>
                                                        <td>
                                                            <input type="text" style="width:250px" class="solid_bottom">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="col-xs-4">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <label><?=$filledup_by?>:</label>
                                                        </td>
                                                        <td>
                                                            <input type="text" style="width:250px" class="solid_bottom">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="col-xs-4">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <label><?=$approved_by?>:</label>
                                                        </td>
                                                        <td>
                                                            <input type="text" style="width:250px" class="solid_bottom">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
								</div>
										<div class='row'>
											
                <button type="button" id='save' class="btn btn-success btn-flat btn-sm pull-right"><i class="fa fa-save"></i>&nbsp;<?=$save?></button>
										</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
</div>