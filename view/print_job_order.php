<style>
    @media print {
            @page {
        size: A4 landscape;
    }
    }
</style>


<div class="sheet_container">
                                        <div class="row">
                                            <div class="col-xs-12 text-center">
                                                <div class="solid_border">
                                                    <fr class="main_title">GOLDEN SUN PLASTIC PRINTING CORP.</fr>
                                                    <fr class="sub_title">Job Order Sheet</fr>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <table class="mt10">
                                                    <tr>
                                                        <td>
                                                            <label>Date : </label>
                                                        </td>
                                                        <td>
                                                            <div type="text" class="w150 solid_bottom"></div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label>Customer Name : </label> 
                                                        </td>
                                                        <td>
                                                            <div type="text" class="w400 solid_bottom"></div>
                                                        </td>
                                                    </tr> 
                                                    <tr>
                                                        <td>
                                                            <label>Product Name : </label> 
                                                        </td>
                                                        <td>
                                                             <div type="text" class="w400 solid_bottom"></div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="col-xs-6">
                                                <table class="mt10 pull-right">
                                                    <tr>
                                                        <td>
                                                            <label>Job Order No. : </label>  
                                                        </td>
                                                        <td>
                                                            <div type="text" class="w150 solid_bottom"></div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label>P.O. No. : </label> 
                                                        </td>
                                                        <td>
                                                            <div type="text" class="w300 solid_bottom"></div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label>P.O. Quantity : </label>  
                                                        </td>
                                                        <td>
                                                            <div type="text" class="w300 solid_bottom"></div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="block">
                                                    <div class="pos_bot">
                                                        <p><b><i>Note : Please Fill up and verify the information needed below, before printing.</i></b></p>
                                                    </div>
                                                    <div>
                                                        <table class="mt10 pull-right">
                                                            <tr>
                                                                <td colspan="2" class="b_all">
                                                                    <p class="text-center">Production Department Acknowledge</p>
                                                                </td>
                                                            </tr>
                                                            <tr class="b_all">
                                                                <td>
                                                                    <label>Received By : </label> 
                                                                </td>
                                                                <td>
                                                                    <div type="text" class="w200 h30"></div>
                                                                </td>
                                                            </tr>
                                                            <tr class="b_all">
                                                                <td>
                                                                    <label>Noted By : </label>  
                                                                </td>
                                                                <td>
                                                                    <div type="text" class="w200 h30"></div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12">
                                                <div>
                                                    <table style="margin-top: -1px">
                                                        <tr>
                                                            <td class="b_all">
                                                                <p class="text-center">Materials</p>
                                                            </td>
                                                            <td class="b_all">
                                                                <p class="text-center">Thickness</p>
                                                            </td>
                                                            <td class="b_all">
                                                                <p class="text-center">Plastic Type & Size</p>
                                                            </td>
                                                            <td class="b_all">
                                                                <p class="text-center">Quantity</p>
                                                            </td>
                                                            <td class="b_all">
                                                                <p class="text-center">Plastic Type & Size</p>
                                                            </td>
                                                            <td class="b_all">
                                                                <p class="text-center">Quantity</p>
                                                            </td>
                                                            <td class="b_all">
                                                                <p class="text-center">Plastic Type & Size</p>
                                                            </td>
                                                            <td class="b_all">
                                                                <p class="text-center">Quantity</p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="b_all">
                                                                <div type="text" style="width:254px" class="h30"></div>
                                                            </td>
                                                            <td class="b_all">
                                                                <div type="text" style="width:80px" class="h30"></div>
                                                            </td>
                                                            <td class="b_all">
                                                                <div type="text" style="width:195px" class="h30"></div>
                                                            </td>
                                                            <td class="b_all">
                                                                <div type="text" style="width:80px" class="h30"></div>
                                                            </td>
                                                            <td class="b_all">
                                                                <div type="text" style="width:195px" class="h30"></div>
                                                            </td>
                                                            <td class="b_all">
                                                                <div type="text" style="width:80px" class="h30"></div>
                                                            </td>
                                                            <td class="b_all">
                                                                <div type="text" style="width:195px" class="h30"></div>
                                                            </td>
                                                            <td class="b_all">
                                                                <div type="text" style="width:80px" class="h30"></div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-xs-12">
                                                <div>
                                                    <table style="margin-top: -5px">
                                                        <tr>
                                                            <td class="b_all" colspan="4">
                                                                <p class="text-center">Slitted Roll Winding Direction</p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="b_x wind_dir">
                                                                <img src="public/images/wind/wind1.png">
                                                            </td>
                                                            <td class="b_x wind_dir">
                                                                <img src="public/images/wind/wind2.png">
                                                            </td>
                                                            <td class="b_x wind_dir">
                                                                <img src="public/images/wind/wind3.png">
                                                            </td>
                                                            <td class="b_x wind_dir">
                                                                <img src="public/images/wind/wind4.png">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="b_x text-center">
                                                                <label>Figure No. 1</label><div class="checkbox_con"><div type="checkbox" style="height: 16px"></div>
                                                            </td>
                                                            <td class="b_x text-center"">
                                                                <label>Figure No. 2</label><div class="checkbox_con"><div type="checkbox" style="height: 16px"></div>
                                                            </td>
                                                            <td class="b_x text-center"">
                                                                <label>Figure No. 3</label><div class="checkbox_con"><div type="checkbox" style="height: 16px"></div>
                                                            </td>
                                                            <td class="b_x text-center"">
                                                                <label>Figure No. 4</label><div class="checkbox_con"><div type="checkbox" style="height: 16px"></div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table style="margin-top: -5px; margin-left: -1px; position: absolute;">
                                                        <tr>
                                                            <td class="b_all">
                                                                <p class="text-center">Cylinder No.</p>
                                                            </td>
                                                            <td class="b_all">
                                                                <p class="text-center">Weight per Slitted Roll</p>
                                                            </td>

                                                        </tr>
                                                        <tr>
                                                            <td class="b_all">
                                                                <div type="text" style="width:331px" class="h30"></div>
                                                            </td>
                                                            <td class="b_all">
                                                                <div type="text" style="width:154px" class="h30"></div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="b_all">
                                                                <p class="text-center">Barcode No.</p>
                                                            </td>
                                                            <td class="b_all">
                                                                <p class="text-center">Meters per Slitted Roll</p>
                                                            </td>

                                                        </tr>
                                                        <tr>
                                                            <td class="b_all">
                                                                <div type="text" style="width:331px" class="h30"></div>
                                                            </td>
                                                            <td class="b_all">
                                                                <div type="text" style="width:154px" class="h30"></div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-xs-12">
                                                <div>
                                                    <table style="margin-top: -5px">
                                                        <tr>
                                                            <th class="b_all" rowspan="2">
                                                                <p>Prod. Date</p>
                                                            </th>
                                                            <th class="b_all" colspan="2">
                                                                <p>Printing Dept.</p>
                                                            </th>
                                                            <th class="b_all" colspan="2">
                                                                <p>Lamination Dept.</p>
                                                            </th>
                                                            <th class="b_all" colspan="2">
                                                                <p>Slitting Dept.</p>
                                                            </th>
                                                            <th class="b_all" colspan="2">
                                                                <p>Bag Forming Dept.</p>
                                                            </th>
                                                            <th class="b_all" colspan="2">
                                                                <p>Logistics Dept.</p>
                                                            </th>
                                                            <th class="b_all">
                                                                <p>Remarks</p>
                                                            </th>
                                                        </tr>
                                                        <tr>
                                                            <th class="b_all">
                                                                <p>Roll</p>
                                                            </th>
                                                            <th class="b_all">
                                                                <p>Weight</p>
                                                            </th>
                                                            <th class="b_all">
                                                                <p>Roll</p>
                                                            </th>
                                                            <th class="b_all">
                                                                <p>Weight</p>
                                                            </th>
                                                            <th class="b_all">
                                                                <p>Roll</p>
                                                            </th>
                                                            <th class="b_all">
                                                                <p>Weight</p>
                                                            </th>
                                                            <th class="b_all">
                                                                <p>Roll</p>
                                                            </th>
                                                            <th class="b_all">
                                                                <p>Weight</p>
                                                            </th>
                                                            <th class="b_all">
                                                                <p>D.R.#</p>
                                                            </th>
                                                            <th class="b_all">
                                                                <p>Weight</p>
                                                            </th>
                                                            <th class="b_all">
                                                                <p></p>
                                                            </th>
                                                        </tr>
                                                        <tr>
                                                            <td class="b_all">
                                                                <div type="text" style="width:105px" class="h30"></div>
                                                            </td>
                                                            <td class="b_all">
                                                                <div type="text" style="width:88px" class="h30"></div>
                                                            </td>
                                                            <td class="b_all">
                                                                <div type="text" style="width:88px" class="h30"></div>
                                                            </td>
                                                            <td class="b_all">
                                                                <div type="text" style="width:88px" class="h30"></div>
                                                            </td>
                                                            <td class="b_all">
                                                                <div type="text" style="width:88px" class="h30"></div>
                                                            </td>
                                                            <td class="b_all">
                                                                <div type="text" style="width:88px" class="h30"></div>
                                                            </td>
                                                            <td class="b_all">
                                                                <div type="text" style="width:88px" class="h30"></div>
                                                            </td>
                                                            <td class="b_all">
                                                                <div type="text" style="width:88px" class="h30"></div>
                                                            </td>
                                                            <td class="b_all">
                                                                <div type="text" style="width:88px" class="h30"></div>
                                                            </td>
                                                            <td class="b_all">
                                                                <div type="text" style="width:88px" class="h30"></div>
                                                            </td>
                                                            <td class="b_all">
                                                                <div type="text" style="width:88px" class="h30"></div>
                                                            </td>
                                                            <td class="b_all">
                                                                <div type="text" style="width:154px" class="h30"></div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="b_all">
                                                                <div type="text" style="width:105px" class="h30"></div>
                                                            </td>
                                                            <td class="b_all">
                                                                <div type="text" style="width:88px" class="h30"></div>
                                                            </td>
                                                            <td class="b_all">
                                                                <div type="text" style="width:88px" class="h30"></div>
                                                            </td>
                                                            <td class="b_all">
                                                                <div type="text" style="width:88px" class="h30"></div>
                                                            </td>
                                                            <td class="b_all">
                                                                <div type="text" style="width:88px" class="h30"></div>
                                                            </td>
                                                            <td class="b_all">
                                                                <div type="text" style="width:88px" class="h30"></div>
                                                            </td>
                                                            <td class="b_all">
                                                                <div type="text" style="width:88px" class="h30"></div>
                                                            </td>
                                                            <td class="b_all">
                                                                <div type="text" style="width:88px" class="h30"></div>
                                                            </td>
                                                            <td class="b_all">
                                                                <div type="text" style="width:88px" class="h30"></div>
                                                            </td>
                                                            <td class="b_all">
                                                                <div type="text" style="width:88px" class="h30"></div>
                                                            </td>
                                                            <td class="b_all">
                                                                <div type="text" style="width:88px" class="h30"></div>
                                                            </td>
                                                            <td class="b_all">
                                                                <div type="text" style="width:154px" class="h30"></div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-xs-4">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <label>Prepared By:</label>
                                                        </td>
                                                        <td>
                                                            <div type="text" style="width:250px" class="solid_bottom"></div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="col-xs-4">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <label>Filled Up By:</label>
                                                        </td>
                                                        <td>
                                                            <div type="text" style="width:250px" class="solid_bottom"></div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="col-xs-4">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <label>Approved By:</label>
                                                        </td>
                                                        <td>
                                                            <div type="text" style="width:250px" class="solid_bottom"></div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>