<style>
    .solid_bottom {
        height: 20px;
        margin-top: 8px;
        font-size: 15px;
        padding-left: 10px;
    }
    .table_set {
        display: table;
    }
    .portrait {
        max-width: 100%;
    }
    .h20 {
        height: 20px;
    }
    @media print {
        @page {
            size: A4 portrait;
        }
    }
</style>
<div class="sheet_container portrait">
    <div class="row">
        <div class="col-xs-12 text-center">
            <div>
                <fr class="main_title">GOLDEN SUN PLASTIC PRINTING CORP.</fr>
            </div>
        </div>
        <div class="col-xs-7 mt40">
            <h4 class="uline in_block">DELIVERY RECEIPT</h4>
            <br>
            <table class="mt20">
                <tr>
                    <td>
                        <label>DELIVERED TO : </label>
                    </td>
                    <td>
                        <div type="text" class="solid_bottom" style="width:300px"><p>Test</p></div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>ADDRESS : </label>
                    </td>
                    <td>
                        <div type="text" class="solid_bottom" style="width:300px"></div>
                    </td>
                </tr>
            </table>
            
        </div>
        <div class="col-xs-5">
            <table class="mt20 pull-right">
                <tr>
                    <td>
                        <label>No. : </label>
                    </td>
                    <td>
                        <div type="text" class="solid_bottom" style="width:200px"><p>1200</p></div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Date : </label>
                    </td>
                    <td>
                        <div type="text" class="solid_bottom" style="width:200px"></div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Terms : </label>
                    </td>
                    <td>
                        <div type="text" class="solid_bottom" style="width:200px"></div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>P.O. No. : </label>
                    </td>
                    <td>
                        <div type="text" class="solid_bottom" style="width:200px"></div>
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-xs-12">
            <table class="w_100 table_set mt20">
                <tr>
                    <th  style="width:50px" class="b_all"><label>Quantity</label></th>
                    <th  style="width:50px" class="b_all"><label>Unit</label></th>
                    <th class="b_all"><label>DESCRIPTION</label></th>
                    <th  width="20%" colspan="2" class="b_all"><label>Unit Price</label></th>
                    <th  width="20%" colspan="2" class="b_all"><label>Amount</label></th>
                </tr>
                <tr>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                </tr>
                <tr>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                </tr>
                <tr>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                </tr>
                <tr>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                </tr>
                <tr>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                </tr>
                <tr>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                </tr>
                <tr>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                </tr>
                <tr>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                </tr>
                <tr>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                </tr>
                <tr>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                </tr>
                <tr>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                </tr>
                <tr>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                </tr>
                <tr>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                </tr>
                <tr>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                </tr>
                <tr>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                    <td class="b_all h20"></td>
                </tr>
            </table>
        </div>
        <div class="col-xs-12">
            <p class="pull-right"><b>Received the above goods in good order and condition.</b></p>
        </div>
        <div class="col-xs-12">
            <table class="pull-right">
                <tr>
                    <td>
                        <label>By : </label>
                    </td>
                    <td>
                        <div type="text" class="solid_bottom" style="width:200px"></div>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td><p class="text-center">Authorized Signature</p></td>
                </tr>
            </table>
        </div>
    </div>
</div>