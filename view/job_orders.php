<style>
.breakingline{
    overflow-y: scroll;
    height: 50px;
    word-break: break-all;
}
</style>

<div class="content-wrapper">
    <section class="content" id="section" data="50">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-default">
                    <div class="box-header">
                        <h3 class="box-title"><?= $job_orders ?></h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="customtable mt20">
                                    <div class="button-box">
                                        <?php
                                        /* <button id="abutton" type="button" class="btn"><i class="fa fa-plus color-blue"></i></button> */
                                        ?>
                                    </div>
                                    <div class="table-scroll">
                                        <div class="table-height">
                                            <div class='alert alert-danger' style="display:none">
                                                <a class='close'>&times;</a>
                                                <center><span></span></center>
                                            </div>
                                            <table data-order='[["1","asc"]]' name="dataTable" id="table_job_order" class="table dataTable table-bordered table-striped table-hover">
                                                <thead>
                                                <th data-class-name="display_col" width="10%" class="nsearch norder" ></th>
                                                <th data-class-name="text-center" width="10%"><?= $date ?></th>
                                                <th data-class-name="text-center" width="10%"><?= $customers ?></th>
                                                <th data-class-name="text-center" width="10%" ><?= $jo_number ?></th>
                                                <th data-class-name="text-center" width="10%"><?= $po_number ?></th>
                                                <th data-class-name="text-center" width="10%"><?= $po_qty ?></th>
                                                <th data-class-name="text-center" width="10%" ><?= $status ?></th>
                                                <th data-class-name="text-center" width="10%" class="norder nsearch"><?= $actions ?></th>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
</div>

<!-- Add -->
<div id="dataModal" class="modal fade custom-modal primarymodal" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title" name='title'></h4>
            </div>
            <div class="modal-body" style='overflow:scroll;max-height:500px'>
                <div class="details-container">
                    <div class='alert alert-danger' style="display:none">
                        <a class='close'>&times;</a>
                        <center><span></span></center>
                    </div>
                    <table>
                        <thead>
                            <tr>
                                <th width="20%"></th>
                                <th width="40%"></th>
                                <th width="40%"></th>
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class='pr20'>
                                <label><?= $prod_date ?>:</label>
                                <input type="text" class="datepicker form-control" placeholder="<?= $prod_date ?>" style="width: 150px;">
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="3"><hr class="hr_5"></td>
                        </tr>
                        <tr>
                            <td>
                                <label class="mt10"><?= $printing_dept ?>:</label>
                            </td>
                            <td class='pr20'>
                                <label class="mt10"><?= $roll ?>:</label>
                                <input  type="text" class="form-control " placeholder="<?= $roll ?>">
                            </td>
                            <td>
                                <label class="mt10"><?= $weight ?>:</label>
                                <input  type="text" class="form-control " placeholder="<?= $weight ?>">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3"><hr class="hr_5"></td>
                        </tr>
                        <tr>
                            <td>
                                <label class="mt10"><?= $lamination_dept ?>:</label>
                            </td>
                            <td  class='pr20'>
                                <label class="mt10"><?= $roll ?>:</label>
                                <input  type="text" class="form-control" placeholder="<?= $roll ?>">
                            </td>
                            <td>
                                <label class="mt10"><?= $weight ?>:</label>
                                <input  type="text" class="form-control" placeholder="<?= $weight ?>">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3"><hr class="hr_5"></td>
                        </tr>
                        <tr>
                            <td>
                                <label class="mt10"><?= $slitting_dept ?>:</label><br>
                                <span><input type="radio" name='deliverys' data_val='1' class="delivery"><?= $partially_deliver ?></span><br>
                                <span><input  type="radio" name='deliverys' data_val='2' class="delivery"><?= $all_deliver ?></span>
                            </td>
                            <td  class='pr20'>
                                <label class="mt10"><?= $roll ?>:</label>
                                <input  type="text" class="form-control" placeholder="<?= $roll ?>">
                            </td>
                            <td>
                                <label class="mt10"><?= $weight ?>:</label>
                                <input  type="text" class="form-control" placeholder="<?= $weight ?>">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3"><hr class="hr_5"></td>
                        </tr>
                        <tr>
                            <td>
                                <label class="mt10"><?= $bagforming_dept ?>:</label><br>
                                <span><input type="radio" name='deliverys' data_val='3' class="delivery"><?= $partially_deliver ?></span><br>
                                <span><input  type="radio" name='deliverys' data_val='4' class="delivery"><?= $all_deliver ?></span>
                            </td>
                            <td  class='pr20'>
                                <label class="mt10"><?= $roll ?>:</label>
                                <input  type="text" class="form-control" placeholder="<?= $roll ?>">
                            </td>
                            <td>
                                <label class="mt10"><?= $weight ?>:</label>
                                <input  type="text" class="form-control" placeholder="<?= $weight ?>">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3"><hr class="hr_5"></td>
                        </tr>
                        <tr>
                            <td>
                                <label class="mt10"><?= $logistic_dept ?>:</label><br>
                                <span><input type="radio" name='deliverys' data_val='5' class="delivery"><?= $partially_deliver ?></span><br>
                                <span><input  type="radio" name='deliverys' data_val='6' class="delivery"><?= $all_deliver ?></span>
                            </td>
                            <td class='pr20'>
                                <label class="mt10">DR#:</label>
                                <input  type="text" class="form-control" placeholder="DR#">
                            </td>
                            <td>
                                <label class="mt10"><?= $weight ?>:</label>
                                <input  type="text" class="form-control" placeholder="<?= $weight ?>">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3"><hr class="hr_5"></td>
                        </tr>
                        <tr>
                            <td colspan='3'>
                                <label class="mt10"><?= $remarks ?>:</label>
                                <textarea class="form-control" placeholder="<?= $remarks ?>"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td colspan='3'><hr>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-flat btn-sm pull-left" data-dismiss="modal"><i class="fa fa-close"></i>&nbsp;<?= $cancel ?></button>
                <button type="button" id='save' class="btn btn-success btn-flat btn-sm pull-right"><i class="fa fa-save"></i>&nbsp;<?= $save ?></button>
            </div>
        </div>
    </div>
</div>



<!-- Add -->
<div id="logModal" class="modal fade custom-modal primarymodal" role="dialog">
    <div class="modal-dialog  modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title" name='title'></h4>
            </div>
            <div class="modal-body">
                <div class="button-box">
                    <button id="add_joblogs" type="button" class="btn btn-primary"><i class="fa fa-plus color-blue"></i>&nbsp;<?= $add ?></button>
                    <button id="DeliverThis" type="button" class="btn btn-success"><i class="fa fa-truck color-black"></i>&nbsp;<?= $deliver ?></button>
                </div>
				<br>
                    <div class="table-responsive table-scroll table-container">
                        <div class="table-height">
                            <div class='alert alert-danger' style="display:none">
                                <a class='close'>&times;</a>
                                <center><span></span></center>
                            </div>
                            <table  name="dataTable" id="table_job_order_production" class="table table-bordered table-hover table-striped table_style">
                                <thead>
                                    <tr>
                                        <th class="nsearch norder" data-class-name="display_col" rowspan="2"></th>
                                        <th width="10%" data-class-name="text-center" rowspan="2"><?= $prod_date ?></th>
                                        <th width="10%" data-class-name="text-center" colspan="2"><?= $printing_dept ?></th>
                                        <th width="10%" data-class-name="text-center" colspan="2"><?= $lamination_dept ?></th>
                                        <th width="10%" data-class-name="text-center" colspan="2"><?= $slitting_dept ?></th>
                                        <th width="10%" data-class-name="text-center" colspan="2"><?= $bagforming_dept ?></th>
                                        <!--th width="10%" data-class-name="text-center" colspan="2"><?= $logistic_dept ?></th-->
                                        <th width="10%" data-class-name="text-center" rowspan="2"><?= $remarks ?></th>
                                        <th width="5%" data-class-name="text-center" rowspan="2"><?= $actions ?></th>
                                    </tr>
                                    <tr>
                                        <th width="5%" data-class-name="text-center"><?= $roll ?></th>
                                        <th width="5%" data-class-name="text-center"><?= $weight ?></th>
                                        <th width="5%" data-class-name="text-center"><?= $roll ?></th>
                                        <th width="5%" data-class-name="text-center"><?= $weight ?></th>
                                        <th width="5%" data-class-name="text-center"><?= $roll ?></th>
                                        <th width="5%" data-class-name="text-center"><?= $weight ?></th>
                                        <th width="5%" data-class-name="text-center"><?= $roll ?></th>
                                        <th width="5%" data-class-name="text-center"><?= $weight ?></th>
                                        <!--th width="5%" data-class-name="text-center">D.R.#</th>
                                        <th width="5%" data-class-name="text-center"><?= $weight ?></th-->
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-flat btn-sm pull-left" data-dismiss="modal"><i class="fa fa-close"></i>&nbsp;<?= $cancel ?></button>
				<button type="button" id='save_multiple' class="btn btn-success btn-flat btn-sm pull-right" style='display:none'><i class="fa fa-save"></i>&nbsp;<?= $save ?></button>
            </div>
        </div>
    </div>
</div>


<!-- Delivery -->
<div id="deliveryModal" class="modal fade custom-modal primarymodal" role="dialog">
    <div class="modal-dialog  modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title" name='title'></h4>
            </div>
            <div class="modal-body">
                <div>
                            <div class='alert alert-danger' style="display:none">
                                <a class='close'>&times;</a>
                                <center><span></span></center>
                            </div>
					<table class='table'>
						<tr>
							<td class='pr20'><label class='mt10'>D.R.#:</label></td>
							<td class='pr20'><input id='dr_no' type="text" class="form-control" placeholder="D.R.#"></td>
							<td class='pr20'></td>
							<td class='pr20'></td>
						</tr>
						<tr>
							<td class='pr20'><label class='mt10'><?= $deliver_to ?>:</label></td>
							<td class='pr20'><input id='del_to' type="text" class="form-control" placeholder="<?= $deliver_to ?>"></td>
							<td class='pr20'><label class='mt10'><?= $date ?>:</label></td>
							<td><input  type="text" id='del_date' class="datepicker form-control" placeholder="<?= $date ?>"></td>
						</tr>
						<tr>
							<td class='pr20'><label class='mt10'><?= $address ?>:</label></td>
							<td class='pr20'><textarea id='del_add'  type="text" class="form-control" placeholder="<?= $address ?>"></textarea></td>
							<td class='pr20'><label class='mt10'><?= $terms ?>:</label></td>
							<td><textarea  type="text" id='del_term' class="form-control" placeholder="<?= $terms ?>"></textarea></td>
						</tr>
					</table>
                </div>
				<br>
                    <div class="table-responsive table-scroll table-container">
                        <div class="table-height">
                            <table  name="dataTable" id="table_job_order_delivery" class="table table-bordered table-hover table-striped table_style">
                                <thead>
                                    <tr>
                                        <th class="nsearch norder" data-class-name="display_col"></th>
                                        <th width="5%" data-class-name="text-center"><?= $total_remaining ?></th>
                                        <th width="5%" data-class-name="text-center"><?= $qty ?></th>
                                        <th width="5%" data-class-name="text-center"><?= $unit ?></th>
                                        <th width="5%" data-class-name="text-center"><?= $description ?></th>
                                        <th width="5%" data-class-name="text-center"><?= $price ?></th>
                                        <th width="5%" data-class-name="text-center"><?= $amount ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-flat btn-sm pull-left" data-dismiss="modal"><i class="fa fa-close"></i>&nbsp;<?= $cancel ?></button>
				<button type="button" id='save_delivery' class="btn btn-success btn-flat btn-sm pull-right"><i class="fa fa-save"></i>&nbsp;<?= $save ?></button>
            </div>
        </div>
    </div>
</div>

<div id="deleteModal" class="modal fade custom-modal primarymodal" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class='alert alert-danger' style="display:none">
                <a class='close'>&times;</a>
                <center><span></span></center>
            </div>
            <div class="modal-body">
                <h4 name='message'></h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-flat btn-sm pull-left" data-dismiss="modal"><i class="fa fa-close"></i>&nbsp;<?= $cancel ?></button><button type="button" id='delete_yes' class="btn btn-success btn-flat btn-sm pull-right"><i class="fa fa-save"></i>&nbsp;<?= $yes ?></button>
            </div>
        </div>
    </div>
</div>