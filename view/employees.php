<div class="content-wrapper">
    <section class="content" id="section" data="50"> 
        <div class="row">
            <div class="col-md-12">
                <div class="box box-default">
                    <div class="box-header">
                        <h3 class="box-title"><?=$employees?></h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="customtable mt20">
									<div class="button-box">
										<button id="abutton" type="button" class="btn"><i class="fa fa-plus color-blue"></i></button>
										<button id="abutton_multiple" type="button" class="btn"><img src="./public/images/multiple.png" class="multiplebtn"/></button>
									</div>
                                    <div class="table-scroll">
										<div class="table-height">
											<div class='alert alert-danger' style="display:none">
												<a class='close'>&times;</a>
												<center><span></span></center>
											</div>
											<table data-order='[["1","asc"]]' name="dataTable" id="table_employee" class="table dataTable table-bordered table-striped table-hover">
												<thead>
													<th data-class-name="display_col" width="10%" class="nsearch norder" ></th>
													<th data-class-name="text-center" width="10%"><?= $name ?></th>
													<th data-class-name="text-center" width="10%"><?= $username?></th>
													<th data-class-name="text-center" width="10%" ><?= $departments ?></th>
													<th data-class-name="text-center" width="10%" class="norder nsearch"><?= $actions ?></th>
												</thead>
												<tbody>
												</tbody>
											</table>
										</div>                   
                                    </div>                                     
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
</div>

<!-- Add -->
<div id="dataModal" class="modal fade custom-modal primarymodal" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title" name='title'></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 details-container">
						<div class='alert alert-danger' style="display:none">
							<a class='close'>&times;</a>
							<center><span></span></center>
						</div>
                        <table class="w_100">
                            <tr>
                                <td>
                                    <label class="mt10"><?=$name?>:</label>
                                    <input type="text" class="form-control" placeholder="<?=$name?>">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="mt10"><?=$username?>:</label>
                                    <input type="text" class="form-control" placeholder="<?=$username?>">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="mt10"><?=$password?>:</label>
                                    <input type="password" class="form-control" placeholder="<?=$password?>">
                                </td>
                            </tr>
							<tr>
								<td>
									<label class="mt10"><?= $departments ?>:</label>
									<select id="id_department" class="select2-field ajax" data-placeholder="<?= $cdepartment ?>"></select></td>
								</tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-flat btn-sm pull-left" data-dismiss="modal"><i class="fa fa-close"></i>&nbsp;<?=$cancel?></button>
                <button type="button" id='save' class="btn btn-success btn-flat btn-sm pull-right"><i class="fa fa-save"></i>&nbsp;<?=$save?></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade lgModal" id="dataModal_Multiple" role="dialog">
    <div class="modal-dialog modal-lg ">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" name="title"></h4>
            </div>
            <div class="modal-body">
                <center class="color-red" style="margin-top:10px;"><i class="fa fa-asterisk"></i>&nbsp;<?= $required_fields ?></center>

                <div name="field_container">
                    <div class='alert alert-danger' style="display:none">
                        <a class='close'>&times;</a>
                        <center><span></span></center>
                    </div>
                    <table class="table table-form">
                        <tr>
                            <td><strong><?= $name ?><span class="color-red">*</span>:&nbsp;</strong></td>
                            <td><input auto_focus class="form-control" type="text"></td>
                            <td><strong><?= $username ?><span class="color-red">*</span>:&nbsp;</strong></td>
                            <td><input  class="form-control" type="text"></td>
                            <td><strong><?= $password ?><span class="color-red">*</span>:&nbsp;</strong></td>
                            <td><input  class="form-control" type="password"></td>
                            <td  width="10%"><strong><?= $departments ?><span class="color-red">*</span>:&nbsp;</strong></td>
                            <td  width="15%"><select class="select2-field ajax dropdown" data-placeholder="<?= $cdepartment ?>"></select></td>
                            <td><button id="add_multiple" class="btn btn-primary"><i class="fa fa-plus-circle"></i>&nbsp;<?= $add ?></button></td>
                        </tr>

                    </table>
                </div>
                <div class="table-responsive table-scroll table-container">
                    <div class="table-height">
                        <div class='alert alert-danger' style="display:none">
                            <a class='close'>&times;</a>
                            <center><span></span></center>
                        </div>


                        <table id="table_multiple" class="table table-bordered table-hover table-striped table_style">
                            <thead>
                            <th class="norder nsearch" data-class-name="display_col">ID</th>
                            <th width="30%"><?= $name ?></th>
                            <th width="30%"><?= $username ?></th>
                            <th width="30%"><?= $password ?></th>
                            <th width="30%"><?= $departments ?></th>
                            <th width="10%"><?= $actions ?></th>
                            </thead>

                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">              


                <button type="button"  class="esc_btn btn btn-danger pull-left"  data-dismiss="modal"><i class="fa fa-times"></i> &nbsp;<?= $close ?></button>
                <button type="button" id="save_multiple" class="save_btn btn btn-success pull-right"><i class="fa fa-save"></i> <?= $save ?>
                </button>

            </div>

        </div>
    </div>
</div>


<div id="deleteModal" class="modal fade custom-modal primarymodal" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
			<div class='alert alert-danger' style="display:none">
				<a class='close'>&times;</a>
				<center><span></span></center>
			</div>
            <div class="modal-body">
                <h4 name='message'></h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-flat btn-sm pull-left" data-dismiss="modal"><i class="fa fa-close"></i>&nbsp;<?=$cancel?></button><button type="button" id='delete_yes' class="btn btn-success btn-flat btn-sm pull-right"><i class="fa fa-save"></i>&nbsp;<?=$yes?></button>
            </div>
        </div>
    </div>
</div>