<div class="content-wrapper">
    <section class="content" id="section" data="50"> 
        <div class="row">
            <div class="col-md-12">
                <div class="box box-default">
                    <div class="box-header">
                        <h3 class="box-title"><?=$cylinders?></h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="customtable mt20">
									<div class="button-box">
										<button id="abutton" type="button" class="btn"><i class="fa fa-plus color-blue"></i></button>
										<button id="abutton_multiple" type="button" class="btn"><img src="./public/images/multiple.png" class="multiplebtn"/></button>
									</div>
                                    <div class="table-scroll">
										<div class="table-height">
											<div class='alert alert-danger' style="display:none">
												<a class='close'>&times;</a>
												<center><span></span></center>
											</div>
											<table data-order='[["1","asc"]]' name="dataTable" id="table_cylinder" class="table dataTable table-bordered table-striped table-hover">
												<thead>
													<th data-class-name="display_col" width="10%" class="nsearch norder" ></th>
													<th data-class-name="text-center" width="10%"><?=   $cylinder.'_ID'  ?></th>
													<th data-class-name="text-center" width="10%"><?= $description?></th>
													<th data-class-name="text-center" width="10%" ><?= $minimum ?></th>
													<th data-class-name="text-center" width="10%" ><?= $initial_qty ?></th>
													<th data-class-name="text-center" width="10%" ><?= $price ?></th>
													<th data-class-name="text-center" width="10%" class="norder nsearch"><?= $actions ?></th>
												</thead>
												<tbody>
												</tbody>
											</table>
										</div>                   
                                    </div>                                     
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
</div>

<!-- Add -->
<div id="dataModal" class="modal fade custom-modal primarymodal" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title" name='title'></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 details-container">
						<div class='alert alert-danger' style="display:none">
							<a class='close'>&times;</a>
							<center><span></span></center>
						</div>
                        <table class="w_100">
                            <tr>
                                <td>
                                    <label class="mt10"><?=  $cylinder.'_ID' ?>:</label>
                                    <input type="text" class="form-control" placeholder="<?=  $cylinder.'_ID' ?>">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="mt10"><?=$description?>:</label>
                                    <input type="text" class="form-control" placeholder="<?=$description?>">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="mt10"><?=$minimum?>:</label>
                                    <input type="number" class="form-control score" placeholder="<?=$minimum?>">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="mt10"><?=$initial_qty?>:</label>
                                    <input type="number" class="form-control score" placeholder="<?=$initial_qty?>">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="mt10"><?=$price?>:</label>
                                    <input type="number" class="form-control score" placeholder="<?=$price?>">
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-flat btn-sm pull-left" data-dismiss="modal"><i class="fa fa-close"></i>&nbsp;<?=$cancel?></button>
                <button type="button" id='save' class="btn btn-success btn-flat btn-sm pull-right"><i class="fa fa-save"></i>&nbsp;<?=$save?></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade lgModal" id="dataModal_Multiple" role="dialog">
    <div class="modal-dialog modal-lg " style='width:1300px'>

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" name="title"></h4>
            </div>
            <div class="modal-body">
                <center class="color-red" style="margin-top:10px;"><i class="fa fa-asterisk"></i>&nbsp;<?= $required_fields ?></center>

                <div name="field_container">
                    <div class='alert alert-danger' style="display:none">
                        <a class='close'>&times;</a>
                        <center><span></span></center>
                    </div>
                    <table class="table table-form">
                        <tr>
                            <td><strong><?=   $cylinder.'_ID'  ?><span class="color-red">*</span>:&nbsp;</strong></td>
                            <td><input auto_focus class="form-control" type="text"></td>
                            <td><strong><?= $description ?><span class="color-red">*</span>:&nbsp;</strong></td>
                            <td><input  class="form-control" type="text"></td>
                            <td><strong><?= $minimum ?><span class="color-red">*</span>:&nbsp;</strong></td>
                            <td><input  class="form-control score" type="number"></td>
                        </tr>
                        <tr>
                            <td><strong><?= $initial_qty ?><span class="color-red">*</span>:&nbsp;</strong></td>
                            <td><input  class="form-control score" type="number"></td>
                            <td><strong><?= $price ?><span class="color-red">*</span>:&nbsp;</strong></td>
                            <td><input  class="form-control score" type="number"></td>
                            <td><button id="add_multiple" class="btn btn-primary"><i class="fa fa-plus-circle"></i>&nbsp;<?= $add ?></button></td>
                        </tr>

                    </table>
                </div>
                <div class="table-responsive table-scroll table-container">
                    <div class="table-height">
                        <div class='alert alert-danger' style="display:none">
                            <a class='close'>&times;</a>
                            <center><span></span></center>
                        </div>


                        <table id="table_multiple" class="table table-bordered table-hover table-striped table_style">
                            <thead>
                            <th class="norder nsearch" data-class-name="display_col">ID</th>
                            <th width="10%"><?=   $cylinder.'_ID'  ?></th>
                            <th width="10%"><?= $description ?></th>
                            <th width="10%"><?= $minimum ?></th>
                            <th width="10%"><?= $initial_qty ?></th>
                            <th width="10%"><?= $price ?></th>
                            <th width="5%"><?= $actions ?></th>
                            </thead>

                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">              


                <button type="button"  class="esc_btn btn btn-danger pull-left"  data-dismiss="modal"><i class="fa fa-times"></i> &nbsp;<?= $close ?></button>
                <button type="button" id="save_multiple" class="save_btn btn btn-success pull-right"><i class="fa fa-save"></i> <?= $save ?>
                </button>

            </div>

        </div>
    </div>
</div>


<div id="deleteModal" class="modal fade custom-modal primarymodal" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
			<div class='alert alert-danger' style="display:none">
				<a class='close'>&times;</a>
				<center><span></span></center>
			</div>
            <div class="modal-body">
                <h4 name='message'></h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-flat btn-sm pull-left" data-dismiss="modal"><i class="fa fa-close"></i>&nbsp;<?=$cancel?></button><button type="button" id='delete_yes' class="btn btn-success btn-flat btn-sm pull-right"><i class="fa fa-save"></i>&nbsp;<?=$yes?></button>
            </div>
        </div>
    </div>
</div>