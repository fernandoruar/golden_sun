<div class="content-wrapper">
    <section class="content" id="section" data="50"> 
        <div class="row">
            <div class="col-md-12">
                <div class="box box-default">
                    <div class="box-header">
                        <h3 class="box-title"></h3>
                    </div>
                    <div class="box-body">
                        <div class="sheet_container portrait">
                            <div class="row">
                                <div class="col-xs-12">
                                    <h2 class="text-center"><b>SUMMARY REPORT</b></h2>
                                </div>
                                <div class="col-xs-12">
                                    <table class="mt10">
                                        <tr>
                                            <td>
                                                <label>COMPANY : </label>
                                            </td>
                                            <td>
                                                <input type="text" class="w400 solid_bottom">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label>SIZE / ROLLS : </label>
                                            </td>
                                            <td>
                                                <input type="text" class="w400 solid_bottom">
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-xs-4">
                                    <div class="b_all p_all_5" style="height:430px;">
                                        PRINTING OPERATOR
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <table class="w_100" style="display:table;">
                                                    <tr>
                                                        <td width="30%"></td>
                                                        <td width="35%">Kls.</td>
                                                        <td width="35%">Pesos</td>
                                                    </tr>
                                                    <tr>
                                                        <td>PRIMARY</td>
                                                        <td class="solid_bottom"><input type="text" class="form-control"></td>
                                                        <td class="solid_bottom"><input type="text" class="form-control"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>COLOR</td>
                                                        <td class="solid_bottom"><input type="text" class="form-control"></td>
                                                        <td class="solid_bottom"><input type="text" class="form-control"></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td>Kls.</td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>PAINT</td>
                                                        <td class="solid_bottom"><input type="text" class="form-control"></td>
                                                        <td class="solid_bottom"><input type="text" class="form-control"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>SOLVENT</td>
                                                        <td class="solid_bottom"><input type="text" class="form-control"></td>
                                                        <td class="solid_bottom"><input type="text" class="form-control"></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td>SUM</td>
                                                        <td>SUM</td>
                                                    </tr>
                                                    <tr>
                                                        <td>TOTAL</td>
                                                        <td class="solid_bottom"><input type="text" class="form-control"></td>
                                                        <td class="solid_bottom"><input type="text" class="form-control"></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="b_all p_all_5" style="height:430px;">
                                        LAMINATION OPERATOR
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <table class="w_100" style="display:table;">
                                                    <tr>
                                                        <td width="30%"></td>
                                                        <td width="35%">Kls.</td>
                                                        <td width="35%">Pesos</td>
                                                    </tr>
                                                    <tr>
                                                        <td>COLOR FILM</td>
                                                        <td class="solid_bottom"><input type="text" class="form-control"></td>
                                                        <td class="solid_bottom"><input type="text" class="form-control"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>CPP /VMCPP</td>
                                                        <td class="solid_bottom"><input type="text" class="form-control"></td>
                                                        <td class="solid_bottom"><input type="text" class="form-control"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>VMPET</td>
                                                        <td class="solid_bottom"><input type="text" class="form-control"></td>
                                                        <td class="solid_bottom"><input type="text" class="form-control"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>PE</td>
                                                        <td class="solid_bottom"><input type="text" class="form-control"></td>
                                                        <td class="solid_bottom"><input type="text" class="form-control"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>FINISH</td>
                                                        <td class="solid_bottom"><input type="text" class="form-control"></td>
                                                        <td class="solid_bottom"><input type="text" class="form-control"></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td>Kls.</td>
                                                        <td>Pesos</td>
                                                    </tr>
                                                    <tr>
                                                        <td>GLUE / HD</td>
                                                        <td class="solid_bottom"><input type="text" class="form-control"></td>
                                                        <td class="solid_bottom"><input type="text" class="form-control"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>EAC</td>
                                                        <td class="solid_bottom"><input type="text" class="form-control"></td>
                                                        <td class="solid_bottom"><input type="text" class="form-control"></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td>SUM</td>
                                                        <td>SUM</td>
                                                    </tr>
                                                    <tr>
                                                        <td>TOTAL</td>
                                                        <td class="solid_bottom"><input type="text" class="form-control"></td>
                                                        <td class="solid_bottom"><input type="text" class="form-control"></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="b_all p_all_5" style="height:430px;">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <table class="w_100" style="display:table;">
                                                    <tr>
                                                        <td colspan="3">SLITTING <input type="text" class="solid_bottom" style="width:299px; margin-left:9px;"></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="10%"></td>
                                                        <td width="45%">ROLLS</td>
                                                        <td width="45%">KGS</td>
                                                    </tr>
                                                    <tr>
                                                        <td>1</td>
                                                        <td class="solid_bottom"><input type="text" class="form-control"></td>
                                                        <td class="solid_bottom"><input type="text" class="form-control"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td class="solid_bottom"><input type="text" class="form-control"></td>
                                                        <td class="solid_bottom"><input type="text" class="form-control"></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3"><hr class="mt40 col_wht"></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">DELIVERY NO. <input type="text" class="solid_bottom" style="width:270px; margin-left:9px;"></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="10%"></td>
                                                        <td width="45%">ROLLS</td>
                                                        <td width="45%">KGS</td>
                                                    </tr>
                                                    <tr>
                                                        <td>1</td>
                                                        <td class="solid_bottom"><input type="text" class="form-control"></td>
                                                        <td class="solid_bottom"><input type="text" class="form-control"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td class="solid_bottom"><input type="text" class="form-control"></td>
                                                        <td class="solid_bottom"><input type="text" class="form-control"></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
</div>