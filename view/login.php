<div class="login-wrap">
    <div class="login-page">
        <div class="form">
            <form class="form-horizontal" id="login_container">
			<img src="./public/images/favicon.png" width='60%' alt="">
			<hr>
                <div class='alert alert-success' style="display:none">
                    <a class='close'>&times;</a>
                    <center><span></span></center>
                </div>
                <div class="input-group">
                    <input auto_focus class="form-control" id="username" type="text" placeholder="<?= $username ?>"/>
                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                </div>
                <div class="input-group">
                    <input class="form-control" id="password" type="password" placeholder="<?= $password ?>"/>
                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                </div>           
                <button id="signin_btn" type="button"><?= $signin ?></button>
                <p><a class="lang" href="javascript:void(0)" value="1">English</a>&nbsp;|&nbsp;<a class="lang" href="javascript:void(0)" value="2" href="#">中文</a></p>
            </form>
        </div>
    </div>
</div>
